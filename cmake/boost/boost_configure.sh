#! /usr/bin/bash

if [ "$#" -eq 3 ]; then
    ./bootstrap.sh --with-toolset=$2 --with-libraries=mpi,serialization,system,filesystem --prefix=$1 --libdir=$3
    if [ "$2" = "intel-linux" ]; then
        echo "using mpi : mpiicpc ;" >> project-config.jam
    else
	echo "using mpi : mpicxx ;" >> project-config.jam
    fi
elif [ "$#" -eq 7 ]; then
    ./bootstrap.sh --with-toolset=$2 --with-libraries=mpi,serialization,system,filesystem --prefix=$1 --libdir=$3
    if [ "$2" = "intel-linux" ]; then
        echo "using mpi : mpiicpc ;" >> project-config.jam
    else
        echo "using mpi : mpicxx ;" >> project-config.jam
    fi
    sed -i "s/--with-mpi --with-serialization --with-system --with-filesystem/--with-mpi --with-serialization --with-system --with-filesystem --with-python/g" project-config.jam
    echo -e "import python ;\nif ! [ python.configured ]\n{\n    using python : ${4} : ${5} : ${6} : ${7} ;\n}" >> project-config.jam
fi
