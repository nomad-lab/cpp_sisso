# Contributing Guidelines

When contributing to this repository, please first discuss the change you wish to make via an issue, email, or any other method with the maintainers of this repository.
This will make life easier for everyone.

## Report Issues

Please use the [issue tracker](https://gitlab.com/sissopp-developers/sissopp/-/issues) to report issues. Before posting an issue please insure that it meets the following requirements:

- The issue has not been reported previously (Have a brief look at the issues page)
- Describe the issue in terms of actual v. expected behavior
- Provide a minimal example of the issue you are seeing


## Contribute Code via Merge Request

In order to contribute code to `SISSO++`, please use a merge request (see guidelines of preparing a merge request [here](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)).

- Please _document_ and _test_ your changes. Tests are found in `sissopp/tests` and written with [pytest](https://docs.pytest.org/en/stable/) for the python bindings and [googletest](https://github.com/google/googletest) for the C++ interface.
- If a new feature is introduced please create a minimal test of the binary file in `exec_tests`
