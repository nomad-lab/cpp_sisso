.. _api_models:


Model
-----
.. doxygenfile:: Model.hpp
   :project: SISSO++

ModelClassifier
---------------
.. doxygenfile:: ModelClassifier.hpp
   :project: SISSO++

ModelRegressor
--------------
.. doxygenfile:: ModelRegressor.hpp
   :project: SISSO++

ModelLogRegressor
-----------------
.. doxygenfile:: ModelLogRegressor.hpp
   :project: SISSO++

