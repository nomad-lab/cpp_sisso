.. _py_api_di:

Descriptor Identification
=========================
.. toctree::
    :maxdepth: 2

Solvers
-------
.. toctree::
    :maxdepth: 2

    py_solvers

Models
------
.. toctree::
    :maxdepth: 2

    py_model
