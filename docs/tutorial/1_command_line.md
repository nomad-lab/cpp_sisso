# The Command Line Interface

## Running `SISSO++` from the Command Line
The `SISSO++` binary uses a `sisso.json` input file to define the parameters needed to read the data and set up the SISSO calculation.
As an example here is the `sisso.json` file we will initially use for this system (for the definition of all options refer to the [Quick Start Guide](../quick_start/code_ref):
```json
{
    "data_file": "data.csv",
    "property_key": "E_RS - E_ZB",
    "desc_dim": 4,
    "n_sis_select": 10,
    "max_rung": 2,
    "calc_type": "regression",
    "min_abs_feat_val": 1e-5,
    "max_abs_feat_val": 1e8,
    "n_residual": 10,
    "n_models_store": 1,
    "leave_out_frac": 0.0,
    "leave_out_inds": [],
    "opset": ["add", "sub", "abs_diff", "mult", "div", "inv", "abs", "exp", "log", "sin", "cos", "sq", "cb", "six_pow", "sqrt", "cbrt", "neg_exp"]
}
```
Of these parameters `n_sis_select`,  `n_residual`,  `max_rung`, and `desc_dim` are the hyperparameters that must be optimized for each calculation.
Additionally `property_key` and `task_key` both must be columns headers in the `data_file` (Here we are only using one task so `task_key` is not included).
With this input file and the provided `data.csv` file we are now able to perform SISSO with the following command
```
mpiexec -n 2 sisso++ sisso.json
```
and get the following on screen output
```
time input_parsing: 0.00142002 s
time to generate feat sapce: 1.15551 s
Projection time: 0.137257 s
Time to get best features on rank : 3.69549e-05 s
Complete final combination/selection from all ranks: 0.00072813 s
Time for SIS: 0.184525 s
Time for l0-norm: 0.025141 s
Projection time: 0.157892 s
Time to get best features on rank : 4.91142e-05 s
Complete final combination/selection from all ranks: 0.00104713 s
Time for SIS: 0.209384 s
Time for l0-norm: 0.00287414 s
Projection time: 0.165635 s
Time to get best features on rank : 9.39369e-05 s
Complete final combination/selection from all ranks: 0.000714064 s
Time for SIS: 0.218518 s
Time for l0-norm: 0.00742316 s
Projection time: 0.166316 s
Time to get best features on rank : 6.10352e-05 s
Complete final combination/selection from all ranks: 0.000627995 s
Time for SIS: 0.218724 s
Time for l0-norm: 0.0982461 s
Train RMSE: 0.125912 eV
c0 + a0 * ((E_HOMO_B / r_p_A) / (r_sigma + r_p_B))

Train RMSE: 0.0931541 eV
c0 + a0 * ((EA_B - IP_A) * (|r_sigma - r_s_B|)) + a1 * ((E_HOMO_B / r_p_A) / (r_sigma + r_p_B))

Train RMSE: 0.0725522 eV
c0 + a0 * ((r_d_B * Z_A) / (|r_p_B - r_s_B|)) + a1 * ((|EA_B - IP_A|) * (|r_sigma - r_s_B|)) + a2 * ((E_HOMO_B / r_p_A) / (r_sigma + r_p_B))

Train RMSE: 0.0588116 eV
c0 + a0 * ((E_LUMO_A / EA_A) / (r_p_B^6)) + a1 * ((|period_B - period_A|) / (r_pi * EA_B)) + a2 * ((EA_B - IP_A) * (|r_sigma - r_s_B|)) + a3 * ((E_HOMO_B / r_p_A) / (r_sigma + r_p_B))
```
The standard output provides information about what step the calculation just finished and how long it took to complete so you can see where a job failed or ran out of time.
If this:
```
Train RMSE: 0.0588116 eV
c0 + a0 * ((E_LUMO_A / EA_A) / (r_p_B^6)) + a1 * ((|period_B - period_A|) / (r_pi * EA_B)) + a2 * ((EA_B - IP_A) * (|r_sigma - r_s_B|)) + a3 * ((E_HOMO_B / r_p_A) / (r_sigma + r_p_B))
```
is not shown at the bottom of standard output then the calculation did not complete successfully.
When all calculations are complete the code prints out a summary of the best 1D, 2D, ..., {desc_dim}D models with their training RMSE/Testing RMSE (Only training if there is no test set provided as in this case).
We also see that, two additional output files are stored in `feature_space/`: `SIS_summary.txt` and `selected_features.txt`.
These files represent a human readable (`SIS_summary.txt`) and computer readable (`selected_features.txt`) summary of the selected feature space from SIS.
Below are reconstructions of both files for this calculation (To see the file click the triangle)

<details>
    <summary>feature_space/SIS_summary.txt</summary>

    # FEAT_ID     Score                   Feature Expression
    0             0.920868624862486329    ((E_HOMO_B / r_p_A) / (r_sigma + r_p_B))
    1             0.919657911026942054    ((|r_pi - r_s_A|) / (r_s_A^3))
    2             0.913575821723830561    ((r_s_B / r_p_A) / (r_pi + r_p_B))
    3             0.913335178071393861    ((IP_B / r_p_A) / (r_sigma + r_d_B))
    4             0.912838504121724292    ((IP_B / r_p_A) / (r_sigma + r_p_B))
    5             0.911915092723797449    ((IP_B / r_p_A) / (r_s_A^2))
    6             0.909420884428965848    ((r_s_B / r_p_A) / (r_p_B * r_p_A))
    7             0.908873529961838345    ((IP_B / r_p_A) / (r_sigma + r_s_B))
    8             0.908165571792444726    ((E_HOMO_B / r_p_A) / (r_s_A^2))
    9             0.90609543772524781     ((E_HOMO_B / r_p_A) / (r_sigma + r_s_B))
    #-----------------------------------------------------------------------
    10            0.477894637470377692    ((|EA_B - IP_A|) * (|r_sigma - r_s_B|))
    11            0.474277869552096332    ((EA_B - IP_A) * (|r_sigma - r_s_B|))
    12            0.45075631366629626     ((|EA_B - IP_A|) * (|r_sigma - r_p_B|))
    13            0.449774820279551457    ((EA_B - IP_A) * (|r_sigma - r_p_B|))
    14            0.429583630959742335    (|(|r_sigma|) - (|r_sigma - r_d_B|)|)
    15            0.420792823945547012    ((E_HOMO_A / r_s_A) * (|r_sigma - r_s_B|))
    16            0.413613786609261791    ((IP_A / r_s_A) * (|r_sigma - r_s_B|))
    17            0.412833543057880004    ((E_HOMO_B / r_d_A) / (|r_sigma - r_d_A|))
    18            0.40708791994290916     ((IP_B / r_d_A) / (|r_sigma - r_d_A|))
    19            0.406210357359531482    ((E_HOMO_A / r_p_A) * (|r_sigma - r_s_B|))
    #-----------------------------------------------------------------------
    20            0.39445559217196724     ((r_sigma * E_HOMO_A) / (r_pi * EA_B))
    21            0.394109435179661072    ((|period_B - period_A|) / (r_pi * EA_B))
    22            0.38222896050736499     ((|Z_B - Z_A|) / (r_pi * EA_B))
    23            0.376740768063882125    ((r_sigma / EA_B) / (|r_p_B - r_s_B|))
    24            0.372118309722369145    ((|Z_B - Z_A|) / (r_pi * Z_B))
    25            0.371428608061153243    ((r_d_B * Z_A) / (|r_p_B - r_s_B|))
    26            0.371280945046756461    ((r_sigma * IP_A) / (r_pi * EA_B))
    27            0.367930264720841615    ((r_sigma * IP_A) / (r_p_A * EA_B))
    28            0.365697893302494692    ((Z_A / r_d_A) / (Z_B^6))
    29            0.364460897568853914    ((|period_B - period_A|) / (EA_B * Z_B))
    #-----------------------------------------------------------------------
    30            0.290108702279498698    ((EA_A / r_pi) / (r_p_B^6))
    31            0.288024533781257031    ((EA_A / r_pi) / (r_s_B^6))
    32            0.287090141076753791    ((E_LUMO_A / EA_A) / (r_p_B^6))
    33            0.277661937205204767    ((E_LUMO_A / EA_A) / (r_s_B^6))
    34            0.273556648033503158    ((E_LUMO_A^6) / (r_p_B^6))
    35            0.269806696772049381    ((E_HOMO_B^6) * (EA_A / r_pi))
    36            0.267089521281145048    ((E_LUMO_A^6) / (r_s_B^6))
    37            0.265175400489099211    ((E_LUMO_A / period_B)^6)
    38            0.262777418218664849    ((E_LUMO_A^6) / (r_p_B^3))
    39            0.253659279222423484    ((E_LUMO_A / r_p_B) * (E_LUMO_B * E_LUMO_A))
    #-----------------------------------------------------------------------
</details>
This file contains the index of the selected feature space, a projection score, and a string representation of the feature.
For regression problems the score represents the Pearson correlation between the feature and target property (all feature above the first dashed line) or the highest Pearson correlation between the feature and the residual of the best `n_residual` models of the previous dimension.

<details>
    <summary>feature_space/selected_features.txt</summary>

    # FEAT_ID     Feature Postfix Expression (RPN)
    0             9|14|div|18|15|add|div
    1             19|12|abd|12|cb|div
    2             13|14|div|19|15|add|div
    3             5|14|div|18|17|add|div
    4             5|14|div|18|15|add|div
    5             5|14|div|12|sq|div
    6             13|14|div|15|14|mult|div
    7             5|14|div|18|13|add|div
    8             9|14|div|12|sq|div
    9             9|14|div|18|13|add|div
    #-----------------------------------------------------------------------
    10            7|4|abd|18|13|abd|mult
    11            7|4|sub|18|13|abd|mult
    12            7|4|abd|18|15|abd|mult
    13            7|4|sub|18|15|abd|mult
    14            18|abs|18|17|abd|abd
    15            8|12|div|18|13|abd|mult
    16            4|12|div|18|13|abd|mult
    17            9|16|div|18|16|abd|div
    18            5|16|div|18|16|abd|div
    19            8|14|div|18|13|abd|mult
    #-----------------------------------------------------------------------
    20            18|8|mult|19|7|mult|div
    21            3|2|abd|19|7|mult|div
    22            1|0|abd|19|7|mult|div
    23            18|7|div|15|13|abd|div
    24            1|0|abd|19|1|mult|div
    25            17|0|mult|15|13|abd|div
    26            18|4|mult|19|7|mult|div
    27            18|4|mult|14|7|mult|div
    28            0|16|div|1|sp|div
    29            3|2|abd|7|1|mult|div
    #-----------------------------------------------------------------------
    30            6|19|div|15|sp|div
    31            6|19|div|13|sp|div
    32            10|6|div|15|sp|div
    33            10|6|div|13|sp|div
    34            10|sp|15|sp|div
    35            9|sp|6|19|div|mult
    36            10|sp|13|sp|div
    37            10|3|div|sp
    38            10|sp|15|cb|div
    39            10|15|div|11|10|mult|mult
    #-----------------------------------------------------------------------

</details>
This files is a computer readable file used to reconstruct the selected feature space.
In these files each feature is displayed an alphanumeric string where the integers represent an index of the primary feature space, and the strings represent operations.
The order of each term matches the order of terms if the equation is written in postfix (reverse polish) notation.
In both files the change in rung is represented by the commented out dashed (--) line.

The `models/` directory is used to store the output files representing the models for each dimension:
```
ls models/
train_dim_1_model_0.dat  train_dim_2_model_0.dat  train_dim_3_model_0.dat  train_dim_4_model_0.dat
```
Each of these files represents one of the {`n_models_store`} model stored for each dimension, and can be used to reconstruct the models within python.
The file has a header that provides metadata associated with the selected features, coefficients, modeled property, and the task sizes for the calculations.
The first six lines of the header are the most important because it defines what the model is, what the error is, and the coefficients for each task.
After the header the value of the property, estimated property, and feature value for each sample is listed with the same label used in `data.csv`.
For a line by line description of the header refer to the [quick-start guide](../quick_start/code_ref.md).
An example of these files is provided here:

<details>
    <summary>models/train_dim_2_model_0.dat</summary>

    # c0 + a0 * ((EA_B - IP_A) * (|r_sigma - r_s_B|)) + a1 * ((E_HOMO_B / r_p_A) / (r_sigma + r_p_B))
    # Property Label: $E_{RS} - E_{ZB}$; Unit of the Property: eV
    # RMSE: 0.0931540779192557; Max AE: 0.356632500670745
    # Coefficients
    # Task   a0                      a1                      c0
    # all , -3.208426383962958e-02, -2.400330764129759e-01, -2.869432663392750e-01,
    # Feature Rung, Units, and Expressions
    # 0;  2; AA * eV_IP;                                       7|4|sub|18|13|abd|mult; ((EA_B - IP_A) * (|r_sigma - r_s_B|)); $\left(\left(EA_{B} - IP_{A}\right) \left(\left|r_{sigma} - r_{s, B}\right|\right)\right)$; ((EA_B - IP_A) .* abs(r_sigma - r_s_B)); EA_B,IP_A,r_sigma,r_s_B
    # 1;  2; AA^-2 * eV;                                       9|14|div|18|15|add|div; ((E_HOMO_B / r_p_A) / (r_sigma + r_p_B)); $\left(\frac{ \left(\frac{ E_{HOMO, B} }{ r_{p, A} } \right) }{ \left(r_{sigma} + r_{p, B}\right) } \right)$; ((E_HOMO_B ./ r_p_A) ./ (r_sigma + r_p_B)); E_HOMO_B,r_p_A,r_sigma,r_p_B
    # Number of Samples Per Task
    # Task, n_mats_train
    # all , 82

    # Sample ID , Property Value        ,  Property Value (EST) ,  Feature 0 Value      ,  Feature 1 Value
    AgBr        , -3.003341671137600e-02,  1.639017458701624e-02,  3.541416008482608e+00, -1.737082125260779e+00
    AgCl        , -4.279727820539900e-02,  1.221788989673778e-02,  4.414283985050924e+00, -1.836372781878862e+00
    AgF         , -1.537576731789160e-01, -1.416891410786941e-02,  7.607045749342502e+00, -2.153206644755162e+00
    AgI         ,  3.692541964119300e-02,  6.662284759163006e-02,  1.499718119621954e+00, -1.673450475112165e+00
    AlAs        ,  2.132618491086760e-01,  2.460156857673837e-01,  1.024737940396364e+00, -2.357328927365386e+00
    AlN         ,  7.294907316963900e-02,  2.456878076268038e-01,  3.482569932972883e+00, -2.684491554934336e+00
    AlP         ,  2.189583414756270e-01,  2.801436096644385e-01,  5.790749046839015e-01, -2.439939014991684e+00
    AlSb        ,  1.568687339604370e-01,  2.007443516059665e-01,  2.950349986552500e+00, -2.426113242539595e+00
    AsGa        ,  2.742777724197370e-01,  3.151675906408351e-01,  1.671179867660379e+00, -2.731829473573980e+00
    AsB         ,  8.749781837650520e-01,  7.881634596496340e-01,  3.810479887758816e+00, -4.988325717257844e+00
    BN          ,  1.712080260836270e+00,  1.606089893577266e+00,  3.161246801594280e-01, -7.928806379528248e+00
    BP          ,  1.019225161195210e+00,  1.010436354881370e+00,  4.200899478533967e+00, -5.966520988922634e+00
    BSb         ,  5.808491143689030e-01,  4.226792460264598e-01,  2.600752635485375e+00, -3.303985258847382e+00
    BaO         , -9.299855386780100e-02, -3.688421176160839e-01,  8.608614320176068e+00, -8.094809464135576e-01
    BaS         , -3.197624294261910e-01, -3.363542918017661e-01,  6.543460373759491e+00, -6.687873437791405e-01
    BaSe        , -3.434451340872330e-01, -3.321656670638599e-01,  6.165281404183006e+00, -6.356878675764703e-01
    BaTe        , -3.753868096682710e-01, -3.026651587599424e-01,  5.015472606607728e+00, -6.048993586215626e-01
    BeO         ,  6.918375772329460e-01,  5.153912242420119e-01,  6.066290623904059e+00, -4.153456575173075e+00
    BeS         ,  5.063276745431720e-01,  6.140186443576880e-01,  2.645797589979559e-01, -3.788855982192108e+00
    BeSe        ,  4.949404427752600e-01,  5.429907890755682e-01,  1.744183600985631e+00, -3.690720110296145e+00
    BeTe        ,  4.685859104938570e-01,  4.516260835242951e-01,  4.959181529624447e+00, -3.739822244098568e+00
    C2          ,  2.628603639133640e+00,  2.783574944051988e+00,  6.386751756964515e+00, -1.364575452594942e+01
    CaO         , -2.652190413191420e-01, -3.238274257878371e-01,  9.342332840592833e+00, -1.095089544385995e+00
    CaS         , -3.691331945374260e-01, -2.680040315596177e-01,  6.270424713694642e+00, -9.170452629688933e-01
    CaSe        , -3.607977344217940e-01, -2.575530743234562e-01,  5.625809745654779e+00, -8.744218061257983e-01
    CaTe        , -3.504562790767520e-01, -2.135956642585606e-01,  3.987719873696632e+00, -8.385955037322098e-01
    CdO         , -8.416135802690400e-02, -1.232460775026244e-01,  1.065231004987821e+01, -2.105829423728353e+00
    CdS         ,  7.267279591178499e-02,  1.431422536464881e-02,  4.311359919493231e+00, -1.831348860072292e+00
    CdSe        ,  8.357194908603600e-02,  4.401882316506024e-02,  2.868768109826145e+00, -1.762275469514607e+00
    CdTe        ,  1.145395321946130e-01,  1.171042634651223e-01,  3.457696716299031e-01, -1.729517037320575e+00
    BrCs        , -1.558673029940110e-01, -1.898926962890557e-01,  8.647554645764165e-01, -5.199100657183604e-01
    ClCs        , -1.503461574466200e-01, -1.571430901173148e-01,  1.238997024850578e-01, -5.573207199827011e-01
    CsF         , -1.082633186742900e-01, -8.428877325507279e-02, -1.184139604981316e+00, -6.859981467376031e-01
    CsI         , -1.623874744982460e-01, -2.139647446584292e-01,  1.354924677944221e+00, -4.851426489675994e-01
    BrCu        ,  1.524426397882050e-01,  1.751625662935415e-01,  2.324749827375701e+00, -2.235915680770981e+00
    ClCu        ,  1.562587131920740e-01,  1.703409819257667e-01,  3.357679763615902e+00, -2.353896138537911e+00
    CuF         , -1.702227234272900e-02,  1.432152572174888e-01,  6.954856486036995e+00, -2.721708123665148e+00
    CuI         ,  2.046745832631130e-01,  2.336424208281593e-01,  4.875295043074224e-02, -2.175324740635570e+00
    GaN         ,  4.334452390939990e-01,  3.544948246079955e-01,  2.884011194974658e+00, -3.057784693724841e+00
    GaP         ,  3.487517977519020e-01,  3.520992885010393e-01,  1.208441824761596e+00, -2.823837994787984e+00
    GaSb        ,  1.546252850966990e-01,  2.794927768939737e-01,  3.614065011465625e+00, -2.842902606558420e+00
    Ge2         ,  2.008525260607710e-01,  2.394501685804619e-01,  6.088560028849320e+00, -3.006837274572150e+00
    CGe         ,  8.114428802000480e-01,  4.548103795293029e-01,  1.138082099212689e+00, -3.242337197194732e+00
    GeSi        ,  2.632101701783540e-01,  2.725388874377264e-01,  6.113819992476531e+00, -3.148064336698105e+00
    AsIn        ,  1.340475751931080e-01,  1.801608579254571e-01,  4.068020252879492e-01, -2.000374593993287e+00
    InN         ,  1.537202926992900e-01,  1.448590522634359e-01,  3.816695674128193e+00, -2.309090888179985e+00
    InP         ,  1.791932872292820e-01,  2.105475238127370e-01,  7.234639577287595e-12, -2.072592650924130e+00
    InSb        ,  7.805987301981100e-02,  1.319903471163014e-01,  2.214419977436284e+00, -2.041308871200640e+00
    BrK         , -1.661759641938260e-01, -1.296547837765485e-01,  1.519640837548166e+00, -8.584026968916481e-01
    ClK         , -1.644606802110500e-01, -1.032766020647462e-01,  1.132879978116893e+00, -9.165998606480857e-01
    FK          , -1.464060984981190e-01, -3.718539436956973e-02,  5.397857467140130e-01, -1.112665405432063e+00
    IK          , -1.670391451625620e-01, -1.431459979136605e-01,  1.563489995919397e+00, -8.080581929117355e-01
    BrLi        , -3.274621288437600e-02, -2.060136493312719e-02,  2.019046121317332e+00, -1.379482839678231e+00
    ClLi        , -3.838148269915100e-02, -2.057167653941297e-03,  2.078199280986889e+00, -1.464646447821141e+00
    FLi         , -5.948831686373500e-02,  4.809501062409577e-02,  2.596776386498242e+00, -1.742901194835849e+00
    ILi         , -2.166093634150500e-02, -1.658372988181873e-02,  1.416168070359142e+00, -1.315636374733589e+00
    MgO         , -2.322747243169940e-01, -1.709637107152052e-01,  9.458655848384394e+00, -1.747482354099491e+00
    MgS         , -8.669950498824600e-02, -7.634077484966063e-02,  4.672979827143721e+00, -1.502008033928903e+00
    MgSe        , -5.530180195637500e-02, -5.633708966390212e-02,  3.594547977146389e+00, -1.441195553246026e+00
    MgTe        , -4.591286648065000e-03,  1.388515614782964e-02,  1.127931194217332e+00, -1.404045098915806e+00
    BrNa        , -1.264287278827400e-01, -1.713365131268951e-01,  2.863734255675713e+00, -8.644123624074346e-01
    ClNa        , -1.329919850813890e-01, -1.536717536375947e-01,  2.742537497136659e+00, -9.218054972107542e-01
    FNa         , -1.457881377878040e-01, -1.146505476072913e-01,  2.962752633454122e+00, -1.113806729932952e+00
    INa         , -1.148382221872450e-01, -1.700256955871654e-01,  2.461824196702322e+00, -8.161516351556487e-01
    BrRb        , -1.638205314229710e-01, -2.129364033898979e-01,  1.681775685044998e+00, -5.331156841370923e-01
    ClRb        , -1.605035540778770e-01, -1.837284810776369e-01,  1.056091935643463e+00, -5.711659393468355e-01
    FRb         , -1.355957769847010e-01, -1.206203707726129e-01,  6.544971557289886e-02, -7.016649706208032e-01
    IRb         , -1.672014421201310e-01, -2.313261599637700e-01,  1.992777843630929e+00, -4.980726751118079e-01
    Si2         ,  2.791658215483040e-01,  2.916041179034760e-01,  6.358817979658935e+00, -3.260239754057512e+00
    CSi         ,  6.690237272359810e-01,  4.822282210388984e-01,  1.101648177462507e+00, -3.351692484156472e+00
    Sn2         ,  1.696389919379700e-02,  2.567935768664463e-02,  6.363815657894407e+00, -2.153040623998902e+00
    CSn         ,  4.535379741428190e-01,  1.672793503038666e-01,  3.023496041144674e+00, -2.296472092858216e+00
    GeSn        ,  8.166336023714400e-02,  8.544807638632528e-02,  3.656280114481603e+00, -2.040137159045913e+00
    SiSn        ,  1.351087991060920e-01,  1.054171704626117e-01,  3.690378073553809e+00, -2.127887990332651e+00
    OSr         , -2.203066231741100e-01, -3.724240237555350e-01,  9.409926969679084e+00, -9.016666595490496e-01
    SSr         , -3.684341299303920e-01, -3.249109578073314e-01,  6.787670829981468e+00, -7.491039692724043e-01
    SeSr        , -3.745109517331000e-01, -3.168489728859340e-01,  6.265945830481024e+00, -7.129540446704449e-01
    SrTe        , -3.792947258625650e-01, -2.790737245833743e-01,  4.846463948497374e+00, -6.805927425088493e-01
    OZn         ,  1.019681767684230e-01,  6.602587728853203e-02,  9.268479722491282e+00, -2.709382815714799e+00
    SZn         ,  2.758133256065780e-01,  2.143486874814196e-01,  2.332991532953503e+00, -2.400270322363168e+00
    SeZn        ,  2.631368992806530e-01,  2.463580576975095e-01,  7.384497385908948e-01, -2.320488278555971e+00
    TeZn        ,  2.450012951740060e-01,  1.776248032825628e-01,  2.763715059556858e+00, -2.304848319397327e+00
</details>


## Determining the Ideal Model Complexity with Cross-Validation
While the training error always decreases with descriptor dimensionality for a given application, over-fitting can reduce the general applicability of the models outside of the training set.
In order to determine the optimal dimensionality of a model and optimize the hyperparameters associated with SISSO, we need to perform cross-validation.
The goal of cross-validation is to test how generalizable a given model is with respect to new data.
In practice, we perform cross-validation by randomly splitting the data-set into separate train/test sets and evaluate the performance of the model on the test set.

As an example we will discuss how to perform leave-out 10% using the command line.
To do this we have to modify the `sisso.json` file to automatically leave out a random sample of the training data and use that as a test set by changing `"leave_out_frac": 0.0` to `"leave_out_frac": 0.10`,
i.e. in this case SISSO will ignore 8 materials (10% of all data) during training.
In each run, this 8 materials are chosen randomly, so each SISSO run will
differ from one another.

<details>
    <summary> updated sisso.json file</summary>

    {
        "data_file": "data.csv",
        "property_key": "E_RS - E_ZB",
        "desc_dim": 4,
        "n_sis_select": 10,
        "max_rung": 2,
        "calc_type": "regression",
        "min_abs_feat_val": 1e-5,
        "max_abs_feat_val": 1e8,
        "n_residual": 10,
        "n_models_store": 1,
        "leave_out_frac": 0.10,
        "leave_out_inds": [],
        "opset": ["add", "sub", "abs_diff", "mult", "div", "inv", "abs", "exp", "log", "sin", "cos", "sq", "cb", "six_pow", "sqrt", "cbrt", "neg_exp"]
    }

</details>

Now lets make ten cross validation directories in the working directory and copy the `data.csv` and `sisso.json` into them and run separate calculations for each run.
Note the decision to begin with ten iterations is arbitrary, and not connected to the amount of data excluded from the test set.
```bash
for ii in `seq -f "%03g" 0 9`; do
    mkdir cv_$ii;
    cp sisso.json data.csv cv_$ii;
    cd cv_$ii;
    mpiexec -n 2 sisso++;
    cd ../;
done
```
Each of these directories has the same kind of output files as the non-cross-validation calculations, with the testing and training data defined in separate files in  `cv_$ii/models/`
```
ls cv_00/models/
test_dim_1_model_0.dat  test_dim_3_model_0.dat  train_dim_1_model_0.dat  train_dim_3_model_0.dat
test_dim_2_model_0.dat  test_dim_4_model_0.dat  train_dim_2_model_0.dat  train_dim_4_model_0.dat
```

A full example of the testing set output file is reproduced below:
<details>
    <summary>The test data file cv_0/models/test_dim_2_model_0.dat</summary>

    # c0 + a0 * ((E_HOMO_B / r_p_A) / (r_sigma + r_p_B))
    # Property Label: $E_{RS} - E_{ZB}$; Unit of the Property: eV
    # RMSE: 0.212994478440008; Max AE: 0.442277221520276
    # Coefficients
    # Task   a0                      c0
    # all , -2.346702839867608e-01, -3.917145321667535e-01,
    # Feature Rung, Units, and Expressions
    # 0;  2; AA^-2 * eV;                                       9|14|div|18|15|add|div; ((E_HOMO_B / r_p_A) / (r_sigma + r_p_B)); $\left(\frac{ \left(\frac{ E_{HOMO, B} }{ r_{p, A} } \right) }{ \left(r_{sigma} + r_{p, B}\right) } \right)$; ((E_HOMO_B ./ r_p_A) ./ (r_sigma + r_p_B)); E_HOMO_B,r_p_A,r_sigma,r_p_B
    # Number of Samples Per Task
    # Task,   n_mats_test
    # all,    8
    # Test Indexes: [ 2, 19, 41, 42, 50, 59, 60, 69 ]

    # Sample ID , Property Value        ,  Property Value (EST) ,  Feature 0 Value
    AgF         , -1.537576731789160e-01,  1.135790826401206e-01, -2.153206644755162e+00
    BeSe        ,  4.949404427752600e-01,  4.743878042320918e-01, -3.690720110296145e+00
    Ge2         ,  2.008525260607710e-01,  3.139008249590708e-01, -3.006837274572150e+00
    CGe         ,  8.114428802000480e-01,  3.691656586797722e-01, -3.242337197194732e+00
    FK          , -1.464060984981190e-01, -1.306050254917670e-01, -1.112665405432063e+00
    MgTe        , -4.591286648065000e-03, -6.222687007396171e-02, -1.404045098915806e+00
    BrNa        , -1.264287278827400e-01, -1.888626375989341e-01, -8.644123624074346e-01
    CSi         ,  6.690237272359810e-01,  3.948280949265375e-01, -3.351692484156472e+00

</details>

## Analyzing the Results with Python
*Note to do this part of the tutorial the python binding must also be built*

Once all of the calculations are completed the python interface provides some useful post-processing tools to easily analyze the results.
The `jackknife_cv_conv_est` tools provides a way to reasonably check the convergence of the cross-validation results with respect to the number number of calculations performed.
This tool uses [jackknife resampling](https://en.wikipedia.org/wiki/Jackknife_resampling) to calculate the mean and variance of the validation RMSEs across all cross-validation runs.
This technique essentially calculates the mean and standard error of all validation RMSEs for the system.
This data can then be used to estimate the overall validation RMSE for a given problem/set of hyper-parameters and the standard error associated with the random sampling of the test indexes.
It is important to mention that the error bars are based on the standard error of the mean of the validation RMSE, which assumes the sampling error follows a normal distribution.
Because the data set may not represent a uniform sampling of materials space, the standard error of the mean may only be a rough estimate of the true sampling error.
To visualize these results we will also use `plot_validation_rmse` at the end, and make the results easier to interpret.
```python
>>> from sissopp.postprocess.check_cv_convergence import jackknife_cv_conv_est
>>> from sissopp.postprocess.plot.cv_error_plot import plot_validation_rmse
>>> import numpy as np
>>> mean_val_rmse, var_val_rmse = jackknife_cv_conv_est("cv*")
>>> print(mean_val_rmse)
[0.16107651 0.10775548 0.10057917 0.10803806]
>>> print(np.sqrt(var_val_rmse))
[0.0159642  0.01732917 0.0118953  0.01717732]
>>> plot_validation_rmse("cv*", "cv_10._error.png").show()
```
Here is an example of the `plot_validation_rmse` output:
<details>
    <summary> Cross-Validation results </summary>

![image](./command_line/cv/cv_10_error.png)

</details>

These initial results suggest that we need to run more cross-validation samples in order to get converged results.
Using these results, we can only clearly state that the there is a significant decrease in the validation error when going from a one-dimensional model to a two-dimensional one.
However, because of the large error bars, it is impossible to determine which of the two, three, or four dimensional model is best.
To solve this lets increase the total number of samples to 100, and redo the analysis

```bash
for ii in `seq -f "%03g" 10 99`; do
    mkdir cv_$ii;
    cp sisso.json data.csv cv_$ii;
    cd cv_$ii;
    mpiexec -n 2 sisso++;
    cd ../;
done
```
```python
>>> from sissopp.postprocess.check_cv_convergence import jackknife_cv_conv_est
>>> from sissopp.postprocess.plot.cv_error_plot import plot_validation_rmse
>>> import numpy as np
>>> mean_val_rmse, var_val_rmse = jackknife_cv_conv_est("cv*")
>>> print(mean_val_rmse)
[0.15597268 0.12273297 0.10921321 0.10870643]
>>> print(np.sqrt(var_val_rmse))
[0.0051855  0.00571521 0.00398963 0.00473639]
>>> plot_validation_rmse("cv*", "cv_100._error.png").show()
```
With the additional calculations we now have relatively well converged results.
The key used in determining this is the relative size of the error bars when compared against the mean value.
For this example the estimate of the validation RMSEs for all dimensions up to the third dimension is outside the error bars of the other error bars, meaning that we can confidently say that the three-dimensional model is better than both the one and two-dimensional models.
Because the validation error for the three and four dimensional models are within each others error bars and the standard error increases when going to the fourth dimension, we can then conclude that the three-dimensional model has the ideal complexity.

<details>
    <summary> Converged cross-validation results </summary>

![image](./command_line/cv/cv_100_error.png)

</details>


## Visualizing the Cross Validation Error
The previous section illustrated how to plot the validation RMSE for each dimension of the model, but the RMSE does not give a complete picture of the model performance.
`SISSO++` also provides some utilities to plot the distribution of the average error for each sample.
To see the distributions for this system we run
```python
>>> from sissopp.postprocess.plot.cv_error_plot import plot_errors_dists
>>> plot_errors_dists("cv*", "error_cv_dist.png").show()
```
<details>
<summary> Distribution of Errors </summary>

![image](./command_line/cv/error_cv_dist.png)

</details>

These plots show the histogram of the error for each dimension with the total area normalized to one.
One thing that stands out in the plot is the large error seen in a single point for both the one and two dimensional models.
By looking at the validation errors, we find that the point with the largest error is diamond for all model dimensions, which is by far the most stable zinc-blende structure in the data set.
As a note for this setup there is a 0.22\% chance that one of the samples is never in the validation set so if `max_error_ind != 21` check if that sample is in one of the validation sets.

```python
>>> import numpy as np
>>> import pandas as pd
>>> from sissopp.postprocess.get_model_errors import get_model_errors
>>> 
>>> df = pd.read_csv("data.csv", index_col=0)
>>> 
>>> te, ve = get_model_errors("cv*", True)
>>> max_err_ind = np.nanmean(ve, axis=1).argmax(axis=0)
>>> print(df.index[max_err_ind])
Index(['C2', 'C2', 'C2', 'C2'], dtype='object', name='# Material')
```

## Optimizing the hyper-parameters of SISSO
As discussed in the previous example `desc_dim` is one of the four hyperparameters used in `SISSO++` with the others being: `n_sis_select`, `max_rung`, and `n_residual`.
Of these `n_sis_select` and `n_residual` need to be optimized together while `desc_dim` and `max_rung` can be optimized independently.
Due to the factorial increase in both computational time and required memory associated with `max_rung` only `desc_dim`, `n_sis_select`, and `n_residual` will be optimized in this exercise, but for production purposes this will also have to be studied.
Additionally the exercise will only use use relatively small SIS subspace sizes and only go up to a 3D model in order to reduce the computational time for the exercise.
The first step of this process will be setting up nine directories for each combination `n_residual` (1, 5, and 10) and `n_sis_select` (10, 50, 100) and modify the base `sisso.json` to match these new parameters (Note: the dimension of the final model will be determined in the same way as the previous example).
Here is the new base `sisso.json file`:

```json
{
    "data_file": "data.csv",
    "property_key": "E_RS - E_ZB",
    "desc_dim": 3,
    "n_sis_select": SSSS,
    "max_rung": 2,
    "calc_type": "regression",
    "min_abs_feat_val": 1e-5,
    "max_abs_feat_val": 1e8,
    "n_residual": RRRR,
    "n_models_store": 1,
    "leave_out_frac": 0.05,
    "leave_out_inds": [],
    "opset": ["add", "sub", "abs_diff", "mult", "div", "inv", "abs", "exp", "log", "sin", "cos", "sq", "cb", "six_pow", "sqrt", "cbrt", "neg_exp"]

}
```

From this base we will set up the nine directories with the following set of commands (Note: we will simply copy over the previously calculated cross-validation data from the previous exercise)
```bash
mkdir ns_10_nr_1;
cp data.csv ns_10_nr_1/;
sed "s/SSSS/10/g" sisso.json > ns_10_nr_1/sisso.json;
sed -i "s/RRRR/1/g" ns_10_nr_1/sisso.json;

mkdir ns_10_nr_5;
cp data.csv ns_10_nr_5/;
sed "s/SSSS/10/g" sisso.json > ns_10_nr_5/sisso.json;
sed -i "s/RRRR/5/g" ns_10_nr_5/sisso.json;

mkdir ns_10_nr_10;
cp data.csv ns_10_nr_10/;
sed "s/SSSS/10/g" sisso.json > ns_10_nr_10/sisso.json;
sed -i "s/RRRR/10/g" ns_10_nr_10/sisso.json;
mv cv* ns_10_nr_10

mkdir ns_50_nr_1;
cp data.csv ns_50_nr_1;
sed "s/SSSS/50/g" sisso.json > ns_50_nr_1/sisso.json;
sed -i "s/RRRR/1/g" ns_50_nr_1/sisso.json;

mkdir ns_50_nr_5;
cp data.csv ns_50_nr_5;
sed "s/SSSS/50/g" sisso.json > ns_50_nr_5/sisso.json;
sed -i "s/RRRR/5/g" ns_50_nr_5/sisso.json;

mkdir ns_50_nr_10;
cp data.csv ns_50_nr_10
sed "s/SSSS/50/g" sisso.json > ns_50_nr_10/sisso.json;
sed -i "s/RRRR/10/g" ns_50_nr_10/sisso.json;

mkdir ns_100_nr_1;
cp data.csv ns_100_nr_1;
sed "s/SSSS/100/g" sisso.json > ns_100_nr_1/sisso.json;
sed -i "s/RRRR/1/g" ns_100_nr_1/sisso.json;

mkdir ns_100_nr_5;
cp data.csv ns_100_nr_5;
sed "s/SSSS/100/g" sisso.json > ns_100_nr_5/sisso.json;
sed -i "s/RRRR/5/g" ns_100_nr_5/sisso.json;

mkdir ns_100_nr_10;
cp data.csv ns_100_nr_10;
sed "s/SSSS/100/g" sisso.json > ns_100_nr_10/sisso.json;
sed -i "s/RRRR/10/g" ns_100_nr_10/sisso.json;
```
From here we can run the same cross-validation analysis from the previous section in each of the results, and then compare the results in python
```python
>>> from sissopp.postprocess.check_cv_convergence import jackknife_cv_conv_est
>>> import numpy as np
>>> 
>>> mean_10_1, var_10_1 = jackknife_cv_conv_est("ns_10_nr_1/cv*")
>>> mean_10_5, var_10_5 = jackknife_cv_conv_est("ns_10_nr_5/cv*")
>>> mean_10_10, var_10_10 = jackknife_cv_conv_est("ns_10_nr_10/cv*")
>>> mean_50_1, var_50_1 = jackknife_cv_conv_est("ns_50_nr_1/cv*")
>>> mean_50_5, var_50_5 = jackknife_cv_conv_est("ns_50_nr_5/cv*")
>>> mean_50_10, var_50_10 = jackknife_cv_conv_est("ns_50_nr_10/cv*")
>>> mean_100_1, var_100_1 = jackknife_cv_conv_est("ns_100_nr_1/cv*")
>>> mean_100_5, var_100_5 = jackknife_cv_conv_est("ns_100_nr_5/cv*")
>>> mean_100_10, var_100_10 = jackknife_cv_conv_est("ns_100_nr_10/cv*")
>>> 
>>> print_str = f"ns:  10; nr:  1; {mean_10_1[:3]} {np.sqrt(var_10_1[:3])}\n"
>>> print_str += f"ns:  10; nr:  5; {mean_10_5[:3]} {np.sqrt(var_10_5[:3])}\n"
>>> print_str += f"ns:  10; nr: 10; {mean_10_10[:3]} {np.sqrt(var_10_10[:3])}\n"
>>> print_str += f"ns:  50; nr:  1; {mean_50_1[:3]} {np.sqrt(var_50_1[:3])}\n"
>>> print_str += f"ns:  50; nr:  5; {mean_50_5[:3]} {np.sqrt(var_50_5[:3])}\n"
>>> print_str += f"ns:  50; nr: 10; {mean_50_10[:3]} {np.sqrt(var_50_10[:3])}\n"
>>> print_str += f"ns: 100; nr:  1; {mean_100_1[:3]} {np.sqrt(var_100_1[:3])}\n"
>>> print_str += f"ns: 100; nr:  5; {mean_100_5[:3]} {np.sqrt(var_100_5[:3])}\n"
>>> print_str += f"ns: 100; nr: 10; {mean_100_10[:3]} {np.sqrt(var_100_10[:3])}"
>>> print(print_str)
ns:  10; nr:  1; [0.15680869 0.17389737 0.16029643] [0.00646652 0.04735888 0.04424095]
ns:  10; nr:  5; [0.15625644 0.12419926 0.15115378] [0.00663913 0.00631875 0.04471696]
ns:  10; nr: 10; [0.15597268 0.12273297 0.10921321] [0.0051855  0.00571521 0.00398963]
ns:  50; nr:  1; [0.15192553 0.12373729 0.13507366] [0.00513279 0.00523007 0.01695709]
ns:  50; nr:  5; [0.15262692 0.12672067 0.11011062] [0.00491465 0.00522488 0.00407753]
ns:  50; nr: 10; [0.15119692 0.13040251 0.10993919] [0.00487215 0.00506964 0.00464264]
ns: 100; nr:  1; [0.15835654 0.13728706 0.12849654] [0.00557331 0.00606579 0.01114889]
ns: 100; nr:  5; [0.15502757 0.14002783 0.12102758] [0.00489507 0.00546934 0.00612467]
ns: 100; nr: 10; [0.14996602 0.13248817 0.1070521 ] [0.00495617 0.0051647  0.00432492]
```
These results indicate that for the small SIS subspace sizes used here the validation error is stable relative to both the number of residuals and SIS subspace size, given the simliarity
However it is important to note that this will not always be the case, particularly for for larger values of `n_sis_select` .
For finding the best model over all an `n_sis_select` of 100 and `n_residual` of 10 will be used.
This choice was made because it has the lowest validation RMSE of 0.107, but all calculations that use 10 residuals will have equivalent performance (at least for the small SIS subspace size).

## Final Training Data
To get the final models we will perform the same calculation we started off the tutorial with, but with the following `sisso.json` file based on the results from the previous steps:
```
{
    "data_file": "data.csv",
    "property_key": "E_RS - E_ZB",
    "desc_dim": 3,
    "n_sis_select": 100,
    "max_rung": 2,
    "calc_type": "regression",
    "min_abs_feat_val": 1e-5,
    "max_abs_feat_val": 1e8,
    "n_residual": 10,
    "n_models_store": 1,
    "leave_out_frac": 0.0,
    "leave_out_inds": [],
    "opset": ["add", "sub", "abs_diff", "mult", "div", "inv", "abs", "exp", "log", "sin", "cos", "sq", "cb", "six_pow", "sqrt", "cbrt", "neg_exp"]
}
```
From here we can use `models/train_dim_3_model_0.dat` for all of the analysis.
In order to generate a machine learning plot for this model in matplotlib, run the following in python
```python
>>> from sissopp.postprocess.plot.parity_plot import plot_model_parity_plot
>>> plot_model_parity_plot("models/train_dim_3_model_0.dat", filename="3d_model.pdf").show()
```
The result of which is shown below:
<details>
<summary> Final 3D model </summary>

![image](./command_line/cv/3d_model.png)
</details>

Additionally you can generate a output the model as a Matlab function or a LaTeX string using the following commands.
```python
>>> from sissopp.postprocess.load_models import load_model
>>> model = load_model("models/train_dim_3_model_0.dat")
>>> print(model.latex_str)
>>> 
>>> model.write_matlab_fxn("matlab_fxn/model.m")
```

A copy of the generated matlab function is below.
<details>
<summary> Matlab function of the Final 3D model </summary>


    function P = model(X)
    % Returns the value of E_{RS} - E_{ZB} = c0 + a0 * ((r_d_B / r_d_A) * (r_p_B * E_HOMO_A)) + a1 * ((IP_A^3) * (|r_sigma - r_s_B|)) + a2 * ((IP_A / r_p_A) / (r_p_B + r_p_A))
    %
    % X = [
    %     r_d_B,
    %     r_d_A,
    %     r_p_B,
    %     E_HOMO_A,
    %     IP_A,
    %     r_sigma,
    %     r_s_B,
    %     r_p_A,
    % ]

    if(size(X, 2) ~= 8)
        error("ERROR: X must have a size of 8 in the second dimension.")
    end
    r_d_B    = reshape(X(:, 1), 1, []);
    r_d_A    = reshape(X(:, 2), 1, []);
    r_p_B    = reshape(X(:, 3), 1, []);
    E_HOMO_A = reshape(X(:, 4), 1, []);
    IP_A     = reshape(X(:, 5), 1, []);
    r_sigma  = reshape(X(:, 6), 1, []);
    r_s_B    = reshape(X(:, 7), 1, []);
    r_p_A    = reshape(X(:, 8), 1, []);

    f0 = ((r_d_B ./ r_d_A) .* (r_p_B .* E_HOMO_A));
    f1 = ((IP_A).^3 .* abs(r_sigma - r_s_B));
    f2 = ((IP_A ./ r_p_A) ./ (r_p_B + r_p_A));

    c0 = -1.3509197357e-01;
    a0 = 2.8311062079e-02;
    a1 = 3.7282871777e-04;
    a2 = -2.3703222974e-01;

    P = reshape(c0 + a0 * f0 + a1 * f1 + a2 * f2, [], 1);
    end

</details>
