# The `scikit-learn` Interface

To facilitate the integration of `SISSO++` into existing machine learning frameworks we also provide a `scikit-learn` interface via `sissopp.sklearn` submodule.
In this module we wrap all of the `SISSOSolver` objects into classes that use the same syntax as `scikit-learn` models, and can then be used in those frameworks.
Additionally we provide some utility functions to mimic the outputs of other `SISSO++` calculations.

## Using the `scikit-learn` Interface
To use the `scikit-learn` interface we must first set up the `SISSORegressor` object, which we can do either by manually specifying the parameters:

```python
>>> import numpy as np
>>> import pandas as pd
>>> from sissopp.sklearn import SISSORegressor
>>> df = pd.read_csv("data.csv", index_col=0)
>>> sisso = SISSORegressor(
...     prop_label = "E_RS - E_ZB",
...     prop_unit = "eV",
...     allowed_ops = "all",
...     n_dim = 4,
...     max_rung = 2,
...     n_sis_select = 10,
...     n_residual = 10,
... )
```

or by using a pre-existing `Inputs` object

```python
>>> import numpy as np
>>> import pandas as pd
>>> from sissopp.sklearn import SISSORegressor
>>> from sissopp.py_interface import read_csv
>>> 
>>> df = pd.read_csv("data.csv", index_col=0)
>>> inputs = read_csv(df, prop_key="E_RS - E_ZB", max_rung=2)
>>> inputs.n_sis_select = 10
>>> inputs.n_dim = 4
>>> inputs.n_residual = 10
>>> inputs.allowed_ops = ["add", "sub", "abs_diff", "mult", "div", "inv", "abs", "exp", "log", "sin", "cos", "sq", "cb", "six_pow", "sqrt", "cbrt", "neg_exp"]
>>> sisso = SISSORegressor.from_inputs(inputs)
```

From here we can fit a model to the data and evaluate its performance using the `scikit-learn` syntax

```python
>>> X = df.iloc[:, 1:]
>>> y = df.iloc[:, 0].values
>>> sisso.fit(X, y)
time to generate feat space: 10.7571 s
Projection time: 0.35614 s
Time to get best features on rank : 7.20024e-05 s
Complete final combination/selection from all ranks: 0.000281096 s
Time for SIS: 0.507253 s
Time for l0-norm: 0.000794172 s
Projection time: 0.422137 s
Time to get best features on rank : 4.1008e-05 s
Complete final combination/selection from all ranks: 0.000236034 s
Time for SIS: 0.573581 s
Time for l0-norm: 0.00115705 s
Projection time: 0.41528 s
Time to get best features on rank : 3.79086e-05 s
Complete final combination/selection from all ranks: 0.000231028 s
Time for SIS: 0.565092 s
Time for l0-norm: 0.0059731 s
Projection time: 0.412625 s
Time to get best features on rank : 3.88622e-05 s
Complete final combination/selection from all ranks: 0.000246048 s
Time for SIS: 0.565728 s
Time for l0-norm: 0.130348 s
```
and then score it using `scikit-learn` or the default metrics provided by `SISSO++` using the `get_default_model_metric` function
```python
>>> from sissopp.sklearn import regression_metric
>>> from sklearn.metrics import mean_absolute_error
>>> 
>>> y_pred = sisso.predict(X)
>>> rmse = regression_metric(y, y_pred)
>>> mae = mean_absolute_error(y, y_pred)
>>> print(rmse, mae)
0.0556078280028383, 0.044361625085390315
```

Examining the file system we see that this calculation was done in a folder based on the initial timestamp of the claculation: `SISSO_YYYY-mm-dd_hh:mm:ss`.
```bash
ls SISSO_YYYY-mm-dd_hh:mm:ss
feature_space/ models/
```
To change this to a user-defined workdir we need to set the workdir paramter of `sisso`
```python
>>> sisso.workdir = "sklearn_sisso_test"
>>> sisso.fit(X, y)
```
Now we see that SISSO was done inside of `sklearn_sisso_test`
```bash
ls sklearn_sisso_test/
feature_space/ models/
```
If we want to remove this working directory after the calculation is completed, then all we have to do is set `clean_workdir` to `True`
```python
>>> sisso.workdir = "sklearn_sisso_test_cleaned"
>>> sisso.clean_workdir = True
>>> sisso.fit(X, y)
```
and we see that `sklearn_sisso_test` was removed
```bash
ls
data.csv SISSO_YYYY-mm-dd_hh:mm:ss/ sklearn_sisso_test/
```

## Performing Cross-Validation with `scikit-learn`

Now that we can use the interface to fit models using `SISSO` we can now use the cross-validation tools of `scikit-learn`.

```python
>>> from sklearn.model_selection import cross_validate, KFold
>>> from sklearn.metrics import make_scorer
>>> from sissopp.sklearn import get_default_model_metric
>>> 
>>> kf = KFold(n_splits=10, random_state=13, shuffle=True)
>>> 
>>> metric = get_default_model_metric(sisso)
>>> scorer = make_scorer(metric, greater_is_better=False)
>>> 
>>> sisso.workdir = None
>>> sisso.clean_workdir = False
>>> 
>>> cv_results = cross_validate(sisso, X, y, cv=kf, scoring=scorer)
>>> print(cv_results["test_score"].mean())
-0.12127230691149424
```
The test score is negative because `scikit-learn` takes the negative of scores if the problem is trying to minimize a loss.
Looking inside this directory we see that we now have many timestamped directories, and that none of them contain a `test_dim{dd}_model_0.dat` file.
```bash
ls SISSO_YYYY-mm-dd_hh/models/
train_dim_1_model_0.dat  train_dim_2_model_0.dat  train_dim_3_model_0.dat  train_dim_4_model_0.dat
```
Additionally if we were to run these files in a sperate working directory
```python
>>> sisso.workdir = "cv_test"
>>> 
>>> cv_results = cross_validate(sisso, X, y, cv=kf, scoring=scorer)
>>> print(cv_results["test_score"].mean())
-0.12127230691149424
```
thme each cross-validation calculation overwrites the others
```bash
ls cv_test/
feature_space  models
```

To recover the test output files and allow for running of cross-validation insisde seperate working directories, we provide a utility function to get the scores for each split, in a seperate directory and output the results of each model into a test file
```python
>>> from sissopp.sklearn import cross_validate_from_splitter
>>> 
>>> test_scores = cross_validate_from_splitter(X, y, sisso, kf, scoring=scorer)
>>> print(np.mean(test_scores))
-0.12127230691149424
```

Now if we look into the `cv_test/` directory we see
```bash
ls cv_test/
cv_0/  cv_1/  cv_2/  cv_3/  cv_4/
cv_5/  cv_6/  cv_7/  cv_8/  cv_9/
```
with the test models stored inside each `cv_{ii}/models/` directory
```bash
ls cv_test/cv_0/models/
test_dim_1_model_0.dat  test_dim_2_model_0.dat
test_dim_3_model_0.dat  test_dim_4_model_0.dat
train_dim_1_model_0.dat  train_dim_2_model_0.dat
train_dim_3_model_0.dat  train_dim_4_model_0.dat
```
