# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from sissopp import (
    initialize_param_storage,
    initialize_values_arr,
    get_reg_optimizer,
    get_log_reg_optimizer,
    get_class_optimizer,
    FeatureNode,
    SqParamNode,
    Unit,
)
from pathlib import Path

import numpy as np

np.random.seed(0)

parent = Path(__file__).parent


def check_feat_parmeters(feat, prop):
    r_sq = linregress(feat.value, prop)[2] ** 2.0
    return r_sq ** 2.0 > 0.99


def test_nl_opt_utils():
    task_sizes_train = [1000]
    task_sizes_test = [0]

    initialize_values_arr(task_sizes_train, task_sizes_test, 1, 1)
    initialize_param_storage()

    value = np.linspace(-100.0, 100.0, task_sizes_train[0])

    test_value = np.zeros(0)

    prop = np.power(value + 1.50, 2.0)
    prop_log_reg = np.log(prop)
    prop_class = np.zeros(1000)
    prop_class[250:750] = 1.0

    feat = FeatureNode(0, "a", value, test_value, Unit())

    optimizer = get_reg_optimizer(task_sizes_train, prop, 1, -1, 0.5, True)
    test_val = optimizer.optimize_feature_params(SqParamNode(feat, 1, 1e-50, 1e50))
    assert abs(test_val) < 1e-5

    optimizer = get_reg_optimizer(task_sizes_train, list(prop), 1, 1, 0.5, True)
    test_val = optimizer.optimize_feature_params(SqParamNode(feat, 1, 1e-50, 1e50))
    assert abs(test_val) < 1e-5

    optimizer = get_reg_optimizer(
        np.array(task_sizes_train, dtype=np.int32), prop, 1, 1, 0.5, True
    )
    test_val = optimizer.optimize_feature_params(SqParamNode(feat, 1, 1e-50, 1e50))
    assert abs(test_val) < 1e-5

    optimizer = get_reg_optimizer(
        np.array(task_sizes_train, dtype=np.int32), list(prop), 1, 1, 0.5, True
    )
    test_val = optimizer.optimize_feature_params(SqParamNode(feat, 1, 1e-50, 1e50))
    assert abs(test_val) < 1e-5

    optimizer = get_log_reg_optimizer(task_sizes_train, prop_log_reg, 1, 1, 0.5, True)
    feat_node = SqParamNode(feat, 1, 1e-50, 1e50)
    test_val = optimizer.optimize_feature_params(feat_node)
    assert abs(test_val) < 1e-5

    optimizer = get_log_reg_optimizer(
        task_sizes_train, list(prop_log_reg), 1, 1, 0.5, True
    )
    test_val = optimizer.optimize_feature_params(SqParamNode(feat, 1, 1e-50, 1e50))
    assert abs(test_val) < 1e-5

    optimizer = get_log_reg_optimizer(
        np.array(task_sizes_train, dtype=np.int32), prop_log_reg, 1, 1, 0.5, True
    )
    test_val = optimizer.optimize_feature_params(SqParamNode(feat, 1, 1e-50, 1e50))
    assert abs(test_val) < 1e-5

    optimizer = get_log_reg_optimizer(
        np.array(task_sizes_train, dtype=np.int32), list(prop_log_reg), 1, 1, 0.5, True
    )
    test_val = optimizer.optimize_feature_params(SqParamNode(feat, 1, 1e-50, 1e50))
    assert abs(test_val) < 1e-5

    value[:250] = np.linspace(-50.0, -20.0, 250)
    value[250:750] = np.linspace(-10.0, -5.0, 500)
    value[750:] = np.linspace(5.0, 50.0, 250)
    feat = FeatureNode(0, "a", value, test_value, Unit())

    optimizer = get_class_optimizer(task_sizes_train, prop_class, 1)
    test_val = optimizer.optimize_feature_params(SqParamNode(feat, 1, 1e-50, 1e50))
    assert np.floor(np.abs((test_val))) < 1e-5

    optimizer = get_class_optimizer(task_sizes_train, list(prop_class), 1)
    test_val = optimizer.optimize_feature_params(SqParamNode(feat, 1, 1e-50, 1e50))
    assert np.floor(np.abs((test_val))) < 1e-5

    optimizer = get_class_optimizer(
        np.array(task_sizes_train, dtype=np.int32), prop_class, 1
    )
    test_val = optimizer.optimize_feature_params(SqParamNode(feat, 1, 1e-50, 1e50))
    assert np.floor(np.abs((test_val))) < 1e-5

    optimizer = get_class_optimizer(
        np.array(task_sizes_train, dtype=np.int32), list(prop_class), 1
    )
    test_val = optimizer.optimize_feature_params(SqParamNode(feat, 1, 1e-50, 1e50))
    assert np.floor(np.abs((test_val))) < 1e-5


if __name__ == "__main__":
    test_nl_opt_utils()
