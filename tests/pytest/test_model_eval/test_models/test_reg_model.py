# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from sissopp import ModelRegressor
from pathlib import Path

import numpy as np
import shutil

np.random.seed(0)


class InvalidFeatureMade(Exception):
    pass


parent = Path(__file__).parent


def test_model_reg_eval():
    model = ModelRegressor(
        str(parent / "model_files/reg_train.dat"),
        str(parent / "model_files/reg_test.dat"),
    )

    data = np.round(np.random.uniform(1.0, 100.0, size=(90, 4)), 3)

    eval_dat = (
        model.coefs[0][-1]
        + model.coefs[0][0] * data[:, 1] / data[:, 0] ** 2.0
        + model.coefs[0][1] * data[:, 2]
    )

    data_dict = {"A": data[0, 0], "B": data[0, 1], "C": data[0, 2]}
    assert abs(model.eval(data_dict) - eval_dat[0]) < 1e-5
    assert abs(model.eval(data[0].flatten().tolist()) - eval_dat[0]) < 1e-5
    assert abs(model.eval(data[0, :].flatten()) - eval_dat[0]) < 1e-5

    data_dict = {"A": data[:, 0], "B": data[:, 1], "C": data[:, 2]}
    assert np.linalg.norm(model.eval_many(data_dict) - eval_dat) < 1e-5
    assert np.linalg.norm(model.eval_many(data) - eval_dat) < 1e-5

    eval_dat = (
        model.coefs[1][-1]
        + model.coefs[1][0] * data[:, 1] / data[:, 0] ** 2.0
        + model.coefs[1][1] * data[:, 2]
    )

    assert abs(model.eval(data[0, :].flatten().tolist(), "Y") - eval_dat[0]) < 1e-5
    assert abs(model.eval(data[0, :].flatten(), "Y") - eval_dat[0]) < 1e-5

    assert np.linalg.norm(model.eval_many(data, ["Y"] * 90) - eval_dat) < 1e-5
    assert np.linalg.norm(model.eval_many(data_dict, ["Y"] * 90) - eval_dat) < 1e-5

    model.task_eval = 1
    data_dict = {"A": data[0, 0], "B": data[0, 1], "C": data[0, 2]}
    assert abs(model.eval(data_dict) - eval_dat[0]) < 1e-5
    assert abs(model.eval(data_dict, "Y") - eval_dat[0]) < 1e-5
    assert abs(model.eval(data[0, :].flatten().tolist()) - eval_dat[0]) < 1e-5
    assert abs(model.eval(data[0, :].flatten()) - eval_dat[0]) < 1e-5

    data_dict = {"A": data[:, 0], "B": data[:, 1], "C": data[:, 2]}
    assert np.linalg.norm(model.eval_many(data_dict) - eval_dat) < 1e-5
    assert np.linalg.norm(model.eval_many(data) - eval_dat) < 1e-5

    task_ids = ["X"] * 20 + ["Y"] * 50 + ["X"] * 20
    eval_dat[:20] = (
        model.coefs[0][-1]
        + model.coefs[0][0] * data[:20, 1] / data[:20, 0] ** 2.0
        + model.coefs[0][1] * data[:20, 2]
        + np.random.normal(0, 1e-12, size=(20))
    )
    eval_dat[20:70] = (
        model.coefs[1][-1]
        + model.coefs[1][0] * data[20:70, 1] / data[20:70, 0] ** 2.0
        + model.coefs[1][1] * data[20:70, 2]
        + np.random.normal(0, 1e-12, size=(50))
    )
    eval_dat[70:] = (
        model.coefs[0][-1]
        + model.coefs[0][0] * data[70:, 1] / data[70:, 0] ** 2.0
        + model.coefs[0][1] * data[70:, 2]
        + np.random.normal(0, 1e-12, size=(20))
    )

    model.prediction_to_file(
        "model_predict/reg.dat",
        eval_dat,
        data,
        np.arange(1000, 1090).astype(str),
        np.array(task_ids),
        np.arange(90).astype(np.int32),
    )
    model_predict = ModelRegressor(
        str(parent / "model_files/reg_train.dat"),
        str("model_predict/reg.dat"),
    )

    assert model_predict.test_rmse < 1e-5

    model.prediction_to_file(
        "model_predict/reg_dict.dat",
        eval_dat,
        data_dict,
        np.arange(1000, 1090).astype(str),
        np.array(task_ids, dtype=str),
        np.arange(90).astype(np.int32),
    )
    model_predict = ModelRegressor(
        str(parent / "model_files/reg_train.dat"),
        str("model_predict/reg_dict.dat"),
    )

    assert model_predict.test_rmse < 1e-5

    shutil.rmtree("model_predict/")

    try:
        data_dict = {"B": data[0, 1], "C": data[0, 2]}
        assert abs(model.eval(data_dict) - eval_dat[0]) < 1e-5
        raise ValueError("Evaluation passed with variable not inside data_dict")
    except RuntimeError:
        pass

    try:
        data_dict = {"B": data[:, 1], "C": data[:, 2]}
        assert abs(model.eval_many(data_dict)[0] - eval_dat[0]) < 1e-5
        raise ValueError("Evaluation passed with variable not inside data_dict")
    except RuntimeError:
        pass

    try:
        data_dict = {"B": data[1:, 1], "C": data[:, 2]}
        assert abs(model.eval_many(data_dict)[0] - eval_dat[0]) < 1e-5
        raise ValueError(
            "Evaluation passed with data arraies being different sizes data_dict"
        )
    except RuntimeError:
        pass

    try:
        assert abs(model.eval_many(data[:, 0].flatten())[0] - eval_dat[0]) < 1e-5
        raise ValueError("Evaluation passed with a 1D array")
    except RuntimeError:
        pass


if __name__ == "__main__":
    test_model_reg_eval()
