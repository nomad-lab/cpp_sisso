# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from sissopp import (
    FeatureNode,
    AddNode,
    SubNode,
    DivNode,
    MultNode,
    AbsDiffNode,
    AbsNode,
    InvNode,
    SinNode,
    CosNode,
    ExpNode,
    LogNode,
    NegExpNode,
    SixPowNode,
    CbNode,
    SqNode,
    SqrtNode,
    CbrtNode,
    ModelNode,
    Unit,
    initialize_values_arr,
)

import numpy as np

node_list = [
    ("add", AddNode, 2, -1e2, 1e2),
    ("sub", SubNode, 2, -1e2, 1e2),
    ("div", DivNode, 2, -1e2, 1e2),
    ("mult", MultNode, 2, -1e2, 1e2),
    ("abs_diff", AbsDiffNode, 2, -1e2, 1e2),
    ("abs", AbsNode, 1, -1e2, 1e2),
    ("inv", InvNode, 1, -1e2, 1e2),
    ("sin", SinNode, 1, -1e2, 1e2),
    ("cos", CosNode, 1, -1e2, 1e2),
    ("exp", ExpNode, 1, -1e0, 1e0),
    ("log", LogNode, 1, 1e-2, 1e2),
    ("neg_exp", NegExpNode, 1, -1e0, 1e0),
    ("six_pow", SixPowNode, 1, -1e1, 1e1),
    ("cb", CbNode, 1, -1e2, 1e2),
    ("sq", SqNode, 1, -1e2, 1e2),
    ("sqrt", SqrtNode, 1, 1e-2, 1e2),
    ("cbrt", CbrtNode, 1, 1e-2, 1e2),
]


class InvalidFeatureMade(Exception):
    pass


def test_op_model_nodes_eval():
    task_sizes_train = [90]
    task_sizes_test = [10]
    initialize_values_arr(task_sizes_train, task_sizes_test, 2, 1)

    failed_calcultions = []

    for nn, node_type in enumerate(node_list):
        n_prim_feat = node_type[2]
        data = np.random.uniform(
            node_type[3], node_type[4], size=(task_sizes_train[0], n_prim_feat)
        )
        test_data = np.random.uniform(
            node_type[3], node_type[4], size=(task_sizes_test[0], n_prim_feat)
        )

        prim_feats = []
        for dd in range(n_prim_feat):
            prim_feats.append(
                FeatureNode(dd, f"t_{dd}", data[:, dd], test_data[:, dd], Unit())
            )

        if n_prim_feat == 1:
            node = node_type[1](prim_feats[0], nn + 2, 1e-50, 1e50)
        elif n_prim_feat == 2:
            node = node_type[1](prim_feats[0], prim_feats[1], nn + 2, 1e-50, 1e50)

        model_node = ModelNode(node)
        data_dict = {feat.expr: data[0, feat.arr_ind] for feat in prim_feats}

        assert abs(model_node.eval(data_dict) - model_node.value[0]) < 1e-5
        assert (
            abs(
                model_node.eval(data[0, :n_prim_feat].flatten().tolist())
                - model_node.value[0]
            )
            < 1e-5
        )
        assert (
            abs(model_node.eval(data[0, :n_prim_feat].flatten()) - model_node.value[0])
            < 1e-5
        )

        data_dict = {feat.expr: data[:, feat.arr_ind] for feat in prim_feats}
        assert np.linalg.norm(model_node.eval_many(data_dict) - model_node.value) < 1e-5
        assert np.linalg.norm(model_node.eval_many(data) - model_node.value) < 1e-5


if __name__ == "__main__":
    test_op_model_nodes_eval()
