# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from sissopp import (
    FeatureNode,
    AddParamNode,
    MultParamNode,
    ModelNode,
    Unit,
    initialize_values_arr,
    initialize_param_storage,
    set_max_param_depth,
)

import numpy as np

np.random.seed(0)


class InvalidFeatureMade(Exception):
    pass


def test_bin_bin_model_eval():
    task_sizes_train = [90]
    task_sizes_test = [10]
    initialize_values_arr(task_sizes_train, task_sizes_test, 3, 2)
    initialize_param_storage()
    set_max_param_depth(2)

    data = np.random.uniform(-10.0, 10.0, size=(task_sizes_train[0], 3))
    test_data = np.random.uniform(-10.0, 10.0, size=(task_sizes_test[0], 3))

    feat_1 = FeatureNode(0, "t_a", data[:, 0], test_data[:, 0], Unit())
    feat_2 = FeatureNode(1, "x_a", data[:, 1], test_data[:, 1], Unit())
    feat_3 = FeatureNode(2, "m_a", data[:, 2], test_data[:, 2], Unit())

    node_1 = MultParamNode(feat_1, feat_2, 3, 1e-50, 1e50)
    node_1.set_parameters(np.array([2.0, 3.0]))

    node_2 = AddParamNode(node_1, feat_3, 4, 1e-50, 1e50)
    node_2.set_parameters(np.array([1.0, 2.0, 3.0, 4.0]))
    model_node = ModelNode(node_2)

    data_dict = {
        "t_a": data[0, 0],
        "x_a": data[0, 1],
        "m_a": data[0, 2],
    }

    assert abs(model_node.eval(data_dict) - model_node.value[0]) < 1e-5
    assert (
        abs(model_node.eval(data[0, :].flatten().tolist()) - model_node.value[0]) < 1e-5
    )
    assert abs(model_node.eval(data[0, :].flatten()) - model_node.value[0]) < 1e-5

    data_dict = {
        "t_a": data[:, 0],
        "x_a": data[:, 1],
        "m_a": data[:, 2],
    }
    assert np.linalg.norm(model_node.eval_many(data_dict) - model_node.value) < 1e-5
    assert np.linalg.norm(model_node.eval_many(data) - model_node.value) < 1e-5


if __name__ == "__main__":
    test_bin_bin_model_eval()
