function P = model_regressor(X)
% Returns the value of Prop = c0 + a0 * ((B / A) / A) + a1 * ((D + C) + (|D|))
%
% X = [
%     B,
%     A,
%     D,
%     C,
% ]

if(size(X, 2) ~= 4)
    error("ERROR: X must have a size of 4 in the second dimension.")
end
B = reshape(X(:, 1), 1, []);
A = reshape(X(:, 2), 1, []);
D = reshape(X(:, 3), 1, []);
C = reshape(X(:, 4), 1, []);

f0 = ((B ./ A) ./ A);
f1 = ((D + C) + abs(D));

c0 = 3.1415899996e+00;
a0 = -7.2154784853e+00;
a1 = 8.2271800000e+01;

P = reshape(c0 + a0 * f0 + a1 * f1, [], 1);
end
