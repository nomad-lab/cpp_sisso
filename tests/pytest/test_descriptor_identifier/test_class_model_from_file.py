# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from sissopp.postprocess.load_models import load_model
from pathlib import Path

import numpy as np
import pickle

np.random.seed(0)


class InvalidFeatureMade(Exception):
    pass


parent = Path(__file__).parent


def test_class_model_from_file():
    try:
        model = load_model(
            str(parent / "model_files/train_classifier_fail_overlap.dat"),
            str(parent / "model_files/test_classifier.dat"),
        )
        raise ValueError("Model created that should fail")
    except RuntimeError:
        pass

    try:
        model = load_model(
            str(parent / "model_files/train_classifier.dat"),
            str(parent / "model_files/test_classifier_fail_overlap.dat"),
        )
        raise ValueError("Model created that should fail")
    except RuntimeError:
        pass

    model = load_model(
        str(parent / "model_files/train_classifier.dat"),
        str(parent / "model_files/test_classifier.dat"),
    )

    mat_fxn_fn = "test_matlab_fxn/model_classifier"
    mat_fxn_fn_real = str(parent / "matlab_functions" / "model_classifier.m")

    model.write_matlab_fxn(mat_fxn_fn)
    actual_lines = open(mat_fxn_fn_real).readlines()
    test_lines = open(mat_fxn_fn + ".m").readlines()

    Path(mat_fxn_fn + ".m").unlink()
    Path("test_matlab_fxn").rmdir()
    for tl, al in zip(test_lines, actual_lines):
        assert tl == al

    assert np.all(np.abs(model.fit - model.prop_train) < 1e-7)
    assert np.all(np.abs(model.predict - model.prop_test) < 1e-7)

    assert np.sum(model.train_error) + 80 < 1e-7
    assert np.sum(model.test_error) + 20 < 1e-7

    assert model.task_sizes_train == [80]
    assert model.task_sizes_test == [20]
    assert model.leave_out_inds == list(range(20))

    assert model.feats[0].postfix_expr == "9|8|sub"
    assert model.feats[1].postfix_expr == "1|0|mult"

    actual_coefs = [
        [1.326205649731981, -1.744239999671528, 0.9075950727790907],
    ]

    assert np.all(
        [
            abs(coef - actual) < 1e-8
            for coef, actual in zip(model.coefs[0], actual_coefs[0])
        ]
    )
    assert (
        model.latex_str
        == "[$\\left(feat_{9} - feat_{8}\\right)$, $\\left(feat_{1} feat_{0}\\right)$]"
    )

    # Check Pickeling
    pickled = pickle.dumps(model)
    model_unpick = pickle.loads(pickled)

    assert np.allclose(model.prop_train, model_unpick.prop_train)
    assert np.allclose(model.prop_test, model_unpick.prop_test)
    assert np.allclose(model.fit, model_unpick.fit)
    assert np.allclose(model.predict, model_unpick.predict)
    assert np.all(model.task_sizes_train == model_unpick.task_sizes_train)
    assert np.all(model.task_sizes_test == model_unpick.task_sizes_test)
    assert np.all(model.leave_out_inds == model_unpick.leave_out_inds)
    assert np.all(model.sample_ids_train == model_unpick.sample_ids_train)
    assert np.all(model.sample_ids_test == model_unpick.sample_ids_test)
    assert np.all(model.task_names == model_unpick.task_names)
    assert np.all(
        [
            np.allclose(f1.value, f2.value)
            for f1, f2 in zip(model.feats, model_unpick.feats)
        ]
    )
    assert model.fix_intercept == model_unpick.fix_intercept
    assert model.prop_label == model_unpick.prop_label
    assert model.prop_unit == model_unpick.prop_unit


if __name__ == "__main__":
    test_class_model_from_file()
