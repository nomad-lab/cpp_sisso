# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import shutil
import numpy as np

np.random.seed(0)

from sissopp import (
    FeatureNode,
    FeatureSpace,
    Inputs,
    Unit,
    initialize_values_arr,
    finalize_values_arr,
)

import pathlib
import pickle

parent = pathlib.Path(__file__).parent.absolute()


def test_feature_space():
    task_sizes_train = [90]
    task_sizes_test = [10]

    initialize_values_arr(task_sizes_train, task_sizes_test, 10, 2)

    inputs = Inputs()
    inputs.phi_0 = [
        FeatureNode(
            ff,
            f"feat_{ff}",
            np.random.random(task_sizes_train[0]) * 1e2 - 50,
            np.random.random(task_sizes_test[0]) * 1e2 - 50,
            Unit("s"),
        )
        for ff in range(10)
    ]

    inputs.prop_train = np.power(inputs.phi_0[0].value + inputs.phi_0[1].value, 2.0)

    inputs.allowed_ops = ["add", "sub", "mult", "sq", "cb", "sqrt", "cbrt"]
    inputs.task_sizes_train = task_sizes_train
    inputs.task_sizes_test = task_sizes_test
    inputs.calc_type = "regression"
    inputs.max_rung = 2
    inputs.n_sis_select = 10
    inputs.phi_out_file = str(parent / "feature_space" / "phi.txt")

    try:
        inputs.n_rung_generate = 2
        feat_space = FeatureSpace(inputs)
        raise ValueError("FeatureSpace created with invalid parameters")
    except RuntimeError:
        inputs.n_rung_generate = 0
        pass

    try:
        inputs.n_rung_generate = 1
        inputs.n_rung_store = 2
        feat_space = FeatureSpace(inputs)
        raise ValueError("FeatureSpace created with invalid parameters")
    except RuntimeError:
        inputs.n_rung_generate = 0
        inputs.n_rung_store = 1
        pass

    try:
        inputs.allowed_ops = ["exp"]
        feat_space = FeatureSpace(inputs)
        raise ValueError(
            "FeatureSpace created when there is a rung with no features created"
        )
    except RuntimeError:
        inputs.allowed_ops = ["add", "sub", "mult", "sq", "cb", "sqrt", "cbrt"]
        pass

    feat_space = FeatureSpace(inputs)
    assert feat_space.phi0[0].expr == inputs.phi_0[0].expr

    feat_space.sis(inputs.prop_train)

    assert feat_space.phi_selected[0].postfix_expr == "1|0|add|sq"
    assert feat_space.phi_selected[1].d_mat_ind == 1
    assert len(feat_space.phi_selected) == 10

    feat_space.sis(list(inputs.prop_train))
    assert len(feat_space.phi_selected) == 20
    assert (
        np.abs(
            np.corrcoef(inputs.prop_train, inputs.phi_0[0].value)[0, 1] ** 2.0
            + feat_space.scores[0]
        )
        < 1e-10
    )
    assert feat_space.task_sizes_train[0] == 90
    assert feat_space.allowed_param_ops == []
    assert feat_space.allowed_ops == ["add", "sub", "mult", "sq", "cb", "sqrt", "cbrt"]
    assert feat_space.start_rung[0] == 0
    assert feat_space.start_rung[1] == 10
    assert feat_space.get_feature(0).expr == "feat_0"

    try:
        feat_space.remove_feature(feat_space.phi_selected[0].feat_ind)
        raise ValueError("Removed selected feature.")
    except RuntimeError:
        pass

    test_expr = feat_space.get_feature(len(feat_space.phi) - 2).expr
    feat_space.remove_feature(len(feat_space.phi) - 2)
    assert feat_space.get_feature(len(feat_space.phi) - 2).expr != test_expr

    if feat_space.parameterized_feats_allowed:
        try:
            inputs.max_param_depth = 10
            feat_space = FeatureSpace(inputs)
            raise ValueError("FeatureSpace created with invalid parameters")
        except RuntimeError:
            inputs.max_param_depth = 0
            pass

    feat_space.output_phi()
    feat_space_2 = FeatureSpace(
        f"{parent}/feature_space/phi.txt",
        inputs.phi_0,
        inputs.prop_train,
        inputs.task_sizes_train,
        inputs.calc_type,
        inputs.n_sis_select,
        inputs.cross_cor_max,
    )
    assert feat_space.n_feat == feat_space_2.n_feat

    feat_space_2.sis(inputs.prop_train)
    assert feat_space.phi_selected[0].expr == feat_space_2.phi_selected[0].expr

    shutil.rmtree(f"{parent}/feature_space/")
    shutil.rmtree("feature_space/")

    # Check Pickeling
    # Test that if an empty centeral storage area is passed to FeatureSpace it will reset it
    pickled = pickle.dumps(feat_space)
    finalize_values_arr()
    feat_space_unpick = pickle.loads(pickled)

    assert np.all(
        [f1.expr == f2.expr for f1, f2 in zip(feat_space.phi, feat_space_unpick.phi)]
    )

    assert np.all(
        [f1.expr == f2.expr for f1, f2 in zip(feat_space.phi0, feat_space_unpick.phi0)]
    )

    assert np.all(
        [
            f1.expr == f2.expr
            for f1, f2 in zip(feat_space.phi_selected, feat_space_unpick.phi_selected)
        ]
    )

    assert np.all(
        [
            op1 == op2
            for op1, op2 in zip(feat_space.allowed_ops, feat_space_unpick.allowed_ops)
        ]
    )

    assert np.all(
        [
            op1 == op2
            for op1, op2 in zip(
                feat_space.allowed_param_ops, feat_space_unpick.allowed_param_ops
            )
        ]
    )

    assert np.all(feat_space.prop == feat_space_unpick.prop)
    assert np.all(feat_space.scores == feat_space_unpick.scores)
    assert np.all(feat_space.task_sizes_train == feat_space_unpick.task_sizes_train)
    assert np.all(feat_space.start_rung == feat_space_unpick.start_rung)
    assert feat_space.feature_space_file == feat_space_unpick.feature_space_file
    assert (
        feat_space.feature_space_summary_file
        == feat_space_unpick.feature_space_summary_file
    )
    assert feat_space.phi_out_file == feat_space_unpick.phi_out_file
    assert feat_space.cross_cor_max == feat_space_unpick.cross_cor_max
    assert feat_space.l_bound == feat_space_unpick.l_bound
    assert feat_space.u_bound == feat_space_unpick.u_bound
    assert feat_space.n_rung_store == feat_space_unpick.n_rung_store
    assert feat_space.n_rung_generate == feat_space_unpick.n_rung_generate
    assert feat_space.max_rung == feat_space_unpick.max_rung
    assert feat_space.n_sis_select == feat_space_unpick.n_sis_select


if __name__ == "__main__":
    test_feature_space()
