# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from sissopp import (
    FeatureNode,
    AddParamNode,
    Unit,
    initialize_values_arr,
    initialize_param_storage,
    get_reg_optimizer,
)

import numpy as np
import pickle
from scipy.stats import linregress

np.random.seed(0)


class InvalidFeatureMade(Exception):
    pass


def check_feat_parmeters(feat, prop):
    return linregress(feat.value, prop)[2] ** 2.0 > 0.99


def test_param_add_node():
    task_sizes_train = [90]
    task_sizes_test = [10]

    initialize_values_arr(
        np.array(task_sizes_train, dtype=np.int32),
        np.array(task_sizes_test, dtype=np.int32),
        2,
        1,
    )
    initialize_param_storage()

    data_1 = np.linspace(-20, 20, task_sizes_train[0])
    test_data_1 = np.linspace(-19.99, 19.99, task_sizes_test[0])
    feat_1 = FeatureNode(0, "t_a", data_1, test_data_1, Unit())

    data_2 = np.linspace(-14.256, 18.6523, task_sizes_train[0])
    test_data_2 = np.linspace(-16.256, 17.6523, task_sizes_test[0])
    feat_2 = FeatureNode(1, "x_a", data_2, test_data_2, Unit())

    prop = -2.3 * (data_1 + 1.5 * data_2) - 1.2
    optimizer = get_reg_optimizer([task_sizes_train[0]], prop, 1, 1, 0.5, False)

    feat_node = AddParamNode(feat_1, feat_2, 2, 1e-50, 1e50)
    feat_node.get_parameters(optimizer)

    assert check_feat_parmeters(feat_node, prop)

    p_0, p_1 = feat_node.parameters
    assert feat_node.matlab_fxn_expr == f"(t_a + ({p_0:.6e}.*x_a))"

    # Check pickling
    feat_node.selected = True
    feat_node.d_mat_ind = 1
    pickled = pickle.dumps(feat_node)

    feat_unpick = pickle.loads(pickled)
    assert feat_unpick.d_mat_ind == feat_node.d_mat_ind
    assert feat_unpick.selected == feat_node.selected

    feat_node.selected = False
    feat_unpick.selected = False
    assert np.all(feat_unpick.value == feat_node.value)
    assert np.all(feat_unpick.test_value == feat_node.test_value)
    assert np.all(feat_unpick.parameters == feat_node.parameters)

    assert feat_unpick.expr == feat_node.expr
    assert feat_unpick.unit == feat_node.unit
    assert feat_unpick.arr_ind == feat_node.arr_ind
    assert feat_unpick.feat_ind == feat_node.feat_ind


if __name__ == "__main__":
    test_param_add_node()
