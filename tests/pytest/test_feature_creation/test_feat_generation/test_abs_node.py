# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from sissopp import FeatureNode, AbsNode, AbsDiffNode, Unit, initialize_values_arr

import numpy as np
import pickle

np.random.seed(0)


class InvalidFeatureMade(Exception):
    pass


def test_abs_node():
    task_sizes_train = [90]
    task_sizes_test = [10]

    initialize_values_arr(task_sizes_train, task_sizes_test, 3, 2)
    data_1 = np.random.random(task_sizes_train[0]) * 1e4 + 1e-10
    test_data_1 = np.random.random(task_sizes_test[0]) * 1e4 + 1e-10

    data_2 = np.random.choice([1.0, -1.0], task_sizes_train[0])
    test_data_2 = np.random.choice([1.0, -1.0], task_sizes_test[0])

    data_3 = np.random.random(task_sizes_train[0]) * 2e4 - 1e4
    test_data_3 = np.random.random(task_sizes_test[0]) * 2e4 - 1e4

    feat_1 = FeatureNode(0, "t_a", data_1, test_data_1, Unit())
    feat_2 = FeatureNode(1, "x_a", data_2, test_data_2, Unit())
    feat_3 = FeatureNode(2, "v_a", data_3, test_data_3, Unit())

    feats = []

    try:
        feats.append(AbsNode(feat_2, 3, 1e-50, 1e50))
        raise InvalidFeatureMade(
            "Taking the absolute value leading to a constant feature"
        )
    except RuntimeError:
        pass

    try:
        feats.append(AbsNode(feat_1, 3, 1e-50, 1e-1))
        raise InvalidFeatureMade(
            "Taking the absolute value outside of user specified bounds"
        )
    except RuntimeError:
        pass

    try:
        feats.append(AbsNode(feat_1, 3, 1e-50, 1e50))
        raise InvalidFeatureMade(
            "Making an absolute value of a feature with strictly positive values"
        )
    except RuntimeError:
        pass

    feats.append(AbsDiffNode(feat_2, feat_1, 4, 1e-50, 1e50))
    feats.append(AbsNode(feat_3, 6, 1e-50, 1e50))

    try:
        feats.append(AbsNode(feats[1], 5, 1e-50, 1e50))
        raise InvalidFeatureMade("Taking the absolute value of an absolute value")
    except RuntimeError:
        pass

    try:
        feats.append(AbsNode(feats[0], 5, 1e-50, 1e50))
        raise InvalidFeatureMade("Taking the absolute value of an absolute difference")
    except RuntimeError:
        pass

    decomp = feats[-1].primary_feat_decomp
    assert len(decomp.keys()) == 1
    assert decomp["v_a"] == 1

    assert feats[-1].n_leaves == 1

    assert feats[-1].n_feats == 1
    assert feats[-1].expr == "(|v_a|)"

    try:
        feats[-1].feat(2)
        raise ValueError("Accessing feature that should throw an error")
    except:
        pass
    assert feats[-1].matlab_fxn_expr == "abs(v_a)"

    # Check pickling
    feats[-1].selected = True
    feats[-1].d_mat_ind = 1
    pickled = pickle.dumps(feats[-1])

    feat_unpick = pickle.loads(pickled)
    assert feat_unpick.d_mat_ind == feats[-1].d_mat_ind
    assert feat_unpick.selected == feats[-1].selected

    feats[-1].selected = False
    feat_unpick.selected = False
    assert np.all(feat_unpick.value == feats[-1].value)
    assert np.all(feat_unpick.test_value == feats[-1].test_value)
    assert feat_unpick.expr == feats[-1].expr
    assert feat_unpick.unit == feats[-1].unit
    assert feat_unpick.arr_ind == feats[-1].arr_ind
    assert feat_unpick.feat_ind == feats[-1].feat_ind


if __name__ == "__main__":
    test_abs_node()
