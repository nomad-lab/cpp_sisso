# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from sissopp import (
    FeatureNode,
    ExpNode,
    NegExpNode,
    LogNode,
    AddNode,
    SubNode,
    Unit,
    initialize_values_arr,
)

import numpy as np
import pickle

np.random.seed(0)


class InvalidFeatureMade(Exception):
    pass


def test_exp_node():
    task_sizes_train = [90]
    task_sizes_test = [10]
    initialize_values_arr(task_sizes_train, task_sizes_test, 3, 2)

    data_1 = np.random.random(task_sizes_train[0]) + 1e-10
    test_data_1 = np.random.random(task_sizes_test[0]) + 1e-10

    data_2 = np.random.random(task_sizes_train[0]) * 2 - 1
    test_data_2 = np.random.random(task_sizes_test[0]) * 2 - 1

    data_3 = np.random.random(task_sizes_train[0]) * 10.0 + 1e-10
    test_data_3 = np.random.random(task_sizes_test[0]) * 10.0 + 1e-10

    feat_1 = FeatureNode(0, "t_a", data_1, test_data_1, Unit())
    feat_2 = FeatureNode(1, "x_a", data_2, test_data_2, Unit())
    feat_3 = FeatureNode(2, "t_b", data_3, test_data_3, Unit("m"))

    feats = []
    try:
        feats.append(ExpNode(feat_1, 3, 1e-50, 1e-10))
        raise InvalidFeatureMade("Exponentiating outside of user specified bounds")
    except RuntimeError:
        pass

    try:
        feats.append(ExpNode(feat_1, 3, 1e1, 1e50))
        raise InvalidFeatureMade("Exponentiating outside of user specified bounds")
    except RuntimeError:
        pass

    try:
        feats.append(ExpNode(feat_3, 3, 1e-50, 1e50))
        raise InvalidFeatureMade("Exponentiating a united quantity")
    except RuntimeError:
        pass

    feats.append(ExpNode(feat_2, 3, 1e-50, 1e50))
    feats.append(NegExpNode(feat_2, 4, 1e-50, 1e50))
    feats.append(LogNode(feat_1, 5, 1e-50, 1e50))
    feats.append(AddNode(feat_1, feat_2, 6, 1e-50, 1e50))
    feats.append(SubNode(feat_1, feat_2, 7, 1e-50, 1e50))

    try:
        feats.append(ExpNode(feats[0], 8, 1e-50, 1e50))
        raise InvalidFeatureMade("Exponentiating an ExpNode")
    except RuntimeError:
        pass

    try:
        feats.append(ExpNode(feats[1], 8, 1e-50, 1e50))
        raise InvalidFeatureMade("Exponentiating a NegExpNode")
    except RuntimeError:
        pass

    try:
        feats.append(ExpNode(feats[2], 8, 1e-50, 1e50))
        raise InvalidFeatureMade("Exponentiating a LogNode")
    except RuntimeError:
        pass

    try:
        feats.append(ExpNode(feats[3], 8, 1e-50, 1e50))
        raise InvalidFeatureMade("Exponentiating an AddNode")
    except RuntimeError:
        pass

    try:
        feats.append(ExpNode(feats[4], 8, 1e-50, 1e50))
        raise InvalidFeatureMade("Exponentiating a SubNode")
    except RuntimeError:
        pass

    decomp = feats[0].primary_feat_decomp
    assert len(decomp.keys()) == 1
    assert decomp["x_a"] == 1

    assert feats[0].n_leaves == 1
    assert feats[0].n_feats == 1
    assert feats[0].feat(0).expr == "x_a"
    try:
        feats[0].feat(2)
        raise ValueError("Accessing feature that should throw an error")
    except:
        pass
    assert feats[0].matlab_fxn_expr == "exp(x_a)"

    # Check pickling
    feats[-1].selected = True
    feats[-1].d_mat_ind = 1
    pickled = pickle.dumps(feats[-1])

    feat_unpick = pickle.loads(pickled)
    assert feat_unpick.d_mat_ind == feats[-1].d_mat_ind
    assert feat_unpick.selected == feats[-1].selected

    feats[-1].selected = False
    feat_unpick.selected = False
    assert np.all(feat_unpick.value == feats[-1].value)
    assert np.all(feat_unpick.test_value == feats[-1].test_value)
    assert feat_unpick.expr == feats[-1].expr
    assert feat_unpick.unit == feats[-1].unit
    assert feat_unpick.arr_ind == feats[-1].arr_ind
    assert feat_unpick.feat_ind == feats[-1].feat_ind


if __name__ == "__main__":
    test_exp_node()
