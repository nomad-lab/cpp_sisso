# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from sissopp import (
    FeatureNode,
    ModelNode,
    AddNode,
    Unit,
    initialize_values_arr,
)

import numpy as np
import pickle

np.random.seed(0)


class InvalidFeatureMade(Exception):
    pass


def test_model_node():
    task_sizes_train = [10]
    task_sizes_test = [10]

    initialize_values_arr(task_sizes_train, task_sizes_test, 1, 1)

    data_1 = np.random.random(task_sizes_train[0]) * 1e10 + 1e-10
    test_data_1 = np.random.random(task_sizes_test[0]) * 1e10 + 1e-10

    data_2 = np.random.random(task_sizes_train[0]) * 1e10 + 1e-10
    test_data_2 = np.random.random(task_sizes_test[0]) * 1e10 + 1e-10

    feat_1 = FeatureNode(0, "t_a", data_1, test_data_1, Unit("s"))
    feat_2 = FeatureNode(1, "t_b", data_2, test_data_2, Unit("s"))
    feat_3 = AddNode(feat_1, feat_2, 2, 1e-50, 1e50)
    model_node = ModelNode(feat_3)

    model_node_list = ModelNode(
        model_node.feat_ind,
        model_node.rung,
        model_node.expr,
        model_node.latex_expr,
        model_node.postfix_expr,
        model_node.matlab_fxn_expr,
        list(model_node.value),
        list(model_node.test_value),
        model_node.x_in_expr_list,
        model_node.unit,
    )
    model_node_arr = ModelNode(
        model_node.feat_ind,
        model_node.rung,
        model_node.expr,
        model_node.latex_expr,
        model_node.postfix_expr,
        model_node.matlab_fxn_expr,
        np.array(model_node.value),
        np.array(model_node.test_value),
        model_node.x_in_expr_list,
        model_node.unit,
    )

    assert model_node.n_leaves == 2

    decomp = model_node.primary_feat_decomp
    assert len(decomp.keys()) == 2
    assert decomp["t_a"] == 1
    assert decomp["t_b"] == 1
    assert model_node.x_in_expr_list[0] == "t_a"
    assert model_node.x_in_expr_list[1] == "t_b"

    assert model_node.n_leaves == 2
    try:
        model_node.feat(2)
        raise ValueError("Accessing feature that should throw an error")
    except:
        pass
    assert model_node.matlab_fxn_expr == "(t_a + t_b)"

    assert model_node.n_leaves == 2

    assert model_node.n_leaves == 2

    decomp = model_node_arr.primary_feat_decomp
    assert len(decomp.keys()) == 2
    assert decomp["t_a"] == 1
    assert decomp["t_b"] == 1
    assert model_node_arr.x_in_expr_list[0] == "t_a"
    assert model_node_arr.x_in_expr_list[1] == "t_b"

    assert model_node_arr.n_leaves == 2
    try:
        model_node_arr.feat(2)
        raise ValueError("Accessing feature that should throw an error")
    except:
        pass
    assert model_node_arr.matlab_fxn_expr == "(t_a + t_b)"

    decomp = model_node_list.primary_feat_decomp
    assert len(decomp.keys()) == 2
    assert decomp["t_a"] == 1
    assert decomp["t_b"] == 1
    assert model_node_list.x_in_expr_list[0] == "t_a"
    assert model_node_list.x_in_expr_list[1] == "t_b"

    assert model_node_list.n_leaves == 2
    try:
        model_node_list.feat(2)
        raise ValueError("Accessing feature that should throw an error")
    except:
        pass
    assert model_node_list.matlab_fxn_expr == "(t_a + t_b)"

    # Check pickling
    pickled = pickle.dumps(model_node_list)
    feat_unpick = pickle.loads(pickled)

    assert np.all(feat_unpick.value == model_node_list.value)
    assert np.all(feat_unpick.test_value == model_node_list.test_value)
    assert np.all(feat_unpick.x_in_expr_list == model_node_list.x_in_expr_list)

    assert feat_unpick.expr == model_node_list.expr
    assert feat_unpick.latex_expr == model_node_list.latex_expr
    assert feat_unpick.postfix_expr == model_node_list.postfix_expr
    assert feat_unpick.matlab_fxn_expr == model_node_list.matlab_fxn_expr

    assert feat_unpick.unit == model_node_list.unit

    assert feat_unpick.arr_ind == model_node_list.arr_ind
    assert feat_unpick.feat_ind == model_node_list.feat_ind


if __name__ == "__main__":
    test_model_node()
