# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from sissopp import FeatureNode, AddNode, LogNode, SubNode, Unit, initialize_values_arr

import numpy as np
import pickle

np.random.seed(0)


class InvalidFeatureMade(Exception):
    pass


def test_add_node():
    task_sizes_train = [90]
    task_sizes_test = [10]

    initialize_values_arr(
        np.array(task_sizes_train, dtype=np.int32),
        np.array(task_sizes_test, dtype=np.int32),
        6,
        2,
    )

    data_1 = np.random.random(task_sizes_train[0]) * 1e4 + 1e-10
    test_data_1 = np.random.random(task_sizes_test[0]) * 1e4 + 1e-10

    data_2 = np.random.random(task_sizes_train[0]) * 2e4 - 1e4
    test_data_2 = np.random.random(task_sizes_test[0]) * 2e4 - 1e4

    data_3 = np.random.random(task_sizes_train[0]) * 1e10 + 1e-10
    test_data_3 = np.random.random(task_sizes_test[0]) * 1e10 + 1e-10

    feat_1 = FeatureNode(0, "t_a", data_1, test_data_1, Unit("s"))
    feat_2 = FeatureNode(1, "x_a", data_2, test_data_2, Unit("m"))
    feat_3 = FeatureNode(2, "t_b", data_3, test_data_3, Unit("s"))
    feat_4 = FeatureNode(3, "t_c", -1.0 * data_3, -1.0 * test_data_3, Unit("s"))
    feat_5 = FeatureNode(4, "a", data_1, test_data_1, Unit())
    feat_6 = FeatureNode(5, "c", data_3, test_data_3, Unit())

    feats = []
    try:
        feats.append(AddNode(feat_1, feat_2, 4, 1e-50, 1e50))
        raise InvalidFeatureMade("Addition with mismatching units")
    except RuntimeError:
        pass

    try:
        feats.append(AddNode(feat_3, feat_4, 4, 1e-50, 1e50))
        raise InvalidFeatureMade("Addition leading to a constant feature")
    except RuntimeError:
        pass

    try:
        feats.append(AddNode(feat_1, feat_2, 4, 1e-50, 1e0))
        raise InvalidFeatureMade("Addition outside of user specified bounds")
    except RuntimeError:
        pass

    try:
        feats.append(AddNode(feat_1, feat_2, 4, 1e-1, 1e50))
        raise InvalidFeatureMade("Addition outside of user specified bounds")
    except RuntimeError:
        pass

    feats.append(AddNode(feat_1, feat_3, 4, 1e-50, 1e50))
    feats.append(SubNode(feat_1, feat_3, 5, 1e-50, 1e50))
    feats.append(AddNode(feats[0], feat_3, 6, 1e-50, 1e50))

    try:
        feats.append(AddNode(feats[0], feats[1], 6, 1e-50, 1e50))
        raise InvalidFeatureMade("Addition with canceled out terms")
    except RuntimeError:
        pass

    try:
        feats.append(AddNode(feat_1, feat_1, 6, 1e-50, 1e50))
        raise InvalidFeatureMade("Adding the same feature")
    except RuntimeError:
        pass

    try:
        feats.append(AddNode(feats[0], feats[0], 6, 1e-50, 1e50))
        raise InvalidFeatureMade("Addition with non-one prefactor")
    except RuntimeError:
        pass

    decomp = feats[-1].primary_feat_decomp
    assert len(decomp.keys()) == 2
    assert decomp["t_a"] == 1
    assert decomp["t_b"] == 2

    assert feats[-1].n_leaves == 3
    assert feats[-1].n_feats == 2
    assert feats[-1].feat(1).expr == "t_b"
    try:
        feats[-1].feat(2)
        raise ValueError("Accessing feature that should throw an error")
    except:
        pass
    assert feats[0].matlab_fxn_expr == "(t_a + t_b)"

    feats.append(LogNode(feat_5, 7, 1e-50, 1e50))
    feats.append(LogNode(feat_6, 8, 1e-50, 1e50))
    try:
        feats.append(AddNode(feats[-1], feats[-2], 9, 1e-50, 1e50))
        raise InvalidFeatureMade("Addition of two LogNodes")
    except RuntimeError:
        pass
    feats.append(AddNode(feats[-1], feat_6, 9, 1e-50, 1e50))

    # Check pickling
    feats[-1].selected = True
    feats[-1].d_mat_ind = 1
    pickled = pickle.dumps(feats[-1])

    feat_unpick = pickle.loads(pickled)
    assert feat_unpick.d_mat_ind == feats[-1].d_mat_ind
    assert feat_unpick.selected == feats[-1].selected

    feats[-1].selected = False
    feat_unpick.selected = False
    assert np.all(feat_unpick.value == feats[-1].value)
    assert np.all(feat_unpick.test_value == feats[-1].test_value)
    assert feat_unpick.expr == feats[-1].expr
    assert feat_unpick.unit == feats[-1].unit
    assert feat_unpick.arr_ind == feats[-1].arr_ind
    assert feat_unpick.feat_ind == feats[-1].feat_ind


if __name__ == "__main__":
    test_add_node()
