# Copyright 2022 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import numpy as np
import pandas as pd

from sissopp.sklearn import SISSORegressor
from sissopp import Unit, Inputs

np.random.seed(15)


def test_sklearn_sisso_regressor():
    c0 = np.random.uniform(-5, 5)
    a0 = np.random.uniform(-5, 5)
    a1 = np.random.uniform(-5, 5)

    def test_fxn(X):
        return c0 + a0 * (X[:, 0] ** 2.0 - X[:, 1]) + a1 * (np.cbrt(X[:, 2] ** 2.0))

    sisso = SISSORegressor(
        prop_label="d",
        prop_unit=Unit("m"),
        n_dim=2,
        max_rung=2,
        workdir="test_reg_sklearn",
        clean_workdir=True,
    )

    X = np.random.uniform(-100, 100, (1000, 5))
    y = test_fxn(X)

    X_test = np.random.uniform(-1000, 1000, (25, 5))
    y_test = test_fxn(X_test)

    sisso.fit(X, y)
    assert np.abs(1.0 - np.corrcoef(sisso.predict(X), y)[0, 1]) < 1e-5
    assert np.abs(1.0 - np.corrcoef(sisso.predict(X_test), y_test)[0, 1]) < 1e-5


def test_sklearn_sisso_columns_index_regressor():
    c00 = np.random.uniform(-5, 5)
    a00 = np.random.uniform(-5, 5)
    a01 = np.random.uniform(-5, 5)

    c10 = np.random.uniform(-5, 5)
    a10 = np.random.uniform(-5, 5)
    a11 = np.random.uniform(-5, 5)

    def test_fxn(X):
        y = c00 + a00 * (X[:, 0] ** 2.0 - X[:, 1]) + a01 * (np.cbrt(X[:, 2] ** 2.0))
        y[::1] = (
            c10
            + a10 * (X[::1, 0] ** 2.0 - X[::1, 1])
            + a11 * (np.cbrt(X[::1, 2] ** 2.0))
        )
        return y

    sisso = SISSORegressor(
        task_key="task",
        prop_label="d",
        prop_unit=Unit("m"),
        n_dim=2,
        max_rung=2,
        workdir="test_reg_sklearn",
        clean_workdir=True,
    )

    X = np.random.uniform(-100, 100, (1000, 6))
    X[:, 5] = 0.0
    X[::1, 5] = 1.0
    y = test_fxn(X)

    X_test = np.random.uniform(-1000, 1000, (25, 6))
    X_test[:, 5] = 0.0
    X_test[::1, 5] = 1.0
    y_test = test_fxn(X_test)

    index = [f"{ii:03d}" for ii in range(1000)]
    sisso.fit(X, y, columns=["a", "b", "c", "e", "f", "task"], index=index)
    assert np.abs(1.0 - np.corrcoef(sisso.predict(X), y)[0, 1]) < 1e-5
    assert np.abs(1.0 - np.corrcoef(sisso.predict(X_test), y_test)[0, 1]) < 1e-5


def test_sklearn_sisso_df_regressor():
    c00 = np.random.uniform(-5, 5)
    a00 = np.random.uniform(-5, 5)
    a01 = np.random.uniform(-5, 5)

    c10 = np.random.uniform(-5, 5)
    a10 = np.random.uniform(-5, 5)
    a11 = np.random.uniform(-5, 5)

    def test_fxn(X):
        y = c00 + a00 * (X[:, 0] ** 2.0 - X[:, 1]) + a01 * (np.cbrt(X[:, 2] ** 2.0))
        y[::1] = (
            c10
            + a10 * (X[::1, 0] ** 2.0 - X[::1, 1])
            + a11 * (np.cbrt(X[::1, 2] ** 2.0))
        )
        return y

    sisso = SISSORegressor(
        task_key="task",
        prop_label="d",
        prop_unit=Unit("m"),
        n_dim=2,
        max_rung=2,
        workdir="test_reg_sklearn",
        clean_workdir=True,
    )

    X = np.random.uniform(-100, 100, (1000, 6))
    X[:, 5] = 0.0
    X[::1, 5] = 1.0
    y = test_fxn(X)

    X_test = np.random.uniform(-1000, 1000, (25, 6))
    X_test[:, 5] = 0.0
    X_test[::1, 5] = 1.0
    y_test = test_fxn(X_test)

    index = [f"{ii:03d}" for ii in range(1000)]
    index_test = [f"{ii:03d}" for ii in range(25)]
    columns = ["a", "b", "c", "e", "f", "task"]

    X = pd.DataFrame(data=X, index=index, columns=columns)
    X_test = pd.DataFrame(data=X_test, index=index_test, columns=columns)

    sisso.fit(X, y)
    assert np.abs(1.0 - np.corrcoef(sisso.predict(X), y)[0, 1]) < 1e-5
    assert np.abs(1.0 - np.corrcoef(sisso.predict(X_test), y_test)[0, 1]) < 1e-5


def test_sklearn_sisso_regressor_omp():
    c00 = np.random.uniform(-5, 5)
    a00 = np.random.uniform(-5, 5)
    a01 = np.random.uniform(-5, 5)

    c10 = np.random.uniform(-5, 5)
    a10 = np.random.uniform(-5, 5)
    a11 = np.random.uniform(-5, 5)

    def test_fxn(X):
        y = c00 + a00 * X[:, 0] + a01 * X[:, 2]
        y = c10 + a10 * X[:, 0] + a11 * X[:, 2]

        return y

    sisso = SISSORegressor.OMP(
        task_key="task",
        prop_label="d",
        prop_unit=Unit("m"),
        n_dim=2,
        workdir="test_reg_sklearn",
        clean_workdir=True,
    )

    X = np.random.uniform(-100, 100, (1000, 6))
    X[:, 5] = 0.0
    X[::1, 5] = 1.0
    y = test_fxn(X)

    X_test = np.random.uniform(-1000, 1000, (25, 6))
    X_test[:, 5] = 0.0
    X_test[::1, 5] = 1.0
    y_test = test_fxn(X_test)

    index = [f"{ii:03d}" for ii in range(1000)]
    index_test = [f"{ii:03d}" for ii in range(25)]
    columns = ["a", "b", "c", "e", "f", "task"]

    X = pd.DataFrame(data=X, index=index, columns=columns)
    X_test = pd.DataFrame(data=X_test, index=index_test, columns=columns)

    sisso.fit(X, y)
    assert np.abs(1.0 - np.corrcoef(sisso.predict(X), y)[0, 1]) < 1e-5
    assert np.abs(1.0 - np.corrcoef(sisso.predict(X_test), y_test)[0, 1]) < 1e-5


def test_sklearn_sisso_regressor_inputs():
    c00 = np.random.uniform(-5, 5)
    a00 = np.random.uniform(-5, 5)
    a01 = np.random.uniform(-5, 5)

    c10 = np.random.uniform(-5, 5)
    a10 = np.random.uniform(-5, 5)
    a11 = np.random.uniform(-5, 5)

    def test_fxn(X):
        y = c00 + a00 * (X[:, 0] ** 2.0 - X[:, 1]) + a01 * (np.cbrt(X[:, 2] ** 2.0))
        y[::1] = (
            c10
            + a10 * (X[::1, 0] ** 2.0 - X[::1, 1])
            + a11 * (np.cbrt(X[::1, 2] ** 2.0))
        )
        return y

    X = np.random.uniform(-100, 100, (1000, 6))
    X[:, 5] = 0.0
    X[::1, 5] = 1.0
    y = test_fxn(X)

    X_test = np.random.uniform(-1000, 1000, (25, 6))
    X_test[:, 5] = 0.0
    X_test[::1, 5] = 1.0
    y_test = test_fxn(X_test)

    index = [f"{ii:03d}" for ii in range(1000)]
    index_test = [f"{ii:03d}" for ii in range(25)]
    columns = ["a", "b", "c", "e", "f", "task"]

    X = pd.DataFrame(data=X, index=index, columns=columns)
    X_test = pd.DataFrame(data=X_test, index=index_test, columns=columns)

    inputs = Inputs()
    inputs.task_key = "task"
    inputs.prop_label = "d"
    inputs.prop_unit = Unit("m")
    inputs.n_dim = 2
    inputs.max_rung = 2
    inputs.allowed_ops = [
        "add",
        "sub",
        "mult",
        "div",
        "abs_diff",
        "inv",
        "abs",
        "cos",
        "sin",
        "exp",
        "neg_exp",
        "log",
        "sq",
        "sqrt",
        "cb",
        "cbrt",
        "six_pow",
    ]
    sisso = SISSORegressor.from_inputs(
        inputs,
        workdir="test_reg_sklearn",
        clean_workdir=True,
    )

    sisso.fit(X, y)
    assert np.abs(1.0 - np.corrcoef(sisso.predict(X), y)[0, 1]) < 1e-5
    assert np.abs(1.0 - np.corrcoef(sisso.predict(X_test), y_test)[0, 1]) < 1e-5


if __name__ == "__main__":
    test_sklearn_sisso_regressor()
    test_sklearn_sisso_columns_index_regressor()
    test_sklearn_sisso_df_regressor()
    test_sklearn_sisso_regressor_omp()
    test_sklearn_sisso_regressor_inputs()
