// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "external/third_party_gtest.hpp"

#include "feature_creation/node/FeatureNode.hpp"
#include "mpi_interface/MPI_Ops.hpp"

namespace
{
class MPI_OpsTests : public ::testing::Test
{
protected:
    void SetUp() override
    {
        node_value_arrs::initialize_values_arr({10}, {0}, 5, 0, false);

        std::vector<double> value_1 = {1.0, -1.0, 1.0, -1.0, 1.0, -1.0, 1.0, -1.0, 1.0, -1.0};
        std::vector<double> value_2 = {2.0, -2.0, 2.0, -2.0, 2.0, -2.0, 2.0, -2.0, 2.0, -2.0};
        std::vector<double> value_3 = {1.0, -1.0, 1.0, -1.0, 1.0, -1.0, 1.0, -1.0, 0.0, 0.0};
        std::vector<double> value_4 = {1.0, -1.0, 1.0, -1.0, 1.0, -1.0, 0.0, 0.0, 1.0, -1.0};
        std::vector<double> value_5 = {1.0, -1.0, 1.0, -1.0, 1.0, -1.0, 0.0, 0.0, 0.0, 0.0};

        node_ptr feat_1 = std::make_shared<FeatureNode>(
            0, "A", value_1, std::vector<double>(0), Unit());
        node_ptr feat_2 = std::make_shared<FeatureNode>(
            1, "B", value_2, std::vector<double>(0), Unit());
        node_ptr feat_3 = std::make_shared<FeatureNode>(
            2, "C", value_3, std::vector<double>(0), Unit());
        node_ptr feat_4 = std::make_shared<FeatureNode>(
            3, "D", value_4, std::vector<double>(0), Unit());
        node_ptr feat_5 = std::make_shared<FeatureNode>(
            4, "E", value_5, std::vector<double>(0), Unit());

        _in_vec_1 = {
            node_sc_pair(feat_1, -1.0), node_sc_pair(feat_3, -0.8), node_sc_pair(feat_5, -0.6)};

        _in_vec_2 = {node_sc_pair(feat_2, -1.0), node_sc_pair(feat_4, -0.8), node_sc_pair()};
    }

    void TearDown() override { node_value_arrs::finalize_values_arr(); }

    std::vector<node_sc_pair> _in_vec_1;
    std::vector<node_sc_pair> _in_vec_2;
};

TEST_F(MPI_OpsTests, Pearson_max_cor_1)
{
    mpi_reduce_op::set_op("regression", 1.0, 4);
    std::vector<node_sc_pair> out_vec = mpi_reduce_op::select_top_feats(_in_vec_1, _in_vec_2);

    EXPECT_EQ(out_vec.size(), 4);
    EXPECT_EQ(out_vec[0].score(), -1.0);
    EXPECT_EQ(out_vec[1].score(), -0.8);
    EXPECT_EQ(out_vec[2].score(), -0.8);
    EXPECT_EQ(out_vec[3].score(), -0.6);
}

TEST_F(MPI_OpsTests, Pearson)
{
    mpi_reduce_op::set_op("regression", 0.8, 4);
    std::vector<node_sc_pair> out_vec = mpi_reduce_op::select_top_feats(_in_vec_1, _in_vec_2);

    EXPECT_EQ(out_vec.size(), 2);
    EXPECT_EQ(out_vec[0].score(), -1.0);
    EXPECT_EQ(out_vec[1].score(), -0.6);
}

TEST_F(MPI_OpsTests, Spearman_max_cor_1)
{
    mpi_reduce_op::set_op("classification", 1.0, 4);
    std::vector<node_sc_pair> out_vec = mpi_reduce_op::select_top_feats(_in_vec_1, _in_vec_2);

    EXPECT_EQ(out_vec.size(), 4);
    EXPECT_EQ(out_vec[0].score(), -1.0);
    EXPECT_EQ(out_vec[1].score(), -0.8);
    EXPECT_EQ(out_vec[2].score(), -0.8);
    EXPECT_EQ(out_vec[3].score(), -0.6);
}

TEST_F(MPI_OpsTests, Spearman)
{
    mpi_reduce_op::set_op("classification", 0.85, 4);
    std::vector<node_sc_pair> out_vec = mpi_reduce_op::select_top_feats(_in_vec_1, _in_vec_2);

    EXPECT_EQ(out_vec.size(), 2);
    EXPECT_EQ(out_vec[0].score(), -1.0);
    EXPECT_EQ(out_vec[1].score(), -0.6);
}
}  // namespace
