// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "external/third_party_gtest.hpp"

#include <random>

#include "mpi_interface/MPI_Interface.hpp"

namespace
{
TEST(MPI_InterfaceTests, MPI_Interface)
{
    mpi_setup::init_mpi_env();
    EXPECT_EQ(mpi_setup::comm->cantor_tag_gen(2, 1, 4, 3), 35);

    std::array<int, 2> start_end = mpi_setup::comm->get_start_end_from_list(101, 25);

    int els_per_rank = 101 / mpi_setup::comm->size();
    int remaineder = 101 % mpi_setup::comm->size();

    int start = 25 + els_per_rank * mpi_setup::comm->rank() +
                std::min(mpi_setup::comm->rank(), remaineder);
    int end = 25 + els_per_rank * (mpi_setup::comm->rank() + 1) +
              std::min(mpi_setup::comm->rank() + 1, remaineder);

    EXPECT_EQ(start_end[0], start);
    EXPECT_EQ(start_end[1], end);
}
}  // namespace
