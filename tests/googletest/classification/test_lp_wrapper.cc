// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "external/third_party_gtest.hpp"

#include <random>

#include "classification/LPWrapper.hpp"
#include "classification/prop_sorted_d_mat.hpp"
#include "feature_creation/node/FeatureNode.hpp"
#include "mpi_interface/MPI_Interface.hpp"

namespace
{
class LPWrapperTests : public ::testing::Test
{
protected:
    void SetUp() override
    {
        mpi_setup::init_mpi_env();

        std::vector<int> task_sizes_train = {80};
        std::vector<int> task_sizes_test = {20};

        node_value_arrs::initialize_values_arr(task_sizes_train, task_sizes_test, 2, 2, false);
        node_value_arrs::initialize_d_matrix_arr();
        node_value_arrs::resize_d_matrix_arr(2);

        std::vector<double> value_1(task_sizes_train[0], 0.0);
        std::vector<double> value_2(task_sizes_train[0], 0.0);

        std::vector<double> test_value_1(task_sizes_test[0], 0.0);
        std::vector<double> test_value_2(task_sizes_test[0], 0.0);

        std::default_random_engine generator(0);
        std::uniform_real_distribution<double> distribution_12_pos(1.0, 2.0);
        std::uniform_real_distribution<double> distribution_12_neg(-2.0, -1.0);

        for (int ii = 0; ii < 20; ++ii)
        {
            value_1[ii] = distribution_12_neg(generator);
            value_2[ii] = distribution_12_neg(generator);
        }
        for (int ii = 20; ii < 40; ++ii)
        {
            value_1[ii] = distribution_12_pos(generator);
            value_2[ii] = distribution_12_pos(generator);
        }
        for (int ii = 40; ii < 60; ++ii)
        {
            value_1[ii] = distribution_12_neg(generator);
            value_2[ii] = distribution_12_pos(generator);
        }
        for (int ii = 60; ii < 80; ++ii)
        {
            value_1[ii] = distribution_12_pos(generator);
            value_2[ii] = distribution_12_neg(generator);
        }

        for (int ii = 0; ii < 5; ++ii)
        {
            test_value_1[ii] = distribution_12_neg(generator);
            test_value_2[ii] = distribution_12_neg(generator);
        }

        for (int ii = 5; ii < 10; ++ii)
        {
            test_value_1[ii] = distribution_12_pos(generator);
            test_value_2[ii] = distribution_12_pos(generator);
        }

        for (int ii = 10; ii < 15; ++ii)
        {
            test_value_1[ii] = distribution_12_neg(generator);
            test_value_2[ii] = distribution_12_pos(generator);
        }

        for (int ii = 15; ii < 20; ++ii)
        {
            test_value_1[ii] = distribution_12_pos(generator);
            test_value_2[ii] = distribution_12_neg(generator);
        }

        _phi = {FeatureNode(0, "A", value_1, test_value_1, Unit("m")),
                FeatureNode(1, "B", value_2, test_value_2, Unit("m"))};

        _tol = 1e-5;
        _error.resize(80, 0.0);
        _test_error.resize(20, 0.0);
        _task_num = 0;
        _n_class = 4;
        _n_dim = 2;
        _n_samp = 80;
        _n_samp_test = 20;

        _samp_per_class = std::vector<int>(4, 20);
        _samp_per_class_test = std::vector<int>(4, 5);

        prop_sorted_d_mat::initialize_sorted_d_matrix_arr(2, 1, 4, _samp_per_class);
        std::copy_n(value_1.data(), value_1.size(), prop_sorted_d_mat::access_sorted_d_matrix(0));
        std::copy_n(value_2.data(), value_2.size(), prop_sorted_d_mat::access_sorted_d_matrix(1));
    }

    void TearDown() override
    {
        prop_sorted_d_mat::finalize_sorted_d_matrix_arr();
        node_value_arrs::finalize_values_arr();
    }

    std::vector<FeatureNode> _phi;

    std::vector<double> _error;
    std::vector<double> _test_error;

    std::vector<int> _samp_per_class;
    std::vector<int> _samp_per_class_test;

    double _tol;

    int _task_num;
    int _n_class;
    int _n_dim;
    int _n_samp;
    int _n_samp_test;
};

TEST_F(LPWrapperTests, WithTestData)
{
    EXPECT_THROW(LPWrapper lp(_samp_per_class,
                              _task_num,
                              _n_class,
                              _n_dim,
                              _n_samp + 1,
                              _tol,
                              _samp_per_class_test,
                              _n_samp_test),
                 std::logic_error);

    EXPECT_THROW(LPWrapper lp(_samp_per_class,
                              _task_num,
                              _n_class,
                              _n_dim,
                              _n_samp,
                              _tol,
                              _samp_per_class_test,
                              _n_samp_test + 1),
                 std::logic_error);
    LPWrapper lp(_samp_per_class,
                 _task_num,
                 _n_class,
                 _n_dim,
                 _n_samp,
                 _tol,
                 _samp_per_class_test,
                 _n_samp_test);

    EXPECT_EQ(lp.get_n_overlap({0, 1}), 0);

    std::vector<double*> val_ptrs = {_phi[0].value_ptr(), _phi[1].value_ptr()};
    std::vector<double*> test_val_ptrs = {_phi[0].test_value_ptr(), _phi[1].test_value_ptr()};

    lp.set_n_overlap(val_ptrs, test_val_ptrs, _error.data(), _test_error.data());
    EXPECT_EQ(lp.n_class(), _n_class);
    EXPECT_EQ(lp.task_num(), _task_num);
    EXPECT_EQ(lp.n_dim(), _n_dim);
    EXPECT_EQ(lp.n_samp(), _n_samp);
    EXPECT_EQ(lp.n_samp_test(), _n_samp_test);
    EXPECT_EQ(lp.n_overlap(), 0);
    EXPECT_EQ(lp.n_overlap_test(), 0);

    EXPECT_THROW(lp.copy_data(0, {0, 1, 2}), std::logic_error);

    val_ptrs = {_phi[0].value_ptr(), _phi[1].value_ptr(), _phi[1].value_ptr()};
    EXPECT_THROW(lp.set_n_overlap(val_ptrs, test_val_ptrs, _error.data(), _test_error.data()),
                 std::logic_error);

    val_ptrs = {_phi[0].value_ptr(), _phi[1].value_ptr()};
    test_val_ptrs = {_phi[0].test_value_ptr(), _phi[1].test_value_ptr(), _phi[1].test_value_ptr()};
    EXPECT_THROW(lp.set_n_overlap(val_ptrs, test_val_ptrs, _error.data(), _test_error.data()),
                 std::logic_error);
}

TEST_F(LPWrapperTests, WithoutTestData)
{
    EXPECT_THROW(LPWrapper lp(_samp_per_class, _task_num, _n_class, _n_dim, _n_samp + 1, _tol),
                 std::logic_error);

    LPWrapper lp(_samp_per_class, _task_num, _n_class, _n_dim, _n_samp, _tol);

    EXPECT_EQ(lp.get_n_overlap({0, 1}), 0);

    std::vector<double*> val_ptrs = {_phi[0].value_ptr(), _phi[1].value_ptr()};
    std::vector<double*> test_val_ptrs = {_phi[0].test_value_ptr(), _phi[1].test_value_ptr()};

    lp.set_n_overlap(val_ptrs, test_val_ptrs, _error.data(), _test_error.data());
    EXPECT_EQ(lp.n_class(), _n_class);
    EXPECT_EQ(lp.task_num(), _task_num);
    EXPECT_EQ(lp.n_dim(), _n_dim);
    EXPECT_EQ(lp.n_samp(), _n_samp);
    EXPECT_EQ(lp.n_samp_test(), 0);
    EXPECT_EQ(lp.n_overlap(), 0);
    EXPECT_EQ(lp.n_overlap_test(), 0);

    EXPECT_THROW(lp.copy_data(0, {0, 1, 2}), std::logic_error);

    val_ptrs = {_phi[0].value_ptr(), _phi[1].value_ptr(), _phi[1].value_ptr()};
    EXPECT_THROW(lp.set_n_overlap(val_ptrs, test_val_ptrs, _error.data(), _test_error.data()),
                 std::logic_error);

    val_ptrs = {_phi[0].value_ptr(), _phi[1].value_ptr()};
    test_val_ptrs = {_phi[0].test_value_ptr(), _phi[1].test_value_ptr(), _phi[1].test_value_ptr()};
    EXPECT_THROW(lp.set_n_overlap(val_ptrs, test_val_ptrs, _error.data(), _test_error.data()),
                 std::logic_error);
}

TEST_F(LPWrapperTests, CopyTest)
{
    LPWrapper lp(_samp_per_class, _task_num, _n_class, _n_dim, _n_samp, _tol);

    LPWrapper lp_copy(lp);

    EXPECT_EQ(lp.n_class(), lp_copy.n_class());
    EXPECT_EQ(lp.task_num(), lp_copy.task_num());
    EXPECT_EQ(lp.n_dim(), lp_copy.n_dim());
    EXPECT_EQ(lp.n_samp(), lp_copy.n_samp());
    EXPECT_EQ(lp.n_samp_test(), lp_copy.n_samp_test());
    EXPECT_EQ(lp.n_overlap(), lp_copy.n_overlap());
    EXPECT_EQ(lp.n_overlap_test(), lp_copy.n_overlap_test());
}

TEST_F(LPWrapperTests, CopyAssignmentTest)
{
    LPWrapper lp(_samp_per_class, _task_num, _n_class, _n_dim, _n_samp, _tol);

    LPWrapper lp_copy = lp;
    lp_copy = lp;

    EXPECT_EQ(lp.n_class(), lp_copy.n_class());
    EXPECT_EQ(lp.task_num(), lp_copy.task_num());
    EXPECT_EQ(lp.n_dim(), lp_copy.n_dim());
    EXPECT_EQ(lp.n_samp(), lp_copy.n_samp());
    EXPECT_EQ(lp.n_samp_test(), lp_copy.n_samp_test());
    EXPECT_EQ(lp.n_overlap(), lp_copy.n_overlap());
    EXPECT_EQ(lp.n_overlap_test(), lp_copy.n_overlap_test());
}
}  // namespace
