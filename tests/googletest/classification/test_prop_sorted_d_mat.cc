// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "external/third_party_gtest.hpp"

#include <random>

#include "classification/prop_sorted_d_mat.hpp"
#include "feature_creation/node/FeatureNode.hpp"
#include "mpi_interface/MPI_Interface.hpp"

namespace
{
class PropSOrtedDMatTests : public ::testing::Test
{
protected:
    void SetUp() override
    {
        _n_samples_per_class = {18, 19, 21, 22};
        _n_feat = 2;
        _n_task = 2;
        _n_class = 2;
    }

    std::vector<int> _n_samples_per_class;
    int _n_feat;
    int _n_task;
    int _n_class;
};

TEST_F(PropSOrtedDMatTests, TestSortedDMat)
{
    prop_sorted_d_mat::initialize_sorted_d_matrix_arr(1, _n_task, _n_class, _n_samples_per_class);
    EXPECT_EQ(prop_sorted_d_mat::CLASS_START[1], 18);
    EXPECT_EQ(prop_sorted_d_mat::N_SAMPLES_PER_CLASS[1], 19);
    EXPECT_EQ(prop_sorted_d_mat::N_FEATURES, 1);
    EXPECT_EQ(prop_sorted_d_mat::N_TASK, _n_task);
    EXPECT_EQ(prop_sorted_d_mat::N_CLASS, _n_class);
    EXPECT_EQ(prop_sorted_d_mat::N_SAMPLES,
              std::accumulate(_n_samples_per_class.begin(), _n_samples_per_class.end(), 0));
    EXPECT_EQ(prop_sorted_d_mat::SORTED_D_MATRIX.size(), 80);

    prop_sorted_d_mat::resize_sorted_d_matrix_arr(_n_feat);
    EXPECT_EQ(prop_sorted_d_mat::N_FEATURES, _n_feat);
    EXPECT_EQ(prop_sorted_d_mat::SORTED_D_MATRIX.size(), 80 * _n_feat);

    EXPECT_EQ(prop_sorted_d_mat::get_class_size(1, 0), 21);

    prop_sorted_d_mat::access_sorted_d_matrix(1)[2] = 3.5;
    EXPECT_EQ(prop_sorted_d_mat::get_sorted_d_matrix_el(1, 2), 3.5);

    prop_sorted_d_mat::access_sorted_d_matrix(0, 1)[1] = 2.0;
    EXPECT_EQ(prop_sorted_d_mat::access_sorted_d_matrix(0, 0, 1)[1], 2.0);

    prop_sorted_d_mat::access_sample_sorted_d_matrix(2)[0] = 1.5;
    EXPECT_EQ(prop_sorted_d_mat::access_sample_sorted_d_matrix(2, 0, 0)[0], 1.5);
    prop_sorted_d_mat::finalize_sorted_d_matrix_arr();

    EXPECT_THROW(prop_sorted_d_mat::initialize_sorted_d_matrix_arr(
                     1, _n_task, _n_class + 1, _n_samples_per_class),
                 std::logic_error);

    EXPECT_THROW(prop_sorted_d_mat::initialize_sorted_d_matrix_arr(
                     1, _n_task + 1, _n_class, _n_samples_per_class),
                 std::logic_error);

    _n_samples_per_class.push_back(1);
    EXPECT_THROW(prop_sorted_d_mat::initialize_sorted_d_matrix_arr(
                     1, _n_task, _n_class, _n_samples_per_class),
                 std::logic_error);
}
}  // namespace
