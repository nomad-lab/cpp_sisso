// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "external/third_party_gtest.hpp"

#include <random>

#include "classification/SVMWrapper.hpp"
#include "feature_creation/node/FeatureNode.hpp"
#include "mpi_interface/MPI_Interface.hpp"

namespace
{
class SVMWrapperTests : public ::testing::Test
{
protected:
    void SetUp() override
    {
        mpi_setup::init_mpi_env();

        std::vector<int> task_sizes_train = {80};
        std::vector<int> task_sizes_test = {20};

        node_value_arrs::initialize_values_arr(task_sizes_train, task_sizes_test, 2, 2, false);
        node_value_arrs::initialize_d_matrix_arr();
        node_value_arrs::resize_d_matrix_arr(2);

        std::vector<double> value_1(task_sizes_train[0], 0.0);
        std::vector<double> value_2(task_sizes_train[0], 0.0);

        std::vector<double> test_value_1(task_sizes_test[0], 0.0);
        std::vector<double> test_value_2(task_sizes_test[0], 0.0);

        std::default_random_engine generator(0);
        std::uniform_real_distribution<double> distribution_12_pos(1.0, 2.0);
        std::uniform_real_distribution<double> distribution_12_neg(-2.0, -1.0);
        std::uniform_real_distribution<double> distribution_3(-10.0, 10.0);

        for (int ii = 0; ii < 20; ++ii)
        {
            value_1[ii] = distribution_12_neg(generator);
            value_2[ii] = distribution_12_neg(generator);
        }
        for (int ii = 20; ii < 40; ++ii)
        {
            value_1[ii] = distribution_12_pos(generator);
            value_2[ii] = distribution_12_pos(generator);
        }
        for (int ii = 40; ii < 60; ++ii)
        {
            value_1[ii] = distribution_12_neg(generator);
            value_2[ii] = distribution_12_pos(generator);
        }
        for (int ii = 60; ii < 80; ++ii)
        {
            value_1[ii] = distribution_12_pos(generator);
            value_2[ii] = distribution_12_neg(generator);
        }

        for (int ii = 0; ii < 5; ++ii)
        {
            test_value_1[ii] = distribution_12_neg(generator);
            test_value_2[ii] = distribution_12_neg(generator);
        }

        for (int ii = 5; ii < 10; ++ii)
        {
            test_value_1[ii] = distribution_12_pos(generator);
            test_value_2[ii] = distribution_12_pos(generator);
        }

        for (int ii = 10; ii < 15; ++ii)
        {
            test_value_1[ii] = distribution_12_neg(generator);
            test_value_2[ii] = distribution_12_pos(generator);
        }

        for (int ii = 15; ii < 20; ++ii)
        {
            test_value_1[ii] = distribution_12_pos(generator);
            test_value_2[ii] = distribution_12_neg(generator);
        }

        _phi = {FeatureNode(0, "A", value_1, test_value_1, Unit("m")),
                FeatureNode(1, "B", value_2, test_value_2, Unit("m"))};

        std::copy_n(value_1.data(), task_sizes_train[0], node_value_arrs::get_d_matrix_ptr(0, 0));
        std::copy_n(value_2.data(), task_sizes_train[0], node_value_arrs::get_d_matrix_ptr(1, 0));
        _n_class = 4;
        _n_dim = 2;
        _n_samp = task_sizes_train[0];
        _n_samp_test = task_sizes_test[0];

        _prop.resize(task_sizes_train[0], 0.0);
        _prop_test.resize(task_sizes_test[0], 0.0);

        std::fill_n(_prop.begin() + 20, 20, 1.0);
        std::fill_n(_prop.begin() + 40, 20, 2.0);
        std::fill_n(_prop.begin() + 60, 20, 3.0);

        std::fill_n(_prop_test.begin() + 5, 5, 1.0);
        std::fill_n(_prop_test.begin() + 10, 5, 2.0);
        std::fill_n(_prop_test.begin() + 15, 5, 3.0);
    }

    void TearDown() override { node_value_arrs::finalize_values_arr(); }

    std::vector<FeatureNode> _phi;

    std::vector<double> _prop;
    std::vector<double> _prop_test;

    int _n_class;
    int _n_dim;
    int _n_samp;
    int _n_samp_test;
};

TEST_F(SVMWrapperTests, DefaultTest)
{
    SVMWrapper svm(_n_class, _n_dim, _prop);

    std::vector<double*> val_ptrs = {_phi[0].value_ptr(), _phi[1].value_ptr()};
    std::vector<double*> test_val_ptrs = {_phi[0].test_value_ptr(), _phi[1].test_value_ptr()};
    std::vector<double> comp(80, 0.0);

    svm.train({0, 1}, 0);
    std::vector<double> prop_est = svm.predict(80, val_ptrs);
    std::vector<double> prop_test_est = svm.predict(20, test_val_ptrs);

    std::transform(
        prop_est.begin(), prop_est.end(), _prop.begin(), comp.begin(), [](double p, double pe) {
            return std::abs(p - pe);
        });
    EXPECT_EQ(std::accumulate(comp.begin(), comp.end(), 0), 0.0);

    std::transform(prop_test_est.begin(),
                   prop_test_est.end(),
                   _prop_test.begin(),
                   comp.begin(),
                   [](double p, double pe) { return std::abs(p - pe); });
    EXPECT_EQ(std::accumulate(comp.begin(), comp.begin() + 20, 0), 0.0);

    svm.train(val_ptrs, 0);
    prop_est = svm.predict(80, val_ptrs);
    prop_test_est = svm.predict(20, test_val_ptrs);

    prop_est = svm.predict(80, val_ptrs);
    prop_test_est = svm.predict(20, test_val_ptrs);

    std::transform(
        prop_est.begin(), prop_est.end(), _prop.begin(), comp.begin(), [](double p, double pe) {
            return std::abs(p - pe);
        });
    EXPECT_EQ(std::accumulate(comp.begin(), comp.end(), 0), 0.0);

    std::transform(prop_test_est.begin(),
                   prop_test_est.end(),
                   _prop_test.begin(),
                   comp.begin(),
                   [](double p, double pe) { return std::abs(p - pe); });
    EXPECT_EQ(std::accumulate(comp.begin(), comp.begin() + 20, 0), 0.0);

    EXPECT_EQ(svm.n_class(), _n_class);
    EXPECT_EQ(svm.n_dim(), _n_dim);
    EXPECT_EQ(svm.n_samp(), _n_samp);
    EXPECT_EQ(svm.n_misclassified(), 0);

    EXPECT_THROW(svm.copy_data({0, 1, 2}, 0), std::logic_error);
    EXPECT_THROW(svm.copy_data({0, 1}, 3), std::logic_error);

    val_ptrs = {_phi[0].value_ptr(), _phi[1].value_ptr(), _phi[1].value_ptr()};
    EXPECT_THROW(svm.copy_data(val_ptrs), std::logic_error);

    test_val_ptrs = {_phi[0].test_value_ptr(), _phi[1].test_value_ptr(), _phi[1].test_value_ptr()};
    EXPECT_THROW(svm.predict(20, test_val_ptrs), std::logic_error);
}

TEST_F(SVMWrapperTests, CTest)
{
    SVMWrapper svm(1.0, _n_class, _n_dim, _prop);

    std::vector<double*> val_ptrs = {_phi[0].value_ptr(), _phi[1].value_ptr()};
    std::vector<double*> test_val_ptrs = {_phi[0].test_value_ptr(), _phi[1].test_value_ptr()};
    std::vector<double> comp(80, 0.0);

    svm.train({0, 1}, 0);
    std::vector<double> prop_est = svm.predict(80, val_ptrs);
    std::vector<double> prop_test_est = svm.predict(20, test_val_ptrs);

    std::transform(
        prop_est.begin(), prop_est.end(), _prop.begin(), comp.begin(), [](double p, double pe) {
            return std::abs(p - pe);
        });
    EXPECT_EQ(std::accumulate(comp.begin(), comp.end(), 0), 0.0);

    std::transform(prop_test_est.begin(),
                   prop_test_est.end(),
                   _prop_test.begin(),
                   comp.begin(),
                   [](double p, double pe) { return std::abs(p - pe); });
    EXPECT_EQ(std::accumulate(comp.begin(), comp.begin() + 20, 0), 0.0);

    svm.train(val_ptrs, 0);
    prop_est = svm.predict(80, val_ptrs);
    prop_test_est = svm.predict(20, test_val_ptrs);

    prop_est = svm.predict(80, val_ptrs);
    prop_test_est = svm.predict(20, test_val_ptrs);

    std::transform(
        prop_est.begin(), prop_est.end(), _prop.begin(), comp.begin(), [](double p, double pe) {
            return std::abs(p - pe);
        });
    EXPECT_EQ(std::accumulate(comp.begin(), comp.end(), 0), 0.0);

    std::transform(prop_test_est.begin(),
                   prop_test_est.end(),
                   _prop_test.begin(),
                   comp.begin(),
                   [](double p, double pe) { return std::abs(p - pe); });
    EXPECT_EQ(std::accumulate(comp.begin(), comp.begin() + 20, 0), 0.0);

    EXPECT_EQ(svm.n_class(), _n_class);
    EXPECT_EQ(svm.n_dim(), _n_dim);
    EXPECT_EQ(svm.n_samp(), _n_samp);
    EXPECT_EQ(svm.n_misclassified(), 0);

    EXPECT_THROW(svm.copy_data({0, 1, 2}, 0), std::logic_error);
    EXPECT_THROW(svm.copy_data({0, 1}, 3), std::logic_error);

    val_ptrs = {_phi[0].value_ptr(), _phi[1].value_ptr(), _phi[1].value_ptr()};
    EXPECT_THROW(svm.copy_data(val_ptrs), std::logic_error);

    test_val_ptrs = {_phi[0].test_value_ptr(), _phi[1].test_value_ptr(), _phi[1].test_value_ptr()};
    EXPECT_THROW(svm.predict(20, test_val_ptrs), std::logic_error);
}
}  // namespace
