// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "external/third_party_gtest.hpp"

#include <random>

#include "classification/ConvexHull1D.hpp"
#include "feature_creation/node/FeatureNode.hpp"
#include "mpi_interface/MPI_Interface.hpp"

namespace
{
class ConvexHull1DTests : public ::testing::Test
{
protected:
    void SetUp() override
    {
        mpi_setup::init_mpi_env();

        _task_sizes_train = {40, 40};
        std::vector<int> task_sizes_test = {0};

        node_value_arrs::initialize_values_arr(_task_sizes_train, task_sizes_test, 1, 0, false);

        std::vector<double> value_1(80, 0.0);
        std::vector<double> test_value_1(80, 0.0);
        _prop.resize(80, 0.0);

        std::default_random_engine generator(0);
        std::uniform_real_distribution<double> distribution_12_pos(1.0, 2.0);
        std::uniform_real_distribution<double> distribution_12_neg(-2.0, -1.0);

        for (int ii = 0; ii < 20; ++ii)
        {
            value_1[ii] = distribution_12_neg(generator);
            _prop[ii] = 0.0;
        }
        for (int ii = 20; ii < 40; ++ii)
        {
            value_1[ii] = distribution_12_pos(generator);
            _prop[ii] = 1.0;
        }
        for (int ii = 40; ii < 60; ++ii)
        {
            value_1[ii] = distribution_12_neg(generator);
            _prop[ii] = 0.0;
        }
        for (int ii = 60; ii < 80; ++ii)
        {
            value_1[ii] = distribution_12_pos(generator);
            _prop[ii] = 1.0;
        }
        value_1[0] = -0.00001;
        value_1[20] = 0.00001;

        _phi = {
            FeatureNode(0, "A", value_1, {}, Unit("m")),
        };
    }

    void TearDown() override { node_value_arrs::finalize_values_arr(); }

    std::vector<FeatureNode> _phi;
    std::vector<double> _prop;
    std::vector<int> _task_sizes_train;
};

TEST_F(ConvexHull1DTests, ConstructorTest)
{
    ConvexHull1D convex(_task_sizes_train, _prop.data());
    EXPECT_EQ(std::ceil(convex.overlap_1d(_phi[0].value_ptr())), 0);
    EXPECT_EQ(std::ceil(convex.overlap_1d(_phi[0].value_ptr(), 1e-2)), 2);
}

TEST_F(ConvexHull1DTests, DefaultTest)
{
    ConvexHull1D convex;
    convex.initialize_prop(_task_sizes_train, _prop.data());

    EXPECT_EQ(std::ceil(convex.overlap_1d(_phi[0].value_ptr())), 0);
    EXPECT_EQ(std::ceil(convex.overlap_1d(_phi[0].value_ptr(), 1e-2)), 2);
}
}  // namespace
