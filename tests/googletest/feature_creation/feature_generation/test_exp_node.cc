// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "external/third_party_gtest.hpp"
#include <math.h>

#include "feature_creation/node/FeatureNode.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/add/add.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/exp/exponential.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/log/log.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/neg_exp/negative_exponential.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/sub/subtract.hpp"
#include "feature_creation/node/value_storage/nodes_value_containers.hpp"
#include "mpi_interface/MPI_Interface.hpp"

namespace
{
class ExpNodeTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
#ifdef PARAMETERIZE
        node_value_arrs::initialize_values_arr({4}, {1}, 4, 2, true);
        nlopt_wrapper::MAX_PARAM_DEPTH = 1;
#else
        node_value_arrs::initialize_values_arr({4}, {1}, 4, 2, false);
#endif

        std::vector<double> value_1 = {0.0, 2.0, 3.0, 4.0};
        std::vector<double> test_value_1 = {0.0};

        std::vector<double> value_2 = {1.0, 2.0 * M_PI, 4.0 * M_PI, 6.0 * M_PI};
        std::vector<double> test_value_2 = {5.0};

        _feat_1 = std::make_shared<FeatureNode>(0, "A", value_1, test_value_1, Unit());
        _feat_2 = std::make_shared<FeatureNode>(1, "B", value_1, test_value_1, Unit("m"));
        _feat_3 = std::make_shared<FeatureNode>(2, "C", value_2, test_value_2, Unit());

        _phi = {_feat_1, _feat_2, _feat_3};
        _phi.push_back(std::make_shared<LogNode>(_feat_3, 3, 1e-50, 1e50));
        _phi.push_back(std::make_shared<NegExpNode>(_feat_1, 3, 1e-50, 1e50));
        _phi.push_back(std::make_shared<AddNode>(_feat_1, _feat_3, 3, 1e-50, 1e50));
        _phi.push_back(std::make_shared<SubNode>(_feat_1, _feat_3, 3, 1e-50, 1e50));
    }

    void TearDown() override { node_value_arrs::finalize_values_arr(); }

    node_ptr _feat_1;
    node_ptr _feat_2;
    node_ptr _feat_3;

    node_ptr _node_test;

    std::vector<node_ptr> _phi;
};

TEST_F(ExpNodeTest, GeneratorTest)
{
    unsigned long int feat_ind = _phi.size();

    generateExpNode(_phi, _phi[1], feat_ind, 1e-50, 1e50);
    EXPECT_EQ(_phi.size(), 7) << "(ExpNode created with a feature that is not unitless)";

    generateExpNode(_phi, _phi[0], feat_ind, 1e-50, 1e-40);
    EXPECT_EQ(_phi.size(), 7) << " (ExpNode created with an absolute value above the upper bound)";

    generateExpNode(_phi, _phi[0], feat_ind, 1e3, 1e50);
    EXPECT_EQ(_phi.size(), 7) << " (ExpNode created with an absolute value below the lower bound)";

    generateExpNode(_phi, _phi[3], feat_ind, 1e-50, 1e50);
    EXPECT_EQ(_phi.size(), 7) << " (ExpNode created from a LogNode)";

    generateExpNode(_phi, _phi[4], feat_ind, 1e-50, 1e50);
    EXPECT_EQ(_phi.size(), 7) << " (ExpNode created from a NegExpNode)";

    generateExpNode(_phi, _phi[5], feat_ind, 1e-50, 1e50);
    EXPECT_EQ(_phi.size(), 7) << " (ExpNode created from a AddNode)";

    generateExpNode(_phi, _phi[6], feat_ind, 1e-50, 1e50);
    EXPECT_EQ(_phi.size(), 7) << " (ExpNode created from a SubNode)";

    generateExpNode(_phi, _phi[0], feat_ind, 1e-50, 1e50);
    EXPECT_EQ(_phi.size(), 8) << " (Failure to create a valid feature)";

    generateExpNode(_phi, _phi[4], feat_ind, 1e-50, 1e50);
    EXPECT_EQ(_phi.size(), 8) << " (ExpNode created from another ExpNode)";
}

TEST_F(ExpNodeTest, ConstructorTest)
{
    unsigned long int feat_ind = _phi.size();

    try
    {
        _node_test = std::make_shared<ExpNode>(_phi[1], feat_ind, 1e-50, 1e50);
        EXPECT_TRUE(false) << "(ExpNode created with a feature that is not unitless)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<ExpNode>(_phi[0], feat_ind, 1e-50, 1e-40);
        EXPECT_TRUE(false) << " (ExpNode created with an absolute value above the upper bound)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<ExpNode>(_phi[0], feat_ind, 1e3, 1e50);
        EXPECT_TRUE(false) << " (ExpNode created with an absolute value below the lower bound)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<ExpNode>(_phi[3], feat_ind, 1e-50, 1e50);
        EXPECT_TRUE(false) << " (ExpNode created from a LogNode)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<ExpNode>(_phi[4], feat_ind, 1e-50, 1e50);
        EXPECT_TRUE(false) << " (ExpNode created from a NegExpNode)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<ExpNode>(_phi[5], feat_ind, 1e-50, 1e50);
        EXPECT_TRUE(false) << " (ExpNode created from a AddNode)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<ExpNode>(_phi[6], feat_ind, 1e-50, 1e50);
        EXPECT_TRUE(false) << " (ExpNode created from a SubNode)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<ExpNode>(_phi[0], feat_ind, 1e-50, 1e50);
        ++feat_ind;
    }
    catch (const InvalidFeatureException& e)
    {
        ASSERT_TRUE(false) << " (Failure to create a valid feature)";
    }

    try
    {
        _node_test = std::make_shared<ExpNode>(_node_test, feat_ind, 1e-50, 1e50);
        EXPECT_TRUE(false) << " (ExpNode created from another ExpNode)";
    }
    catch (const InvalidFeatureException& e)
    {
    }
}

TEST_F(ExpNodeTest, HardCopyTest)
{
    _node_test = std::make_shared<ExpNode>(_phi[0], 5, 1e-50, 1e50);

    node_ptr copy_test = _node_test->hard_copy();

    EXPECT_EQ(copy_test->rung(), 1);

    EXPECT_EQ(copy_test->value_ptr()[0], 1.0);
    EXPECT_EQ(copy_test->test_value_ptr()[0], 1.0);

    EXPECT_EQ(copy_test->value()[0], 1.0);
    EXPECT_EQ(copy_test->test_value()[0], 1.0);

    EXPECT_STREQ(copy_test->unit().toString().c_str(), "Unitless");

    EXPECT_STREQ(copy_test->expr().c_str(), "exp(A)");
    EXPECT_STREQ(copy_test->postfix_expr().c_str(), "0|exp");
}

TEST_F(ExpNodeTest, AttributesTest)
{
    if (mpi_setup::comm->rank() == 0)
    {
        _node_test = std::make_shared<ExpNode>(_phi[0], 5, 1e-50, 1e50);
    }
    mpi::broadcast(*mpi_setup::comm, _node_test, 0);

    EXPECT_EQ(_node_test->rung(), 1);

    EXPECT_EQ(_node_test->value_ptr()[0], 1.0);
    EXPECT_EQ(_node_test->test_value_ptr()[0], 1.0);

    EXPECT_EQ(_node_test->value()[0], 1.0);
    EXPECT_EQ(_node_test->test_value()[0], 1.0);

    EXPECT_STREQ(_node_test->unit().toString().c_str(), "Unitless");

    EXPECT_STREQ(_node_test->expr().c_str(), "exp(A)");
    EXPECT_STREQ(_node_test->postfix_expr().c_str(), "0|exp");

#ifdef PARAMETERIZE
    EXPECT_THROW(_node_test->param_pointer(), std::logic_error);
    EXPECT_EQ(_node_test->n_params(), 0);

    nlopt_wrapper::MAX_PARAM_DEPTH = 0;
    EXPECT_EQ(_node_test->n_params_possible(), 0);
    nlopt_wrapper::MAX_PARAM_DEPTH = 1;
    EXPECT_EQ(_node_test->n_params_possible(), 2);

    EXPECT_EQ(_node_test->parameters().size(), 0);

    std::vector<double> params = {1.0, 0.0};
    std::vector<double> dfdp(4, 0.0);
    std::vector<double> grad(2, 0.0);
    EXPECT_THROW(_node_test->gradient(grad.data(), dfdp.data()), std::logic_error);

    _node_test->set_parameters({}, true);
    _node_test->set_parameters(nullptr);
    _node_test->param_derivative(params.data(), dfdp.data());

    EXPECT_LT(std::abs(dfdp[0] - _node_test->value()[0]), 1e-10);
    EXPECT_LT(std::abs(dfdp[1] - _node_test->value()[1]), 1e-10);

    EXPECT_LT(std::abs(dfdp[2] - _node_test->value()[2]), 1e-10);
    EXPECT_LT(std::abs(dfdp[3] - _node_test->value()[3]), 1e-10);
#endif
}
}  // namespace
