// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "external/third_party_gtest.hpp"

#include "feature_creation/node/FeatureNode.hpp"
#include "feature_creation/node/value_storage/nodes_value_containers.hpp"

namespace
{
class FeatureNodeTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
        node_value_arrs::initialize_values_arr({8}, {1}, 4, 2, false);

        _value_1 = {2, 4, 4, 4, 5, 5, 7, 9};
        _test_value_1 = {5.0};

        _value_2 = {10.0, 10.0, 10.0, 1.0, 10.0, 10.0, 10.0, 1.0};
        _test_value_2 = {10.0};

        _value_3 = {1.0, 2.0, 3.0, 1.0, 1.0, 4.0, 5.0, 1.0};
        _test_value_3 = {5.0};

        _value_4 = {
            1.0,
            2.0,
            3.0,
        };
        _test_value_4 = {};
    }

    void TearDown() override { node_value_arrs::finalize_values_arr(); }

    std::vector<double> _value_1;
    std::vector<double> _test_value_1;

    std::vector<double> _value_2;
    std::vector<double> _test_value_2;

    std::vector<double> _value_3;
    std::vector<double> _test_value_3;

    std::vector<double> _value_4;
    std::vector<double> _test_value_4;
};

TEST_F(FeatureNodeTest, ConstructorTest)
{
    std::shared_ptr<FeatureNode> feat_1 = std::make_shared<FeatureNode>(
        0, "A", _value_1, _test_value_1, Unit("m"));
    std::shared_ptr<FeatureNode> feat_2 = std::make_shared<FeatureNode>(
        1, "B", _value_2, _test_value_2, Unit());
    std::shared_ptr<FeatureNode> feat_3 = std::make_shared<FeatureNode>(
        2, "C", _value_3, _test_value_3, Unit("m"));
    node_ptr feat_4 = feat_1->hard_copy();

    try
    {
        node_ptr feat_5 = std::make_shared<FeatureNode>(3, "D", _value_4, _test_value_3, Unit("m"));
        EXPECT_TRUE(false)
            << " (Creation of FeatureNode with wrong number of samples in the training data)";
    }
    catch (const std::logic_error& e)
    {
    }

    try
    {
        node_ptr feat_5 = std::make_shared<FeatureNode>(3, "D", _value_3, _test_value_4, Unit("m"));
        EXPECT_TRUE(false)
            << " (Creation of FeatureNode with wrong number of samples in the test data)";
    }
    catch (const std::logic_error& e)
    {
    }

    node_ptr feat_5 = std::make_shared<FeatureNode>(3, "D", _value_3, _test_value_3, Unit("m"));

    EXPECT_FALSE(feat_1->is_const());
    EXPECT_FALSE(feat_1->is_nan());
    EXPECT_STREQ(feat_1->unit().toString().c_str(), "m");
    EXPECT_STREQ(feat_1->expr().c_str(), "A");
    EXPECT_STREQ(feat_1->postfix_expr().c_str(), "0");
    EXPECT_EQ(feat_1->value()[0], _value_1[0]);
    EXPECT_EQ(feat_1->test_value()[0], _test_value_1[0]);
    EXPECT_EQ(feat_1->value_ptr()[0], _value_1[0]);
    EXPECT_EQ(feat_1->test_value_ptr()[0], _test_value_1[0]);
    EXPECT_EQ(feat_1->rung(), 0);
    EXPECT_EQ(feat_1->n_feats(), 0);
    EXPECT_EQ(feat_1->sort_score(10), 0);

    EXPECT_FALSE(feat_2->is_const());
    EXPECT_FALSE(feat_2->is_nan());
    EXPECT_STREQ(feat_2->unit().toString().c_str(), "Unitless");
    EXPECT_STREQ(feat_2->expr().c_str(), "B");
    EXPECT_STREQ(feat_2->postfix_expr().c_str(), "1");
    EXPECT_EQ(feat_2->value()[0], _value_2[0]);
    EXPECT_EQ(feat_2->test_value()[0], _test_value_2[0]);
    EXPECT_EQ(feat_2->value_ptr()[0], _value_2[0]);
    EXPECT_EQ(feat_2->test_value_ptr()[0], _test_value_2[0]);
    EXPECT_EQ(feat_2->rung(), 0);
    EXPECT_EQ(feat_2->n_feats(), 0);
    EXPECT_EQ(feat_2->sort_score(10), 1);

    EXPECT_FALSE(feat_3->is_const());
    EXPECT_FALSE(feat_3->is_nan());
    EXPECT_STREQ(feat_3->unit().toString().c_str(), "m");
    EXPECT_STREQ(feat_3->expr().c_str(), "C");
    EXPECT_STREQ(feat_3->postfix_expr().c_str(), "2");
    EXPECT_EQ(feat_3->value()[0], _value_3[0]);
    EXPECT_EQ(feat_3->test_value()[0], _test_value_3[0]);
    EXPECT_EQ(feat_3->value_ptr()[0], _value_3[0]);
    EXPECT_EQ(feat_3->test_value_ptr()[0], _test_value_3[0]);
    EXPECT_EQ(feat_3->rung(), 0);
    EXPECT_EQ(feat_3->n_feats(), 0);
    EXPECT_EQ(feat_3->sort_score(10), 2);

    EXPECT_FALSE(feat_4->is_const());
    EXPECT_FALSE(feat_4->is_nan());
    EXPECT_STREQ(feat_4->unit().toString().c_str(), "m");
    EXPECT_STREQ(feat_4->expr().c_str(), "A");
    EXPECT_STREQ(feat_4->postfix_expr().c_str(), "0");
    EXPECT_EQ(feat_4->value()[0], _value_1[0]);
    EXPECT_EQ(feat_4->test_value()[0], _test_value_1[0]);
    EXPECT_EQ(feat_4->value_ptr()[0], _value_1[0]);
    EXPECT_EQ(feat_4->test_value_ptr()[0], _test_value_1[0]);
    EXPECT_EQ(feat_4->rung(), 0);
    EXPECT_EQ(feat_4->n_feats(), 0);
    EXPECT_EQ(feat_4->sort_score(10), 0);
}

TEST_F(FeatureNodeTest, DefaultFunctionTests)
{
    std::shared_ptr<FeatureNode> feat_1 = std::make_shared<FeatureNode>(
        0, "A", _value_1, _test_value_1, Unit("m"));
    node_ptr feat_2 = feat_1->hard_copy();

    std::vector<node_ptr> phi = {feat_1, feat_2};
    feat_1->reset_feats(phi);

#ifdef PARAMETERIZE
    feat_1->set_parameters({}, true);
    feat_1->set_parameters(nullptr);

    std::vector<double> params = {1.0, 0.0};
    std::vector<double> dfdp(4, 0.0);
    feat_1->param_derivative(params.data(), dfdp.data());

    EXPECT_EQ(dfdp[0], 0.0);
    EXPECT_EQ(dfdp[1], 0.0);

    EXPECT_THROW(feat_1->gradient(params.data(), dfdp.data()), std::logic_error);

    feat_1->set_value(nullptr);
    feat_1->set_test_value(nullptr);
#endif
}

TEST_F(FeatureNodeTest, StandValTests)
{
    std::vector<double> test_std = {-1.5, -0.5, -0.5, -0.5, 0, 0, 1, 2};

    std::shared_ptr<FeatureNode> feat_1 = std::make_shared<FeatureNode>(
        0, "A", _value_1, _test_value_1, Unit("m"));

    double* stand_value_ptr = feat_1->stand_value_ptr();
    std::transform(
        test_std.begin(), test_std.end(), stand_value_ptr, test_std.begin(), std::minus<double>());
    EXPECT_TRUE(std::all_of(
        test_std.begin(), test_std.end(), [](double val) { return std::abs(val) < 1e-10; }));

    stand_value_ptr = feat_1->stand_test_value_ptr();
    EXPECT_LT(std::abs(*stand_value_ptr), 1e-10);
}
}  // namespace
