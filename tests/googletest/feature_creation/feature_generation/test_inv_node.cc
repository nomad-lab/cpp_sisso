// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "external/third_party_gtest.hpp"
#include <math.h>

#include "feature_creation/node/FeatureNode.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/div/divide.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/exp/exponential.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/inv/inverse.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/neg_exp/negative_exponential.hpp"
#include "feature_creation/node/value_storage/nodes_value_containers.hpp"
#include "mpi_interface/MPI_Interface.hpp"

namespace
{
class InvNodeTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
#ifdef PARAMETERIZE
        node_value_arrs::initialize_values_arr({4}, {1}, 4, 2, true);
        nlopt_wrapper::MAX_PARAM_DEPTH = 1;
#else
        node_value_arrs::initialize_values_arr({4}, {1}, 4, 2, false);
#endif

        std::vector<double> value_1 = {1.0, 2.0, 3.0, 8.0};
        std::vector<double> test_value_1 = {2.0};

        std::vector<double> value_2 = {1.0, 2.0 * M_PI, 4.0 * M_PI, 6.0 * M_PI};
        std::vector<double> test_value_2 = {5.0};

        std::vector<double> value_3 = {0.0, 2.0, 3.0, 4.0};
        std::vector<double> test_value_3 = {0.0};

        _feat_1 = std::make_shared<FeatureNode>(0, "A", value_1, test_value_1, Unit("m"));
        _feat_2 = std::make_shared<FeatureNode>(1, "B", value_1, test_value_1, Unit());
        _feat_3 = std::make_shared<FeatureNode>(2, "C", value_2, test_value_2, Unit("m"));
        _feat_4 = std::make_shared<FeatureNode>(3, "D", value_3, test_value_3, Unit("m"));

        _phi = {_feat_1, _feat_2, _feat_3, _feat_4};
        _phi.push_back(std::make_shared<ExpNode>(_feat_2, 6, 1e-50, 1e50));
        _phi.push_back(std::make_shared<NegExpNode>(_feat_2, 10, 1e-50, 1e50));
        _phi.push_back(std::make_shared<DivNode>(_feat_1, _feat_3, 11, 1e-50, 1e50));
    }

    void TearDown() override { node_value_arrs::finalize_values_arr(); }

    node_ptr _feat_1;
    node_ptr _feat_2;
    node_ptr _feat_3;
    node_ptr _feat_4;

    node_ptr _node_test;

    std::vector<node_ptr> _phi;
};

TEST_F(InvNodeTest, GeneratorTest)
{
    unsigned long int feat_ind = _phi.size();
    int phi_sz = _phi.size();

    generateInvNode(_phi, _phi[3], feat_ind, 1e-50, 1e-40);
    EXPECT_EQ(_phi.size(), phi_sz)
        << " (InvNode created with a feature that has a value equal 0.0)";

    generateInvNode(_phi, _phi[0], feat_ind, 1e-50, 1e-40);
    EXPECT_EQ(_phi.size(), phi_sz)
        << " (InvNode created with an absolute value above the upper bound)";

    generateInvNode(_phi, _phi[0], feat_ind, 1e10, 1e50);
    EXPECT_EQ(_phi.size(), phi_sz)
        << " (InvNode created with an absolute value below the lower bound)";

    generateInvNode(_phi, _phi[4], feat_ind, 1e-50, 1e50);
    EXPECT_EQ(_phi.size(), phi_sz) << " (InvNode created from a ExpNode)";

    generateInvNode(_phi, _phi[5], feat_ind, 1e-50, 1e50);
    EXPECT_EQ(_phi.size(), phi_sz) << " (InvNode created from a NegExpNode)";

    generateInvNode(_phi, _phi[6], feat_ind, 1e-50, 1e50);
    EXPECT_EQ(_phi.size(), phi_sz) << " (InvNode created from a DivNode)";

    generateInvNode(_phi, _phi[0], feat_ind, 1e-50, 1e50);
    ++phi_sz;
    EXPECT_EQ(_phi.size(), phi_sz) << " (Failure to create a valid feature)";

    generateInvNode(_phi, _phi[7], feat_ind, 1e-50, 1e50);
    EXPECT_EQ(_phi.size(), phi_sz) << " (InvNode created from another InvNode)";
}

TEST_F(InvNodeTest, ConstructorTest)
{
    unsigned long int feat_ind = _phi.size();

    try
    {
        _node_test = std::make_shared<InvNode>(_phi[3], feat_ind, 1e-50, 1e-40);
        EXPECT_TRUE(false) << " (InvNode created with a feature that has a value equal to 0.0)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<InvNode>(_phi[0], feat_ind, 1e-50, 1e-40);
        EXPECT_TRUE(false) << " (InvNode created with an absolute value above the upper bound)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<InvNode>(_phi[0], feat_ind, 1e10, 1e50);
        EXPECT_TRUE(false) << " (InvNode created with an absolute value below the lower bound)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<InvNode>(_phi[4], feat_ind, 1e-50, 1e50);
        EXPECT_TRUE(false) << " (InvNode created from a ExpNode)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<InvNode>(_phi[5], feat_ind, 1e-50, 1e50);
        EXPECT_TRUE(false) << " (InvNode created from a NegExpNode)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<InvNode>(_phi[6], feat_ind, 1e-50, 1e50);
        EXPECT_TRUE(false) << " (InvNode created from a DivNode)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<InvNode>(_phi[0], feat_ind, 1e-50, 1e50);
        ++feat_ind;
    }
    catch (const InvalidFeatureException& e)
    {
        ASSERT_TRUE(false) << " (Failure to create a valid feature)";
    }

    try
    {
        _node_test = std::make_shared<InvNode>(_node_test, feat_ind, 1e-50, 1e50);
        EXPECT_TRUE(false) << " (InvNode created from another InvNode)";
    }
    catch (const InvalidFeatureException& e)
    {
    }
}

TEST_F(InvNodeTest, HardCopyTest)
{
    _node_test = std::make_shared<InvNode>(_phi[0], 5, 1e-50, 1e50);

    node_ptr copy_test = _node_test->hard_copy();

    EXPECT_EQ(copy_test->rung(), 1);

    EXPECT_EQ(copy_test->value_ptr()[1], 0.5);
    EXPECT_EQ(copy_test->test_value_ptr()[0], 0.5);

    EXPECT_EQ(copy_test->value()[1], 0.5);
    EXPECT_EQ(copy_test->test_value()[0], 0.50);

    EXPECT_STREQ(copy_test->unit().toString().c_str(), "m^-1");

    EXPECT_STREQ(copy_test->expr().c_str(), "(1.0 / A)");
    EXPECT_STREQ(copy_test->postfix_expr().c_str(), "0|inv");
}

TEST_F(InvNodeTest, AttributesTest)
{
    if (mpi_setup::comm->rank() == 0)
    {
        _node_test = std::make_shared<InvNode>(_phi[0], 5, 1e-50, 1e50);
    }
    mpi::broadcast(*mpi_setup::comm, _node_test, 0);

    EXPECT_EQ(_node_test->rung(), 1);

    EXPECT_EQ(_node_test->value_ptr()[1], 0.5);
    EXPECT_EQ(_node_test->test_value_ptr()[0], 0.5);

    EXPECT_EQ(_node_test->value()[1], 0.5);
    EXPECT_EQ(_node_test->test_value()[0], 0.50);

    EXPECT_STREQ(_node_test->unit().toString().c_str(), "m^-1");

    EXPECT_STREQ(_node_test->expr().c_str(), "(1.0 / A)");
    EXPECT_STREQ(_node_test->postfix_expr().c_str(), "0|inv");

#ifdef PARAMETERIZE
    EXPECT_THROW(_node_test->param_pointer(), std::logic_error);
    EXPECT_EQ(_node_test->n_params(), 0);

    nlopt_wrapper::MAX_PARAM_DEPTH = 0;
    EXPECT_EQ(_node_test->n_params_possible(), 0);
    nlopt_wrapper::MAX_PARAM_DEPTH = 1;
    EXPECT_EQ(_node_test->n_params_possible(), 2);

    EXPECT_EQ(_node_test->parameters().size(), 0);

    std::vector<double> params = {1.0, 0.0};
    std::vector<double> dfdp(4, 0.0);
    std::vector<double> grad(2, 0.0);
    EXPECT_THROW(_node_test->gradient(grad.data(), dfdp.data()), std::logic_error);

    _node_test->set_parameters({}, true);
    _node_test->set_parameters(nullptr);
    _node_test->param_derivative(params.data(), dfdp.data());

    EXPECT_LT(std::abs(dfdp[0] + 1.0), 1e-10);
    EXPECT_LT(std::abs(dfdp[1] + 1.0 / 4.0), 1e-10);

    EXPECT_LT(std::abs(dfdp[2] + 1.0 / 9.0), 1e-10);
    EXPECT_LT(std::abs(dfdp[3] + 1.0 / 64.0), 1e-10);
#endif
}
}  // namespace
