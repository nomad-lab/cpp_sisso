// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "external/third_party_gtest.hpp"

#include "feature_creation/node/FeatureNode.hpp"
#include "feature_creation/node/ModelNode.hpp"
#include "feature_creation/node/value_storage/nodes_value_containers.hpp"
#include "mpi_interface/MPI_Interface.hpp"

namespace
{
class ModelNodeTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
        node_value_arrs::initialize_values_arr({4}, {1}, 4, 2, false);

        _value_1 = {1.0, 2.0, 3.0, 4.0};
        _test_value_1 = {5.0};

        _value_2 = {10.0, 10.0, 10.0, 1.0};
        _test_value_2 = {10.0};

        _value_3 = {1.0, 2.0, 3.0, 1.0};
        _test_value_3 = {5.0};
    }

    void TearDown() override { node_value_arrs::finalize_values_arr(); }

    std::vector<double> _value_1;
    std::vector<double> _test_value_1;

    std::vector<double> _value_2;
    std::vector<double> _test_value_2;

    std::vector<double> _value_3;
    std::vector<double> _test_value_3;
};

TEST_F(ModelNodeTest, ConstructorTest)
{
    std::shared_ptr<ModelNode> feat_1 = std::make_shared<ModelNode>(
        0,
        1,
        "A",
        "$A$",
        "0",
        "A",
        _value_1,
        _test_value_1,
        std::vector<std::string>(1, "A"),
        Unit("m"));
    std::shared_ptr<ModelNode> feat_2 = std::make_shared<ModelNode>(
        1,
        1,
        "B",
        "$B$",
        "1",
        "B",
        _value_2,
        _test_value_2,
        std::vector<std::string>(1, "B"),
        Unit());
    std::shared_ptr<ModelNode> feat_3 = std::make_shared<ModelNode>(
        2,
        1,
        "C",
        "$C$",
        "2",
        "C",
        _value_3,
        _test_value_3,
        std::vector<std::string>(1, "C"),
        Unit("m"));
    node_ptr feat_4 = feat_1->hard_copy();

    EXPECT_FALSE(feat_1->is_const());
    EXPECT_FALSE(feat_1->is_nan());
    EXPECT_STREQ(feat_1->unit().toString().c_str(), "m");
    EXPECT_STREQ(feat_1->expr().c_str(), "A");
    EXPECT_STREQ(feat_1->postfix_expr().c_str(), "0");
    EXPECT_EQ(feat_1->value()[0], _value_1[0]);
    EXPECT_EQ(feat_1->test_value()[0], _test_value_1[0]);
    EXPECT_EQ(feat_1->rung(), 1);
    EXPECT_EQ(feat_1->n_feats(), 0);
    EXPECT_EQ(feat_1->n_feats(), 0);

    EXPECT_FALSE(feat_2->is_const());
    EXPECT_FALSE(feat_2->is_nan());
    EXPECT_STREQ(feat_2->unit().toString().c_str(), "Unitless");
    EXPECT_STREQ(feat_2->expr().c_str(), "B");
    EXPECT_STREQ(feat_2->postfix_expr().c_str(), "1");
    EXPECT_EQ(feat_2->value()[0], _value_2[0]);
    EXPECT_EQ(feat_2->test_value()[0], _test_value_2[0]);
    EXPECT_EQ(feat_2->rung(), 1);
    EXPECT_EQ(feat_2->n_feats(), 0);
    EXPECT_EQ(feat_2->n_feats(), 0);

    EXPECT_FALSE(feat_3->is_const());
    EXPECT_FALSE(feat_3->is_nan());
    EXPECT_STREQ(feat_3->unit().toString().c_str(), "m");
    EXPECT_STREQ(feat_3->expr().c_str(), "C");
    EXPECT_STREQ(feat_3->postfix_expr().c_str(), "2");
    EXPECT_EQ(feat_3->value()[0], _value_3[0]);
    EXPECT_EQ(feat_3->test_value()[0], _test_value_3[0]);
    EXPECT_EQ(feat_3->rung(), 1);
    EXPECT_EQ(feat_3->n_feats(), 0);
    EXPECT_EQ(feat_3->n_feats(), 0);

    EXPECT_FALSE(feat_4->is_const());
    EXPECT_FALSE(feat_4->is_nan());
    EXPECT_STREQ(feat_4->unit().toString().c_str(), "m");
    EXPECT_STREQ(feat_4->expr().c_str(), "A");
    EXPECT_STREQ(feat_4->postfix_expr().c_str(), "0");
    EXPECT_EQ(feat_4->value()[0], _value_1[0]);
    EXPECT_EQ(feat_4->test_value()[0], _test_value_1[0]);
    EXPECT_EQ(feat_4->rung(), 1);
    EXPECT_EQ(feat_4->n_feats(), 0);
    EXPECT_EQ(feat_4->n_feats(), 0);

    try
    {
        std::shared_ptr<ModelNode> feat_5 = std::make_shared<ModelNode>(
            4,
            1,
            "C^2",
            "$C^2$",
            "2|squ",
            "C.^2",
            _value_3,
            _test_value_3,
            std::vector<std::string>(1, "C"),
            Unit("m"));
        EXPECT_TRUE(false) << " (Creation of ModelNode with wrong postfix_expr.)";
    }
    catch (const std::logic_error& e)
    {
    }

    feat_1->set_value();
    EXPECT_EQ(feat_1->value()[0], _value_1[0]);

    feat_1->set_test_value();
    EXPECT_EQ(feat_1->test_value()[0], _test_value_1[0]);

#ifdef PARAMETERIZE
    try
    {
        std::shared_ptr<ModelNode> feat_5 = std::make_shared<ModelNode>(
            4,
            1,
            "(C + 1.0)^2",
            "$\\left(C + 1\\right)^2$",
            "2|sq: 1.0, 1.0, 2.0",
            "(C + 1).^2",
            _value_3,
            _test_value_3,
            std::vector<std::string>(1, "C"),
            Unit("m"));
        EXPECT_TRUE(false) << " (Creation of ModelNode with wrong postfix_expr.)";
    }
    catch (const std::logic_error& e)
    {
    }
    std::vector<double> params = {1.0, 1.0};
    EXPECT_STREQ(feat_1->matlab_fxn_expr(params.data()).c_str(), feat_1->matlab_fxn_expr().c_str());
#endif
}

TEST_F(ModelNodeTest, EvalTest)
{
    std::shared_ptr<ModelNode> feat_1 = std::make_shared<ModelNode>(
        0,
        1,
        "A^2",
        "$A^2$",
        "0|sq",
        "A^2",
        _value_1,
        _test_value_1,
        std::vector<std::string>(1, "A"),
        Unit("m"));

    std::vector<double> pt = {2.0};
    std::vector<std::vector<double>> pts = {{2.0}};

    std::map<std::string, double> pt_dct;
    pt_dct["A"] = 2.0;

    std::map<std::string, std::vector<double>> pts_dct;
    pts_dct["A"] = {2.0};

    EXPECT_LT(std::abs(feat_1->eval(pt) - 4.0), 1e-10);
    EXPECT_LT(std::abs(feat_1->eval(pt_dct) - 4.0), 1e-10);

    double val = feat_1->eval(pts)[0];
    EXPECT_LT(std::abs(val - 4.0), 1e-10);

    val = feat_1->eval(pts_dct)[0];
    EXPECT_LT(std::abs(val - 4.0), 1e-10);
}

TEST_F(ModelNodeTest, EvalFailTest)
{
    std::shared_ptr<ModelNode> feat_1 = std::make_shared<ModelNode>(
        0,
        1,
        "A^2",
        "$A^2$",
        "1|0|sq",
        "A^2",
        _value_1,
        _test_value_1,
        std::vector<std::string>(1, "A"),
        Unit("m"));

    EXPECT_THROW(feat_1->eval(_value_1.data()), std::logic_error);
    EXPECT_THROW(feat_1->eval((_value_1)), std::logic_error);

    std::vector<std::vector<double>> values = {{1.0}, {1.0}};
    EXPECT_THROW(feat_1->eval(values.data()), std::logic_error);
    EXPECT_THROW(feat_1->eval(values), std::logic_error);

    std::vector<std::string> x_in = {"A", "B"};
    feat_1 = std::make_shared<ModelNode>(
        0, 1, "A + B", "$A + B$", "0|add", "(A + B)", _value_1, _test_value_1, x_in, Unit("m"));
    EXPECT_THROW(feat_1->eval(_value_1), std::logic_error);
    EXPECT_THROW(feat_1->eval(values.data()), std::logic_error);

    values = {{1.0}, {1.0, 1.0}};
    EXPECT_THROW(feat_1->eval(values), std::logic_error);

    std::map<std::string, double> pt_dct;
    pt_dct["B"] = 1.0;

    std::map<std::string, std::vector<double>> pts_dct;
    pt_dct["B"] = {1.0};
    EXPECT_THROW(feat_1->eval(pt_dct), std::logic_error);
}
}  // namespace
