// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "external/third_party_gtest.hpp"

#include <feature_creation/units/Unit.hpp>

namespace
{
//test unit's str constructor
TEST(Unit, StringConstructor)
{
    Unit u_1("m");
    Unit u_2("s");

    EXPECT_NE(u_1, u_2);
    EXPECT_NE(u_1, Unit());
    EXPECT_EQ(u_1, Unit(u_1));
    EXPECT_EQ(u_2, Unit(u_2));

    EXPECT_STREQ(u_1.toString().c_str(), "m");
    EXPECT_STREQ(u_1.toLatexString().c_str(), "m");

    EXPECT_STREQ(u_2.toString().c_str(), "s");
    EXPECT_STREQ(u_2.toLatexString().c_str(), "s");

    EXPECT_EQ(u_1 / u_2, Unit("m/s"));
    EXPECT_EQ(u_1 / u_1, Unit());
    EXPECT_EQ(u_1 * u_2, Unit("m*s"));
    EXPECT_EQ(u_1 * u_1, Unit("m^2.0"));
    EXPECT_EQ(u_1 ^ 2.0, Unit("m^2.0"));
    EXPECT_EQ(u_1 ^ 0.0, Unit(""));
    EXPECT_EQ(u_1.inverse(), Unit("1 / m"));

    u_2 /= u_1;
    EXPECT_EQ(u_2, Unit("s/m"));

    u_1 *= u_2;
    EXPECT_EQ(u_1, Unit("s"));

    u_1 /= u_2;
    EXPECT_EQ(u_1, Unit("m"));

    u_2 *= u_2;
    EXPECT_EQ(u_2, Unit("s * s/m^2"));
    EXPECT_EQ(u_2.toLatexString(), "m$^\\mathrm{-2}$s$^\\mathrm{2}$");

    EXPECT_EQ(Unit().toLatexString(), "Unitless");
}

//test mean calculations
TEST(Unit, MapConstructor)
{
    std::map<std::string, double> dct_1;
    std::map<std::string, double> dct_2;

    dct_1["m"] = 1.0;
    dct_2["s"] = 1.0;
    Unit u_1(dct_1);
    Unit u_2(dct_2);

    EXPECT_NE(u_1, u_2);
    EXPECT_NE(u_1, Unit());
    EXPECT_EQ(u_1, Unit(u_1));
    EXPECT_EQ(u_2, Unit(u_2));

    EXPECT_STREQ(u_1.toString().c_str(), "m");
    EXPECT_STREQ(u_1.toLatexString().c_str(), "m");

    EXPECT_STREQ(u_2.toString().c_str(), "s");
    EXPECT_STREQ(u_2.toLatexString().c_str(), "s");

    EXPECT_EQ(u_1 / u_2, Unit("m/s"));
    EXPECT_EQ(u_1 / u_1, Unit());
    EXPECT_EQ(u_1 * u_2, Unit("m*s"));
    EXPECT_EQ(u_1 * u_1, Unit("m^2.0"));
    EXPECT_EQ(u_1 ^ 2.0, Unit("m^2.0"));
    EXPECT_EQ(u_1 ^ 0.0, Unit(""));
    EXPECT_EQ(u_1.inverse(), Unit("1 / m"));

    u_2 /= u_1;
    EXPECT_EQ(u_2, Unit("s/m"));

    u_1 *= u_2;
    EXPECT_EQ(u_1, Unit("s"));

    u_1 /= u_2;
    EXPECT_EQ(u_1, Unit("m"));

    u_2 *= u_2;
    EXPECT_EQ(u_2, Unit("s * s/m^2"));
    EXPECT_EQ(u_2.toLatexString(), "m$^\\mathrm{-2}$s$^\\mathrm{2}$");

    EXPECT_EQ(Unit().toLatexString(), "Unitless");
}
}  // namespace
