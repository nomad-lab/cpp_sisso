// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "external/third_party_gtest.hpp"

#include <feature_creation/node/value_storage/nodes_value_containers.hpp>
#include <utils/project.hpp>

namespace
{
//test mean calculations
TEST(ValueStorage, ValueStorageTest)
{
    EXPECT_THROW(node_value_arrs::initialize_values_arr({5}, {2}, 2, -2, true), std::logic_error);

    node_value_arrs::initialize_values_arr({5}, {2}, 2, 2, true);

    EXPECT_THROW(node_value_arrs::set_task_sz_train({20}), std::logic_error);
    EXPECT_THROW(node_value_arrs::set_task_sz_test({6}), std::logic_error);

    EXPECT_EQ(node_value_arrs::N_SAMPLES, 5);
    EXPECT_EQ(node_value_arrs::N_SAMPLES_TEST, 2);
    EXPECT_EQ(node_value_arrs::N_RUNGS_STORED, 0);
    EXPECT_EQ(node_value_arrs::N_STORE_FEATURES, 2);
    EXPECT_EQ(node_value_arrs::N_OP_SLOTS, 6);
    EXPECT_EQ(node_value_arrs::MAX_RUNG, 2);
    EXPECT_EQ(node_value_arrs::VALUES_ARR.size(), 10);
    EXPECT_EQ(node_value_arrs::TEST_VALUES_ARR.size(), 4);
    EXPECT_EQ(node_value_arrs::TEMP_STORAGE_ARR.size(),
              node_value_arrs::MAX_N_THREADS * (6 * 2 + 1) * 5);
    EXPECT_EQ(node_value_arrs::TEMP_STORAGE_REG.size(),
              node_value_arrs::MAX_N_THREADS * (6 * 2 + 1));
    EXPECT_EQ(node_value_arrs::TEMP_STORAGE_TEST_ARR.size(),
              node_value_arrs::MAX_N_THREADS * (6 * 2 + 1) * 2);
    EXPECT_EQ(node_value_arrs::TEMP_STORAGE_TEST_REG.size(),
              node_value_arrs::MAX_N_THREADS * (6 * 2 + 1));

    EXPECT_EQ(node_value_arrs::STANDARDIZED_D_MATRIX.size(), 0);
    EXPECT_EQ(node_value_arrs::STANDARDIZED_STORAGE_ARR.size(),
              node_value_arrs::MAX_N_THREADS * 2 * 3 * 5);
    EXPECT_EQ(node_value_arrs::STANDARDIZED_TEST_STORAGE_ARR.size(),
              node_value_arrs::MAX_N_THREADS * 2 * 3 * 2);

    EXPECT_THROW(node_value_arrs::resize_values_arr(10, 2), std::logic_error);
    node_value_arrs::resize_values_arr(1, 2);
    EXPECT_EQ(node_value_arrs::N_SAMPLES, 5);
    EXPECT_EQ(node_value_arrs::N_SAMPLES_TEST, 2);
    EXPECT_EQ(node_value_arrs::N_RUNGS_STORED, 1);
    EXPECT_EQ(node_value_arrs::N_STORE_FEATURES, 2);
    EXPECT_EQ(node_value_arrs::VALUES_ARR.size(), 10);
    EXPECT_EQ(node_value_arrs::TEST_VALUES_ARR.size(), 4);

    node_value_arrs::initialize_d_matrix_arr();
    EXPECT_EQ(node_value_arrs::N_SELECTED, 0);
    EXPECT_EQ(node_value_arrs::D_MATRIX.size(), 0);
    EXPECT_EQ(node_value_arrs::STANDARDIZED_D_MATRIX.size(), 0);

    node_value_arrs::resize_d_matrix_arr(2);
    EXPECT_EQ(node_value_arrs::N_SELECTED, 2);
    EXPECT_EQ(node_value_arrs::D_MATRIX.size(), 10);
    EXPECT_EQ(node_value_arrs::STANDARDIZED_D_MATRIX.size(), 10);

    node_value_arrs::resize_d_matrix_arr(3);
    EXPECT_EQ(node_value_arrs::N_SELECTED, 5);
    EXPECT_EQ(node_value_arrs::D_MATRIX.size(), 25);
    EXPECT_EQ(node_value_arrs::STANDARDIZED_D_MATRIX.size(), 25);

    node_value_arrs::get_value_ptr(1, 1, 0)[1] = 1.0;
    EXPECT_EQ(node_value_arrs::VALUES_ARR[6], 1.0);

#pragma GCC diagnostic ignored "-Wunused-value"
    EXPECT_THROW(node_value_arrs::get_test_value_ptr(1000, 100, 0, 0)[1], std::logic_error);
#pragma GCC diagnostic pop
    node_value_arrs::get_test_value_ptr(0, 1, 0, 0)[1] = 1.0;
    EXPECT_EQ(node_value_arrs::TEST_VALUES_ARR[1], 1.0);

    node_value_arrs::get_value_ptr(10, 141, 2, 0)[0] = 1.0;
    EXPECT_EQ(node_value_arrs::temp_storage_reg(10, 2, 0, false), 141);
    EXPECT_EQ(
        node_value_arrs::access_temp_storage(node_value_arrs::get_op_slot(2, 0, false) * 2)[0],
        1.0);

    node_value_arrs::get_test_value_ptr(10, 141, 2, 0)[0] = 1.0;
    EXPECT_EQ(node_value_arrs::temp_storage_test_reg(10, 2, 0, false), 141);
    EXPECT_EQ(
        node_value_arrs::access_temp_storage_test(node_value_arrs::get_op_slot(2, 0, false) * 2)[0],
        1.0);

    node_value_arrs::get_d_matrix_ptr(1)[0] = 1.0;
    EXPECT_EQ(node_value_arrs::D_MATRIX[5], 1.0);

    node_value_arrs::access_temp_stand_storage(1, false)[0] = 3.0;
    EXPECT_EQ(node_value_arrs::STANDARDIZED_STORAGE_ARR[5 + omp_get_thread_num() * 30], 3.0);

    node_value_arrs::access_temp_stand_storage_test(0, true)[0] = 3.0;
    EXPECT_EQ(node_value_arrs::STANDARDIZED_TEST_STORAGE_ARR[4 + omp_get_thread_num() * 12], 3.0);

#pragma omp parallel
    {
        int sz_reg = (node_value_arrs::N_OP_SLOTS * node_value_arrs::N_PRIMARY_FEATURES + 1);
        std::fill_n(node_value_arrs::TEMP_STORAGE_REG.data() + sz_reg * omp_get_thread_num(),
                    sz_reg,
                    omp_get_thread_num() + 1);
        EXPECT_EQ(node_value_arrs::TEMP_STORAGE_REG[1 + sz_reg * omp_get_thread_num()],
                  omp_get_thread_num() + 1);
        node_value_arrs::clear_temp_reg_thread();
        EXPECT_EQ(node_value_arrs::TEMP_STORAGE_REG[1 + sz_reg * omp_get_thread_num()], 0);
    }

    std::fill_n(
        node_value_arrs::TEMP_STORAGE_REG.data(), node_value_arrs::TEMP_STORAGE_REG.size(), 2.0);
    node_value_arrs::clear_temp_reg();
    EXPECT_EQ(node_value_arrs::TEMP_STORAGE_REG[0], 0);

    std::fill_n(node_value_arrs::TEMP_STORAGE_TEST_REG.data(),
                node_value_arrs::TEMP_STORAGE_REG.size(),
                2.0);
    node_value_arrs::clear_temp_test_reg();
    EXPECT_EQ(node_value_arrs::TEMP_STORAGE_TEST_REG[0], 0);

    node_value_arrs::finalize_values_arr();
    EXPECT_EQ(node_value_arrs::VALUES_ARR.size(), 0);
    EXPECT_EQ(node_value_arrs::TEST_VALUES_ARR.size(), 0);
    EXPECT_EQ(node_value_arrs::TEMP_STORAGE_ARR.size(), 0);
    EXPECT_EQ(node_value_arrs::TEMP_STORAGE_TEST_ARR.size(), 0);
    EXPECT_EQ(node_value_arrs::TEMP_STORAGE_REG.size(), 0);
    EXPECT_EQ(node_value_arrs::TEMP_STORAGE_TEST_REG.size(), 0);
    EXPECT_EQ(node_value_arrs::PARAM_STORAGE_ARR.size(), 0);
    EXPECT_EQ(node_value_arrs::PARAM_STORAGE_TEST_ARR.size(), 0);
    EXPECT_EQ(node_value_arrs::D_MATRIX.size(), 0);
    EXPECT_EQ(node_value_arrs::STANDARDIZED_D_MATRIX.size(), 0);
    EXPECT_EQ(node_value_arrs::STANDARDIZED_STORAGE_ARR.size(), 0);
    EXPECT_EQ(node_value_arrs::STANDARDIZED_TEST_STORAGE_ARR.size(), 0);
    EXPECT_EQ(node_value_arrs::TASK_SZ_TRAIN.size(), 0);
    EXPECT_EQ(node_value_arrs::TASK_START_TRAIN.size(), 0);
    EXPECT_EQ(node_value_arrs::TASK_SZ_TEST.size(), 0);
    EXPECT_EQ(node_value_arrs::N_SELECTED, 0);
    EXPECT_EQ(node_value_arrs::N_SAMPLES, 0);
    EXPECT_EQ(node_value_arrs::N_SAMPLES_TEST, 0);
    EXPECT_EQ(node_value_arrs::N_PRIMARY_FEATURES, 0);
    EXPECT_EQ(node_value_arrs::N_STORE_FEATURES, 0);
    EXPECT_EQ(node_value_arrs::N_RUNGS_STORED, 0);
    EXPECT_EQ(node_value_arrs::MAX_RUNG, 0);

    EXPECT_EQ(node_value_arrs::MAX_N_THREADS, omp_get_max_threads());
    EXPECT_EQ(node_value_arrs::N_OP_SLOTS, 0);
    EXPECT_EQ(node_value_arrs::N_PARAM_OP_SLOTS, 0);

    node_value_arrs::set_task_sz_train({3, 2});
    EXPECT_EQ(node_value_arrs::TASK_SZ_TRAIN[0], 3);
    EXPECT_EQ(node_value_arrs::TASK_START_TRAIN[0], 0);

    node_value_arrs::set_task_sz_test({2, 0});
    EXPECT_EQ(node_value_arrs::TASK_SZ_TEST[0], 2);
}
}  // namespace
