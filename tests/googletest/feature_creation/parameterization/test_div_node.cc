// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#ifdef PARAMETERIZE
#include "external/third_party_gtest.hpp"

#include <random>

#include "feature_creation/node/FeatureNode.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/div/parameterized_divide.hpp"
#include "mpi_interface/MPI_Interface.hpp"

namespace
{
class DivParamNodeTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
        nlopt_wrapper::MAX_PARAM_DEPTH = 2;

        _task_sizes_train = {900};
        _task_sizes_test = {100};

        node_value_arrs::initialize_values_arr(_task_sizes_train, _task_sizes_test, 2, 2, true);

        std::vector<double> value_1(_task_sizes_train[0], 0.0);
        std::vector<double> value_2(_task_sizes_train[0], 0.0);

        std::vector<double> test_value_1(_task_sizes_test[0], 0.0);
        std::vector<double> test_value_2(_task_sizes_test[0], 0.0);

        std::default_random_engine generator(0);
        nlopt::srand(42);
        std::uniform_real_distribution<double> distribution_feats(-50.0, 50.0);
        std::uniform_real_distribution<double> distribution_params(1e-10, 2.50);

        for (int ii = 0; ii < _task_sizes_train[0]; ++ii)
        {
            value_1[ii] = distribution_feats(generator);
            value_2[ii] = std::abs(distribution_feats(generator)) + 1e-10;
        }

        for (int ii = 0; ii < _task_sizes_test[0]; ++ii)
        {
            test_value_1[ii] = distribution_feats(generator);
            test_value_2[ii] = std::abs(distribution_feats(generator)) + 1e-10;
        }

        _feat_1 = std::make_shared<FeatureNode>(0, "A", value_1, test_value_1, Unit("m"));
        _feat_2 = std::make_shared<FeatureNode>(1, "B", value_2, test_value_2, Unit("s"));
        node_ptr feat_3 = std::make_shared<DivNode>(_feat_1, _feat_2, 2);

        _phi = {_feat_1, _feat_2, feat_3};
        _a = distribution_params(generator);
        _alpha = distribution_params(generator);

        _prop = std::vector<double>(_task_sizes_train[0], 0.0);
        _gradient.resize(_task_sizes_train[0] * 4, 1.0);
        _dfdp.resize(_task_sizes_train[0]);

        allowed_op_funcs::div(_task_sizes_train[0],
                              _phi[2]->value_ptr(),
                              _phi[1]->value_ptr(),
                              _alpha,
                              _a,
                              _prop.data());

        _optimizer = nlopt_wrapper::get_optimizer("regression", _task_sizes_train, _prop, 2);
    }

    void TearDown() override { node_value_arrs::finalize_values_arr(); }

    node_ptr _feat_1;
    node_ptr _feat_2;
    node_ptr _node_test;

    std::vector<node_ptr> _phi;
    std::vector<double> _prop;
    std::vector<double> _gradient;
    std::vector<double> _dfdp;
    std::vector<int> _task_sizes_train;
    std::vector<int> _task_sizes_test;

    double _a;
    double _alpha;
    std::shared_ptr<NLOptimizer> _optimizer;
};

TEST_F(DivParamNodeTest, GeneratorTest)
{
    unsigned long int feat_ind = _phi.size();

    generateDivParamNode(_phi, _phi[0], _phi[1], feat_ind, 1e-50, 1e-40, _optimizer);
    EXPECT_EQ(_phi.size(), 3)
        << " (DivParamNode created with an absolute value above the upper bound)";

    generateDivParamNode(_phi, _phi[0], _phi[1], feat_ind, 1e49, 1e50, _optimizer);
    EXPECT_EQ(_phi.size(), 3)
        << " (DivParamNode created with an absolute value below the lower bound)";

    generateDivParamNode(_phi, _phi[2], _phi[1], feat_ind, 1e-50, 1e50, _optimizer);
    EXPECT_EQ(_phi.size(), 4) << " (Failure to create a valid feature)";
    EXPECT_LT(1.0 - util_funcs::r2(_prop.data(), _phi.back()->value_ptr(), _task_sizes_train[0]),
              1e-5);
}

TEST_F(DivParamNodeTest, ConstructorTest)
{
    unsigned long int feat_ind = _phi.size();

    try
    {
        _node_test = std::make_shared<DivParamNode>(
            _phi[0], _phi[1], feat_ind, 1e-50, 1e-40, _optimizer);
        EXPECT_TRUE(false)
            << " (DivParamNode created with an absolute value above the upper bound)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<DivParamNode>(
            _phi[0], _phi[1], feat_ind, 1e49, 1e50, _optimizer);
        EXPECT_TRUE(false)
            << " (DivParamNode created with an absolute value below the lower bound)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<DivParamNode>(
            _phi[2], _phi[1], feat_ind, 1e-50, 1e50, _optimizer);
        EXPECT_LT(1.0 - util_funcs::r2(_prop.data(), _node_test->value_ptr(), _task_sizes_train[0]),
                  1e-5);
    }
    catch (const InvalidFeatureException& e)
    {
        EXPECT_TRUE(false) << " (Failure to create a valid feature)";
    }
}

TEST_F(DivParamNodeTest, HardCopyTest)
{
    unsigned long int feat_ind = _phi.size();
    _node_test = std::make_shared<DivParamNode>(
        _phi[2], _phi[1], feat_ind, 1e-50, 1e50, _optimizer);

    node_ptr copy_test = _node_test->hard_copy();

    EXPECT_EQ(copy_test->rung(), 2);

    std::vector<double> expected_val(_task_sizes_train[0], 0.0);
    std::vector<double> params = copy_test->parameters();

    allowed_op_funcs::div(_task_sizes_train[0],
                          _phi[2]->value_ptr(&params[2]),
                          _phi[1]->value_ptr(&params[2]),
                          params[0],
                          params[1],
                          expected_val.data());
    EXPECT_LT(std::abs(copy_test->value_ptr()[0] - expected_val[0]), 1e-5);
    EXPECT_LT(std::abs(copy_test->value()[0] - expected_val[0]), 1e-5);

    allowed_op_funcs::div(_task_sizes_test[0],
                          _phi[2]->test_value_ptr(&params[2]),
                          _phi[1]->test_value_ptr(&params[2]),
                          params[0],
                          params[1],
                          expected_val.data());
    EXPECT_LT(std::abs(copy_test->test_value_ptr()[0] - expected_val[0]), 1e-5);
    EXPECT_LT(std::abs(copy_test->test_value()[0] - expected_val[0]), 1e-5);

    std::stringstream postfix;
    postfix << "0|1|div|1|div:" << std::setprecision(13) << std::scientific << params[0] << ','
            << params[1] << ',' << params[2] << ',' << params[3];
    EXPECT_STREQ(copy_test->unit().toString().c_str(), "m * s^-2");
    EXPECT_STREQ(copy_test->postfix_expr().c_str(), postfix.str().c_str());

    copy_test->gradient(_gradient.data(), _dfdp.data());
    double v1 = copy_test->feat(0)->value_ptr(&params[2])[0];
    double v2 = copy_test->feat(1)->value_ptr(&params[2])[0];

    double alpha = params[0];
    double a = params[1];
    double df_dp = -1.0 * v1 / std::pow(alpha * v2 + a, 2.0);

    EXPECT_LT(std::abs(_gradient[0] - df_dp * v2), 1e-5);
    EXPECT_LT(std::abs(_gradient[_task_sizes_train[0]] - df_dp), 1e-5);
}

TEST_F(DivParamNodeTest, AttributesTest)
{
    unsigned long int feat_ind = _phi.size();
    if (mpi_setup::comm->rank() == 0)
    {
        _node_test = std::make_shared<DivParamNode>(
            _phi[2], _phi[1], feat_ind, 1e-50, 1e50, _optimizer);
    }
    mpi::broadcast(*mpi_setup::comm, _node_test, 0);

    _node_test->set_value();
    _node_test->set_test_value();

    EXPECT_EQ(_node_test->rung(), 2);

    std::vector<double> expected_val(_task_sizes_train[0], 0.0);
    std::vector<double> params = _node_test->parameters();

    allowed_op_funcs::div(_task_sizes_train[0],
                          _phi[2]->value_ptr(&params[2]),
                          _phi[1]->value_ptr(&params[2]),
                          params[0],
                          params[1],
                          expected_val.data());
    EXPECT_LT(std::abs(_node_test->value_ptr()[0] - expected_val[0]), 1e-5);
    EXPECT_LT(std::abs(_node_test->value()[0] - expected_val[0]), 1e-5);

    allowed_op_funcs::div(_task_sizes_test[0],
                          _phi[2]->test_value_ptr(&params[2]),
                          _phi[1]->test_value_ptr(&params[2]),
                          params[0],
                          params[1],
                          expected_val.data());
    EXPECT_LT(std::abs(_node_test->test_value_ptr()[0] - expected_val[0]), 1e-5);
    EXPECT_LT(std::abs(_node_test->test_value()[0] - expected_val[0]), 1e-5);

    std::stringstream postfix;
    postfix << "0|1|div|1|div:" << std::setprecision(13) << std::scientific << params[0] << ','
            << params[1] << ',' << params[2] << ',' << params[3];
    EXPECT_STREQ(_node_test->unit().toString().c_str(), "m * s^-2");
    EXPECT_STREQ(_node_test->postfix_expr().c_str(), postfix.str().c_str());

    EXPECT_THROW(_node_test->set_parameters({1.0, 0.0, 1.0}), std::logic_error);
}
}  // namespace
#endif
