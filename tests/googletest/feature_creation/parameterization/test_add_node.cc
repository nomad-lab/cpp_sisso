// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#ifdef PARAMETERIZE

#include "external/third_party_gtest.hpp"

#include <random>

#include "feature_creation/node/FeatureNode.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/add/parameterized_add.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/log/log.hpp"
#include "mpi_interface/MPI_Interface.hpp"

namespace
{
class AddParamNodeTest : public ::testing::Test
{
protected:
    void SetUp() override
    {
        nlopt_wrapper::MAX_PARAM_DEPTH = 2;

        _task_sizes_train = {900};
        _task_sizes_test = {100};
        node_value_arrs::initialize_values_arr(_task_sizes_train, _task_sizes_test, 4, 2, true);

        std::vector<double> value_1(_task_sizes_train[0], 0.0);
        std::vector<double> value_2(_task_sizes_train[0], 0.0);
        std::vector<double> value_3(_task_sizes_train[0], 0.0);
        std::vector<double> value_4(_task_sizes_train[0], 0.0);

        std::vector<double> test_value_1(_task_sizes_test[0], 0.0);
        std::vector<double> test_value_2(_task_sizes_test[0], 0.0);
        std::vector<double> test_value_3(_task_sizes_test[0], 0.0);
        std::vector<double> test_value_4(_task_sizes_test[0], 0.0);

        std::default_random_engine generator(0);
        nlopt::srand(42);
        std::uniform_real_distribution<double> distribution_feats(-25.0, 25.0);
        std::uniform_real_distribution<double> distribution_params(-2.50, 2.50);

        for (int ii = 0; ii < _task_sizes_train[0]; ++ii)
        {
            value_1[ii] = distribution_feats(generator);
            value_2[ii] = distribution_feats(generator);
            value_3[ii] = std::exp(distribution_feats(generator));
            value_4[ii] = distribution_feats(generator);
        }

        for (int ii = 0; ii < _task_sizes_test[0]; ++ii)
        {
            test_value_1[ii] = distribution_feats(generator);
            test_value_2[ii] = distribution_feats(generator);
            test_value_3[ii] = std::exp(distribution_feats(generator));
            test_value_4[ii] = distribution_feats(generator);
        }

        _feat_1 = std::make_shared<FeatureNode>(0, "A", value_1, test_value_1, Unit("m"));
        _feat_2 = std::make_shared<FeatureNode>(1, "B", value_2, test_value_2, Unit("m"));
        _feat_3 = std::make_shared<FeatureNode>(2, "C", value_3, test_value_3, Unit());
        _feat_4 = std::make_shared<FeatureNode>(3, "D", value_4, test_value_4, Unit());

        node_ptr feat_5 = std::make_shared<AddNode>(_feat_1, _feat_2, 4);
        node_ptr feat_6 = std::make_shared<LogNode>(_feat_3, 5);

        _phi = {_feat_1, _feat_2, feat_5, _feat_3, _feat_4, feat_6};
        _a = 0.0;
        _alpha = distribution_params(generator);

        _prop = std::vector<double>(_task_sizes_train[0], 0.0);
        _prop_attr = std::vector<double>(_task_sizes_train[0], 0.0);
        _gradient.resize(_task_sizes_train[0] * 4, 1.0);
        _dfdp.resize(_task_sizes_train[0]);

        allowed_op_funcs::add(_task_sizes_train[0],
                              _phi[1]->value_ptr(),
                              _phi[2]->value_ptr(),
                              _alpha,
                              _a,
                              _prop.data());

        allowed_op_funcs::add(_task_sizes_train[0],
                              _phi[4]->value_ptr(),
                              _phi[5]->value_ptr(),
                              _alpha,
                              _a,
                              _prop_attr.data());

        _optimizer = nlopt_wrapper::get_optimizer("regression", _task_sizes_train, _prop, 2);
        _optimizer_attr = nlopt_wrapper::get_optimizer(
            "regression", _task_sizes_train, _prop_attr, 2);
    }

    void TearDown() override { node_value_arrs::finalize_values_arr(); }

    node_ptr _feat_1;
    node_ptr _feat_2;
    node_ptr _feat_3;
    node_ptr _feat_4;
    node_ptr _node_test;

    std::vector<node_ptr> _phi;
    std::vector<double> _prop;
    std::vector<double> _prop_attr;
    std::vector<double> _gradient;
    std::vector<double> _dfdp;
    std::vector<int> _task_sizes_train;
    std::vector<int> _task_sizes_test;

    double _a;
    double _alpha;
    std::shared_ptr<NLOptimizer> _optimizer;
    std::shared_ptr<NLOptimizer> _optimizer_attr;
};

TEST_F(AddParamNodeTest, GeneratorTest)
{
    unsigned long int feat_ind = _phi.size();
    int phi_sz = _phi.size();

    generateAddParamNode(_phi, _phi[0], _phi[1], feat_ind, 1e-50, 1e-40, _optimizer);
    EXPECT_EQ(_phi.size(), phi_sz)
        << " (AddParamNode created with an absolute value above the upper bound)";

    generateAddParamNode(_phi, _phi[0], _phi[1], feat_ind, 1e49, 1e50, _optimizer);
    EXPECT_EQ(_phi.size(), phi_sz)
        << " (AddParamNode created with an absolute value below the lower bound)";

    generateAddParamNode(_phi, _phi[1], _phi[2], feat_ind, 1e-50, 1e50, _optimizer);
    EXPECT_EQ(_phi.size(), phi_sz + 1) << " (Failure to create a valid feature)";
    EXPECT_LT(1.0 - util_funcs::r2(_prop.data(), _phi.back()->value_ptr(), _task_sizes_train[0]),
              1e-5);
}

TEST_F(AddParamNodeTest, ConstructorTest)
{
    unsigned long int feat_ind = _phi.size();

    try
    {
        _node_test = std::make_shared<AddParamNode>(
            _phi[0], _phi[1], feat_ind, 1e-50, 1e-40, _optimizer);
        EXPECT_TRUE(false)
            << " (AddParamNode created with an absolute value above the upper bound)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<AddParamNode>(
            _phi[0], _phi[1], feat_ind, 1e3, 1e50, _optimizer);
        EXPECT_TRUE(false)
            << " (AddParamNode created with an absolute value below the lower bound)";
    }
    catch (const InvalidFeatureException& e)
    {
    }

    try
    {
        _node_test = std::make_shared<AddParamNode>(
            _phi[1], _phi[2], feat_ind, 1e-50, 1e50, _optimizer);
        EXPECT_LT(1.0 - util_funcs::r2(_prop.data(), _node_test->value_ptr(), _task_sizes_train[0]),
                  1e-5);
    }
    catch (const InvalidFeatureException& e)
    {
        EXPECT_TRUE(false) << " (Failure to create a valid feature)";
    }
}

TEST_F(AddParamNodeTest, HardCopyTest)
{
    unsigned long int feat_ind = _phi.size();
    _node_test = std::make_shared<AddParamNode>(
        _phi[4], _phi[5], feat_ind, 1e-50, 1e50, _optimizer_attr);

    _node_test->set_value();
    _node_test->set_test_value();

    node_ptr copy_test = _node_test->hard_copy();

    EXPECT_EQ(copy_test->rung(), 2);

    std::vector<double> expected_val(_task_sizes_train[0], 0.0);
    std::vector<double> params = copy_test->parameters();

    allowed_op_funcs::add(_task_sizes_train[0],
                          _phi[4]->value_ptr(),
                          _phi[5]->value_ptr(&params[2]),
                          params[0],
                          params[1],
                          expected_val.data());
    EXPECT_LT(std::abs(copy_test->value_ptr()[0] - expected_val[0]), 1e-5);
    EXPECT_LT(std::abs(copy_test->value()[0] - expected_val[0]), 1e-5);

    allowed_op_funcs::add(_task_sizes_test[0],
                          _phi[4]->test_value_ptr(),
                          _phi[5]->test_value_ptr(&params[2]),
                          params[0],
                          params[1],
                          expected_val.data());
    EXPECT_LT(std::abs(copy_test->test_value_ptr()[0] - expected_val[0]), 1e-5);
    EXPECT_LT(std::abs(copy_test->test_value()[0] - expected_val[0]), 1e-5);

    std::stringstream postfix;
    postfix << "3|2|log|add:" << std::setprecision(13) << std::scientific << params[0] << ','
            << params[1] << ',' << params[2] << ',' << params[3];
    EXPECT_STREQ(copy_test->unit().toString().c_str(), "Unitless");
    EXPECT_STREQ(copy_test->postfix_expr().c_str(), postfix.str().c_str());

    copy_test->gradient(_gradient.data(), _dfdp.data());
    double v2 = copy_test->feat(1)->value_ptr(&params[2])[0];
    double df_dp = 1.0;

    EXPECT_LT(std::abs(_gradient[0] - df_dp * v2), 1e-5);
    EXPECT_LT(std::abs(_gradient[_task_sizes_train[0]] - df_dp), 1e-5);
}

TEST_F(AddParamNodeTest, AttributesTest)
{
    unsigned long int feat_ind = _phi.size();
    if (mpi_setup::comm->rank() == 0)
    {
        _node_test = std::make_shared<AddParamNode>(
            _phi[4], _phi[5], feat_ind, 1e-50, 1e50, _optimizer_attr);
    }
    mpi::broadcast(*mpi_setup::comm, _node_test, 0);

    EXPECT_EQ(_node_test->rung(), 2);

    std::vector<double> expected_val(_task_sizes_train[0], 0.0);
    std::vector<double> params = _node_test->parameters();

    allowed_op_funcs::add(_task_sizes_train[0],
                          _phi[4]->value_ptr(),
                          _phi[5]->value_ptr(&params[2]),
                          params[0],
                          params[1],
                          expected_val.data());
    EXPECT_LT(std::abs(_node_test->value_ptr()[0] - expected_val[0]), 1e-5);
    EXPECT_LT(std::abs(_node_test->value()[0] - expected_val[0]), 1e-5);

    allowed_op_funcs::add(_task_sizes_test[0],
                          _phi[4]->test_value_ptr(),
                          _phi[5]->test_value_ptr(&params[2]),
                          params[0],
                          params[1],
                          expected_val.data());
    EXPECT_LT(std::abs(_node_test->test_value_ptr()[0] - expected_val[0]), 1e-5);
    EXPECT_LT(std::abs(_node_test->test_value()[0] - expected_val[0]), 1e-5);

    std::stringstream postfix;
    postfix << "3|2|log|add:" << std::setprecision(13) << std::scientific << params[0] << ','
            << params[1] << ',' << params[2] << ',' << params[3];
    EXPECT_STREQ(_node_test->unit().toString().c_str(), "Unitless");
    EXPECT_STREQ(_node_test->postfix_expr().c_str(), postfix.str().c_str());

    EXPECT_THROW(_node_test->set_parameters({1.0, 0.0, 1.0}), std::logic_error);
}
}  // namespace
#endif
