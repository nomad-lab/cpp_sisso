// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "external/third_party_gtest.hpp"

#include <feature_creation/domain/Domain.hpp>

namespace
{
TEST(Domain, StringConstructor)
{
    EXPECT_THROW(Domain("(1.0, 2.0"), std::logic_error);
    EXPECT_THROW(Domain("1.0, 2.0)"), std::logic_error);
    EXPECT_THROW(Domain("(1.0 2.0)"), std::logic_error);

    Domain d1("(-2.0, 4.0] | [3.5, 0.0]");
    Domain d2("[2.0, 4.0] | [3.5, 0.0]");
    Domain d3("(-4.0, -2.0) | [-3.5, 0.0]");
    Domain d4("[-2.0, 4.0) | [3.5, 0.0]");

    Domain d5("[-2.0, 0.0)");
    Domain d6("[0.0, 2.0)");
    Domain d7("(-4.0, -20.0) | [-3.5, 0.0]");

    EXPECT_STREQ(d1.toString().c_str(),
                 "(-2.000000000000000e+00, 4.000000000000000e+00] | [3.500000000000000e+00, "
                 "0.000000000000000e+00]");
    EXPECT_STREQ(d2.toString().c_str(),
                 "[2.000000000000000e+00, 4.000000000000000e+00] | [3.500000000000000e+00, "
                 "0.000000000000000e+00]");
    EXPECT_STREQ(d3.toString().c_str(),
                 "(-4.000000000000000e+00, -2.000000000000000e+00) | [-3.500000000000000e+00, "
                 "0.000000000000000e+00]");
    EXPECT_STREQ(d4.toString().c_str(),
                 "[-2.000000000000000e+00, 4.000000000000000e+00) | [3.500000000000000e+00, "
                 "0.000000000000000e+00]");

    EXPECT_EQ(d3 * d4, Domain("(-16, 8)|[0.0]"));
    EXPECT_EQ(d4 * d3, Domain("(-16, 8)|[0.0]"));
    EXPECT_EQ(d1 * d3, Domain("(-16, 8)|[0.0]"));
    EXPECT_EQ(d3 * d1, Domain("(-16, 8)|[0.0]"));
    EXPECT_EQ(d4 * d1, Domain("(-8, 16)|[0.0]"));
    EXPECT_EQ(d1 * d4, Domain("[-8, 16)|[0.0]"));
    EXPECT_EQ(d2 * d3, Domain("(-16, -4)|[0.0]"));
    EXPECT_EQ(d3 * d2, Domain("(-16, -4)|[0.0]"));
    EXPECT_EQ(d7 * d1, Domain("(-80, 40.0)|[0.0]"));

    EXPECT_EQ(d2 + d4, Domain("[0, 8)"));

    EXPECT_EQ(d3.abs(), d3.neg());
    EXPECT_EQ(d2.abs(), d2);

    EXPECT_EQ(d5.inv(), Domain("(-inf, -0.5]"));
    EXPECT_EQ(d6.inv(), Domain("(0.5, inf]"));

    EXPECT_EQ(d1.end_points()[0], -2.0);
    EXPECT_EQ(d1.end_points()[1], 4.0);

    EXPECT_EQ(d1.excluded_pts().size(), 3);
    EXPECT_LT(std::abs(d1.excluded_pts()[0] - (-2.0)), 1e-10);
    EXPECT_LT(std::abs(d1.excluded_pts()[1] - (3.5)), 1e-10);
    EXPECT_LT(std::abs(d1.excluded_pts()[2] - (0.0)), 1e-10);

    EXPECT_EQ(d2.end_points()[0], 2.0);
    EXPECT_EQ(d2.end_points()[1], 4.0);

    EXPECT_EQ(d2.excluded_pts().size(), 2);
    EXPECT_LT(std::abs(d2.excluded_pts()[0] - (3.5)), 1e-10);
    EXPECT_LT(std::abs(d2.excluded_pts()[1] - (0.0)), 1e-10);

    EXPECT_EQ(d3.end_points()[0], -4.0);
    EXPECT_EQ(d3.end_points()[1], -2.0);

    EXPECT_EQ(d3.excluded_pts().size(), 4);
    EXPECT_LT(std::abs(d3.excluded_pts()[0] - (-4.0)), 1e-10);
    EXPECT_LT(std::abs(d3.excluded_pts()[1] - (-2.0)), 1e-10);
    EXPECT_LT(std::abs(d3.excluded_pts()[2] - (-3.5)), 1e-10);
    EXPECT_LT(std::abs(d3.excluded_pts()[3] - (0.0)), 1e-10);

    EXPECT_EQ(d4.end_points()[0], -2.0);
    EXPECT_EQ(d4.end_points()[1], 4.0);

    EXPECT_EQ(d4.excluded_pts().size(), 3);
    EXPECT_LT(std::abs(d4.excluded_pts()[0] - (4.0)), 1e-10);
    EXPECT_LT(std::abs(d4.excluded_pts()[1] - (3.5)), 1e-10);
    EXPECT_LT(std::abs(d4.excluded_pts()[2] - (0.0)), 1e-10);

    EXPECT_TRUE(d1.contains(2.0));
    EXPECT_TRUE(d1.contains(4.0));

    EXPECT_TRUE(d1 == d1);
    EXPECT_FALSE(d1 != d1);

    EXPECT_FALSE(d1.contains(0.0));
    EXPECT_FALSE(d1.contains(-2.0));
    EXPECT_FALSE(d1.contains(3.5));
    EXPECT_FALSE(d1.contains(-3.0));
    EXPECT_FALSE(d1.contains(4.1));

    EXPECT_FALSE(d1.is_empty());

    Domain d_sq = d1 ^ 2.0;
    EXPECT_TRUE(d1 != d_sq);
    EXPECT_FALSE(d1 == d_sq);

    EXPECT_EQ(d_sq.end_points()[0], 0.0);
    EXPECT_EQ(d_sq.end_points()[1], 16.0);

    EXPECT_EQ(d_sq.excluded_pts().size(), 2);
    EXPECT_LT(std::abs(d_sq.excluded_pts()[0] - (3.5 * 3.5)), 1e-10);

    Domain d_cb = d1 ^ 3.0;
    EXPECT_EQ(d_cb.end_points()[0], -8.0);
    EXPECT_EQ(d_cb.end_points()[1], 64.0);

    EXPECT_EQ(d_cb.excluded_pts().size(), 3);
    EXPECT_LT(std::abs(d_cb.excluded_pts()[0] - (-8.0)), 1e-10);
    EXPECT_LT(std::abs(d_cb.excluded_pts()[1] - (3.5 * 3.5 * 3.5)), 1e-10);
    EXPECT_LT(std::abs(d_cb.excluded_pts()[2] - (0.0)), 1e-10);

    Domain d_inv_sq = d1 ^ -2.0;
    EXPECT_EQ(d_inv_sq.end_points()[0], 0.0625);
    EXPECT_EQ(d_inv_sq.end_points()[1], std::numeric_limits<double>::infinity());

    EXPECT_EQ(d_inv_sq.excluded_pts().size(), 2);
    EXPECT_LT(std::abs(d_inv_sq.excluded_pts()[0] - (1 / (3.5 * 3.5))), 1e-10);
    EXPECT_EQ(d_inv_sq.end_points()[1], std::numeric_limits<double>::infinity());

    Domain d_inv_cb = d1 ^ -3.0;
    EXPECT_EQ(d_inv_cb.end_points()[0], -std::numeric_limits<double>::infinity());
    EXPECT_EQ(d_inv_cb.end_points()[1], std::numeric_limits<double>::infinity());

    EXPECT_EQ(d_inv_cb.excluded_pts().size(), 3);
    EXPECT_EQ(d_inv_cb.excluded_pts()[0], 0);
    EXPECT_EQ(d_inv_cb.excluded_pts()[1], -std::numeric_limits<double>::infinity());
    EXPECT_EQ(d_inv_cb.excluded_pts()[2], std::numeric_limits<double>::infinity());

    Domain d_neg = d1.neg();
    EXPECT_EQ(d_neg.end_points()[0], -4.0);
    EXPECT_EQ(d_neg.end_points()[1], 2.0);

    EXPECT_EQ(d_neg.excluded_pts().size(), 3);
    EXPECT_LT(std::abs(d_neg.excluded_pts()[0] - (2.0)), 1e-10);
    EXPECT_LT(std::abs(d_neg.excluded_pts()[1] - (-3.5)), 1e-10);
    EXPECT_LT(std::abs(d_neg.excluded_pts()[2] - (-0.0)), 1e-10);

    Domain d_exp = d1.exp();
    EXPECT_EQ(d_exp.end_points()[0], std::exp(-2.0));
    EXPECT_EQ(d_exp.end_points()[1], std::exp(4.0));

    EXPECT_EQ(d_exp.excluded_pts().size(), 3);
    EXPECT_LT(std::abs(d_exp.excluded_pts()[0] - (std::exp(-2.0))), 1e-10);
    EXPECT_LT(std::abs(d_exp.excluded_pts()[1] - (std::exp(3.5))), 1e-10);
    EXPECT_LT(std::abs(d_exp.excluded_pts()[2] - (std::exp(0.0))), 1e-10);

    Domain d_abs = d1.abs();
    EXPECT_EQ(d_abs.end_points()[0], 0.0);
    EXPECT_EQ(d_abs.end_points()[1], 4.0);

    EXPECT_EQ(d_abs.excluded_pts().size(), 2);
    EXPECT_LT(std::abs(d_abs.excluded_pts()[0] - (3.5)), 1e-10);
    EXPECT_LT(std::abs(d_abs.excluded_pts()[1] - (0.0)), 1e-10);

    Domain d_log = d_exp.log();
    EXPECT_EQ(d_log.end_points()[0], -2.0);
    EXPECT_EQ(d_log.end_points()[1], 4.0);

    EXPECT_EQ(d_log.excluded_pts().size(), 3);
    EXPECT_LT(std::abs(d_log.excluded_pts()[0] - (-2.0)), 1e-10);
    EXPECT_LT(std::abs(d_log.excluded_pts()[1] - (3.5)), 1e-10);
    EXPECT_LT(std::abs(d_log.excluded_pts()[2] - (0.0)), 1e-10);

    Domain d_inv = d_sq.inv();
    EXPECT_EQ(d_inv.end_points()[0], 1.0 / 16.0);
    EXPECT_EQ(d_inv.end_points()[1], std::numeric_limits<double>::infinity());

    EXPECT_EQ(d_inv.excluded_pts().size(), 2);
    EXPECT_LT(std::abs(d_inv.excluded_pts()[0] - (1.0 / (3.5 * 3.5))), 1e-10);
    EXPECT_EQ(d_inv.excluded_pts()[1], std::numeric_limits<double>::infinity());

    EXPECT_EQ((d_sq ^ 2) / d_sq, Domain("(0.0, inf)"));
    EXPECT_EQ(d1 * d_sq, Domain("(-32.0, 64.0] | [0.0]"));

    Domain d_add = d1 + d_neg;
    EXPECT_EQ(d_add.end_points()[0], -6.0);
    EXPECT_EQ(d_add.end_points()[1], 6.0);

    EXPECT_EQ(d_add.excluded_pts().size(), 2);
    EXPECT_LT(std::abs(d_add.excluded_pts()[0] - (6.0)), 1e-10);
    EXPECT_LT(std::abs(d_add.excluded_pts()[1] - (-6.0)), 1e-10);

    Domain d_sub = d1 - d_neg;
    EXPECT_EQ(d_sub.end_points()[0], -4.0);
    EXPECT_EQ(d_sub.end_points()[1], 8.0);

    EXPECT_EQ(d_sub.excluded_pts().size(), 1);
    EXPECT_LT(std::abs(d_sub.excluded_pts()[0] - (-4.0)), 1e-10);

    Domain d_scale_add = d1 + 4.0;
    EXPECT_EQ(d_scale_add.end_points()[0], 2.0);
    EXPECT_EQ(d_scale_add.end_points()[1], 8.0);

    EXPECT_EQ(d_scale_add.excluded_pts().size(), 3);
    EXPECT_LT(std::abs(d_scale_add.excluded_pts()[0] - (2.0)), 1e-10);
    EXPECT_LT(std::abs(d_scale_add.excluded_pts()[1] - (7.5)), 1e-10);
    EXPECT_LT(std::abs(d_scale_add.excluded_pts()[2] - (4.0)), 1e-10);

    EXPECT_EQ(d_scale_add - 4.0, d1);

    Domain d_scale_mult = d1 * 4.0;
    EXPECT_EQ(d_scale_mult.end_points()[0], -8.0);
    EXPECT_EQ(d_scale_mult.end_points()[1], 16.0);

    EXPECT_EQ(d_scale_mult.excluded_pts().size(), 3);
    EXPECT_LT(std::abs(d_scale_mult.excluded_pts()[0] - (-8.0)), 1e-10);
    EXPECT_LT(std::abs(d_scale_mult.excluded_pts()[1] - (14.0)), 1e-10);
    EXPECT_LT(std::abs(d_scale_mult.excluded_pts()[2] - (0.0)), 1e-10);

    EXPECT_EQ(d_scale_mult / 4.0, d1);

    Domain d_param = d1.parameterize(2.0, 5.0);
    EXPECT_EQ(d_param.end_points()[0], 1.0);
    EXPECT_EQ(d_param.end_points()[1], 13.0);

    EXPECT_EQ(d_param.excluded_pts().size(), 3);
    EXPECT_LT(std::abs(d_param.excluded_pts()[0] - (1.0)), 1e-10);
    EXPECT_LT(std::abs(d_param.excluded_pts()[1] - (12.0)), 1e-10);
    EXPECT_LT(std::abs(d_param.excluded_pts()[2] - (5.0)), 1e-10);

    EXPECT_EQ(4.0 + d1, d_scale_add);
    EXPECT_EQ(4.0 * d1, d_scale_mult);
    EXPECT_EQ(4.0 - d1.neg(), d_scale_add);

    Domain d_scalar_div = 4.0 / d_param;
    EXPECT_EQ(d_scalar_div.end_points()[0], 4.0 / 13.0);
    EXPECT_EQ(d_scalar_div.end_points()[1], 4.0);

    EXPECT_EQ(d_scalar_div.excluded_pts().size(), 3);
    EXPECT_LT(std::abs(d_scalar_div.excluded_pts()[0] - (4.0)), 1e-10);
    EXPECT_LT(std::abs(d_scalar_div.excluded_pts()[1] - (4.0 / 12.0)), 1e-10);
    EXPECT_LT(std::abs(d_scalar_div.excluded_pts()[2] - (4.0 / 5.0)), 1e-10);
}

//test mean calculations
TEST(Domain, EmptyConstructor)
{
    Domain d_emp_1 = Domain();
    Domain d_emp_2 = Domain("[1.0, 1.0]");
    Domain d_emp_3 = Domain("[1.0, 1.0] | [1.0]");
    Domain d_emp_4 = Domain(std::array<double, 2>({1.0, 1.0}));
    Domain d_emp_5 = Domain(std::array<double, 2>({1.0, 1.0}), {1.0});
    Domain d_emp_6 = Domain("");

    ASSERT_TRUE(d_emp_1.is_empty());
    ASSERT_TRUE(d_emp_2.is_empty());
    ASSERT_TRUE(d_emp_3.is_empty());
    ASSERT_TRUE(d_emp_4.is_empty());
    ASSERT_TRUE(d_emp_5.is_empty());
    ASSERT_TRUE(d_emp_6.is_empty());

    EXPECT_EQ(d_emp_2, d_emp_1);
    EXPECT_EQ(d_emp_3, d_emp_2);
    EXPECT_EQ(d_emp_1, d_emp_3);
    EXPECT_FALSE(d_emp_1.contains(1.0));

    EXPECT_STREQ("", d_emp_1.toString().c_str());
    EXPECT_STREQ("", d_emp_2.toString().c_str());

    EXPECT_TRUE(d_emp_2.abs().is_empty());
    EXPECT_TRUE(d_emp_2.neg().is_empty());
    EXPECT_TRUE(d_emp_2.log().is_empty());
    EXPECT_TRUE(d_emp_2.exp().is_empty());
    EXPECT_TRUE(d_emp_2.inv().is_empty());
    EXPECT_TRUE(d_emp_2.pow(2.0).is_empty());
    EXPECT_TRUE(d_emp_2.parameterize(2.0, 1.0).is_empty());
    EXPECT_TRUE((d_emp_2 + 4.0).is_empty());
    EXPECT_TRUE((d_emp_2 - 4.0).is_empty());
    EXPECT_TRUE((d_emp_2 * 4.0).is_empty());
    EXPECT_TRUE((d_emp_2 / 4.0).is_empty());

    EXPECT_TRUE((4.0 + d_emp_2).is_empty());
    EXPECT_TRUE((4.0 - d_emp_2).is_empty());
    EXPECT_TRUE((4.0 * d_emp_2).is_empty());
    EXPECT_TRUE((4.0 / d_emp_2).is_empty());

    Domain d_not_emp = Domain("(0.1, 1.1)");
    EXPECT_NE(d_not_emp, d_emp_1);
    EXPECT_NE(d_not_emp, d_emp_2);

    EXPECT_NE(d_emp_1, d_not_emp);
    EXPECT_NE(d_emp_2, d_not_emp);

    EXPECT_TRUE((d_emp_2 + d_not_emp).is_empty());
    EXPECT_TRUE((d_emp_2 - d_not_emp).is_empty());
    EXPECT_TRUE((d_emp_2 * d_not_emp).is_empty());
    EXPECT_TRUE((d_emp_2 / d_not_emp).is_empty());

    EXPECT_TRUE((d_not_emp + d_emp_2).is_empty());
    EXPECT_TRUE((d_not_emp - d_emp_2).is_empty());
    EXPECT_TRUE((d_not_emp * d_emp_2).is_empty());
    EXPECT_TRUE((d_not_emp / d_emp_2).is_empty());
}
}  // namespace
