// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "external/third_party_gtest.hpp"

#include <descriptor_identifier/model/ModelClassifier.hpp>

namespace
{
class ModelClassifierTests : public ::testing::Test
{
protected:
    void SetUp() override
    {
        _leave_out_inds = {5, 11};
        _task_sizes_train = {12};
        _task_sizes_test = {2};

        std::vector<double> value_1 = {
            1.0, -2.0, 3.0, -4.0, 5.0, -6.0, 7.0, -8.0, 9.0, -10.0, 0.5, -0.5};
        std::vector<double> test_value_1 = {10.0, -7.0};

        model_node_ptr feat_1 = std::make_shared<ModelNode>(0,
                                                            0,
                                                            "A",
                                                            "$A$",
                                                            "0",
                                                            "A",
                                                            value_1,
                                                            test_value_1,
                                                            std::vector<std::string>(1, "A"),
                                                            Unit("m"));

        _features = {feat_1};

        _prop = {0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0};
        _prop_test = {0.0, 1.0};
        _error.resize(12);

        _loss = std::make_shared<LossFunctionConvexHull>(
            _prop, _prop_test, _task_sizes_train, _task_sizes_test, 1);

        task_names = {"all"};
        _sample_ids_train = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"};
        _sample_ids_test = {"20", "21"};
    }
    std::vector<std::string> _sample_ids_train;
    std::vector<std::string> _sample_ids_test;
    std::vector<std::string> task_names;

    std::vector<double> _error;
    std::vector<double> _prop;
    std::vector<double> _prop_test;

    std::vector<model_node_ptr> _features;
    std::shared_ptr<LossFunction> _loss;

    std::vector<int> _task_sizes_train;
    std::vector<int> _task_sizes_test;
    std::vector<int> _leave_out_inds;
};

TEST_F(ModelClassifierTests, NodesTest)
{
    ModelClassifier model("Property",
                          Unit("m"),
                          _loss,
                          _features,
                          _leave_out_inds,
                          _sample_ids_train,
                          _sample_ids_test,
                          task_names);

    model.copy_error(_error.data());
    EXPECT_LT(std::accumulate(_error.begin(),
                              _error.end(),
                              -12,
                              [](double tot, double el) { return tot + std::abs(el); }),
              1e-5);

    EXPECT_STREQ(model.toString().c_str(), "[A]");
    EXPECT_EQ(model.n_convex_overlap_train(), 0);
    EXPECT_EQ(model.n_convex_overlap_test(), 0);
    EXPECT_EQ(model.n_svm_misclassified_train(), 0);
    EXPECT_EQ(model.n_svm_misclassified_test(), 0);
    EXPECT_LT(model.percent_train_error(), 1.0e-10);
    EXPECT_LT(model.percent_test_error(), 1.0e-10);

    EXPECT_EQ(model.n_samp_train(), 12);
    EXPECT_EQ(model.n_samp_test(), 2);
    EXPECT_EQ(model.n_dim(), 1);
    EXPECT_EQ(model.prop_unit(), Unit("m"));
    EXPECT_EQ(model.prop_label(), "Property");

    EXPECT_FALSE(model.fix_intercept());

    model.to_file("train_class_mods.dat", true);
    model.to_file("test_class_mods.dat", false);
}

TEST_F(ModelClassifierTests, FileTest)
{
    ModelClassifier model("train_class_mods.dat", "test_class_mods.dat");

    model.copy_error(_error.data());
    EXPECT_LT(std::accumulate(_error.begin(),
                              _error.end(),
                              -12,
                              [](double tot, double el) { return tot + std::abs(el); }),
              1e-5);

    EXPECT_STREQ(model.toString().c_str(), "[A]");
    EXPECT_EQ(model.n_convex_overlap_train(), 0);
    EXPECT_EQ(model.n_convex_overlap_test(), 0);
    EXPECT_EQ(model.n_svm_misclassified_train(), 0);
    EXPECT_EQ(model.n_svm_misclassified_test(), 0);
    EXPECT_LT(model.percent_train_error(), 1.0e-10);
    EXPECT_LT(model.percent_test_error(), 1.0e-10);

    EXPECT_EQ(model.n_samp_train(), 12);
    EXPECT_EQ(model.n_samp_test(), 2);
    EXPECT_EQ(model.n_dim(), 1);
    EXPECT_EQ(model.prop_unit(), Unit("m"));
    EXPECT_EQ(model.prop_label(), "Property");

    EXPECT_FALSE(model.fix_intercept());

    boost::filesystem::remove("train_class_mods.dat");
    boost::filesystem::remove("test_class_mods.dat");
}

TEST_F(ModelClassifierTests, EvalTest)
{
    ModelClassifier model("Property",
                          Unit("m"),
                          _loss,
                          _features,
                          _leave_out_inds,
                          _sample_ids_train,
                          _sample_ids_test,
                          task_names);

    std::vector<double> pt = {20.0};
    EXPECT_EQ(model.eval(pt.data(), task_names[0]), 0.0);

    std::vector<std::vector<double>> pts = {{20.0}};
    EXPECT_EQ(model.eval(pts.data(), {task_names[0]})[0], 0.0);
}
}  // namespace
