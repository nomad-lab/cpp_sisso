// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "external/third_party_gtest.hpp"

#include <descriptor_identifier/model/ModelLogRegressor.hpp>

namespace
{
class ModelLogRegssorTests : public ::testing::Test
{
protected:
    void SetUp() override
    {
        _leave_out_inds = {5, 11};
        _task_sizes_train = {10};
        _task_sizes_test = {2};

        std::vector<double> value_1 = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0};
        std::vector<double> test_value_1 = {1.0, 7.0};

        std::vector<double> value_2 = {1.10, 2.20, 3.10, 4.20, 5.10, 6.20, 7.10, 8.20, 9.10, 10.20};
        std::vector<double> test_value_2 = {2.10, 4.80};

        model_node_ptr feat_1 = std::make_shared<ModelNode>(0,
                                                            0,
                                                            "A",
                                                            "$A$",
                                                            "0",
                                                            "A",
                                                            value_1,
                                                            test_value_1,
                                                            std::vector<std::string>(1, "A"),
                                                            Unit("m"));
        model_node_ptr feat_2 = std::make_shared<ModelNode>(1,
                                                            0,
                                                            "B",
                                                            "$B$",
                                                            "1",
                                                            "B",
                                                            value_2,
                                                            test_value_2,
                                                            std::vector<std::string>(1, "B"),
                                                            Unit("m"));

        _features = {feat_1, feat_2};

        _error.resize(10);
        _prop = std::vector<double>(10, 0.0);
        _prop_test = std::vector<double>(2, 0.0);

        std::transform(value_1.begin(),
                       value_1.end(),
                       value_2.begin(),
                       _prop.begin(),
                       [](double v1, double v2) {
                           return std::log(0.001 * std::pow(v1, 0.1) * std::pow(v2, -2.1));
                       });
        std::transform(test_value_1.begin(),
                       test_value_1.end(),
                       test_value_2.begin(),
                       _prop_test.begin(),
                       [](double v1, double v2) {
                           return std::log(0.001 * std::pow(v1, 0.1) * std::pow(v2, -2.1));
                       });

        task_names = {"all"};
        _sample_ids_train = {"0", "1", "2", "3", "4", "6", "7", "8", "9", "10"};
        _sample_ids_test = {"5", "11"};
    }
    std::vector<std::string> _sample_ids_train;
    std::vector<std::string> _sample_ids_test;
    std::vector<std::string> task_names;

    std::vector<int> _leave_out_inds;
    std::vector<int> _task_sizes_train;
    std::vector<int> _task_sizes_test;

    std::vector<double> _error;
    std::vector<double> _prop;
    std::vector<double> _prop_test;

    std::vector<model_node_ptr> _features;
    std::shared_ptr<LossFunction> _loss;
};

TEST_F(ModelLogRegssorTests, FixInterceptFalseTest)
{
    _loss = std::make_shared<LossFunctionLogPearsonRMSE>(
        _prop, _prop_test, _task_sizes_train, _task_sizes_test, false, 2);

    ModelLogRegressor model("Property",
                            Unit("m"),
                            _loss,
                            _features,
                            _leave_out_inds,
                            _sample_ids_train,
                            _sample_ids_test,
                            task_names);

    model.copy_error(_error.data());
    EXPECT_LT(std::accumulate(_error.begin(),
                              _error.end(),
                              0.0,
                              [](double tot, double el) { return tot + std::abs(el); }),
              1e-5);

    EXPECT_STREQ(model.toString().c_str(), "exp(c0) * (A)^a0 * (B)^a1");
    EXPECT_LT(model.rmse(), 1e-10);
    EXPECT_LT(model.test_rmse(), 1e-10);
    EXPECT_LT(model.max_ae(), 1e-10);
    EXPECT_LT(model.test_max_ae(), 1e-10);
    EXPECT_LT(model.mae(), 1e-10);
    EXPECT_LT(model.test_mae(), 1e-10);
    EXPECT_LT(model.mape(), 1e-10);
    EXPECT_LT(model.test_mape(), 1e-10);
    EXPECT_LT(model.percentile_25_ae(), 1e-10);
    EXPECT_LT(model.percentile_25_test_ae(), 1e-10);
    EXPECT_LT(model.percentile_50_ae(), 1e-10);
    EXPECT_LT(model.percentile_50_test_ae(), 1e-10);
    EXPECT_LT(model.percentile_75_ae(), 1e-10);
    EXPECT_LT(model.percentile_75_test_ae(), 1e-10);
    EXPECT_LT(model.percentile_95_ae(), 1e-10);
    EXPECT_LT(model.percentile_95_test_ae(), 1e-10);

    EXPECT_EQ(model.n_samp_train(), 10);
    EXPECT_EQ(model.n_samp_test(), 2);
    EXPECT_EQ(model.n_dim(), 2);
    EXPECT_EQ(model.prop_unit(), Unit("m"));
    EXPECT_EQ(model.prop_label(), "Property");

    EXPECT_FALSE(model.fix_intercept());

    EXPECT_LT(std::abs(model.coefs()[0][0] - 0.1), 1e-10);
    EXPECT_LT(std::abs(model.coefs()[0][1] + 2.1), 1e-10);
    EXPECT_LT(std::abs(model.coefs()[0][2] - std::log(0.001)), 1e-10);

    EXPECT_STREQ(model.toLatexString().c_str(),
                 "$\\exp\\left(c_0\\right)\\left(A\\right)^{a_0}\\left(B\\right)^{a_1}$");

    model.to_file("train_false_log_reg.dat", true);
    model.to_file("test_false_log_reg.dat", false);
}

TEST_F(ModelLogRegssorTests, FixInterceptFalseFileTest)
{
    ModelLogRegressor model("train_false_log_reg.dat", "test_false_log_reg.dat");

    model.copy_error(_error.data());
    EXPECT_LT(std::accumulate(_error.begin(),
                              _error.end(),
                              0.0,
                              [](double tot, double el) { return tot + std::abs(el); }),
              1e-5);

    EXPECT_STREQ(model.toString().c_str(), "exp(c0) * (A)^a0 * (B)^a1");
    EXPECT_LT(model.rmse(), 1e-10);
    EXPECT_LT(model.test_rmse(), 1e-10);
    EXPECT_LT(model.max_ae(), 1e-10);
    EXPECT_LT(model.test_max_ae(), 1e-10);
    EXPECT_LT(model.mae(), 1e-10);
    EXPECT_LT(model.test_mae(), 1e-10);
    EXPECT_LT(model.mape(), 1e-10);
    EXPECT_LT(model.test_mape(), 1e-10);
    EXPECT_LT(model.percentile_25_ae(), 1e-10);
    EXPECT_LT(model.percentile_25_test_ae(), 1e-10);
    EXPECT_LT(model.percentile_50_ae(), 1e-10);
    EXPECT_LT(model.percentile_50_test_ae(), 1e-10);
    EXPECT_LT(model.percentile_75_ae(), 1e-10);
    EXPECT_LT(model.percentile_75_test_ae(), 1e-10);
    EXPECT_LT(model.percentile_95_ae(), 1e-10);
    EXPECT_LT(model.percentile_95_test_ae(), 1e-10);

    EXPECT_EQ(model.n_samp_train(), 10);
    EXPECT_EQ(model.n_samp_test(), 2);
    EXPECT_EQ(model.n_dim(), 2);
    EXPECT_EQ(model.prop_unit(), Unit("m"));
    EXPECT_EQ(model.prop_label(), "Property");

    EXPECT_FALSE(model.fix_intercept());

    EXPECT_LT(std::abs(model.coefs()[0][0] - 0.1), 1e-10);
    EXPECT_LT(std::abs(model.coefs()[0][1] + 2.1), 1e-10);
    EXPECT_LT(std::abs(model.coefs()[0][2] - std::log(0.001)), 1e-10);

    boost::filesystem::remove("train_false_log_reg.dat");
    boost::filesystem::remove("test_false_log_reg.dat");
}

TEST_F(ModelLogRegssorTests, FixInterceptTrueTest)
{
    std::transform(
        _features[0]->value_ptr(),
        _features[0]->value_ptr() + 10,
        _features[1]->value_ptr(),
        _prop.begin(),
        [](double v1, double v2) { return std::log(std::pow(v1, 0.1) * std::pow(v2, -2.1)); });
    std::transform(
        _features[0]->test_value_ptr(),
        _features[0]->test_value_ptr() + 2,
        _features[1]->test_value_ptr(),
        _prop_test.begin(),
        [](double v1, double v2) { return std::log(std::pow(v1, 0.1) * std::pow(v2, -2.1)); });

    _loss = std::make_shared<LossFunctionLogPearsonRMSE>(
        _prop, _prop_test, _task_sizes_train, _task_sizes_test, true, 2);

    ModelLogRegressor model("Property",
                            Unit("m"),
                            _loss,
                            _features,
                            _leave_out_inds,
                            _sample_ids_train,
                            _sample_ids_test,
                            task_names);

    model.copy_error(_error.data());
    EXPECT_LT(std::accumulate(_error.begin(),
                              _error.end(),
                              0.0,
                              [](double tot, double el) { return tot + std::abs(el); }),
              1e-5);

    EXPECT_STREQ(model.toString().c_str(), "(A)^a0 * (B)^a1");

    EXPECT_LT(model.rmse(), 1e-10);
    EXPECT_LT(model.test_rmse(), 1e-10);
    EXPECT_LT(model.max_ae(), 1e-10);
    EXPECT_LT(model.test_max_ae(), 1e-10);
    EXPECT_LT(model.mae(), 1e-10);
    EXPECT_LT(model.test_mae(), 1e-10);
    EXPECT_LT(model.mape(), 1e-10);
    EXPECT_LT(model.test_mape(), 1e-10);
    EXPECT_LT(model.percentile_25_ae(), 1e-10);
    EXPECT_LT(model.percentile_25_test_ae(), 1e-10);
    EXPECT_LT(model.percentile_50_ae(), 1e-10);
    EXPECT_LT(model.percentile_50_test_ae(), 1e-10);
    EXPECT_LT(model.percentile_75_ae(), 1e-10);
    EXPECT_LT(model.percentile_75_test_ae(), 1e-10);
    EXPECT_LT(model.percentile_95_ae(), 1e-10);
    EXPECT_LT(model.percentile_95_test_ae(), 1e-10);

    EXPECT_EQ(model.n_samp_train(), 10);
    EXPECT_EQ(model.n_samp_test(), 2);
    EXPECT_EQ(model.n_dim(), 2);
    EXPECT_EQ(model.prop_unit(), Unit("m"));
    EXPECT_EQ(model.prop_label(), "Property");

    EXPECT_TRUE(model.fix_intercept());

    EXPECT_LT(std::abs(model.coefs()[0][0] - 0.1), 1e-10);
    EXPECT_LT(std::abs(model.coefs()[0][1] + 2.1), 1e-10);

    model.to_file("train_true_log_reg.dat", true);
    model.to_file("test_true_log_reg.dat", false);

    EXPECT_STREQ(model.toLatexString().c_str(), "$\\left(A\\right)^{a_0}\\left(B\\right)^{a_1}$");
}

TEST_F(ModelLogRegssorTests, FixInterceptTrueFileTest)
{
    ModelLogRegressor model("train_true_log_reg.dat", "test_true_log_reg.dat");

    model.copy_error(_error.data());
    EXPECT_LT(std::accumulate(_error.begin(),
                              _error.end(),
                              0.0,
                              [](double tot, double el) { return tot + std::abs(el); }),
              1e-5);

    EXPECT_STREQ(model.toString().c_str(), "(A)^a0 * (B)^a1");
    EXPECT_LT(model.rmse(), 1e-10);
    EXPECT_LT(model.test_rmse(), 1e-10);
    EXPECT_LT(model.max_ae(), 1e-10);
    EXPECT_LT(model.test_max_ae(), 1e-10);
    EXPECT_LT(model.mae(), 1e-10);
    EXPECT_LT(model.test_mae(), 1e-10);
    EXPECT_LT(model.mape(), 1e-10);
    EXPECT_LT(model.test_mape(), 1e-10);
    EXPECT_LT(model.percentile_25_ae(), 1e-10);
    EXPECT_LT(model.percentile_25_test_ae(), 1e-10);
    EXPECT_LT(model.percentile_50_ae(), 1e-10);
    EXPECT_LT(model.percentile_50_test_ae(), 1e-10);
    EXPECT_LT(model.percentile_75_ae(), 1e-10);
    EXPECT_LT(model.percentile_75_test_ae(), 1e-10);
    EXPECT_LT(model.percentile_95_ae(), 1e-10);
    EXPECT_LT(model.percentile_95_test_ae(), 1e-10);

    EXPECT_EQ(model.n_samp_train(), 10);
    EXPECT_EQ(model.n_samp_test(), 2);
    EXPECT_EQ(model.n_dim(), 2);
    EXPECT_EQ(model.prop_unit(), Unit("m"));
    EXPECT_EQ(model.prop_label(), "Property");

    EXPECT_TRUE(model.fix_intercept());

    EXPECT_LT(std::abs(model.coefs()[0][0] - 0.1), 1e-10);
    EXPECT_LT(std::abs(model.coefs()[0][1] + 2.1), 1e-10);

    boost::filesystem::remove("train_true_log_reg.dat");
    boost::filesystem::remove("test_true_log_reg.dat");
}

TEST_F(ModelLogRegssorTests, EvalTest)
{
    _loss = std::make_shared<LossFunctionLogPearsonRMSE>(
        _prop, _prop_test, _task_sizes_train, _task_sizes_test, false, 2);

    ModelLogRegressor model("Property",
                            Unit("m"),
                            _loss,
                            _features,
                            _leave_out_inds,
                            _sample_ids_train,
                            _sample_ids_test,
                            task_names);

    model.set_task_eval(0);
    std::vector<double> pt = {2.0, 2.0};
    double val = model.eval(pt.data(), task_names[0]);
    EXPECT_LT(val - 0.00025, 1e-10);

    std::vector<std::vector<double>> pts = {{2.0}, {2.0}};
    val = model.eval(pts.data(), {task_names[0]})[0];
    EXPECT_LT(val - 0.00025, 1e-10);
}
}  // namespace
