// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "external/third_party_gtest.hpp"

#include <cstdint>
#include <descriptor_identifier/solver/KSmallest.hpp>

TEST(KSmallest, ksmallest)
{
    KSmallest<int64_t> ksmallest(10, std::numeric_limits<int64_t>::max());

    for (auto& val : ksmallest.data())
    {
        EXPECT_GT(val, 10);
    }

    for (int64_t val = 100; val > 0; --val)
        ksmallest.insert(val);

    EXPECT_EQ(ksmallest.data().size(), 10);
    for (auto& val : ksmallest.data())
    {
        EXPECT_LE(val, 10);
    }
}
