// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#ifdef PARAMETERIZE
#include "external/third_party_gtest.hpp"

#include <random>

#include "feature_creation/node/FeatureNode.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/sq/parameterized_square.hpp"
#include "nl_opt/NLOptimizerLogRegression.hpp"

namespace
{
class NLOptLogRegTests : public ::testing::Test
{
protected:
    void SetUp() override
    {
        _task_sizes_train = {100};
        node_value_arrs::initialize_values_arr(_task_sizes_train, {0}, 1, 1, true);

        std::vector<double> value_1(_task_sizes_train[0], 0.0);
        std::vector<double> test_value_1(0);

        std::default_random_engine generator(0);
        nlopt::srand(42);
        std::uniform_real_distribution<double> distribution_feats(-50.0, 50.0);
        std::uniform_real_distribution<double> distribution_params(1e-10, 2.50);

        for (int ii = 0; ii < _task_sizes_train[0]; ++ii)
        {
            value_1[ii] = distribution_feats(generator);
        }

        node_ptr prim_feat = std::make_shared<FeatureNode>(
            0, "A", value_1, test_value_1, Unit("m"));
        _feat = std::make_shared<SqParamNode>(prim_feat, 1, 1e-50, 1e-40);

        _prop_train.resize(_task_sizes_train[0]);
        _params = {1.0, distribution_params(generator)};
        std::transform(_feat->value_ptr(_params.data()),
                       _feat->value_ptr(_params.data()) + _task_sizes_train[0],
                       _prop_train.data(),
                       [](double val) { return std::log(val); });
    }

    void TearDown() override { node_value_arrs::finalize_values_arr(); }

    std::vector<int> _task_sizes_train;
    std::vector<double> _prop_train;
    std::vector<double> _params;
    node_ptr _feat;
};

TEST_F(NLOptLogRegTests, ConstructorTest)
{
    NLOptimizerLogRegression opt(_task_sizes_train, _prop_train, 1, 1, 0.5, true);
    EXPECT_LT(std::abs(opt.optimize_feature_params(_feat.get())), 1e-5);
    EXPECT_LT(std::abs(_feat->parameters()[0] - _params[0]), 1e-5);
    EXPECT_LT(std::abs(_feat->parameters()[1] - _params[1]), 1e-5);
    EXPECT_FALSE(opt.free_param(2));
    EXPECT_TRUE(opt.free_param(3));
    EXPECT_EQ(opt.type(), DI_TYPE::LOG_REG);
}
}  // namespace
#endif
