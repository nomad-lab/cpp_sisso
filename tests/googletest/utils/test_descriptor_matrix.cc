// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "external/third_party_gtest.hpp"


#include "feature_creation/node/Node.hpp"
#include "utils/DescriptorMatrix.hpp"

namespace
{
TEST(DescriptorMatrix, CheckCopy)
{
    node_value_arrs::initialize_values_arr({2}, {0}, 1, 0, false);
    node_value_arrs::initialize_d_matrix_arr();
    node_value_arrs::resize_d_matrix_arr(2);

    EXPECT_EQ(node_value_arrs::N_SAMPLES, 2);
    EXPECT_EQ(node_value_arrs::N_SELECTED, 2);

    node_value_arrs::get_d_matrix_ptr(0)[0] = 2;
    node_value_arrs::get_d_matrix_ptr(0)[1] = 3;
    node_value_arrs::get_d_matrix_ptr(0)[2] = 4;
    node_value_arrs::get_d_matrix_ptr(0)[3] = 5;

    ::DescriptorMatrix descriptorMatrix;

    EXPECT_FLOAT_EQ(descriptorMatrix.getHostDescriptorMatrix()(0, 0), 2);
    EXPECT_FLOAT_EQ(descriptorMatrix.getHostDescriptorMatrix()(1, 0), 3);
    EXPECT_FLOAT_EQ(descriptorMatrix.getHostDescriptorMatrix()(0, 1), 4);
    EXPECT_FLOAT_EQ(descriptorMatrix.getHostDescriptorMatrix()(1, 1), 5);

    auto dm = Kokkos::create_mirror_view_and_copy(Kokkos::HostSpace(), descriptorMatrix.getDeviceDescriptorMatrix());

    EXPECT_FLOAT_EQ(dm(0, 0), 2);
    EXPECT_FLOAT_EQ(dm(1, 0), 3);
    EXPECT_FLOAT_EQ(dm(0, 1), 4);
    EXPECT_FLOAT_EQ(dm(1, 1), 5);

    node_value_arrs::finalize_values_arr();
}
}  // namespace
