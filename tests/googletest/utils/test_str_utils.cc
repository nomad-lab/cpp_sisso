// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "external/third_party_gtest.hpp"

#include <utils/string_utils.hpp>

namespace
{
//test mean calculations
TEST(StrUtils, SplitTrimTest)
{
    std::vector<std::string> str_split = str_utils::split_string_trim("A ; B ; C , D :     5");
    EXPECT_EQ(str_split.size(), 5);
    EXPECT_STREQ(str_split[0].c_str(), "A");
    EXPECT_STREQ(str_split[1].c_str(), "B");
    EXPECT_STREQ(str_split[2].c_str(), "C");
    EXPECT_STREQ(str_split[3].c_str(), "D");
    EXPECT_STREQ(str_split[4].c_str(), "5");

    str_split = str_utils::split_string_trim("A ; B ; C , D :     5", ";,");
    EXPECT_EQ(str_split.size(), 4);
    EXPECT_STREQ(str_split[0].c_str(), "A");
    EXPECT_STREQ(str_split[1].c_str(), "B");
    EXPECT_STREQ(str_split[2].c_str(), "C");
    EXPECT_STREQ(str_split[3].c_str(), "D :     5");

    EXPECT_STREQ(str_utils::latexify("A_sd_fg").c_str(), "A_{sd, fg}");
}
}  // namespace
