// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "external/third_party_gtest.hpp"

#include <utils/EnumerateUniqueCombinations.hpp>

namespace
{

TEST(EnumerateUniqueCombinations, constructor)
{
    using namespace ::testing;
    EXPECT_THAT(EnumerateUniqueCombinations(5, 3).get_current_combination(), ElementsAre(5, 4, 3));
    EXPECT_THAT(EnumerateUniqueCombinations(7, 4).get_current_combination(),
                ElementsAre(7, 6, 5, 4));

    EXPECT_THROW(EnumerateUniqueCombinations(5, 0), std::invalid_argument);
    EXPECT_THROW(EnumerateUniqueCombinations(3, 5), std::invalid_argument);
}

TEST(EnumerateUniqueCombinations, increment)
{
    using namespace ::testing;
    EnumerateUniqueCombinations comb(3, 2);
    EXPECT_THAT(comb.get_current_combination(), ElementsAre(3, 2));
    ++comb;
    EXPECT_THAT(comb.get_current_combination(), ElementsAre(3, 1));
    ++comb;
    EXPECT_THAT(comb.get_current_combination(), ElementsAre(3, 0));
    ++comb;
    EXPECT_THAT(comb.get_current_combination(), ElementsAre(2, 1));
    ++comb;
    EXPECT_THAT(comb.get_current_combination(), ElementsAre(2, 0));
    ++comb;
    EXPECT_THAT(comb.get_current_combination(), ElementsAre(1, 0));
    ++comb;
}

TEST(EnumerateUniqueCombinations, counting)
{
    using namespace ::testing;
    EnumerateUniqueCombinations comb(4, 2);
    int64_t counter = 0;
    while (!comb.is_finished())
    {
        ++counter;
        ++comb;
    }
    EXPECT_EQ(comb.get_num_total_combinations(), 10);
    EXPECT_EQ(comb.get_num_total_combinations(), counter);
    EXPECT_THAT(comb.get_current_combination(), ElementsAre(1, 0));
}

TEST(EnumerateUniqueCombinations, add_equal)
{
    using namespace ::testing;
    EnumerateUniqueCombinations comb1(4, 2);
    auto comb2 = comb1;
    ++comb1;
    ++comb1;
    comb2 += 2;
    EXPECT_THAT(comb1.get_current_combination(), ContainerEq(comb2.get_current_combination()));
}

TEST(EnumerateUniqueCombinations, overshoot)
{
    using namespace ::testing;
    EnumerateUniqueCombinations comb(4, 2);
    comb += 20;
    EXPECT_EQ(comb.is_finished(), true);
    EXPECT_THAT(comb.get_current_combination(), ElementsAre(1, 0));
}
}  // namespace
