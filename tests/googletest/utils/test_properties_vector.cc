// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "external/third_party_gtest.hpp"

#include "feature_creation/node/Node.hpp"
#include "utils/PropertiesVector.hpp"

namespace
{
TEST(DescriptorMatrix, CheckCopy)
{
    PropertiesVector properties({2,3,4,5});

    EXPECT_FLOAT_EQ(properties.getHostPropertiesVector()(0), 2);
    EXPECT_FLOAT_EQ(properties.getHostPropertiesVector()(1), 3);
    EXPECT_FLOAT_EQ(properties.getHostPropertiesVector()(2), 4);
    EXPECT_FLOAT_EQ(properties.getHostPropertiesVector()(3), 5);

    auto prop = Kokkos::create_mirror_view_and_copy(Kokkos::HostSpace(), properties.getDevicePropertiesVector());

    EXPECT_FLOAT_EQ(prop(0), 2);
    EXPECT_FLOAT_EQ(prop(1), 3);
    EXPECT_FLOAT_EQ(prop(2), 4);
    EXPECT_FLOAT_EQ(prop(3), 5);
}
}  // namespace
