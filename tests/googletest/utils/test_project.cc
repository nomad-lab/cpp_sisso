// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "external/third_party_gtest.hpp"

#include <feature_creation/node/ModelNode.hpp>
#include <utils/project.hpp>

namespace
{
//test mean calculations
TEST(Project, ProjectTest)
{
    node_value_arrs::initialize_values_arr({4}, {0}, 1, 0, false);
    std::vector<double> prop = {1.0, 3.0, 5.0, 6.0};
    std::vector<double> prop_class = {0.0, 0.0, 0.0, 1.0};
    std::vector<double> val = {2.0, 2.0, 3.0, 4.0};
    std::vector<double> scores(1, 0.0);
    std::vector<int> sizes(1, 4);

    std::vector<node_ptr> phi = {
        std::make_shared<FeatureNode>(0, "A", val, std::vector<double>(), Unit())};

    project_funcs::project_r(prop.data(), scores.data(), phi, sizes, 1);
    EXPECT_LT(std::abs(-0.9028289727756884 - scores[0]), 1e-10);
    scores[0] = 0.0;

    project_funcs::project_r_no_omp(prop.data(), scores.data(), phi, sizes, 1);
    EXPECT_LT(std::abs(-0.9028289727756884 - scores[0]), 1e-10);
    scores[0] = 0.0;
    node_value_arrs::finalize_values_arr();
}
}  // namespace
