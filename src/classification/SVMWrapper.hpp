// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file classification/SVMWrapper.hpp
 *  @brief Defines a class used to wrap libsvm in more C++ oriented data structure
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef LIBSVM_WRAPPER
#define LIBSVM_WRAPPER

#include <algorithm>
#include <iostream>
#include <map>

#include "external/third_party_class.hpp"
#include "feature_creation/node/value_storage/nodes_value_containers.hpp"
#include "utils/vector_utils.hpp"

// DocString: cls_svm_wrapper
/**
 * @brief Wrapper for libSVM that uses more C++ friendly data structures
 *
 */
class SVMWrapper
{
protected:
    // clang-format off
    svm_parameter _param; //!< Parameter struct for libsvm
    svm_problem _prob; //!< Problem struct for libsvm
    svm_model* _model; //!< Pointer to the model struct for libsvm

    std::vector<double> _y; //!< The vector for the data labels
    std::vector<double> _y_est; //!< The vector for the estimates of the data labels
    std::vector<svm_node> _x_space; //!< vector containing the svm_nodes representing the data
    std::vector<svm_node*> _x; //!< vector containing the pointers to the start of a new row of data

    std::vector<std::vector<double>> _coefs; //!< The coefficients for the SVM decision planes
    std::vector<double> _intercept; //!< The bias for term for the SVM decision planes
    std::vector<double> _w_remap; //!< Prefactors to convert the data to/from the preferred SVM range (0 to 1)
    std::vector<double> _b_remap;  //!< Prefactors to map the minimum of the features to 0.0

    std::map<double, double> _map_prop_vals; //!< Map of the property values to the values used for SVM
    const double _C; //!< The C parameter for the SVM calculation

    const unsigned int _n_dim; //!< The number of dimensions for the SVM problem
    const int _n_samp; //!< The number of training samples per feature
    const int _n_class; //!< The number of classes in the data set
    int _n_misclassified; //!< The number of misclassified data points in the training set
    // clang-format on

public:
    /**
     * @brief The constructor for the SVMWrapper
     *
     * @param n_class Number of classes in the dataset
     * @param n_dim Number of dimensions of the problem
     * @param n_samp Number of training samples in the dataset
     * @param prop pointer to the start of the property vector
     */
    SVMWrapper(const int n_class, const int n_dim, const int n_samp, const double* prop);

    /**
     * @brief The constructor for the SVMWrapper
     *
     * @param n_class Number of classes in the dataset
     * @param n_dim Number of dimensions of the problem
     * @param prop The property vector
     */
    SVMWrapper(const int n_class, const int n_dim, const std::vector<double> prop);

    /**
     * @brief The constructor for the SVMWrapper
     *
     * @param C The C value for the SVM calculation
     * @param n_class Number of classes in the dataset
     * @param n_dim Number of dimensions of the problem
     * @param n_samp Number of training samples in the dataset
     * @param prop pointer to the start of the property vector
     */
    SVMWrapper(
        const double C, const int n_class, const int n_dim, const int n_samp, const double* prop);

    /**
     * @brief The constructor for the SVMWrapper
     *
     * @param C The C value for the SVM calculation
     * @param n_class Number of classes in the dataset
     * @param n_dim Number of dimensions of the problem
     * @param prop The property vector
     */
    SVMWrapper(const double C, const int n_class, const int n_dim, const std::vector<double> prop);

    /**
     * @brief The copy constructor
     *
     * @param o The object to be copied
     */
    SVMWrapper(const SVMWrapper& o);

    /**
     * @brief The move constructor
     *
     * @param o The object to be moved
     */
    SVMWrapper(SVMWrapper&& o) = default;

    /**
     * @brief The destructor for the SVMWrapper
     * @details Must free the param/model while destructing
     */
    ~SVMWrapper();

    /**
     * @brief Set up the parameter object for libsvm
     *
     * @param C The C value for the SVM calculation
     */
    void setup_parameter_obj(const double C);

    /**
     * @brief Set up the x_space and objects for libsvm
     * @details Creates the svm_nodes for the data and defaults it to index=-1, value = 0.0
     */
    void setup_x_space();

    /**
     * @brief Copies the data from a set of feature indexes (sorted_dmatrix) into the x_space
     *
     * @param inds A vector storing the _d_mat_ind of the features to pull data from the Descriptor Matrix storage array
     * @param task the task number for the calculation
     */
    void copy_data(const std::vector<int> inds, const size_t task);

    /**
     * @brief Copies the data from a set data pointers
     *
     * @param val_ptrs The pointers to the feature's data
     */
    void copy_data(const std::vector<double*> val_ptrs);

    /**
     * @brief Train the SVM model
     *
     * @param remap_coefs If true remap the final coefficients back to the unscaled feature space
     */
    void train(const bool remap_coefs = true);

    /**
     * @brief Copy the data from a set of feature indexes (sorted_dmatrix) into the x_space and train the SVM model
     *
     * @param inds A vector storing the _d_mat_ind of the features to pull data from the Descriptor Matrix storage array
     * @param task the task number for the calculation
     * @param remap_coefs If true remap the final coefficients back to the unscaled feature space
     */
    void train(const std::vector<int> inds, const int task, const bool remap_coefs = true);

    /**
     * @brief Copy the data from a set data pointers and train the SVM model
     *
     * @param val_ptrs A vector storing pointers to the features training data
     * @param remap_coefs If true remap the final coefficients back to the unscaled feature space
     */
    void train(const std::vector<double*> val_ptrs, const bool remap_coefs = true);

    /**
     * @brief Predict the class of a set of data from the SVM model
     *
     * @param n_samp_test the number of test samples to predict the class off
     * @param val_ptrs A vector storing pointers to the features test data
     *
     * @return The predicted class of the test samples
     */
    std::vector<double> predict(const int n_samp_test, const std::vector<double*> val_ptrs);

    /**
     * @brief Predict the class of a set of data from the SVM model
     *
     * @param pt A vector containing the data point
     *
     * @return The predicted class of the test samples
     */
    inline double predict(const std::vector<double>& pt) const
    {
        return predict(pt.data(), pt.size());
    }

    /**
     * @brief Predict the class of a set of data from the SVM model
     *
     * @param pt A pointer to the head of the data point
     * @param n_feat The nubmer of features of the point
     *
     * @return The predicted class of the test samples
     */
    double predict(const double* pt, size_t n_feat) const;

    /**
     * @brief the coefficients of the decision planes for each of the one against one SVM models
     */
    inline std::vector<std::vector<double>> coefs() { return _coefs; }

    /**
     * @brief the list of the bias terms in all the SVM models
     */
    inline std::vector<double> intercept() const { return _intercept; }

    /**
     * @brief The number of classes in the training set
     */
    inline int n_class() const { return _n_class; }

    /**
     * @brief The number of dimensions of the SVM model
     */
    inline unsigned int n_dim() const { return _n_dim; }

    /**
     * @brief The number of samples in the training set
     */
    inline int n_samp() const { return _n_samp; }

    /**
     * @brief The predicted class for each sample in the data set
     */
    inline std::vector<double> y_estimate() const { return _y_est; }

    /**
     * @brief The actual class for each sample in the data set
     */
    inline std::vector<double> y_actual() const { return _y; }

    /**
     * @brief Number of misclassified samples in the data set
     */
    inline int n_misclassified() const { return _n_misclassified; }
};

#endif
