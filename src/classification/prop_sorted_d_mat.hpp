// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file classification/prop_sorted_d_mat.hpp
 *  @brief Central storage area for a sorted descriptor matrix based on the task/class of a sample
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef PROP_SORTED_D_MAT
#define PROP_SORTED_D_MAT

#include <numeric>
#include <stdexcept>
#include <string>
#include <vector>

namespace prop_sorted_d_mat
{
// clang-format off
    extern std::vector<double> SORTED_D_MATRIX; //!< The descriptor matrix with the feature's input data sorted by the class index

    extern std::vector<int> CLASS_START; //!< The starting point for each class
    extern std::vector<int> N_SAMPLES_PER_CLASS; //!< The number of samples in each class/task combination [task * N_TASK * N_CLASS + class]

    extern int N_TASK; //!< Number of tasks for the calculation
    extern int N_CLASS; //!< Number of classes per task for the calculation
    extern int N_FEATURES; //!< Number of features selected
    extern int N_SAMPLES; //!< Total number of samples per feature
// clang-format on

/**
     * @brief Initialize the descriptor matrix
     * @details Initialize an empty descriptor matrix
     *
     * @param n_feat The number of features
     * @param n_task The number of tasks
     * @param n_class The number of classes
     * @param n_samples_per_class The number of samples in each class/task combination [task * N_TASK * N_CLASS + class]
     */
void initialize_sorted_d_matrix_arr(int n_feat,
                                    int n_task,
                                    int n_class,
                                    std::vector<int> n_samples_per_class);

/**
     * @brief Resize the descriptor matrix to match the current number of features
     *
     * @param n_feats Number of features to select
     */
void resize_sorted_d_matrix_arr(int n_feats);

/**
     * @brief Restores all global variables to initial state
     */
void finalize_sorted_d_matrix_arr();

/**
     * @brief Get the number of samples in a particular task/class combination
     *
     * @param task The task number
     * @param cls the class number
     *
     * @return the number of samples in the class
     */
inline int get_class_size(int task, int cls) { return N_SAMPLES_PER_CLASS[task * N_CLASS + cls]; }

/**
     * @brief Get an element from the SORTED_D_MATRIX
     *
     * @param feat_ind feature index
     * @param samp_ind sample number
     *
     * @return The value of SORTED_D_MATRIX for a given feature and sample index
     */
inline double get_sorted_d_matrix_el(int feat_ind, int samp_ind)
{
    return SORTED_D_MATRIX[feat_ind * N_SAMPLES + samp_ind];
}

/**
     * @brief Access the sorted descriptor matrix by feature ind
     *
     * @param feat_ind The feature index
     *
     * @return Pointer to the start of the selected feature
     */
inline double* access_sorted_d_matrix(int feat_ind)
{
    return &SORTED_D_MATRIX[feat_ind * N_SAMPLES];
}

/**
     * @brief Get a pointer to the head of the sorted description matrix for a given task/class combination
     *
     * @param task The task number
     * @param cls The class number
     * @return Pointer to the start of a given class and task for all features
     */
inline double* access_sorted_d_matrix(int task, int cls)
{
    return &SORTED_D_MATRIX[CLASS_START[task * N_CLASS + cls]];
}

/**
     * @brief Access the sorted descriptor matrix by feature ind, class number, and task number
     *
     * @param feat_ind feature index
     * @param task The task number
     * @param cls The class number
     * @return pointer to the start of the selected feature for a given feature, class, and task
     */
inline double* access_sorted_d_matrix(int feat_ind, int task, int cls)
{
    return &SORTED_D_MATRIX[feat_ind * N_SAMPLES + CLASS_START[task * N_CLASS + cls]];
}

/**
     * @brief Access the sorted descriptor matrix by the sample index, class number, and task number
     *
     * @param sample_ind The index of the sample to point to
     * @param task The task number
     * @param cls The class number
     * @return pointer to the element of the given sample within a class and task in the first feature
     */
inline double* access_sample_sorted_d_matrix(int sample_ind, int task, int cls)
{
    return &SORTED_D_MATRIX[CLASS_START[task * N_CLASS + cls] + sample_ind];
}

/**
     * @brief Access the sorted descriptor matrix by the sample index
     *
     * @param sample_ind start of the sample index outside of the class/task number
     * @return pointer to the element of the sample in the first feature
     */
inline double* access_sample_sorted_d_matrix(int sample_ind)
{
    return &SORTED_D_MATRIX[sample_ind];
}
}  // namespace prop_sorted_d_mat

#endif
