// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file classification/ConvexHull1D.cpp
 *  @brief Implements a class that calculates the 1D Convex Hull overlap of a vector with property labels
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "classification/ConvexHull1D.hpp"

ConvexHull1D::ConvexHull1D()
    : _sorted_value(),
      _cls_max(),
      _cls_min(),
      _sorted_prop_inds(),
      _cls_start(),
      _cls_sz(),
      _n_class(0),
      _n_task(0),
      _exclude_cl_ind(-1)
{
}

ConvexHull1D::ConvexHull1D(std::vector<int> task_sizes, const double* prop)
    : _sorted_value(std::accumulate(task_sizes.begin(), task_sizes.end(), 0), 0.0),
      _cls_max(),
      _cls_min(),
      _sorted_prop_inds(std::accumulate(task_sizes.begin(), task_sizes.end(), 0), 0),
      _cls_start(),
      _cls_sz(),
      _n_class(0),
      _n_task(task_sizes.size()),
      _exclude_cl_ind(-1)
{
    initialize_prop(task_sizes, prop);
}

void ConvexHull1D::initialize_prop(std::vector<int> task_sizes, const double* prop)
{
    // Set the number of tasks and samples
    _n_task = task_sizes.size();
    _task_scores.resize(_n_task, 0.0);
    int n_samp = std::accumulate(task_sizes.begin(), task_sizes.end(), 0);

    // Set number of classes
    std::vector<double> unique_prop_vals;
    for (int pp = 0; pp < n_samp; ++pp)
    {
        if (std::find(unique_prop_vals.begin(), unique_prop_vals.end(), prop[pp]) ==
            unique_prop_vals.end())
        {
            unique_prop_vals.push_back(prop[pp]);
        }
    }
    std::sort(unique_prop_vals.begin(), unique_prop_vals.end());
    _n_class = unique_prop_vals.size();

    // Set sizes for the _cls helper variables
    _cls_max.resize(_n_class * _n_task, 0.0);
    _cls_min.resize(_n_class * _n_task, 0.0);

    _cls_sz.resize(_n_class * _n_task, 0);
    _cls_start.resize(_n_class * _n_task, 0);

    std::fill_n(_cls_sz.data(), _cls_sz.size(), 0);
    std::fill_n(_cls_start.data(), _cls_start.size(), 0);

    // Set the values of the cls vectors and sorted inds
    _sorted_value.resize(n_samp, 0.0);
    _sorted_prop_inds.resize(n_samp, 0);
    std::iota(_sorted_prop_inds.begin(), _sorted_prop_inds.end(), 0);

    std::map<double, int> cl_ind;
    for (int cc = 0; cc < _n_class; ++cc)
    {
        cl_ind[unique_prop_vals[cc]] = cc;
    }

    if (cl_ind.count(-1.0) > 0)
    {
        _exclude_cl_ind = cl_ind[-1.0];
    }

    int start = 0;
    for (size_t tt = 0; tt < task_sizes.size(); ++tt)
    {
        util_funcs::argsort<double>(_sorted_prop_inds.data() + start,
                                    _sorted_prop_inds.data() + start + task_sizes[tt],
                                    prop);

        for (int ind = start; ind < start + task_sizes[tt]; ++ind)
        {
            ++_cls_sz[tt * _n_class + cl_ind[prop[ind]]];
        }

        for (int cc = 0; cc < _n_class; ++cc)
        {
            _cls_start[tt * _n_class + cc] = start;
            start += _cls_sz[tt * _n_class + cc];
        }

        if (_exclude_cl_ind >= 0)
        {
            _cls_sz[tt * _n_class + _exclude_cl_ind] = 0;
        }
    }
}

double ConvexHull1D::overlap_1d(double* value, double width)
{
    // Initialize scores and value arrays
    std::fill_n(_task_scores.data(), _task_scores.size(), 0.0);
    for (size_t ii = 0; ii < _sorted_prop_inds.size(); ++ii)
    {
        _sorted_value[ii] = value[_sorted_prop_inds[ii]];
    }

    // Get min/max for each class
    for (int tt = 0; tt < _n_task; ++tt)
    {
        for (int cc = 0; cc < _n_class; ++cc)
        {
            _cls_max[tt * _n_class + cc] = width +
                                           *std::max_element(_sorted_value.begin() +
                                                                 _cls_start[tt * _n_class + cc],
                                                             _sorted_value.begin() +
                                                                 _cls_start[tt * _n_class + cc] +
                                                                 _cls_sz[tt * _n_class + cc]);
            _cls_min[tt * _n_class + cc] = -1.0 * width +
                                           *std::min_element(_sorted_value.begin() +
                                                                 _cls_start[tt * _n_class + cc],
                                                             _sorted_value.begin() +
                                                                 _cls_start[tt * _n_class + cc] +
                                                                 _cls_sz[tt * _n_class + cc]);
        }
    }
    // Calculate the score for the feature
    for (int tt = 0; tt < _n_task; ++tt)
    {
        double cls_norm = 1.0 / (static_cast<double>(_n_class - (_exclude_cl_ind >= 0)) *
                                 static_cast<double>(_n_class - (_exclude_cl_ind >= 0) - 1) *
                                 static_cast<double>(_n_task));
        for (int c1 = 0; c1 < _n_class; ++c1)
        {
            if (_cls_sz[tt * _n_class + c1] == 0)
            {
                continue;
            }

            double min = _cls_min[tt * _n_class + c1];
            double max = _cls_max[tt * _n_class + c1];
            for (int c2 = 0; c2 < _n_class; ++c2)
            {
                if ((c1 == c2) || (_cls_sz[tt * _n_class + c2] == 0))
                {
                    continue;
                }
                // Score is based on the number of data points in the convex overlap regions
                _task_scores[tt] += std::count_if(
                    _sorted_value.begin() + _cls_start[tt * _n_class + c2],
                    _sorted_value.begin() + _cls_start[tt * _n_class + c2] +
                        _cls_sz[tt * _n_class + c2],
                    [min, max](double val) { return (val >= min) && (val <= max); });

                // Add the distance between the class convex hulls
                _task_scores[tt] += cls_norm * (std::min(_cls_max[tt * _n_class + c1] -
                                                             _cls_min[tt * _n_class + c2],
                                                         _cls_max[tt * _n_class + c2] -
                                                             _cls_min[tt * _n_class + c1]) /
                                                std::max(_cls_max[tt * _n_class + c1] -
                                                             _cls_min[tt * _n_class + c2],
                                                         _cls_max[tt * _n_class + c2] -
                                                             _cls_min[tt * _n_class + c1]));
            }
        }
    }
    return std::accumulate(_task_scores.begin(), _task_scores.end(), 0.0);
}
