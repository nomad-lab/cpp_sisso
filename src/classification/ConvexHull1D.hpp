// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file classification/ConvexHull1D.hpp
 *  @brief Defines a class that calculates the 1D Convex Hull overlap of a vector with property labels
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef CONVEX_UTILS
#define CONVEX_UTILS

#include "external/third_party_class.hpp"

#include "classification/prop_sorted_d_mat.hpp"
#include "utils/math_funcs.hpp"
#include "utils/vector_utils.hpp"

// DocString: cls_convex_hull_1d
/**
 * @brief Calculates the 1D convex hull overlap between a vector with property labels
 */
class ConvexHull1D
{
protected:
    // clang-format off
    std::vector<double> _sorted_value; //!< The value of the input vector sorted based on the property vector
    std::vector<double> _cls_max; //!< The maximum value of the property in each class
    std::vector<double> _cls_min; //!< The minimum value of the property in each class

    std::vector<int> _sorted_prop_inds; //!< The indexes of where each element of an input vector is in _sorted_value
    std::vector<int> _cls_start; //!< The element where each new class starts
    std::vector<int> _cls_sz; //!< The number of elements in each class

    std::vector<double> _task_scores; //!< The scores for each task

    int _n_class; //!< Number of classes
    int _n_task; //!< Number of tasks
    int _exclude_cl_ind; //!< Ind corresponding to a class number of -1.0
    // clang-format on

public:
    /**
     * @brief Constructor
     *
     * @param task_sizes The size of each task
     * @param prop The pointer to the property vector
     */
    ConvexHull1D(std::vector<int> task_sizes, const double* prop);

    /**
     * @brief Default constructor
     */
    ConvexHull1D();

    /**
     * @brief Initialize the projection objects
     *
     * @param task_sizes The size of each task
     * @param prop The pointer to the property vector
     */
    void initialize_prop(std::vector<int> task_sizes, const double* prop);

    /**
     * @brief Calculate the projection scores of a set of features to a vector via Pearson correlation
     *
     * @param value The pointer to the input data
     * @param width The buffer used for calculating the overlap
     *
     * @returns The projection score for the particular feature
     */
    double overlap_1d(double* value, double width = 1e-5);

    /**
     * @brief Number of tasks
     */
    inline int n_task() const { return _n_task; }
};

#endif
