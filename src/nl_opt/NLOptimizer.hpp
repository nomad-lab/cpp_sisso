// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file nl_opt/NLOptWrapper.hpp
 *  @brief Defines a wrapper for NLopt library functions and functions for using the library
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef NL_OPT_WRAPPER
#define NL_OPT_WRAPPER
#include "external/third_party_param.hpp"

#include "classification/ConvexHull1D.hpp"
#include "feature_creation/node/Node.hpp"

// DocString: cls_nlopt_optimizer
/**
 * @brief A wrapper class to use NLOpt for finding parameters
 */
class NLOptimizer
{
protected:
    // clang-format off
    const nlopt::func _objective; //!< Objective function to use

    std::vector<double> _a; //!< The A matrix used for calculating the least squares solution of the problem
    std::vector<double> _params; //!< The scale and bias terms for the OperatorNode
    std::vector<double> _lb_init; //!< The lower bound for the scale and bias terms during the global optimization
    std::vector<double> _ub_init; //!< The upper bound for the scale and bias terms during the global optimization
    std::vector<double> _lb_final; //!< The lower bound for the scale and bias terms during the local optimization
    std::vector<double> _ub_final; //!< The upper bound for the scale and bias terms during the local optimization
    const std::vector<double> _prop; //!< The value of the property to evaluate the loss function against for the training set
    std::vector<double> _prop_copy; //!< A copy of the value of the property to evaluate the loss function against for the training set
    std::vector<double> _work; //!< Work vector for dgels
    const std::vector<double> _zeros; //!< A vector filled with zeros to fill parameters
    const std::vector<int> _task_sizes; //!< Number of training samples per task
    std::vector<bool> _free_param; //!< Elements are true if parameter is free

    const int _n_samp; //!< Total number of samples
    const int _n_rung; //!< Maximum rung of the features
    const int _n_prop; //!< Number of property vectors to optimize against
    int _max_params; //!< Maximum number of possible parameters
    int _max_param_depth; //!< The maximum depth in the binary expression tree to set non-linear optimization

    nlopt::algorithm _local_opt_alg; //!< Algorithm used for local optimization
    // clang-format on

public:
    /**
     * @brief Constructor
     *
     * @param task_sizes Number of training samples per task
     * @param prop The value of the property to evaluate the loss function against for the training set
     * @param n_rung Maximum rung of the features
     * @param objective The objective function to use for the optimization
     * @param max_param_depth The maximum depth in the binary expression tree to set non-linear optimization
     * @param reset_max_param_depth If true reset the maximum parameter depth
     * @param local_opt_alg Algorithm used for local optimization
     */
    NLOptimizer(const std::vector<int>& task_sizes,
                const std::vector<double>& prop,
                const int n_rung,
                const nlopt::func objective,
                int max_param_depth = -1,
                bool reset_max_param_depth = false,
                const nlopt::algorithm local_opt_alg = nlopt::LN_SBPLX);

    virtual ~NLOptimizer() = default;

    // DocString: nloptimizer_optimize_feature_params
    /**
     * @brief uses nlopt to optimize the parameters of a feature
     *
     * @param feat Pointer to the node to optimize
     */
    double optimize_feature_params(Node* feat);

    /**
     * @brief The number of training samples per task
     */
    inline const std::vector<int>& task_sizes() const { return _task_sizes; }

    /**
     * @brief The value of the property to evaluate the loss function against for the training set
     */
    inline const std::vector<double>& prop() const { return _prop; }

    /**
     * @brief Work vector for dgels
     */
    inline double* work() { return _work.data(); }

    /**
     * @brief Total number of samples
     */
    inline int n_samp() const { return _n_samp; }

    /**
     * @brief Maximum rung of the features
     */
    inline int n_rung() const { return _n_rung; }

    /**
     * @brief Number of property vectors to optimize against
     */
    inline int n_prop() const { return _n_prop; }

    /**
     * @brief Maximum number of possible parameters
     */
    inline int max_params() const { return _max_params; }

    /**
     * @brief The maximum depth in the binary expression tree to set non-linear optimization
     */
    inline int max_param_depth() const { return _max_param_depth; }

    /**
     * @brief Algorithm used for local optimization
     */
    inline nlopt::algorithm local_opt_alg() const { return _local_opt_alg; }

    /**
     * @brief Returns if a parameter is free
     *
     * @param pp parameter to check
     * @return True if _params[pp] is free
     */
    inline bool free_param(int pp) { return _free_param[pp]; }

    /**
     * @brief Access one of the convex hull objects for projection (If using the Classifier)
     *
     * @param ind The index of the _convex_hull vector to access
     */
    virtual std::shared_ptr<ConvexHull1D> convex_hull(int ind) const = 0;

    /**
     * @brief Access an element of parameter gradient for non-linear optimization
     *
     * @param ind The index of the gradient vector to access
     */
    virtual double* feature_gradient(int ind) = 0;

    /**
     * @brief Access an element of the residuals for non-linear optimization
     *
     * @param ind The index of the gradient vector to access
     */
    virtual double* residuals(int ind) = 0;

    /**
     * @brief The Cauchy scaling factor for regression problems
     */
    virtual double cauchy_scaling() const = 0;

    /**
     * @brief Return the type of descriptor identifier that is used
     */
    virtual DI_TYPE type() = 0;
};

namespace nlopt_objectives
{
typedef struct
{
    const double*
        _prop;  //!< A pointer to the value of the property to evaluate the loss function against for the training set
    Node* _feat;              //!< Node pointer of the feature to parameterize
    NLOptimizer* _optimizer;  //!< Data structure to store information for the optimization
} feat_data;

/**
 * @brief The objective function for a classification problem
 *
 * @param n The number of parameters for the problem
 * @param p Pointer to the start of the parameter vector
 * @param grad Pointer to the start of the gradient calculator
 * @param data Data structure to describe the problem
 * @return The minimum value of the objective function
 */
double classification(unsigned int n, const double* p, double* grad, void* data);

/**
 * @brief The objective function for a regression problem
 *
 * @param n The number of parameters for the problem
 * @param p Pointer to the start of the parameter vector
 * @param grad Pointer to the start of the gradient calculator
 * @param data Data structure to describe the problem
 * @return The minimum value of the objective function
 */
double regression(unsigned int n, const double* p, double* grad, void* data);

/**
 * @brief The objective function for a log regression problem
 *
 * @param n The number of parameters for the problem
 * @param p Pointer to the start of the parameter vector
 * @param grad Pointer to the start of the gradient calculator
 * @param data Data structure to describe the problem
 * @return The minimum value of the objective function
 */
double log_regression(unsigned int n, const double* p, double* grad, void* data);
}  // namespace nlopt_objectives

namespace nlopt_wrapper
{
// clang-format off
extern int MAX_PARAM_DEPTH; //!< The maximum depth in the binary expression tree to set non-linear optimization
extern int NLOPT_SEED; //!< The seed used for the nlOpt library
extern bool USE_GLOBAL; //!< True if global optimization is requested for non-linear optimization of parameters (Can break reproducibility)
// clang-format on
}  // namespace nlopt_wrapper

// GCOV_EXCL_START     GCOVR_EXCL_START       LCOV_EXCL_START
#ifdef PY_BINDINGS
class PyNLOptimizer : public NLOptimizer
{
    std::shared_ptr<ConvexHull1D> convex_hull(int ind) const override
    {
        PYBIND11_OVERRIDE_PURE(std::shared_ptr<ConvexHull1D>, NLOptimizer, convex_hull, ind);
    }

    double* feature_gradient(int ind) override
    {
        PYBIND11_OVERRIDE_PURE(double*, NLOptimizer, feature_gradient, ind);
    }

    double* residuals(int ind) override
    {
        PYBIND11_OVERRIDE_PURE(double*, NLOptimizer, residuals, ind);
    }

    double cauchy_scaling() const override
    {
        PYBIND11_OVERRIDE_PURE(double, NLOptimizer, cauchy_scaling, );
    }

    DI_TYPE type() override { PYBIND11_OVERRIDE_PURE(DI_TYPE, NLOptimizer, type, ); }
};
#endif
// GCOV_EXCL_STOP     GCOVR_EXCL_STOP       LCOV_EXCL_STOP
#endif
