// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file nl_opt/NLOptimizer.cpp
 *  @brief Defines a wrapper for NLopt library functions and functions for using the library
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "nl_opt/NLOptimizer.hpp"

int nlopt_wrapper::MAX_PARAM_DEPTH = -1;
int nlopt_wrapper::NLOPT_SEED = -1;
bool nlopt_wrapper::USE_GLOBAL = false;

NLOptimizer::NLOptimizer(const std::vector<int>& task_sizes,
                         const std::vector<double>& prop,
                         const int n_rung,
                         const nlopt::func objective,
                         int max_param_depth,
                         bool reset_max_param_depth,
                         const nlopt::algorithm local_opt_alg)
    : _objective(objective),
      _a(*std::max_element(task_sizes.begin(), task_sizes.end()) * 2),
      _prop(prop),
      _prop_copy(prop),
      _work(prop.size(), 0.0),
      _zeros(prop.size(), 0.0),
      _task_sizes(task_sizes),
      _n_samp(std::accumulate(task_sizes.begin(), task_sizes.end(), 0.0)),
      _n_rung(n_rung),
      _n_prop(prop.size() / _n_samp),
      _max_params(2 * task_sizes.size()),
      _max_param_depth(std::min(n_rung, max_param_depth)),
      _local_opt_alg(local_opt_alg)
{
    if (static_cast<int>(prop.size()) != _n_samp * _n_prop)
    {
        throw std::logic_error("Property vector size (" + std::to_string(_prop.size()) +
                               ") and number of samples (" + std::to_string(_n_samp * _n_prop) +
                               ") are not the same");
    }
    if (_max_param_depth == -1)
    {
        _max_param_depth = n_rung;
    }

    if (reset_max_param_depth || (nlopt_wrapper::MAX_PARAM_DEPTH == -1))
    {
        nlopt_wrapper::MAX_PARAM_DEPTH = _max_param_depth;
    }
    else if (nlopt_wrapper::MAX_PARAM_DEPTH != _max_param_depth)
    {
        throw std::logic_error("_max_param_depth (" + std::to_string(_max_param_depth) +
                               ") is not the same as the global one (" +
                               std::to_string(nlopt_wrapper::MAX_PARAM_DEPTH) + ").");
    }

    for (int rr = 1; rr <= _max_param_depth; ++rr)
    {
        _max_params += std::pow(2, rr);
    }
    _lb_init.reserve(_max_params);
    _ub_init.reserve(_max_params);
    _lb_final.reserve(_max_params);
    _ub_final.reserve(_max_params);
    _params.reserve(_max_params);
}

double NLOptimizer::optimize_feature_params(Node* feat)
{
    // Create the data struct needed by the regression functions
    nlopt_objectives::feat_data data;
    data._feat = feat;
    data._prop = _prop.data();
    data._optimizer = this;

    // Initialize data structures used for creating the parameters
    double minf = 0.0;
    int n_params = feat->n_params() + 2 * _task_sizes.size();
    // resize arrays
    _params.resize(n_params);
    _lb_init.resize(n_params);
    _ub_init.resize(n_params);
    _lb_final.resize(n_params);
    _ub_final.resize(n_params);
    _free_param.resize(n_params);

    // Set params to be 1.0 for all scale parame0.0 for all shift parameters
    std::fill_n(_params.begin(), _task_sizes.size() * 2, 1.0);
    dcopy_(_task_sizes.size(), _zeros.data(), 1, &_params[1], 2);
    feat->initialize_params(_params.data() + 2 * _task_sizes.size());

    std::string expr = feat->expr(&_params[2 * _task_sizes.size()]);

    // Set the bounds of the global search to -100 and 100
    std::fill_n(_lb_init.data(), n_params, -1e2);
    std::fill_n(_ub_init.data(), n_params, 1e2);

    if (type() == DI_TYPE::CLASS)
    {
        std::fill_n(_lb_init.data(), _task_sizes.size() * 2, 1.0);
        dcopy_(_task_sizes.size(), _zeros.data(), 1, &_lb_init[1], 2);

        std::fill_n(_ub_init.data(), _task_sizes.size() * 2, 1.0);
        dcopy_(_task_sizes.size(), _zeros.data(), 1, &_ub_init[1], 2);
    }

    // Update bounds based on the feature limits (Some have to be constants)
    feat->set_bounds(_lb_init.data() + 2 * _task_sizes.size(),
                     _ub_init.data() + 2 * _task_sizes.size());

    // Unrestricted local optimization
    std::copy_n(_lb_init.begin(), n_params, _lb_final.begin());
    std::copy_n(_ub_init.begin(), n_params, _ub_final.begin());

    std::replace(_lb_final.begin(), _lb_final.end(), -1e2, -1.0 * HUGE_VAL);
    std::replace(_ub_final.begin(), _ub_final.end(), 1e2, HUGE_VAL);

    // Helper variables
    int start = 0;
    double* val_ptr = feat->value_ptr(&_params[2 * _task_sizes.size()]);

    // Make a copy of the property vector to use in dgels
    if (type() != DI_TYPE::CLASS)
    {
        std::copy_n(_prop.data(), _prop.size(), _prop_copy.data());
        int min_prop_ind = 0;
        double max_r2 = util_funcs::r2(val_ptr, _prop.data(), _task_sizes);
        for (int pp = 1; pp < _n_prop; ++pp)
        {
            double r2 = util_funcs::r2(val_ptr, _prop.data() + pp * _n_samp, _task_sizes);
            if (r2 > max_r2)
            {
                max_r2 = r2;
                min_prop_ind = pp;
            }
        }
        for (size_t tt = 0; tt < _task_sizes.size(); ++tt)
        {
            // Create the a matrix for dgels
            std::fill_n(_a.data(), _a.size(), 1.0);
            std::copy_n(val_ptr + start, _task_sizes[tt], _a.data());

            // Solve for the best initial b/c for this task and update the parameter vector
            int info = 0;
            dgels_('N',
                   _task_sizes[tt],
                   2,
                   1,
                   _a.data(),
                   _task_sizes[tt],
                   &_prop_copy[min_prop_ind * _n_samp + start],
                   _task_sizes[tt],
                   _work.data(),
                   _work.size(),
                   &info);
            if (info == 0)
            {
                _params[tt * 2] = util_funcs::round2(_prop_copy[min_prop_ind * _n_samp + start],
                                                     33);
                _params[tt * 2 + 1] = util_funcs::round2(
                    _prop_copy[min_prop_ind * _n_samp + start + 1], 33);
            }
            // Go to the next task
            start += _task_sizes[tt];
        }
    }
    std::transform(_lb_init.begin(),
                   _lb_init.end(),
                   _ub_init.begin(),
                   _free_param.begin(),
                   [](double l, double u) { return abs(u - l) > 1e-10; });

    // Ensure that all values in the parameter vector are inside the bounds
    std::transform(_lb_init.begin(),
                   _lb_init.end(),
                   _params.begin(),
                   _params.begin(),
                   [](double lb, double p) { return p < lb ? lb : p; });

    std::transform(_ub_init.begin(),
                   _ub_init.end(),
                   _params.begin(),
                   _params.begin(),
                   [](double ub, double p) { return p > ub ? ub : p; });

    // Set up initial optimizer
    nlopt::opt opt_init(_local_opt_alg, n_params);
    opt_init.set_min_objective(_objective, &data);
    opt_init.set_maxeval(5000);
    opt_init.set_xtol_rel(1e-3);
    opt_init.set_lower_bounds(_lb_init);
    opt_init.set_upper_bounds(_ub_init);

    // Set up final optimizer
    nlopt::opt opt_final(_local_opt_alg, n_params);
    opt_final.set_min_objective(_objective, &data);
    opt_final.set_maxeval(10000);
    opt_final.set_xtol_rel(1e-6);
    opt_final.set_lower_bounds(_lb_final);
    opt_final.set_upper_bounds(_ub_final);

    // Try to optimize the parameters, if it fails set minf to a large value
    try
    {
        //Restricted local then global optimization
        opt_init.optimize(_params, minf);

        if (nlopt_wrapper::USE_GLOBAL)
        {
            // Set up global optimizer
            nlopt::opt opt_global(nlopt::GN_ISRES, n_params);
            opt_global.set_min_objective(_objective, &data);
            opt_global.set_maxeval(5000);
            opt_global.set_xtol_rel(1e-2);
            opt_global.set_lower_bounds(_lb_init);
            opt_global.set_upper_bounds(_ub_init);

            // Run the global optimization
            opt_global.optimize(_params, minf);
        }

        opt_final.optimize(_params, minf);
    }
    catch (std::exception& e)
    {
        minf = HUGE_VAL;
        std::fill_n(_params.begin() + _task_sizes.size() * 2, feat->n_params(), 1.0);
        dcopy_(feat->n_params() / 2, _zeros.data(), 1, &_params[_task_sizes.size() * 2 + 1], 2);
    }
    // Set the feature parameters to be the optimized ones
    feat->set_parameters(_params.data() + _task_sizes.size() * 2);

    // Return the min function value or infinity if the min function value is NaN
    return std::isnan(minf) ? std::numeric_limits<double>::infinity() : minf;
}

double nlopt_objectives::classification(unsigned int n, const double* p, double* grad, void* data)
{
    feat_data* d = reinterpret_cast<feat_data*>(data);
    double min_overlap = HUGE_VAL;
    for (int pp = 0; pp < d->_optimizer->n_prop(); ++pp)
    {
        double overlap = d->_optimizer->convex_hull(pp)->overlap_1d(
            d->_feat->value_ptr(p + d->_optimizer->convex_hull(pp)->n_task() * 2));
        if (std::isfinite(overlap) && (overlap < min_overlap))
        {
            min_overlap = overlap;
        }
    }
    return min_overlap;
}
double nlopt_objectives::regression(unsigned int n, const double* p, double* grad, void* data)
{
    feat_data* d = reinterpret_cast<feat_data*>(data);
    int n_task = d->_optimizer->task_sizes().size();
    int n_samp = d->_optimizer->n_samp();
    int n_prop = d->_optimizer->n_prop();
    double ci = d->_optimizer->cauchy_scaling();

    double* val_ptr = d->_feat->value_ptr(p + 2 * n_task);

    int start = 0;
    for (int pp = 0; pp < n_prop; ++pp)
    {
        start = 0;
        for (int tt = 0; tt < n_task; ++tt)
        {
            // Calculate the residual
            std::transform(val_ptr + start,
                           val_ptr + d->_optimizer->task_sizes()[tt] + start,
                           d->_prop + start + pp * n_samp,
                           d->_optimizer->residuals(start + pp * n_samp),
                           [p, tt, ci, &n_samp](double vp, double prop) {
                               double s = prop - (vp * p[2 * tt] + p[2 * tt + 1]);
                               return (ci / n_samp * std::log(1 + s * s / ci));
                           });
            start += d->_optimizer->task_sizes()[tt];
        }
    }

    // int min_prop_ind = 0;
    double min_res = util_funcs::round2(
        std::accumulate(
            d->_optimizer->residuals(0),
            d->_optimizer->residuals(n_samp),
            0.0,
            [](double total, double residual) { return total + util_funcs::round2(residual, 33); }),
        33);
    for (int pp = 1; pp < n_prop; ++pp)
    {
        double res = util_funcs::round2(
            std::accumulate(
                d->_optimizer->residuals(pp * n_samp),
                d->_optimizer->residuals((pp + 1) * n_samp),
                0.0,
                [](double total, double residual) { return total + util_funcs::round2(residual, 33); }),
            33);
        if ((std::isfinite(res) && (res < min_res)) || !std::isfinite(min_res))
        {
            // min_prop_ind = pp;
            min_res = res;
        }
    }
    // Calculate the gradient if requested
    // if(grad)
    // {
    //     std::fill_n(d->_optimizer->feature_gradient(0), n * n_samp, 0.0);
    //     for(int tt = 0; tt < n_task; ++tt)
    //     {
    //         // Calculate the difference between y and f(p, x)
    //         std::transform(
    //             val_ptr + start,
    //             val_ptr + d->_optimizer->task_sizes()[tt] + start,
    //             d->_prop + start + min_prop_ind * n_samp,
    //             d->_optimizer->feature_gradient((2 * tt + 1)*n_samp + start),
    //             [p, tt](double vp, double prop){
    //                 return prop - (vp * p[2*tt] + p[2*tt + 1]);
    //             }
    //         );

    //         // Contribution from log(1 + s/a^2)
    //         std::transform(
    //             d->_optimizer->feature_gradient((2 * tt + 1)*n_samp + start),
    //             d->_optimizer->feature_gradient((2 * tt + 1)*n_samp + start + d->_optimizer->task_sizes()[tt]),
    //             d->_optimizer->feature_gradient((2 * tt + 1)*n_samp + start),
    //             [ci, &n_samp](double R){
    //                 return -2.0 * R / n_samp / (1.0 + (R * R / ci));
    //             }
    //         );

    //         // \partial s_i/\partial \alpha_task = \partial s_i/\partial a_task * f_i
    //         std::transform(
    //             val_ptr + start,
    //             val_ptr + start + d->_optimizer->task_sizes()[tt],
    //             d->_optimizer->feature_gradient((2 * tt + 1) * n_samp + start),
    //             d->_optimizer->feature_gradient((2 * tt) * n_samp + start),
    //             [](double vp, double s){
    //                 return vp * s;
    //             }
    //         );

    //         // Calculate the gradients of the individual feature parameters
    //         // First Calculate contribution from \partial s / \partial p
    //         // \partial s_i / \partial(\alpha_f or a_f) = \partial s_i/\partial a_task * \alpha_task
    //         std::transform(
    //             d->_optimizer->feature_gradient((2 * tt + 1) * n_samp + start),
    //             d->_optimizer->feature_gradient((2 * tt + 1) * n_samp + start + d->_optimizer->task_sizes()[tt]),
    //             d->_optimizer->feature_gradient(2 * n_task * n_samp + start),
    //             [p, tt](double s){return p[2 * tt] * s;}
    //         );
    //         for(int pp = 2 * n_task + 1; pp < n; ++pp)
    //         {
    //             std::copy_n(
    //                 d->_optimizer->feature_gradient(2 * n_task * n_samp + start),
    //                 d->_optimizer->task_sizes()[tt],
    //                 d->_optimizer->feature_gradient(pp * n_samp + start)
    //             );
    //         }
    //         start += d->_optimizer->task_sizes()[tt];
    //     }
    //     // Add the component from the feature gradient

    //     d->_feat->gradient(
    //         d->_optimizer->feature_gradient(n_samp * n_task * 2),
    //         d->_optimizer->work(),
    //         p + 2 * n_task
    //     );
    //     // Total the individual residual derivatives
    //     for(int pp = 0; pp < 2 * n_task + d->_feat->n_params(); ++pp)
    //     {
    //         grad[pp] = d->_optimizer->free_param(pp) * util_funcs::round2(
    //             std::accumulate(
    //                 d->_optimizer->feature_gradient(pp * n_samp),
    //                 d->_optimizer->feature_gradient((pp + 1) * n_samp),
    //                 0.0,
    //                 [](double total, double res){return total + util_funcs::round2(res, 33);}
    //             ),
    //             33
    //         );
    //     }
    // }
    return min_res;
}

double nlopt_objectives::log_regression(unsigned int n, const double* p, double* grad, void* data)
{
    feat_data* d = reinterpret_cast<feat_data*>(data);
    int n_task = d->_optimizer->task_sizes().size();
    int n_samp = d->_optimizer->n_samp();
    int n_prop = d->_optimizer->n_prop();

    double ci = d->_optimizer->cauchy_scaling();

    double* val_ptr = d->_feat->value_ptr(p + 2 * n_task);

    int start = 0;
    for (int pp = 0; pp < n_prop; ++pp)
    {
        start = 0;
        for (int tt = 0; tt < n_task; ++tt)
        {
            // Calculate the residual
            std::transform(val_ptr + start,
                           val_ptr + d->_optimizer->task_sizes()[tt] + start,
                           d->_prop + start + pp * n_samp,
                           d->_optimizer->residuals(start + pp * n_samp),
                           [p, tt, ci, &n_samp](double vp, double prop) {
                               double s = prop - (std::log(vp) * p[2 * tt] + p[2 * tt + 1]);
                               return (ci / n_samp * std::log(1 + s * s / ci));
                           });
            start += d->_optimizer->task_sizes()[tt];
        }
    }

    // int min_prop_ind = -1;
    double min_res = HUGE_VAL;
    for (int pp = 0; pp < n_prop; ++pp)
    {
        double res = util_funcs::round2(
            std::accumulate(
                d->_optimizer->residuals(pp * n_samp),
                d->_optimizer->residuals((pp + 1) * n_samp),
                0.0,
                [](double total, double residual) { return total + util_funcs::round2(residual, 33); }),
            33);
        if (std::isfinite(res) && (res < min_res))
        {
            // min_prop_ind = pp;
            min_res = res;
        }
    }

    // if(grad)
    // {
    //     std::fill_n(d->_optimizer->feature_gradient(0), n * n_samp, 0.0);
    //     for(int tt = 0; tt < n_task; ++tt)
    //     {
    //         // Calculate the base of the gradient for each step
    //         // Contribution to the derivative from (p - (\alpha_task * log(feat_val) + a_task) )^2
    //         std::transform(
    //             val_ptr + start,
    //             val_ptr + d->_optimizer->task_sizes()[tt] + start,
    //             d->_prop + start + min_prop_ind * n_samp,
    //             d->_optimizer->feature_gradient((2 * tt + 1)*n_samp + start),
    //             [p, tt](double vp, double prop){
    //                 return prop - (std::log(vp) * p[2*tt] + p[2*tt + 1]);
    //             }
    //         );

    //         // Contribution from log(1 + s/a^2)
    //         std::transform(
    //             d->_optimizer->feature_gradient((2 * tt + 1)*n_samp + start),
    //             d->_optimizer->feature_gradient((2 * tt + 1)*n_samp + start +d->_optimizer->task_sizes()[tt]),
    //             d->_optimizer->feature_gradient((2 * tt + 1)*n_samp + start),
    //             [ci, n_samp](double s){
    //                 return (-2.0 * ci / n_samp) / (1.0 + s * s / ci) * s;
    //             }
    //         );

    //         // \partial s_i/\partial \alpha_task = \partial s_i/\partial a_task * log(f_i)
    //         std::transform(
    //             val_ptr + start,
    //             val_ptr + d->_optimizer->task_sizes()[tt] + start,
    //             d->_optimizer->feature_gradient((2 * tt + 1) * n_samp + start),
    //             d->_optimizer->feature_gradient(2 * tt * n_samp + start),
    //             [](double vp, double s){
    //                 return vp * std::log(s);
    //             }
    //         );

    //         // \partial s_i / \partial(\alpha_f or a_f) = \partial s_i/\partial a_task * \alpha_task / feat_i
    //         std::transform(
    //             d->_optimizer->feature_gradient((2 * tt + 1) * n_samp + start),
    //             d->_optimizer->feature_gradient((2 * tt + 1) * n_samp + start + d->_optimizer->task_sizes()[tt]),
    //             val_ptr + start + min_prop_ind * n_samp,
    //             d->_optimizer->feature_gradient(2 * n_task * n_samp + start),
    //             [p, tt](double s, double vp){return p[2 * tt] * s / vp;}
    //         );
    //         for(int pp = 2 * n_task + 1; pp < n; ++pp)
    //         {
    //             std::copy_n(
    //                 d->_optimizer->feature_gradient(2 * n_task * n_samp + start),
    //                 d->_optimizer->task_sizes()[tt],
    //                 d->_optimizer->feature_gradient(pp * n_samp + start)
    //             );
    //         }
    //         start += d->_optimizer->task_sizes()[tt];
    //     }

    //     // Add the component from the feature gradient
    //     d->_feat->gradient(
    //         d->_optimizer->feature_gradient(n_samp * n_task * 2),
    //         d->_optimizer->work(),
    //         p + 2 * n_task
    //     );
    //     for(int pp = 0; pp < 2 * n_task + d->_feat->n_params(); ++ pp)
    //     {
    //         grad[pp] = util_funcs::round2(
    //             std::accumulate(
    //                 d->_optimizer->feature_gradient(pp * n_samp),
    //                 d->_optimizer->feature_gradient((pp + 1) * n_samp),
    //                 0.0,
    //                 [](double total, double res){return total + util_funcs::round2(res, 33);}
    //             ),
    //             33
    //         );
    //     }
    // }

    return min_res;
}
