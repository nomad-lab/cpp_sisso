// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file nl_opt/NLOptizerRegression.cpp
 *  @brief Defines a class to perform non-linear optimization for regression problems
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "nl_opt/NLOptimizerRegression.hpp"

NLOptimizerRegression::NLOptimizerRegression(const std::vector<int>& task_sizes,
                                             const std::vector<double>& prop,
                                             const int n_rung,
                                             int max_param_depth,
                                             const double cauchy_scaling,
                                             const bool log_reg,
                                             bool reset_max_param_depth,
                                             const nlopt::algorithm local_opt_alg)
    : NLOptimizer(task_sizes,
                  prop,
                  n_rung,
                  log_reg ? nlopt_objectives::log_regression : nlopt_objectives::regression,
                  max_param_depth,
                  reset_max_param_depth,
                  local_opt_alg),
      _feature_gradient(_max_params * _n_samp, 0.0),
      _residuals(_n_samp * _n_prop, 0.0),
      _cauchy_scaling(cauchy_scaling * cauchy_scaling)
{
}
