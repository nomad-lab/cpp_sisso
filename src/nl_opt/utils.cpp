// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file nl_opt/utils.cpp
 *  @brief Defines utilities to get optimizers
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "nl_opt/utils.hpp"

std::shared_ptr<NLOptimizer> nlopt_wrapper::get_optimizer(std::string project_type,
                                                          const std::vector<int>& task_sizes,
                                                          const std::vector<double>& prop,
                                                          const int n_rung,
                                                          int max_param_depth,
                                                          const double cauchy_scaling,
                                                          bool reset_max_param_depth)
{
    if (project_type.compare("classification") == 0)
    {
        return std::make_shared<NLOptimizerClassification>(
            task_sizes, prop, n_rung, max_param_depth, reset_max_param_depth);
    }
    else if (project_type.compare("regression") == 0)
    {
        return std::make_shared<NLOptimizerRegression>(
            task_sizes, prop, n_rung, max_param_depth, cauchy_scaling, reset_max_param_depth);
    }
    else if (project_type.compare("log_regression") == 0)
    {
        return std::make_shared<NLOptimizerLogRegression>(
            task_sizes, prop, n_rung, max_param_depth, cauchy_scaling, reset_max_param_depth);
    }
    else
    {
        throw std::logic_error("Invalid project type (" + project_type +
                               ") was passed to get_optimizer.");
    }

    return nullptr;
}
