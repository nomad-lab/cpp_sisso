// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/FeatureNode.cpp
 *  @brief Implements the class that represent the primary features in SISSO
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 *
 *  This class represents the leaves of all of the binary expression trees.
 */

#include "feature_creation/node/FeatureNode.hpp"

FeatureNode::FeatureNode() {}

FeatureNode::FeatureNode(const unsigned long int feat_ind,
                         const std::string expr,
                         const std::vector<double> value,
                         const std::vector<double> test_value,
                         const Unit unit,
                         const bool set_val)
    : FeatureNode(feat_ind, expr, value, test_value, unit, Domain(), set_val)
{
}

FeatureNode::FeatureNode(const unsigned long int feat_ind,
                         const std::string expr,
                         const std::vector<double> value,
                         const std::vector<double> test_value,
                         const Unit unit,
                         const Domain domain,
                         const bool set_val)
    : Node(feat_ind, value.size(), test_value.size()),
      _value(value),
      _test_value(test_value),
      _domain(domain),
      _unit(unit),
      _expr(expr)
{
    if (set_val)
    {
        // Automatically resize the storage arrays
        if (node_value_arrs::N_STORE_FEATURES == 0)
        {
            node_value_arrs::initialize_values_arr(_n_samp, _n_samp_test, 1 + feat_ind);
        }
        else if ((_n_samp != node_value_arrs::N_SAMPLES) ||
                 (_n_samp_test != node_value_arrs::N_SAMPLES_TEST))
        {
            throw std::logic_error(
                "Number of samples in current feature is not the same as the others, (" +
                std::to_string(_n_samp) + " and " + std::to_string(_n_samp_test) + " vs. " +
                std::to_string(node_value_arrs::N_SAMPLES) + " and " +
                std::to_string(node_value_arrs::N_SAMPLES_TEST) + ")");
        }
        else if (feat_ind >= node_value_arrs::N_PRIMARY_FEATURES)
        {
            node_value_arrs::resize_values_arr(0, feat_ind + 1);
        }
        set_value();
        set_test_value();
    }
}

FeatureNode::~FeatureNode() {}

bool FeatureNode::is_const() const
{
    bool is_c = false;
    int pos = 0;

    double* val_ptr = value_ptr();
    for (auto& sz : node_value_arrs::TASK_SZ_TRAIN)
    {
        double mean = util_funcs::mean<double>(val_ptr + pos, sz);
        is_c = is_c || std::all_of(val_ptr + pos, val_ptr + pos + sz, [&mean](double d) {
                   return std::abs(d - mean) < 1e-12;
               });
        pos += sz;
    }
    return is_c;
}

void FeatureNode::update_add_sub_leaves(std::map<std::string, int>& add_sub_leaves,
                                        const int pl_mn,
                                        int& expected_abs_tot) const
{
    if (add_sub_leaves.count(_expr) > 0)
    {
        add_sub_leaves[_expr] += pl_mn;
    }
    else
    {
        add_sub_leaves[_expr] = pl_mn;
    }

    ++expected_abs_tot;
}

void FeatureNode::update_div_mult_leaves(std::map<std::string, double>& div_mult_leaves,
                                         const double fact,
                                         double& expected_abs_tot) const
{
    if (div_mult_leaves.count(_expr) > 0)
    {
        div_mult_leaves[_expr] += fact;
    }
    else
    {
        div_mult_leaves[_expr] = fact;
    }

    expected_abs_tot += std::abs(fact);
}

void FeatureNode::update_primary_feature_decomp(std::map<std::string, int>& pf_decomp) const
{
    if (pf_decomp.count(_expr) > 0)
    {
        pf_decomp[_expr] += 1;
    }
    else
    {
        pf_decomp[_expr] = 1;
    }
}

// BOOST_CLASS_EXPORT(FeatureNode)
