// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/ModelNode.cpp
 *  @brief Implements the class used to transfer features (Nodes) to the Model objects
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "feature_creation/node/ModelNode.hpp"

ModelNode::ModelNode() {}

ModelNode::ModelNode(const unsigned long int feat_ind,
                     const int rung,
                     const std::string expr,
                     const std::string latex_expr,
                     const std::string postfix_expr,
                     const std::string matlab_fxn_expr,
                     const std::vector<double> value,
                     const std::vector<double> test_value,
                     const std::vector<std::string> x_in_expr_list,
                     const Unit unit)
    : FeatureNode(feat_ind, expr, value, test_value, unit, Domain(), false),
      _value_svm(_n_samp),
      _test_value_svm(_n_samp_test),
      _x_in_expr_list(x_in_expr_list),
      _postfix_expr(postfix_expr),
      _latex_expr(latex_expr),
      _matlab_fxn_expr(matlab_fxn_expr),
      _b_remap_svm(0.0),
      _w_remap_svm(1.0),
      _rung(rung),
      _n_leaves(0)
{
    double w_remap_svm_temp = 1.0 / (*std::max_element(_value.begin(), _value.end()) -
                                     *std::min_element(_value.begin(), _value.end()));
    double b_remap_svm_temp = *std::min_element(_value.begin(), _value.end());

    _w_remap_svm = w_remap_svm_temp;
    _b_remap_svm = b_remap_svm_temp;

    std::transform(_value.begin(),
                   _value.end(),
                   _value_svm.begin(),
                   [w_remap_svm_temp, b_remap_svm_temp](double val) {
                       return (val - b_remap_svm_temp) * w_remap_svm_temp;
                   });

    std::transform(_test_value.begin(),
                   _test_value.end(),
                   _test_value_svm.begin(),
                   [w_remap_svm_temp, b_remap_svm_temp](double val) {
                       return (val - b_remap_svm_temp) * w_remap_svm_temp;
                   });

    generate_fxn_list();
}

ModelNode::ModelNode(const unsigned long int feat_ind,
                     const int rung,
                     const std::string expr,
                     const std::string latex_expr,
                     const std::string postfix_expr,
                     const std::string matlab_fxn_expr,
                     const std::vector<double> value,
                     const std::vector<double> test_value,
                     const std::vector<std::string> x_in_expr_list,
                     const Unit unit,
                     const Domain domain)
    : FeatureNode(feat_ind, expr, value, test_value, unit, domain, false),
      _value_svm(_n_samp),
      _test_value_svm(_n_samp_test),
      _x_in_expr_list(x_in_expr_list),
      _postfix_expr(postfix_expr),
      _latex_expr(latex_expr),
      _matlab_fxn_expr(matlab_fxn_expr),
      _b_remap_svm(0.0),
      _w_remap_svm(1.0),
      _rung(rung),
      _n_leaves(0)
{
    double w_remap_svm_temp = 1.0 / (*std::max_element(_value.begin(), _value.end()) -
                                     *std::min_element(_value.begin(), _value.end()));
    double b_remap_svm_temp = *std::min_element(_value.begin(), _value.end());

    _w_remap_svm = w_remap_svm_temp;
    _b_remap_svm = b_remap_svm_temp;

    std::transform(_value.begin(),
                   _value.end(),
                   _value_svm.begin(),
                   [w_remap_svm_temp, b_remap_svm_temp](double val) {
                       return (val - b_remap_svm_temp) * w_remap_svm_temp;
                   });

    std::transform(_test_value.begin(),
                   _test_value.end(),
                   _test_value_svm.begin(),
                   [w_remap_svm_temp, b_remap_svm_temp](double val) {
                       return (val - b_remap_svm_temp) * w_remap_svm_temp;
                   });

    generate_fxn_list();
}

ModelNode::ModelNode(node_ptr in_node)
    : FeatureNode(in_node->feat_ind(),
                  in_node->expr(),
                  in_node->value(),
                  in_node->test_value(),
                  in_node->unit(),
                  in_node->domain(),
                  false),
      _value_svm(_n_samp),
      _test_value_svm(_n_samp_test),
      _x_in_expr_list(in_node->x_in_expr_list()),
      _postfix_expr(in_node->postfix_expr()),
      _latex_expr(in_node->latex_expr()),
      _matlab_fxn_expr(in_node->matlab_fxn_expr()),
      _b_remap_svm(0.0),
      _w_remap_svm(1.0),
      _rung(in_node->rung()),
      _n_leaves(0)
{
    _d_mat_ind = in_node->d_mat_ind();
    _selected = in_node->selected();

    double w_remap_svm_temp = 1.0 / (*std::max_element(_value.begin(), _value.end()) -
                                     *std::min_element(_value.begin(), _value.end()));
    double b_remap_svm_temp = *std::min_element(_value.begin(), _value.end());

    _w_remap_svm = w_remap_svm_temp;
    _b_remap_svm = b_remap_svm_temp;

    std::transform(_value.begin(),
                   _value.end(),
                   _value_svm.begin(),
                   [w_remap_svm_temp, b_remap_svm_temp](double val) {
                       return (val - b_remap_svm_temp) * w_remap_svm_temp;
                   });

    std::transform(_test_value.begin(),
                   _test_value.end(),
                   _test_value_svm.begin(),
                   [w_remap_svm_temp, b_remap_svm_temp](double val) {
                       return (val - b_remap_svm_temp) * w_remap_svm_temp;
                   });

    generate_fxn_list();
}

void ModelNode::generate_fxn_list()
{
    std::vector<std::string> postfix_split = str_utils::split_string_trim(_postfix_expr, "|");
    std::vector<double> params;
    size_t pp = 0;
    std::vector<fxn_nterms_pair> fxn_list_rev;
    // read postfix in reverse to get inverse prefix
    for (int pf = postfix_split.size() - 1; pf >= 0; --pf)
    {
        if (pp == params.size())
        {
            pp = 0;
            params.resize(0);
        }
        std::string term = postfix_split[pf];
        if (term.find_first_not_of("0123456789") == std::string::npos)
        {
            ++_n_leaves;
        }
        else
        {
            std::vector<std::string> op_terms = str_utils::split_string_trim(term, ":,");
            if (op_terms[0] == "add")
            {
#ifndef PARAMETERIZE
                fxn_list_rev.push_back(std::make_pair(
                    [=](int n_samp, std::vector<double*>& stack) {
                        allowed_op_funcs::add(n_samp,
                                              stack[stack.size() - 2],
                                              stack[stack.size() - 1],
                                              1.0,
                                              0.0,
                                              stack[stack.size() - 2]);
                        stack.pop_back();
                    },
                    2));
#else
                if ((op_terms.size() > 1) && (params.size() == 0))
                {
                    params.resize(op_terms.size() - 1);
                    std::transform(
                        op_terms.begin() + 1, op_terms.end(), params.begin(), [](std::string s) {
                            return std::stod(s);
                        });
                }
                if (params.size() > 0)
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::add(n_samp,
                                                  stack[stack.size() - 2],
                                                  stack[stack.size() - 1],
                                                  params[pp],
                                                  params[pp + 1],
                                                  stack[stack.size() - 2]);
                            stack.pop_back();
                        },
                        2));
                    pp += 2;
                }
                else
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::add(n_samp,
                                                  stack[stack.size() - 2],
                                                  stack[stack.size() - 1],
                                                  1.0,
                                                  0.0,
                                                  stack[stack.size() - 2]);
                            stack.pop_back();
                        },
                        2));
                }
#endif
            }
            else if (op_terms[0] == "sub")
            {
#ifndef PARAMETERIZE
                fxn_list_rev.push_back(std::make_pair(
                    [=](int n_samp, std::vector<double*>& stack) {
                        allowed_op_funcs::sub(n_samp,
                                              stack[stack.size() - 2],
                                              stack[stack.size() - 1],
                                              1.0,
                                              0.0,
                                              stack[stack.size() - 2]);
                        stack.pop_back();
                    },
                    2));
#else
                if ((op_terms.size() > 1) && (params.size() == 0))
                {
                    params.resize(op_terms.size() - 1);
                    std::transform(
                        op_terms.begin() + 1, op_terms.end(), params.begin(), [](std::string s) {
                            return std::stod(s);
                        });
                }
                if (params.size() > 0)
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::sub(n_samp,
                                                  stack[stack.size() - 2],
                                                  stack[stack.size() - 1],
                                                  params[pp],
                                                  params[pp + 1],
                                                  stack[stack.size() - 2]);
                            stack.pop_back();
                        },
                        2));
                    pp += 2;
                }
                else
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::sub(n_samp,
                                                  stack[stack.size() - 2],
                                                  stack[stack.size() - 1],
                                                  1.0,
                                                  0.0,
                                                  stack[stack.size() - 2]);
                            stack.pop_back();
                        },
                        2));
                }
#endif
            }
            else if (op_terms[0] == "abd")
            {
#ifndef PARAMETERIZE
                fxn_list_rev.push_back(std::make_pair(
                    [=](int n_samp, std::vector<double*>& stack) {
                        allowed_op_funcs::abs_diff(n_samp,
                                                   stack[stack.size() - 2],
                                                   stack[stack.size() - 1],
                                                   1.0,
                                                   0.0,
                                                   stack[stack.size() - 2]);
                        stack.pop_back();
                    },
                    2));
#else
                if ((op_terms.size() > 1) && (params.size() == 0))
                {
                    params.resize(op_terms.size() - 1);
                    std::transform(
                        op_terms.begin() + 1, op_terms.end(), params.begin(), [](std::string s) {
                            return std::stod(s);
                        });
                }
                if (params.size() > 0)
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::abs_diff(n_samp,
                                                       stack[stack.size() - 2],
                                                       stack[stack.size() - 1],
                                                       params[pp],
                                                       params[pp + 1],
                                                       stack[stack.size() - 2]);
                            stack.pop_back();
                        },
                        2));
                    pp += 2;
                }
                else
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::abs_diff(n_samp,
                                                       stack[stack.size() - 2],
                                                       stack[stack.size() - 1],
                                                       1.0,
                                                       0.0,
                                                       stack[stack.size() - 2]);
                            stack.pop_back();
                        },
                        2));
                }
#endif
            }
            else if (op_terms[0] == "mult")
            {
#ifndef PARAMETERIZE
                fxn_list_rev.push_back(std::make_pair(
                    [=](int n_samp, std::vector<double*>& stack) {
                        allowed_op_funcs::mult(n_samp,
                                               stack[stack.size() - 2],
                                               stack[stack.size() - 1],
                                               1.0,
                                               0.0,
                                               stack[stack.size() - 2]);
                        stack.pop_back();
                    },
                    2));
#else
                if ((op_terms.size() > 1) && (params.size() == 0))
                {
                    params.resize(op_terms.size() - 1);
                    std::transform(
                        op_terms.begin() + 1, op_terms.end(), params.begin(), [](std::string s) {
                            return std::stod(s);
                        });
                }
                if (params.size() > 0)
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::mult(n_samp,
                                                   stack[stack.size() - 2],
                                                   stack[stack.size() - 1],
                                                   params[pp],
                                                   params[pp + 1],
                                                   stack[stack.size() - 2]);
                            stack.pop_back();
                        },
                        2));
                    pp += 2;
                }
                else
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::mult(n_samp,
                                                   stack[stack.size() - 2],
                                                   stack[stack.size() - 1],
                                                   1.0,
                                                   0.0,
                                                   stack[stack.size() - 2]);
                            stack.pop_back();
                        },
                        2));
                }
#endif
            }
            else if (op_terms[0] == "div")
            {
#ifndef PARAMETERIZE
                fxn_list_rev.push_back(std::make_pair(
                    [=](int n_samp, std::vector<double*>& stack) {
                        allowed_op_funcs::div(n_samp,
                                              stack[stack.size() - 2],
                                              stack[stack.size() - 1],
                                              1.0,
                                              0.0,
                                              stack[stack.size() - 2]);
                        stack.pop_back();
                    },
                    2));
#else
                if ((op_terms.size() > 1) && (params.size() == 0))
                {
                    params.resize(op_terms.size() - 1);
                    std::transform(
                        op_terms.begin() + 1, op_terms.end(), params.begin(), [](std::string s) {
                            return std::stod(s);
                        });
                }
                if (params.size() > 0)
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::div(n_samp,
                                                  stack[stack.size() - 2],
                                                  stack[stack.size() - 1],
                                                  params[pp],
                                                  params[pp + 1],
                                                  stack[stack.size() - 2]);
                            stack.pop_back();
                        },
                        2));
                    pp += 2;
                }
                else
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::div(n_samp,
                                                  stack[stack.size() - 2],
                                                  stack[stack.size() - 1],
                                                  1.0,
                                                  0.0,
                                                  stack[stack.size() - 2]);
                            stack.pop_back();
                        },
                        2));
                }
#endif
            }
            else if (op_terms[0] == "abs")
            {
#ifndef PARAMETERIZE
                fxn_list_rev.push_back(std::make_pair(
                    [=](int n_samp, std::vector<double*>& stack) {
                        allowed_op_funcs::abs(
                            n_samp, stack[stack.size() - 1], 1.0, 0.0, stack[stack.size() - 1]);
                    },
                    1));
#else
                if ((op_terms.size() > 1) && (params.size() == 0))
                {
                    params.resize(op_terms.size() - 1);
                    std::transform(
                        op_terms.begin() + 1, op_terms.end(), params.begin(), [](std::string s) {
                            return std::stod(s);
                        });
                }
                if (params.size() > 0)
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::abs(n_samp,
                                                  stack[stack.size() - 1],
                                                  params[pp],
                                                  params[pp + 1],
                                                  stack[stack.size() - 1]);
                        },
                        1));
                    pp += 2;
                }
                else
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::abs(
                                n_samp, stack[stack.size() - 1], 1.0, 0.0, stack[stack.size() - 1]);
                        },
                        1));
                }
#endif
            }
            else if (op_terms[0] == "inv")
            {
#ifndef PARAMETERIZE
                fxn_list_rev.push_back(std::make_pair(
                    [=](int n_samp, std::vector<double*>& stack) {
                        allowed_op_funcs::inv(
                            n_samp, stack[stack.size() - 1], 1.0, 0.0, stack[stack.size() - 1]);
                    },
                    1));
#else
                if ((op_terms.size() > 1) && (params.size() == 0))
                {
                    params.resize(op_terms.size() - 1);
                    std::transform(
                        op_terms.begin() + 1, op_terms.end(), params.begin(), [](std::string s) {
                            return std::stod(s);
                        });
                }
                if (params.size() > 0)
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::inv(n_samp,
                                                  stack[stack.size() - 1],
                                                  params[pp],
                                                  params[pp + 1],
                                                  stack[stack.size() - 1]);
                        },
                        1));
                    pp += 2;
                }
                else
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::inv(
                                n_samp, stack[stack.size() - 1], 1.0, 0.0, stack[stack.size() - 1]);
                        },
                        1));
                }
#endif
            }
            else if (op_terms[0] == "exp")
            {
#ifndef PARAMETERIZE
                fxn_list_rev.push_back(std::make_pair(
                    [=](int n_samp, std::vector<double*>& stack) {
                        allowed_op_funcs::exp(
                            n_samp, stack[stack.size() - 1], 1.0, 0.0, stack[stack.size() - 1]);
                    },
                    1));
#else
                if ((op_terms.size() > 1) && (params.size() == 0))
                {
                    params.resize(op_terms.size() - 1);
                    std::transform(
                        op_terms.begin() + 1, op_terms.end(), params.begin(), [](std::string s) {
                            return std::stod(s);
                        });
                }
                if (params.size() > 0)
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::exp(n_samp,
                                                  stack[stack.size() - 1],
                                                  params[pp],
                                                  params[pp + 1],
                                                  stack[stack.size() - 1]);
                        },
                        1));
                    pp += 2;
                }
                else
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::exp(
                                n_samp, stack[stack.size() - 1], 1.0, 0.0, stack[stack.size() - 1]);
                        },
                        1));
                }
#endif
            }
            else if (op_terms[0] == "nexp")
            {
#ifndef PARAMETERIZE
                fxn_list_rev.push_back(std::make_pair(
                    [=](int n_samp, std::vector<double*>& stack) {
                        allowed_op_funcs::neg_exp(
                            n_samp, stack[stack.size() - 1], 1.0, 0.0, stack[stack.size() - 1]);
                    },
                    1));
#else
                if ((op_terms.size() > 1) && (params.size() == 0))
                {
                    params.resize(op_terms.size() - 1);
                    std::transform(
                        op_terms.begin() + 1, op_terms.end(), params.begin(), [](std::string s) {
                            return std::stod(s);
                        });
                }
                if (params.size() > 0)
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::neg_exp(n_samp,
                                                      stack[stack.size() - 1],
                                                      params[pp],
                                                      params[pp + 1],
                                                      stack[stack.size() - 1]);
                        },
                        1));
                    pp += 2;
                }
                else
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::neg_exp(
                                n_samp, stack[stack.size() - 1], 1.0, 0.0, stack[stack.size() - 1]);
                        },
                        1));
                }
#endif
            }
            else if (op_terms[0] == "log")
            {
#ifndef PARAMETERIZE
                fxn_list_rev.push_back(std::make_pair(
                    [=](int n_samp, std::vector<double*>& stack) {
                        allowed_op_funcs::log(
                            n_samp, stack[stack.size() - 1], 1.0, 0.0, stack[stack.size() - 1]);
                    },
                    1));
#else
                if ((op_terms.size() > 1) && (params.size() == 0))
                {
                    params.resize(op_terms.size() - 1);
                    std::transform(
                        op_terms.begin() + 1, op_terms.end(), params.begin(), [](std::string s) {
                            return std::stod(s);
                        });
                }
                if (params.size() > 0)
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::log(n_samp,
                                                  stack[stack.size() - 1],
                                                  params[pp],
                                                  params[pp + 1],
                                                  stack[stack.size() - 1]);
                        },
                        1));
                    pp += 2;
                }
                else
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::log(
                                n_samp, stack[stack.size() - 1], 1.0, 0.0, stack[stack.size() - 1]);
                        },
                        1));
                }
#endif
            }
            else if (op_terms[0] == "sin")
            {
#ifndef PARAMETERIZE
                fxn_list_rev.push_back(std::make_pair(
                    [=](int n_samp, std::vector<double*>& stack) {
                        allowed_op_funcs::sin(
                            n_samp, stack[stack.size() - 1], 1.0, 0.0, stack[stack.size() - 1]);
                    },
                    1));
#else
                if ((op_terms.size() > 1) && (params.size() == 0))
                {
                    params.resize(op_terms.size() - 1);
                    std::transform(
                        op_terms.begin() + 1, op_terms.end(), params.begin(), [](std::string s) {
                            return std::stod(s);
                        });
                }
                if (params.size() > 0)
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::sin(n_samp,
                                                  stack[stack.size() - 1],
                                                  params[pp],
                                                  params[pp + 1],
                                                  stack[stack.size() - 1]);
                        },
                        1));
                    pp += 2;
                }
                else
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::sin(
                                n_samp, stack[stack.size() - 1], 1.0, 0.0, stack[stack.size() - 1]);
                        },
                        1));
                }
#endif
            }
            else if (op_terms[0] == "cos")
            {
#ifndef PARAMETERIZE
                fxn_list_rev.push_back(std::make_pair(
                    [=](int n_samp, std::vector<double*>& stack) {
                        allowed_op_funcs::cos(
                            n_samp, stack[stack.size() - 1], 1.0, 0.0, stack[stack.size() - 1]);
                    },
                    1));
#else
                if ((op_terms.size() > 1) && (params.size() == 0))
                {
                    params.resize(op_terms.size() - 1);
                    std::transform(
                        op_terms.begin() + 1, op_terms.end(), params.begin(), [](std::string s) {
                            return std::stod(s);
                        });
                }
                if (params.size() > 0)
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::cos(n_samp,
                                                  stack[stack.size() - 1],
                                                  params[pp],
                                                  params[pp + 1],
                                                  stack[stack.size() - 1]);
                        },
                        1));
                    pp += 2;
                }
                else
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::cos(
                                n_samp, stack[stack.size() - 1], 1.0, 0.0, stack[stack.size() - 1]);
                        },
                        1));
                }
#endif
            }
            else if (op_terms[0] == "sq")
            {
#ifndef PARAMETERIZE
                fxn_list_rev.push_back(std::make_pair(
                    [=](int n_samp, std::vector<double*>& stack) {
                        allowed_op_funcs::sq(
                            n_samp, stack[stack.size() - 1], 1.0, 0.0, stack[stack.size() - 1]);
                    },
                    1));
#else
                if ((op_terms.size() > 1) && (params.size() == 0))
                {
                    params.resize(op_terms.size() - 1);
                    std::transform(
                        op_terms.begin() + 1, op_terms.end(), params.begin(), [](std::string s) {
                            return std::stod(s);
                        });
                }
                if (params.size() > 0)
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::sq(n_samp,
                                                 stack[stack.size() - 1],
                                                 params[pp],
                                                 params[pp + 1],
                                                 stack[stack.size() - 1]);
                        },
                        1));
                    pp += 2;
                }
                else
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::sq(
                                n_samp, stack[stack.size() - 1], 1.0, 0.0, stack[stack.size() - 1]);
                        },
                        1));
                }
#endif
            }
            else if (op_terms[0] == "sqrt")
            {
#ifndef PARAMETERIZE
                fxn_list_rev.push_back(std::make_pair(
                    [=](int n_samp, std::vector<double*>& stack) {
                        allowed_op_funcs::sqrt(
                            n_samp, stack[stack.size() - 1], 1.0, 0.0, stack[stack.size() - 1]);
                    },
                    1));
#else
                if ((op_terms.size() > 1) && (params.size() == 0))
                {
                    params.resize(op_terms.size() - 1);
                    std::transform(
                        op_terms.begin() + 1, op_terms.end(), params.begin(), [](std::string s) {
                            return std::stod(s);
                        });
                }
                if (params.size() > 0)
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::sqrt(n_samp,
                                                   stack[stack.size() - 1],
                                                   params[pp],
                                                   params[pp + 1],
                                                   stack[stack.size() - 1]);
                        },
                        1));
                    pp += 2;
                }
                else
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::sqrt(
                                n_samp, stack[stack.size() - 1], 1.0, 0.0, stack[stack.size() - 1]);
                        },
                        1));
                }
#endif
            }
            else if (op_terms[0] == "cb")
            {
#ifndef PARAMETERIZE
                fxn_list_rev.push_back(std::make_pair(
                    [=](int n_samp, std::vector<double*>& stack) {
                        allowed_op_funcs::cb(
                            n_samp, stack[stack.size() - 1], 1.0, 0.0, stack[stack.size() - 1]);
                    },
                    1));
#else
                if ((op_terms.size() > 1) && (params.size() == 0))
                {
                    params.resize(op_terms.size() - 1);
                    std::transform(
                        op_terms.begin() + 1, op_terms.end(), params.begin(), [](std::string s) {
                            return std::stod(s);
                        });
                }
                if (params.size() > 0)
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::cb(n_samp,
                                                 stack[stack.size() - 1],
                                                 params[pp],
                                                 params[pp + 1],
                                                 stack[stack.size() - 1]);
                        },
                        1));
                    pp += 2;
                }
                else
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::cb(
                                n_samp, stack[stack.size() - 1], 1.0, 0.0, stack[stack.size() - 1]);
                        },
                        1));
                }
#endif
            }
            else if (op_terms[0] == "cbrt")
            {
#ifndef PARAMETERIZE
                fxn_list_rev.push_back(std::make_pair(
                    [=](int n_samp, std::vector<double*>& stack) {
                        allowed_op_funcs::cbrt(
                            n_samp, stack[stack.size() - 1], 1.0, 0.0, stack[stack.size() - 1]);
                    },
                    1));
#else
                if ((op_terms.size() > 1) && (params.size() == 0))
                {
                    params.resize(op_terms.size() - 1);
                    std::transform(
                        op_terms.begin() + 1, op_terms.end(), params.begin(), [](std::string s) {
                            return std::stod(s);
                        });
                }
                if (params.size() > 0)
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::cbrt(n_samp,
                                                   stack[stack.size() - 1],
                                                   params[pp],
                                                   params[pp + 1],
                                                   stack[stack.size() - 1]);
                        },
                        1));
                    pp += 2;
                }
                else
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::cbrt(
                                n_samp, stack[stack.size() - 1], 1.0, 0.0, stack[stack.size() - 1]);
                        },
                        1));
                }
#endif
            }
            else if (op_terms[0] == "sp")
            {
#ifndef PARAMETERIZE
                fxn_list_rev.push_back(std::make_pair(
                    [=](int n_samp, std::vector<double*>& stack) {
                        allowed_op_funcs::sixth_pow(
                            n_samp, stack[stack.size() - 1], 1.0, 0.0, stack[stack.size() - 1]);
                    },
                    1));
#else
                if ((op_terms.size() > 1) && (params.size() == 0))
                {
                    params.resize(op_terms.size() - 1);
                    std::transform(
                        op_terms.begin() + 1, op_terms.end(), params.begin(), [](std::string s) {
                            return std::stod(s);
                        });
                }
                if (params.size() > 0)
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::sixth_pow(n_samp,
                                                        stack[stack.size() - 1],
                                                        params[pp],
                                                        params[pp + 1],
                                                        stack[stack.size() - 1]);
                        },
                        1));
                    pp += 2;
                }
                else
                {
                    fxn_list_rev.push_back(std::make_pair(
                        [=](int n_samp, std::vector<double*>& stack) {
                            allowed_op_funcs::sixth_pow(
                                n_samp, stack[stack.size() - 1], 1.0, 0.0, stack[stack.size() - 1]);
                        },
                        1));
                }
#endif
            }
            else
            {
                throw std::logic_error("Term in postfix expression does not represent a node");
            }
        }
    }
    if (params.size() > 0)
    {
        throw std::logic_error("Not all of the parameters have been used.");
    }
    _fxn_list.resize(fxn_list_rev.size());
    std::copy_n(fxn_list_rev.rbegin(), fxn_list_rev.size(), _fxn_list.begin());
}
double ModelNode::eval(double* x_in)
{
    std::vector<double*> stack;
    std::vector<std::string> postfix_split = str_utils::split_string_trim(_postfix_expr, "|");

    int ff = 0;
    int xx = 0;
    for (auto& term : postfix_split)
    {
        size_t tt;
        for (tt = 0; tt < term.size(); ++tt)
        {
            if (!std::isdigit(term[tt]))
            {
                if (static_cast<int>(stack.size()) < _fxn_list[ff].second)
                {
                    throw std::logic_error("Wrong number of features stored in the stack.");
                }
                _fxn_list[ff].first(1, stack);
                ++ff;
                break;
            }
        }
        if (tt == term.size())
        {
            stack.push_back(x_in + xx);
            ++xx;
        }
    }
    if (stack.size() != 1)
    {
        throw std::logic_error(
            "The final stack size is not one, something wrong happened during the calculation.");
    }

    return *stack[0];
}

double ModelNode::eval(std::vector<double> x_in)
{
    std::vector<double> x_in_converted;
    std::vector<std::string> postfix_split = str_utils::split_string_trim(_postfix_expr, "|");
    for (auto& ps : postfix_split)
    {
        if (ps.find_first_not_of("0123456789") == std::string::npos)
        {
            if (std::stoi(ps) > static_cast<int>(x_in.size()))
            {
                throw std::logic_error("Incorrect number of variables passed to eval operator.");
            }
            x_in_converted.push_back(x_in[std::stoi(ps)]);
        }
    }
    return eval(x_in_converted.data());
}

double ModelNode::eval(std::map<std::string, double> x_in_dct)
{
    std::vector<double> x_in;
    for (auto& in_expr : x_in_expr_list())
    {
        if (x_in_dct.count(in_expr) == 0)
        {
            throw std::logic_error("The value of " + in_expr + " is not in x_in_dct.");
        }
        x_in.push_back(x_in_dct[in_expr]);
    }
    return eval(x_in.data());
}

std::vector<double> ModelNode::eval(std::vector<double>* x_in)
{
    std::vector<double*> stack;
    std::vector<std::string> postfix_split = str_utils::split_string_trim(_postfix_expr, "|");

    int ff = 0;
    int xx = 0;
    for (auto& term : postfix_split)
    {
        size_t tt;
        for (tt = 0; tt < term.size(); ++tt)
        {
            if (!std::isdigit(term[tt]))
            {
                if (static_cast<int>(stack.size()) < _fxn_list[ff].second)
                {
                    throw std::logic_error("Wrong number of features stored in the stack.");
                }
                _fxn_list[ff].first((x_in)->size(), stack);
                ++ff;
                break;
            }
        }
        if (tt == term.size())
        {
            stack.push_back((x_in + xx)->data());
            ++xx;
        }
    }
    if (stack.size() != 1)
    {
        throw std::logic_error(
            "The final stack size is not one, something wrong happened during the calculation.");
    }

    std::vector<double> to_ret(x_in->size());
    std::copy_n(stack[0], to_ret.size(), to_ret.data());
    return to_ret;
}

std::vector<double> ModelNode::eval(std::vector<std::vector<double>> x_in)
{
    int x_in_sz = x_in[0].size();
    bool same_szs = std::accumulate(
        x_in.begin(), x_in.end(), true, [&x_in_sz](bool cond, std::vector<double> x) {
            return cond && (static_cast<int>(x.size()) == x_in_sz);
        });
    if (!same_szs)
    {
        throw std::logic_error("Not all vectors in x_in are the same size.");
    }

    std::vector<std::vector<double>> x_in_converted;
    std::vector<std::string> postfix_split = str_utils::split_string_trim(_postfix_expr, "|");

    for (auto& ps : postfix_split)
    {
        if (ps.find_first_not_of("0123456789") == std::string::npos)
        {
            x_in_converted.push_back(x_in[std::stoi(ps)]);
        }
    }

    x_in_sz = x_in_converted[0].size();
    same_szs = std::accumulate(x_in_converted.begin(),
                               x_in_converted.end(),
                               true,
                               [&x_in_sz](bool cond, std::vector<double> x) {
                                   return cond && (static_cast<int>(x.size()) == x_in_sz);
                               });
    if (!same_szs)
    {
        throw std::logic_error("Not all vectors in x_in are the same size.");
    }

    return eval(x_in_converted.data());
}

std::vector<double> ModelNode::eval(std::map<std::string, std::vector<double>> x_in_dct)
{
    std::vector<std::vector<double>> x_in;
    for (auto& in_expr : x_in_expr_list())
    {
        if (x_in_dct.count(in_expr) == 0)
        {
            throw std::logic_error("The value of " + in_expr + " is not in x_in_dct.");
        }

        x_in.push_back(x_in_dct[in_expr]);
    }

    return eval(x_in.data());
}

ModelNode::~ModelNode() {}

void ModelNode::update_add_sub_leaves(std::map<std::string, int>& add_sub_leaves,
                                      const int pl_mn,
                                      int& expected_abs_tot) const
{
}

void ModelNode::update_div_mult_leaves(std::map<std::string, double>& div_mult_leaves,
                                       const double fact,
                                       double& expected_abs_tot) const
{
}

void ModelNode::update_primary_feature_decomp(std::map<std::string, int>& pf_decomp) const
{
    for (auto& leaf : _x_in_expr_list)
    {
        if (pf_decomp.count(leaf))
        {
            ++pf_decomp[leaf];
        }
        else
        {
            pf_decomp[leaf] = 1;
        }
    }
}
