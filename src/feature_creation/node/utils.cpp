// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/utils.cpp
 *  @brief Implements utility functions for handling Features (Nodes)
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "feature_creation/node/utils.hpp"

#ifdef PARAMETERIZE
void str2node::set_parameters(node_ptr feat, const std::vector<std::string> op_terms)
{
    std::vector<double> parameters(op_terms.size() - 1, 0.0);
    std::transform(op_terms.begin() + 1, op_terms.end(), parameters.begin(), [](std::string str) {
        return std::stod(str);
    });
    feat->set_parameters(parameters, false);
}
#endif

node_ptr str2node::postfix2node(const std::string postfix_expr,
                                const std::vector<node_ptr>& phi_0,
                                unsigned long int& feat_ind,
                                const std::vector<int>& excluded_inds)
{
    std::vector<node_ptr> stack;
    std::vector<std::string> postfix_split = str_utils::split_string_trim(postfix_expr, "|");

    if (postfix_split.size() == 1)
    {
        if (std::stoi(postfix_split[0]) >= static_cast<int>(phi_0.size()))
        {
            throw std::logic_error("Accessing feature outside of phi_0");
        }
        if (std::find(excluded_inds.begin(), excluded_inds.end(), std::stoi(postfix_split[0])) !=
            excluded_inds.end())
        {
            throw InvalidFeatureException();
            return nullptr;
        }
        return phi_0[std::stoi(postfix_split[0])];
    }

    for (size_t ff = 0; ff < postfix_split.size(); ++ff)
    {
        std::string term = postfix_split[ff];
        if (term.find_first_not_of("0123456789") == std::string::npos)
        {
            if (std::stoi(term) >= static_cast<int>(phi_0.size()))
            {
                throw std::logic_error("Accessing feature outside of phi_0");
            }

            if (std::find(excluded_inds.begin(),
                          excluded_inds.end(),
                          std::stoi(postfix_split[0])) != excluded_inds.end())
            {
                throw InvalidFeatureException();
                return nullptr;
            }

            stack.push_back(phi_0[std::stoi(term)]);
            ++feat_ind;
        }
        else
        {
            std::vector<std::string> op_terms = str_utils::split_string_trim(term, ":,");
            if (op_terms[0] == "add")
            {
#ifndef PARAMETERIZE
                stack[stack.size() - 2] = std::make_shared<AddNode>(
                    stack[stack.size() - 2], stack[stack.size() - 1], feat_ind);
#else
                if (op_terms.size() > 1)
                {
                    stack[stack.size() - 2] = std::make_shared<AddParamNode>(
                        stack[stack.size() - 2], stack[stack.size() - 1], feat_ind);
                    set_parameters(stack[stack.size() - 2], op_terms);
                }
                else
                {
                    stack[stack.size() - 2] = std::make_shared<AddNode>(
                        stack[stack.size() - 2], stack[stack.size() - 1], feat_ind);
                }
#endif
                stack.pop_back();
            }
            else if (op_terms[0] == "sub")
            {
#ifndef PARAMETERIZE
                stack[stack.size() - 2] = std::make_shared<SubNode>(
                    stack[stack.size() - 2], stack[stack.size() - 1], feat_ind);
#else
                if (op_terms.size() > 1)
                {
                    stack[stack.size() - 2] = std::make_shared<SubParamNode>(
                        stack[stack.size() - 2], stack[stack.size() - 1], feat_ind);
                    set_parameters(stack[stack.size() - 2], op_terms);
                }
                else
                {
                    stack[stack.size() - 2] = std::make_shared<SubNode>(
                        stack[stack.size() - 2], stack[stack.size() - 1], feat_ind);
                }
#endif
                stack.pop_back();
            }
            else if (op_terms[0] == "abd")
            {
#ifndef PARAMETERIZE
                stack[stack.size() - 2] = std::make_shared<AbsDiffNode>(
                    stack[stack.size() - 2], stack[stack.size() - 1], feat_ind);
#else
                if (op_terms.size() > 1)
                {
                    stack[stack.size() - 2] = std::make_shared<AbsDiffParamNode>(
                        stack[stack.size() - 2], stack[stack.size() - 1], feat_ind);
                    set_parameters(stack[stack.size() - 2], op_terms);
                }
                else
                {
                    stack[stack.size() - 2] = std::make_shared<AbsDiffNode>(
                        stack[stack.size() - 2], stack[stack.size() - 1], feat_ind);
                }
#endif
                stack.pop_back();
            }
            else if (op_terms[0] == "mult")
            {
#ifndef PARAMETERIZE
                stack[stack.size() - 2] = std::make_shared<MultNode>(
                    stack[stack.size() - 2], stack[stack.size() - 1], feat_ind);
#else
                if (op_terms.size() > 1)
                {
                    stack[stack.size() - 2] = std::make_shared<MultParamNode>(
                        stack[stack.size() - 2], stack[stack.size() - 1], feat_ind);
                    set_parameters(stack[stack.size() - 2], op_terms);
                }
                else
                {
                    stack[stack.size() - 2] = std::make_shared<MultNode>(
                        stack[stack.size() - 2], stack[stack.size() - 1], feat_ind);
                }
#endif
                stack.pop_back();
            }
            else if (op_terms[0] == "div")
            {
#ifndef PARAMETERIZE
                stack[stack.size() - 2] = std::make_shared<DivNode>(
                    stack[stack.size() - 2], stack[stack.size() - 1], feat_ind);
#else
                if (op_terms.size() > 1)
                {
                    stack[stack.size() - 2] = std::make_shared<DivParamNode>(
                        stack[stack.size() - 2], stack[stack.size() - 1], feat_ind);
                    set_parameters(stack[stack.size() - 2], op_terms);
                }
                else
                {
                    stack[stack.size() - 2] = std::make_shared<DivNode>(
                        stack[stack.size() - 2], stack[stack.size() - 1], feat_ind);
                }
#endif
                stack.pop_back();
            }
            else if (op_terms[0] == "abs")
            {
#ifndef PARAMETERIZE
                stack[stack.size() - 1] = std::make_shared<AbsNode>(stack[stack.size() - 1],
                                                                    feat_ind);
#else
                if (op_terms.size() > 1)
                {
                    stack[stack.size() - 1] = std::make_shared<AbsParamNode>(
                        stack[stack.size() - 1], feat_ind);
                    set_parameters(stack[stack.size() - 1], op_terms);
                    set_parameters(stack[stack.size() - 1], op_terms);
                }
                else
                {
                    stack[stack.size() - 1] = std::make_shared<AbsNode>(stack[stack.size() - 1],
                                                                        feat_ind);
                }
#endif
            }
            else if (op_terms[0] == "inv")
            {
#ifndef PARAMETERIZE
                stack[stack.size() - 1] = std::make_shared<InvNode>(stack[stack.size() - 1],
                                                                    feat_ind);
#else
                if (op_terms.size() > 1)
                {
                    stack[stack.size() - 1] = std::make_shared<InvParamNode>(
                        stack[stack.size() - 1], feat_ind);
                    set_parameters(stack[stack.size() - 1], op_terms);
                }
                else
                {
                    stack[stack.size() - 1] = std::make_shared<InvNode>(stack[stack.size() - 1],
                                                                        feat_ind);
                }
#endif
            }
            else if (op_terms[0] == "exp")
            {
#ifndef PARAMETERIZE
                stack[stack.size() - 1] = std::make_shared<ExpNode>(stack[stack.size() - 1],
                                                                    feat_ind);
#else
                if (op_terms.size() > 1)
                {
                    stack[stack.size() - 1] = std::make_shared<ExpParamNode>(
                        stack[stack.size() - 1], feat_ind);
                    set_parameters(stack[stack.size() - 1], op_terms);
                }
                else
                {
                    stack[stack.size() - 1] = std::make_shared<ExpNode>(stack[stack.size() - 1],
                                                                        feat_ind);
                }
#endif
            }
            else if (op_terms[0] == "nexp")
            {
#ifndef PARAMETERIZE
                stack[stack.size() - 1] = std::make_shared<NegExpNode>(stack[stack.size() - 1],
                                                                       feat_ind);
#else
                if (op_terms.size() > 1)
                {
                    stack[stack.size() - 1] = std::make_shared<NegExpParamNode>(
                        stack[stack.size() - 1], feat_ind);
                    set_parameters(stack[stack.size() - 1], op_terms);
                }
                else
                {
                    stack[stack.size() - 1] = std::make_shared<NegExpNode>(stack[stack.size() - 1],
                                                                           feat_ind);
                }
#endif
            }
            else if (op_terms[0] == "log")
            {
#ifndef PARAMETERIZE
                stack[stack.size() - 1] = std::make_shared<LogNode>(stack[stack.size() - 1],
                                                                    feat_ind);
#else
                if (op_terms.size() > 1)
                {
                    stack[stack.size() - 1] = std::make_shared<LogParamNode>(
                        stack[stack.size() - 1], feat_ind);
                    set_parameters(stack[stack.size() - 1], op_terms);
                }
                else
                {
                    stack[stack.size() - 1] = std::make_shared<LogNode>(stack[stack.size() - 1],
                                                                        feat_ind);
                }
#endif
            }
            else if (op_terms[0] == "sin")
            {
#ifndef PARAMETERIZE
                stack[stack.size() - 1] = std::make_shared<SinNode>(stack[stack.size() - 1],
                                                                    feat_ind);
#else
                if (op_terms.size() > 1)
                {
                    stack[stack.size() - 1] = std::make_shared<SinParamNode>(
                        stack[stack.size() - 1], feat_ind);
                    set_parameters(stack[stack.size() - 1], op_terms);
                }
                else
                {
                    stack[stack.size() - 1] = std::make_shared<SinNode>(stack[stack.size() - 1],
                                                                        feat_ind);
                }
#endif
            }
            else if (op_terms[0] == "cos")
            {
#ifndef PARAMETERIZE
                stack[stack.size() - 1] = std::make_shared<CosNode>(stack[stack.size() - 1],
                                                                    feat_ind);
#else
                if (op_terms.size() > 1)
                {
                    stack[stack.size() - 1] = std::make_shared<CosParamNode>(
                        stack[stack.size() - 1], feat_ind);
                    set_parameters(stack[stack.size() - 1], op_terms);
                }
                else
                {
                    stack[stack.size() - 1] = std::make_shared<CosNode>(stack[stack.size() - 1],
                                                                        feat_ind);
                }
#endif
            }
            else if (op_terms[0] == "sq")
            {
#ifndef PARAMETERIZE
                stack[stack.size() - 1] = std::make_shared<SqNode>(stack[stack.size() - 1],
                                                                   feat_ind);
#else
                if (op_terms.size() > 1)
                {
                    stack[stack.size() - 1] = std::make_shared<SqParamNode>(stack[stack.size() - 1],
                                                                            feat_ind);
                    set_parameters(stack[stack.size() - 1], op_terms);
                }
                else
                {
                    stack[stack.size() - 1] = std::make_shared<SqNode>(stack[stack.size() - 1],
                                                                       feat_ind);
                }
#endif
            }
            else if (op_terms[0] == "sqrt")
            {
#ifndef PARAMETERIZE
                stack[stack.size() - 1] = std::make_shared<SqrtNode>(stack[stack.size() - 1],
                                                                     feat_ind);
#else
                if (op_terms.size() > 1)
                {
                    stack[stack.size() - 1] = std::make_shared<SqrtParamNode>(
                        stack[stack.size() - 1], feat_ind);
                    set_parameters(stack[stack.size() - 1], op_terms);
                }
                else
                {
                    stack[stack.size() - 1] = std::make_shared<SqrtNode>(stack[stack.size() - 1],
                                                                         feat_ind);
                }
#endif
            }
            else if (op_terms[0] == "cb")
            {
#ifndef PARAMETERIZE
                stack[stack.size() - 1] = std::make_shared<CbNode>(stack[stack.size() - 1],
                                                                   feat_ind);
#else
                if (op_terms.size() > 1)
                {
                    stack[stack.size() - 1] = std::make_shared<CbParamNode>(stack[stack.size() - 1],
                                                                            feat_ind);
                    set_parameters(stack[stack.size() - 1], op_terms);
                }
                else
                {
                    stack[stack.size() - 1] = std::make_shared<CbNode>(stack[stack.size() - 1],
                                                                       feat_ind);
                }
#endif
            }
            else if (op_terms[0] == "cbrt")
            {
#ifndef PARAMETERIZE
                stack[stack.size() - 1] = std::make_shared<CbrtNode>(stack[stack.size() - 1],
                                                                     feat_ind);
#else
                if (op_terms.size() > 1)
                {
                    stack[stack.size() - 1] = std::make_shared<CbrtParamNode>(
                        stack[stack.size() - 1], feat_ind);
                    set_parameters(stack[stack.size() - 1], op_terms);
                }
                else
                {
                    stack[stack.size() - 1] = std::make_shared<CbrtNode>(stack[stack.size() - 1],
                                                                         feat_ind);
                }
#endif
            }
            else if (op_terms[0] == "sp")
            {
#ifndef PARAMETERIZE
                stack[stack.size() - 1] = std::make_shared<SixPowNode>(stack[stack.size() - 1],
                                                                       feat_ind);
#else
                if (op_terms.size() > 1)
                {
                    stack[stack.size() - 1] = std::make_shared<SixPowParamNode>(
                        stack[stack.size() - 1], feat_ind);
                    set_parameters(stack[stack.size() - 1], op_terms);
                }
                else
                {
                    stack[stack.size() - 1] = std::make_shared<SixPowNode>(stack[stack.size() - 1],
                                                                           feat_ind);
                }
#endif
            }
            else
            {
                throw std::logic_error("Term in postfix expression does not represent a node");
            }
            ++feat_ind;
        }
    }
    if (stack.size() != 1)
    {
        throw std::logic_error(
            "Went through postfix expression and still more than one node in the list. This must "
            "be an invalid expression: " +
            postfix_expr + ".");
    }
    return stack[0];
}

std::vector<node_ptr> str2node::phi_selected_from_file(const std::string filename,
                                                       const std::vector<node_ptr> phi_0,
                                                       const std::vector<int>& excluded_inds)
{
    int max_rung = node_value_arrs::MAX_RUNG;
    node_value_arrs::set_max_rung(10, node_value_arrs::PARAM_STORAGE_ARR.size() > 0);

    node_value_arrs::initialize_d_matrix_arr();

    std::ifstream file_stream;
    file_stream.open(filename, std::ios::in);

    std::string line;
    std::vector<std::string> split_line;

    std::vector<node_ptr> phi_selected;
    unsigned long int feat_ind = phi_0.size();
    int feat_sel = 0;

    while (std::getline(file_stream, line))
    {
        if (line[0] == '#')
        {
            continue;
        }
        node_value_arrs::resize_d_matrix_arr(1);
        boost::algorithm::split(
            split_line, line, boost::algorithm::is_any_of("\t "), boost::token_compress_on);

        node_ptr new_feat = postfix2node(split_line[1], phi_0, feat_ind, excluded_inds);
        if (new_feat->rung() > node_value_arrs::MAX_RUNG)
        {
            node_value_arrs::set_max_rung(new_feat->rung(), true);
        }

        new_feat->set_selected(true);
        new_feat->set_d_mat_ind(feat_sel);
        new_feat->set_value();

        phi_selected.push_back(new_feat);

        if (new_feat->rung() > max_rung)
        {
            max_rung = new_feat->rung();
        }

        ++feat_ind;
        ++feat_sel;
    }

    file_stream.close();
    node_value_arrs::set_max_rung(max_rung, node_value_arrs::PARAM_STORAGE_ARR.size() > 0);

    return phi_selected;
}

std::vector<node_ptr> str2node::phi_from_file(const std::string filename,
                                              const std::vector<node_ptr> phi_0,
                                              const std::vector<int>& excluded_inds)
{
    node_value_arrs::resize_values_arr(0, phi_0.size());

    std::ifstream file_stream;
    file_stream.open(filename, std::ios::in);

    int num_lines = 0;
    int n_invalid_feat = 0;
    std::string line;

    std::vector<node_ptr> phi;
    unsigned long int feat_ind = phi_0.size();

    while (std::getline(file_stream, line))
    {
        ++num_lines;
        if (line[0] == '#')
        {
            continue;
        }
        try
        {
            node_ptr feat = postfix2node(line, phi_0, feat_ind, excluded_inds);
            if (feat->type() == NODE_TYPE::FEAT)
            {
                continue;
            }
            phi.push_back(feat);
            phi.back()->set_selected(false);
            ++feat_ind;
        }
        catch (const InvalidFeatureException& e)
        {
            ++n_invalid_feat;
            // Do Nothing
        }
    }
    file_stream.close();
    if (num_lines < 1)
    {
        throw std::logic_error("File, " + filename + " not present");
    }
    return phi;
}

std::string node_identifier::feature_type_to_string(const NODE_TYPE nt)
{
    if (nt == NODE_TYPE::FEAT)
    {
        return "feature";
    }
    if (nt == NODE_TYPE::MODEL_FEATURE)
    {
        return "model";
    }
    if (nt == NODE_TYPE::ADD)
    {
        return "add";
    }
    if (nt == NODE_TYPE::SUB)
    {
        return "sub";
    }
    if (nt == NODE_TYPE::ABS_DIFF)
    {
        return "abs_diff";
    }
    if (nt == NODE_TYPE::MULT)
    {
        return "mult";
    }
    if (nt == NODE_TYPE::DIV)
    {
        return "div";
    }
    if (nt == NODE_TYPE::EXP)
    {
        return "exp";
    }
    if (nt == NODE_TYPE::NEG_EXP)
    {
        return "neg_exp";
    }
    if (nt == NODE_TYPE::INV)
    {
        return "inv";
    }
    if (nt == NODE_TYPE::SQ)
    {
        return "sq";
    }
    if (nt == NODE_TYPE::CB)
    {
        return "cb";
    }
    if (nt == NODE_TYPE::SIX_POW)
    {
        return "six_pow";
    }
    if (nt == NODE_TYPE::SQRT)
    {
        return "sqrt";
    }
    if (nt == NODE_TYPE::CBRT)
    {
        return "cbrt";
    }
    if (nt == NODE_TYPE::LOG)
    {
        return "log";
    }
    if (nt == NODE_TYPE::ABS)
    {
        return "abs";
    }
    if (nt == NODE_TYPE::SIN)
    {
        return "sin";
    }
    if (nt == NODE_TYPE::COS)
    {
        return "cos";
    }
    if (nt == NODE_TYPE::PARAM_ADD)
    {
        return "p:add";
    }
    if (nt == NODE_TYPE::PARAM_SUB)
    {
        return "p:sub";
    }
    if (nt == NODE_TYPE::PARAM_ABS_DIFF)
    {
        return "p:abs_diff";
    }
    if (nt == NODE_TYPE::PARAM_MULT)
    {
        return "p:mult";
    }
    if (nt == NODE_TYPE::PARAM_DIV)
    {
        return "p:div";
    }
    if (nt == NODE_TYPE::PARAM_EXP)
    {
        return "p:exp";
    }
    if (nt == NODE_TYPE::PARAM_NEG_EXP)
    {
        return "p:neg_exp";
    }
    if (nt == NODE_TYPE::PARAM_INV)
    {
        return "p:inv";
    }
    if (nt == NODE_TYPE::PARAM_SQ)
    {
        return "p:sq";
    }
    if (nt == NODE_TYPE::PARAM_CB)
    {
        return "p:cb";
    }
    if (nt == NODE_TYPE::PARAM_SIX_POW)
    {
        return "p:six_pow";
    }
    if (nt == NODE_TYPE::PARAM_SQRT)
    {
        return "p:sqrt";
    }
    if (nt == NODE_TYPE::PARAM_CBRT)
    {
        return "p:cbrt";
    }
    if (nt == NODE_TYPE::PARAM_LOG)
    {
        return "p:log";
    }
    if (nt == NODE_TYPE::PARAM_ABS)
    {
        return "p:abs";
    }
    if (nt == NODE_TYPE::PARAM_SIN)
    {
        return "p:sin";
    }
    if (nt == NODE_TYPE::PARAM_COS)
    {
        return "p:cos";
    }
    throw std::logic_error("Invalid feature type");
}
