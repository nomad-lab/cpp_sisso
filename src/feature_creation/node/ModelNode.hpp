// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/ModelNode.hpp
 *  @brief Defines the class used to transfer features (Nodes) to the Model objects
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 *
 *  This is the only Node object that does not use the central storage area defined in value_storage/node_value_containers
 *  All information that is normally calculated recursively is stored here directly.
 */

#ifndef MODEL_NODE
#define MODEL_NODE

#include "feature_creation/node/FeatureNode.hpp"
#include "feature_creation/node/operator_nodes/functions.hpp"

typedef std::pair<std::function<void(int, std::vector<double*>&)>, int> fxn_nterms_pair;

// DocString: cls_model_node
/**
 * @brief Nodes that are used to describe the features passed to a Model
 *
 * @details This Node allows for the reconstruction of features from an output model. (inherits from FeatureNode)
 */
class ModelNode : public FeatureNode
{
    friend class boost::serialization::access;

    /**
     * @brief Serialization function to send over MPI
     *
     * @param ar Archive representation of node
     */
    template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ar& boost::serialization::base_object<FeatureNode>(*this);
        ar& _value_svm;
        ar& _test_value_svm;
        ar& _fxn_list;
        ar& _x_in_expr_list;
        ar& _postfix_expr;
        ar& _latex_expr;
        ar& _matlab_fxn_expr;
        ar& _b_remap_svm;
        ar& _w_remap_svm;
        ar& _rung;
        ar& _n_leaves;
    }

    using FeatureNode::get_latex_expr;
    using FeatureNode::set_test_value;
    using FeatureNode::set_value;
    using FeatureNode::test_value_ptr;
    using FeatureNode::value_ptr;

protected:
    // clang-format off
    std::vector<double> _value_svm; //!< Value of the feature converted to a range of -1.0 to 1.0
    std::vector<double> _test_value_svm; //!< Value of the feature converted to a range of -1.0 to 1.0

    std::vector<fxn_nterms_pair> _fxn_list; //!< list of functions used for evaluating feature values

    std::vector<std::string> _x_in_expr_list; //!< A vector storing the expressions for all primary features in the order they appear in the postfix expression
    std::string _postfix_expr; //!< A computer readable representation of the feature. Primary features represented by their index in phi_0, node types are represented by abbreviations, and the order is the same as the postfix representation of the expression
    std::string _latex_expr; //!< The valid LaTeX expression that represents the feature
    std::string _matlab_fxn_expr; //!< The string that corresponds to the code needed to evaluate the node in matlab

    double _b_remap_svm; //!< value to remap the b from SVM to the real value
    double _w_remap_svm; //!< value to remap the w from SVM to the real value

    int _rung; //!< The rung of the feature (Height of the binary expression tree - 1)
    int _n_leaves; //!< The number of primary features (non-unique) this feature contains (The number of leaves of the Binary Expression Tree)
    // clang-format on
public:
    using Node::n_leaves;

    /**
     * @brief Base Constructor
     * @details This is only used for serialization
     */
    ModelNode();

    // DocString: model_node_init_domain
    /**
     * @brief Constructs a feature node
     *
     * @param feat_ind (int) The index of the feature
     * @param rung (int) the rung of the feature
     * @param expr (str) Expression for the feature
     * @param latex_expr (str) Get the valid LaTeX expression that represents the feature for the feature
     * @param matlab_fxn_expr (str) The code to evaluate the feature in matlab
     * @param value (listf floats)  The value for this feature's training data
     * @param test_value (list of floats)  The value for this feature's test data
     * @param x_in_expr_list (list of str)  vector storing the expressions for all primary features that show up in feature in the order they appear in the postfix notation
     * @param unit (Unit) Unit of the feature
     * @param domain (Domain) The domain of the Feature
     */
    ModelNode(const unsigned long int feat_ind,
              const int rung,
              const std::string expr,
              const std::string latex_expr,
              const std::string postfix_expr,
              const std::string matlab_fxn_expr,
              const std::vector<double> value,
              const std::vector<double> test_value,
              const std::vector<std::string> x_in_expr_list,
              const Unit unit,
              const Domain domain);

    // DocString: model_node_init
    /**
     * @brief Constructs a feature node
     *
     * @param feat_ind (int) The index of the feature
     * @param rung (int) the rung of the feature
     * @param expr (str) Expression for the feature
     * @param latex_expr (str) Get the valid LaTeX expression that represents the feature for the feature
     * @param matlab_fxn_expr (str) The code to evaluate the feature in matlab
     * @param value (list of floats) The value for this feature's training data
     * @param test_value (list of floats) The value for this feature's test data
     * @param x_in_expr_list (list of str) vector storing the expressions for all primary features that show up in feature in the order they appear in the postfix notation
     * @param unit (Unit) Unit of the feature
     */
    ModelNode(const unsigned long int feat_ind,
              const int rung,
              const std::string expr,
              const std::string latex_expr,
              const std::string postfix_expr,
              const std::string matlab_fxn_expr,
              const std::vector<double> value,
              const std::vector<double> test_value,
              const std::vector<std::string> x_in_expr_list,
              const Unit unit);

    // DocString: model_node_init_node_ptr
    /**
     * @brief Copy constructor from general node_ptr
     *
     * @param in_node (Node) Node to be copied
     */
    ModelNode(node_ptr in_node);

    /**
     * @brief Copy Constructor
     *
     * @param o ModelNode to be copied
     */
    ModelNode(const ModelNode&) = default;

    /**
     * @brief Move Constructor
     *
     * @param o ModelNode to be moved
     */
    ModelNode(ModelNode&&) = default;

    /**
     * @brief Copy Assignment operator
     *
     * @param o ModelNode to be copied
     */
    ModelNode& operator=(const ModelNode&) = default;
    /**
     * @brief Move Assignment operator
     *
     * @param o ModelNode to be moved
     */
    ModelNode& operator=(ModelNode&&) = default;

    /**
     * @brief Destructor
     */
    ~ModelNode();

    /**
     * @brief Makes a hard copy node (All members of the Node are independent of the original one)
     * @return A shared_ptr to the copied node
     */
    inline node_ptr hard_copy() const override { return std::make_shared<ModelNode>(*this); }

    /**
     * @brief Generate the list of functions used to evaluate the value of this feature for a new data point from the postfix expression
     */
    void generate_fxn_list();

    /**
     * @brief Evaluate the model for a new point
     *
     * @param x_in pointer to the new data point (order the same as appending the results of _feats[nn]->x_in_expr_list() for all feature)
     * @return The prediction of the model for a given data point
     */
    double eval(double* x_in);

    // DocString: model_node_eval_list
    /**
     * @brief Evaluate the model for a new point
     *
     * @param x_in (list of float) The data point to evaluate the model (order the same as appending the results of _feats[nn]->x_in_expr_list() for all feature)
     * @return The prediction of the model for a given data point
     */
    double eval(std::vector<double> x_in);

    // DocString: model_node_eval_dict
    /**
     * @brief Evaluate the model for a new point
     *
     * @param x_in_dct (dict, key: str, val: float) Dictionary describing the new point (\"feature expr\": value)
     * @return The prediction of the model for a given data point
     */
    double eval(std::map<std::string, double> x_in_dct);

    /**
     * @brief Evaluate the model for a new set of new points
     *
     * @param x_in a vector of pointers to the set of values for new data points (one pointer per each feature and order the same as appending the results of _feats[nn]->x_in_expr_list() for all feature)
     * @return The prediction of the model for a given set of data points
     */
    std::vector<double> eval(std::vector<double>* x_in);

    /**
     * @brief Evaluate the model for a set of new points
     *
     * @param x_in The set data for a set of new data points (size of n_feature x n_points, and order the same as appending the results of _feats[nn]->x_in_expr_list() for all feature)
     * @return The prediction of the model for a given data point
     */
    std::vector<double> eval(std::vector<std::vector<double>> x_in);

    /**
     * @brief Evaluate the model for a set of new points
     *
     * @param x_in_dct The set of data points to evaluate the model. Keys must be strings representing feature expressions and vectors must be the same length
     * @return The prediction of the model for a given data point
     */
    std::vector<double> eval(std::map<std::string, std::vector<double>> x_in_dct);

    // DocString: model_node_x_in_expr
    /**
     * @brief A vector storing the expressions for all primary features in the order they appear in the postfix expression
     */
    inline std::vector<std::string> x_in_expr_list() const override { return _x_in_expr_list; }

    // DocString: model_node_n_leaves
    /**
     * @brief The number of primary features (non-unique) this feature contains (The number of leaves of the Binary Expression Tree)
     *
     */
    inline void update_n_leaves(int& cur_n_leaves) const override { cur_n_leaves += _n_leaves; }

    /**
     * @return Value of the feature converted to a range of -1.0 to 1.0
     */
    inline std::vector<double> svm_value() const { return _value_svm; }

    /**
     * @return pointer to the start of the vector storing the value of the feature transformed into a range of -1.0 to 1.0
     */
    inline double* svm_value_ptr() { return _value_svm.data(); }

    /**
     * @return Value of the test value fo the feature converted to a range of -1.0 to 1.0
     */
    inline std::vector<double> svm_test_value() const { return _test_value_svm; }

    /**
     * @return Pointer to the start of the vector storing the test value of the feature converted to a range of -1.0 to 1.0
     */
    inline double* svm_test_value_ptr() { return _test_value_svm.data(); }

    /**
     * @return value used to map the b value from SVM from -1.0 to 1.0 range to the real one
     */
    inline double remap_b_svm(double w) const { return w * _b_remap_svm; }

    /**
     * @return value used to map the w value from SVM from -1.0 to 1.0 range to the real one
     */
    inline double remap_w_svm(double w) const { return w * _w_remap_svm; }

    // DocString: model_node_set_value
    /**
     * @brief Set the value of all training samples for the feature inside the central data storage arrays
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     */
    inline void set_value(int offset = -1, const bool for_comp = false) const override { return; }

    // DocString: model_node_set_test_value
    /**
     * @brief Set the value of all test samples for the feature inside the central data storage array
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     */
    inline void set_test_value(int offset = -1, const bool for_comp = false) const override { return; }

    // DocString: model_node_is_nan
    /**
     * @brief Check if the feature has a NaN value in it
     *
     * @return True if one of the values of the feature is a NaN
     */
    inline bool is_nan() const override { return false; }

    // DocString: model_node_is_const
    /**
     * @brief Check if feature is constant for one of the tasks
     *
     * @return True if the feature is constant for all samples in any task
     */
    inline bool is_const() const override { return false; }

    /**
     * @brief Returns the type of node this is
     */
    inline NODE_TYPE type() const override { return NODE_TYPE::MODEL_FEATURE; }

    /**
     * @brief The pointer to where the feature's training data is stored
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     */
    inline double* value_ptr(int offset = -1, const bool for_comp = false) { return _value.data(); }

    /**
     * @brief The pointer to where the feature's test data is stored
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     */
    inline double* test_value_ptr(int offset = -1, const bool for_comp = false)
    {
        return _test_value.data();
    }

    /**
     * @brief The pointer to where the feature's training data is stored
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     */
    inline double* value_ptr(int offset = -1, const bool for_comp = false) const override
    {
        throw std::logic_error("const version of value_ptr for ModelNode is impossible.");
    }

    /**
     * @brief The pointer to where the feature's test data is stored
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     */
    inline double* test_value_ptr(int offset = -1, const bool for_comp = false) const override
    {
        throw std::logic_error("const version of test_value_ptr for ModelNode is impossible.");
    }

    /**
     * @brief Update the rung of the feature (does nothing as the feature nodes are rung 0)
     *
     * @param cur_rung The current rung of the feature
     */
    inline virtual void rung_update(int& cur_rung) const override { cur_rung += _rung; };

    /**
     * @brief Update the primary feature decomposition of a feature
     *
     * @param pf_decomp The primary feature decomposition of the feature calling this function.
     */
    void update_primary_feature_decomp(std::map<std::string, int>& pf_decomp) const override;

    /**
     * @brief Get the term used in the postfix expression for this Node
     */
    inline std::string get_postfix_term() const override { return _postfix_expr; }

    // DocString: model_node_latex_expr
    /**
     * @brief Get the valid LaTeX expression that represents the feature
     */
    inline std::string get_latex_expr() const override
    {
        return _latex_expr.substr(1, _latex_expr.size() - 2);
    }

    // DocString: model_node_matlab_expr
    /**
     * @brief Get the string that corresponds to the code needed to evaluate the node in matlab
     *
     * @return The matlab code for the feature
     */
    inline std::string matlab_fxn_expr() const override { return _matlab_fxn_expr; }

    /**
     * @brief update the dictionary used to check if an Add/Sub/AbsDiff node is valid
     *
     * @param add_sub_leaves the dictionary used to check if an Add/Sub node is valid
     * @param pl_mn 1 for addition and -1 for subtraction
     * @param expected_abs_tot The expected absolute sum of all values in add_sub_leaves
     */
    void update_add_sub_leaves(std::map<std::string, int>& add_sub_leaves,
                               const int pl_mn,
                               int& expected_abs_tot) const override;

    /**
     * @brief update the dictionary used to check if an Mult/Div node is valid
     *
     * @param div_mult_leaves the dictionary used to check if an Mult/Div node is valid
     * @param fact amount to increment the element (a primary features) of the dictionary by
     * @param expected_abs_tot The expected absolute sum of all values in div_mult_leaves
     */
    void update_div_mult_leaves(std::map<std::string, double>& div_mult_leaves,
                                const double fact,
                                double& expected_abs_tot) const override;

#ifdef PY_BINDINGS

    // DocString: model_node_eval_many_arr
    /**
     * @brief Evaluate the model for a set of new points
     *
     * @param x_in (np.ndarray of float) The set data for a set of new data points (size of n_feature x n_points, and order the same as appending the results of _feats[nn]->x_in_expr_list() for all feature)
     * @return The prediction of the model for a given data point
     */
    py::array_t<double> eval_many_py(py::array_t<double> x_in);

    // DocString: model_node_eval_many_dict
    /**
     * @brief Evaluate the model for a set of new points
     *
     * @param x_in (dict, key: str, val: np.ndarray) The set of data points to evaluate the model. Keys must be strings representing feature expressions and vectors must be the same length
     * @return The prediction of the model for a given data point
     */
    py::array_t<double> eval_many_py(std::map<std::string, py::array_t<double>> x_in);
#endif

#ifdef PARAMETERIZE
    // DocString: model_node_matlab_expr_param
    /**
     * @brief Get the string that corresponds to the code needed to evaluate the node in matlab
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param depth How far down a given Node is from the root OperatorNode
     * @return The matlab code for the feature
     */
    inline std::string matlab_fxn_expr(const double* params, const int depth = 1) const override
    {
        std::cerr
            << "WARNING: ModelNode can't accept new parameters for getting Matlab code expressions"
            << std::endl;
        return _matlab_fxn_expr;
    }
#endif
};

typedef std::shared_ptr<ModelNode> model_node_ptr;

#endif
