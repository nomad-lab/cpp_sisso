// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/FeatureNode.hpp
 *  @brief Defines the class that represent the primary features in SISSO
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 *
 *  This class represents the leaves of all of the binary expression trees.
 */

#ifndef FEATURE_NODE
#define FEATURE_NODE

#include "feature_creation/node/pyNode.hpp"

#ifdef PY_BINDINGS
namespace py = pybind11;
#endif

// DocString: cls_feat_node
/**
 * @brief Node that describe the leaves of the operator graph (Primary features in Phi_0) (inherits from Node)
 */
class FeatureNode : public Node
{
    friend class boost::serialization::access;

    /**
     * @brief Serialization function to send over MPI
     *
     * @param ar Archive representation of node
     */
    template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ar& boost::serialization::base_object<Node>(*this);
        ar& _expr;
        ar& _domain;
        ar& _unit;
        ar& _value;
        ar& _test_value;
    }

    using Node::get_latex_expr;
    using Node::set_test_value;
    using Node::set_value;
    using Node::test_value_ptr;
    using Node::value_ptr;

protected:
    // clang-format off
    std::vector<double> _value; //!< The value for this feature's training data
    std::vector<double> _test_value; //!< The value for this feature's test data
    Domain _domain; //!< The domain of the feature
    Unit _unit; //!< Unit for the feature
    std::string _expr; //!< Expression of the feature
    // clang-format on

public:
    /**
     * @brief Base Constructor
     * @details This is only used for serialization
     */
    FeatureNode();

    // DocString: feat_node_init_list_no_domain
    /**
     * @brief Constructs a feature node
     *
     * @param feat_ind (int) The index of the feature
     * @param expr (str) Expression for the feature
     * @param value (np.ndarray) The value for this feature's training data
     * @param test_value (np.ndarray) The value for this feature's test data
     * @param unit (Unit) Unit of the feature
     * @param domain (Domain) The domain of the feature
     * @param set_val (bool) If true set the value inside the VALUES_ARR object (always true in python)
     */
    FeatureNode(const unsigned long int feat_ind,
                const std::string expr,
                const std::vector<double> value,
                const std::vector<double> test_value,
                const Unit unit,
                const Domain domain,
                const bool set_val = true);

    // DocString: feat_node_init_list
    /**
     * @brief Constructs a feature node
     *
     * @param feat_ind (int) The index of the feature
     * @param expr (str) Expression for the feature
     * @param value (np. The value for this feature's training data
     * @param test_value (np. The value for this feature's test data
     * @param unit (Unit) Unit of the feature
     * @param set_val (bool) If true set the value inside the VALUES_ARR object
     */
    FeatureNode(const unsigned long int feat_ind,
                const std::string expr,
                const std::vector<double> value,
                const std::vector<double> test_value,
                const Unit unit,
                const bool set_val = true);

    /**
     * @brief Copy Constructor
     *
     * @param o FeatureNode to be copied
     */
    FeatureNode(const FeatureNode&) = default;

    /**
     * @brief Move Constructor
     *
     * @param o FeatureNode to be moved
     */
    FeatureNode(FeatureNode&&) = default;

    /**
     * @brief Copy Assignment operator
     *
     * @param o FeatureNode to be copied
     */
    FeatureNode& operator=(const FeatureNode&) = default;
    /**
     * @brief Move Assignment operator
     *
     * @param o FeatureNode to be moved
     */
    FeatureNode& operator=(FeatureNode&&) = default;

    /**
     * @brief Destructor
     */
    virtual ~FeatureNode();

    /**
     * @brief Makes a hard copy node (All members of the Node are independent of the original one)
     * @return A shared_ptr to the copied node
     */
    virtual inline node_ptr hard_copy() const override { return std::make_shared<FeatureNode>(*this); }

    /**
     * @brief Relinks all features of an OperatorNodes's _feat member to Nodes inside of phi
     *
     * @param phi A vector containing all Nodes that this Node can relink to
     */
    void reset_feats(std::vector<node_ptr>& phi) override {};

    // DocString: feat_node_x_in_expr
    /**
     * @brief A vector storing the expressions for all primary features in the order they appear in the postfix expression
     */
    virtual inline std::vector<std::string> x_in_expr_list() const override
    {
        return std::vector<std::string>(1, _expr);
    }

    // DocString: feat_node_n_leaves
    /**
     * @brief Get the number of primary features (non-unique) this feature contains (The number of leaves of the Binary Expression Tree)
     */
    virtual inline void update_n_leaves(int& cur_n_leaves) const override { cur_n_leaves += 1; }

    /**
     * @brief Get the score used to sort the features in the feature space (Based on the type of node and _feat_ind)
     *
     * @param max_ind The maximum index of the input nodes
     * @return The score used to sort the feature space
     */
    inline unsigned long long sort_score(unsigned int max_ind) const override { return _feat_ind; }

    // DocString: feat_node_expr_const
    /**
     * @brief A human readable equation representing the feature
     */
    inline std::string expr() const override { return _expr; }

    // DocString: feat_node_latex_expr
    /**
     * @brief Get the valid LaTeX expression that represents the feature
     */
    inline std::string get_latex_expr() const override { return str_utils::latexify(_expr); }

    // DocString: feat_node_unit
    /**
     * @brief The unit of the primary feature
     */
    inline Unit unit() const override { return _unit; }

    /**
     * @brief A vector containing the values of the training set samples for the feature
     */
    inline std::vector<double> value() const override { return _value; }

    /**
     * @brief A vector containing the values of the test set samples for the feature
     */
    inline std::vector<double> test_value() const override { return _test_value; }

    // DocString: feat_node_set_value
    /**
     * @brief Set the value of all training samples for the feature inside the central data storage array
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp(bool) If true then the evaluation is used for comparing features
     */
    inline void set_value(int offset = -1, const bool for_comp = false) const override
    {
        std::copy_n(_value.data(), _n_samp, value_ptr());
    }

    // DocString: feat_node_set_test_value
    /**
     * @brief Set the value of all test samples for the feature inside the central data storage array
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp(bool) If true then the evaluation is used for comparing features
     */
    inline void set_test_value(int offset = -1, const bool for_comp = false) const override
    {
        if (!_selected)
        {
            std::copy_n(_test_value.data(), _n_samp_test, test_value_ptr());
        }
    }

    // DocString: feat_node_is_nan
    /**
     * @brief Check if the feature has a NaN value in it
     *
     * @return True if one of the values of the feature is a NaN
     */
    inline bool is_nan() const override
    {
        return std::any_of(
            value_ptr(), value_ptr() + _n_samp, [](double d) { return !std::isfinite(d); });
    }

    // DocString: feat_node_is_const
    /**
     * @brief Check if feature is constant for one of the tasks
     *
     * @return True if the feature is constant for all samples in any task
     */
    bool is_const() const override;

    /**
     * @brief Returns the type of node this is
     */
    inline NODE_TYPE type() const override { return NODE_TYPE::FEAT; }

    /**
     * @brief The pointer to where the feature's training data is stored
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     */
    virtual inline double* value_ptr(int offset = -1, const bool for_comp = false) const override
    {
        return _selected ? node_value_arrs::get_d_matrix_ptr(_d_mat_ind)
                         : node_value_arrs::get_value_ptr(_arr_ind, _feat_ind, 0, offset, for_comp);
    }

    /**
     * @brief The pointer to where the feature's test data is stored
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     */
    virtual inline double* test_value_ptr(int offset = -1, const bool for_comp = false) const override
    {
        return node_value_arrs::get_test_value_ptr(_arr_ind, _feat_ind, 0, offset, for_comp);
    }

    /**
     * @brief Update the rung of the feature (does nothing as the feature nodes are rung 0)
     *
     * @param cur_rung The current rung of the feature
     */
    inline virtual void rung_update(int& cur_rung) const override {};

    /**
     * @brief Update the primary feature decomposition of a feature
     *
     * @param pf_decomp The primary feature decomposition of the feature calling this function.
     */
    virtual void update_primary_feature_decomp(std::map<std::string, int>& pf_decomp) const override;

    /**
     * @brief Converts a feature into a postfix expression (reverse polish notation)
     *
     * @details Recursively creates a postfix representation of the string
     *
     * @param cur_expr The current expression
     * @param add_params If true add the parameters
     * @return The current postfix expression of the feature
     */
    inline void update_postfix(std::string& cur_expr, const bool add_params = true) const override
    {
        cur_expr = get_postfix_term() + "|" + cur_expr;
    }

    /**
     * @brief Get the term used in the postfix expression for this Node
     */
    inline std::string get_postfix_term() const override { return std::to_string(_feat_ind); }

    // DocString: feat_node_matlab_expr
    /**
     * @brief Get the string that corresponds to the code needed to evaluate the node in matlab
     *
     * @return The matlab code for the feature
     */
    virtual inline std::string matlab_fxn_expr() const override { return str_utils::matlabify(_expr); }

    // DocString: feat_node_nfeats
    /**
     * @brief Number of features used for an operator node
     */
    inline int n_feats() const override { return 0; }

    // DocString: feat_node_feat
    /**
     * @brief Return the ind node_ptr in the operator node's feat list
     *
     * @param ind (int) the index of the node to access
     * @return the ind feature in feature_list
     */
    inline node_ptr feat(const int ind) const override
    {
        if (ind > -1)
        {
            throw std::logic_error("Can't get a child node from a FeatureNode.");
        }
        return nullptr;
    }

    /**
     * @brief update the dictionary used to check if an Add/Sub/AbsDiff node is valid
     *
     * @param add_sub_leaves the dictionary used to check if an Add/Sub node is valid
     * @param pl_mn 1 for addition and -1 for subtraction
     * @param expected_abs_tot The expected absolute sum of all values in add_sub_leaves
     */
    void update_add_sub_leaves(std::map<std::string, int>& add_sub_leaves,
                               const int pl_mn,
                               int& expected_abs_tot) const override;

    /**
     * @brief update the dictionary used to check if an Mult/Div node is valid
     *
     * @param div_mult_leaves the dictionary used to check if an Mult/Div node is valid
     * @param fact amount to increment the element (a primary features) of the dictionary by
     * @param expected_abs_tot The expected absolute sum of all values in div_mult_leaves
     */
    void update_div_mult_leaves(std::map<std::string, double>& div_mult_leaves,
                                const double fact,
                                double& expected_abs_tot) const override;

    /**
     * @brief Get the domain of a feature
     *
     * @return The domain of the feature
     */
    Domain domain() const override { return _domain; };

#ifdef PARAMETERIZE
    /**
     * @brief The parameters used for including individual scale and bias terms to each operator in the Node
     */
    inline std::vector<double> parameters() const override { return std::vector<double>(); };

    /**
     * @brief Set the non-linear parameters
     * @param params The new scale and bias terms of this node
     * @param check_sz If true check the size of the parameters
     */
    inline void set_parameters(const std::vector<double> params, const bool check_sz = true) override {};

    /**
     * @brief Set the non-linear parameters
     * @param params The new scale and bias terms of this node
     */
    inline void set_parameters(const double* params) override {};

    // DocString: feat_node_get_params
    /**
     * @brief Optimize the scale and bias terms for each operation in the Node.
     * @details Use optimizer to find the scale and bias terms that minimizes the associated loss function
     *
     * @param optimizer The optimizer used to evaluate the loss function for each optimization and find the optimal parameters
     */
    void get_parameters(std::shared_ptr<NLOptimizer> optimizer) override {};

    /**
     * @brief returns the number of parameters for this feature
     *
     * @param n_cur A recursive helper variable to count the number of parameters of the Node
     * @param depth How far down a given Node is from the root OperatorNode
     * @return the number of parameters (_params.size())
     */
    inline int n_params_possible(int n_cur = 0, int depth = 1) const override { return n_cur; };

    /**
     * @brief Set the value of all test samples for the feature inside the central data storage array
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param for_comp(bool) If true then the evaluation is used for comparing features
     * @param depth How far down a given Node is from the root OperatorNode
     */
    inline void set_value(const double* params,
                          int offset = -1,
                          const bool for_comp = false,
                          const int depth = 1) const override
    {
        set_value(offset);
    };

    /**
     * @brief The pointer to where the feature's training data is stored
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     * @param depth How far down a given Node is from the root OperatorNode
     * @returns The pointer to the feature's data
     */
    inline double* value_ptr(const double* params,
                             int offset = -1,
                             const bool for_comp = false,
                             const int depth = 1) const override
    {
        return value_ptr(offset);
    };

    /**
     * @brief Set the value of all test samples for the feature inside the central data storage array
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     * @param depth How far down a given Node is from the root OperatorNode
     */
    inline void set_test_value(const double* params,
                               int offset = -1,
                               const bool for_comp = false,
                               const int depth = 1) const override
    {
        set_test_value(offset);
    };

    /**
     * @brief The pointer to where the feature's test data is stored
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     * @param depth How far down a given Node is from the root OperatorNode
     * @returns The pointer to the feature's data
     */
    inline double* test_value_ptr(const double* params,
                                  int offset = -1,
                                  const bool for_comp = false,
                                  const int depth = 1) const override
    {
        return test_value_ptr(offset);
    };

    /**
     * @brief The expression of the feature
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param depth How far down a given Node is from the root OperatorNode
     * @return A human readable equation representing the feature
     */
    inline std::string expr(const double* params, const int depth = 1) const override { return _expr; };

    /**
     * @brief Get the valid LaTeX expression that represents the feature
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param depth How far down a given Node is from the root OperatorNode
     * @return Get the valid LaTeX expression that represents the feature
     */
    inline std::string get_latex_expr(const double* params, const int depth = 1) const override
    {
        return str_utils::latexify(_expr);
    }

    // DocString: feat_node_matlab_expr_param
    /**
     * @brief Get the string that corresponds to the code needed to evaluate the node in matlab
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param depth How far down a given Node is from the root OperatorNode
     * @return The matlab code for the feature
     */
    virtual inline std::string matlab_fxn_expr(const double* params, const int depth = 1) const override
    {
        return str_utils::matlabify(_expr);
    }

    /**
     * @brief Set the upper and lower bounds for the scale and bias term of this Node and its children
     *
     * @param lb A pointer to the location where the lower bounds for the scale and bias term of this Node is set
     * @param ub A pointer to the location where the upper bounds for the scale and bias term of this Node is set
     * @param depth How far down a given Node is from the root OperatorNode
     */
    inline void set_bounds(double* lb, double* ub, const int depth = 1) const override {};

    /**
     * @brief Initialize the scale and bias terms for this Node and its children
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param depth How far down a given Node is from the root OperatorNode
     */
    virtual void initialize_params(double* params, const int depth = 1) const override {};

    /**
     * @brief Calculates the derivative of an operation with respect to the parameters for a given sample
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param dfdp pointer to where the feature derivative pointers are located
     */
    void param_derivative(const double* params, double* dfdp, const int depth = 1) const override {}

    /**
     * @brief Get the parameter gradient for non-linear optimization
     *
     * @param grad pointer to the gradient storage
     * @param dfdp pointer to where the feature derivative pointers are located
     */
    virtual void gradient(double* grad, double* dfdp) const override
    {
        throw std::logic_error("Asking for the gradient of non-parameterized feature");
    }

    /**
     * @brief Get the parameter gradient for non-linear optimization
     *
     * @param grad pointer to the gradient storage
     * @param dfdp pointer to where the feature derivative pointers are located
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param depth The current depth in the binary expression tree
     */
    inline void gradient(double* grad,
                         double* dfdp,
                         const double* params,
                         const int depth = 1) const override {};

    /**
     * @brief Get the domain of a feature
     *
     * @param params Pointer to the scale and shift parameters of the feature
     * @return The domain of the feature
     */
    inline Domain domain(const double* params, const int depth = 1) const override { return _domain; };
#endif
};

// GCOV_EXCL_START     GCOVR_EXCL_START       LCOV_EXCL_START
#ifdef PY_BINDINGS
template <class FeatureNodeBase = FeatureNode>
class PyFeatureNode : public PyNode<FeatureNodeBase>
{
    using PyNode<FeatureNodeBase>::PyNode;  // Inherit constructors

    std::shared_ptr<Node> hard_copy() const override
    {
        PYBIND11_OVERRIDE(std::shared_ptr<Node>, FeatureNodeBase, hard_copy, );
    }
    std::vector<std::string> x_in_expr_list() const override
    {
        PYBIND11_OVERRIDE(std::vector<std::string>, FeatureNodeBase, x_in_expr_list, );
    }

    double* value_ptr(int offset = -1, const bool for_comp = false) const override
    {
        PYBIND11_OVERRIDE(double*, FeatureNodeBase, value_ptr, offset, for_comp);
    }

    double* test_value_ptr(int offset = -1, const bool for_comp = false) const override
    {
        PYBIND11_OVERRIDE(double*, FeatureNodeBase, test_value_ptr, offset, for_comp);
    }

    void update_primary_feature_decomp(std::map<std::string, int>& pf_decomp) const override
    {
        PYBIND11_OVERRIDE(void, FeatureNodeBase, update_primary_feature_decomp, pf_decomp);
    }

    std::string matlab_fxn_expr() const override
    {
        PYBIND11_OVERRIDE(std::string, FeatureNodeBase, matlab_fxn_expr, );
    }

    void update_n_leaves(int& cur_n_leaves) const override
    {
        PYBIND11_OVERRIDE(void, FeatureNodeBase, update_n_leaves, cur_n_leaves);
    }

    unsigned long long sort_score(unsigned int max_ind) const override
    {
        PYBIND11_OVERRIDE(unsigned long long, FeatureNodeBase, sort_score, max_ind);
    }

    std::string expr() const override { PYBIND11_OVERRIDE(std::string, FeatureNodeBase, expr, ); }

    std::string get_latex_expr() const override
    {
        PYBIND11_OVERRIDE(std::string, FeatureNodeBase, get_latex_expr, );
    }

    Unit unit() const override { PYBIND11_OVERRIDE(Unit, FeatureNodeBase, unit, ); }

    Domain domain() const override { PYBIND11_OVERRIDE(Domain, FeatureNodeBase, domain, ); }

    std::vector<double> value() const override
    {
        PYBIND11_OVERRIDE(std::vector<double>, FeatureNodeBase, value, );
    }

    std::vector<double> test_value() const override
    {
        PYBIND11_OVERRIDE(std::vector<double>, FeatureNodeBase, test_value, );
    }

    void set_value(int offset = -1, const bool for_comp = false) const override
    {
        PYBIND11_OVERRIDE(void, FeatureNodeBase, set_value, offset, for_comp);
    }

    void set_test_value(int offset = -1, const bool for_comp = false) const override
    {
        PYBIND11_OVERRIDE(void, FeatureNodeBase, set_test_value, offset, for_comp);
    }

    bool is_nan() const override { PYBIND11_OVERRIDE(bool, FeatureNodeBase, is_nan, ); }

    bool is_const() const override { PYBIND11_OVERRIDE(bool, FeatureNodeBase, is_const, ); }

    NODE_TYPE type() const override { PYBIND11_OVERRIDE(NODE_TYPE, FeatureNodeBase, type, ); }

    void rung_update(int& cur_rung) const override
    {
        PYBIND11_OVERRIDE(void, FeatureNodeBase, rung_update, cur_rung);
    }

    void update_postfix(std::string& cur_expr, const bool add_params = true) const override
    {
        PYBIND11_OVERRIDE(void, FeatureNodeBase, update_postfix, cur_expr, add_params);
    }

    std::string get_postfix_term() const override
    {
        PYBIND11_OVERRIDE(std::string, FeatureNodeBase, get_postfix_term, );
    }

    void update_add_sub_leaves(std::map<std::string, int>& add_sub_leaves,
                               const int pl_mn,
                               int& expected_abs_tot) const override
    {
        PYBIND11_OVERRIDE(
            void, FeatureNodeBase, update_add_sub_leaves, add_sub_leaves, pl_mn, expected_abs_tot);
    }

    void update_div_mult_leaves(std::map<std::string, double>& div_mult_leaves,
                                const double fact,
                                double& expected_abs_tot) const override
    {
        PYBIND11_OVERRIDE(
            void, FeatureNodeBase, update_div_mult_leaves, div_mult_leaves, fact, expected_abs_tot);
    }

    int n_feats() const override { PYBIND11_OVERRIDE(int, FeatureNodeBase, n_feats, ); }

#ifdef PARAMETERIZE
    void set_value(const double* params,
                   int offset = -1,
                   const bool for_comp = false,
                   const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(void, FeatureNodeBase, set_value, params, offset, for_comp, depth);
    }

    double* value_ptr(const double* params,
                      int offset = -1,
                      const bool for_comp = false,
                      const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(
            double*, FeatureNodeBase, value_ptr, params, offset, for_comp, depth);
    }

    void set_test_value(const double* params,
                        int offset = -1,
                        const bool for_comp = false,
                        const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(
            void, FeatureNodeBase, set_test_value, params, offset, for_comp, depth);
    }

    double* test_value_ptr(const double* params,
                           int offset = -1,
                           const bool for_comp = false,
                           const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(
            double*, FeatureNodeBase, test_value_ptr, params, offset, for_comp, depth);
    }

    Domain domain(const double* params, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(Domain, FeatureNodeBase, domain, params, depth);
    }

    std::string expr(const double* params, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(std::string, FeatureNodeBase, expr, params, depth);
    }

    std::string get_latex_expr(const double* params, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(std::string, FeatureNodeBase, get_latex_expr, params, depth);
    }

    std::string matlab_fxn_expr(const double* params, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(std::string, FeatureNodeBase, matlab_fxn_expr, params, depth);
    }
#endif
};
#endif
// GCOV_EXCL_STOP     GCOVR_EXCL_STOP       LCOV_EXCL_STOP

#endif
