// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/operator_nodes/allowed_operator_nodes/negative_exponential.hpp
 *  @brief Defines a class for the negative exponential operator
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 *
 *  This class represents the unary operator -> exp(-1 * A)
 */

#ifndef NEG_EXP_NODE
#define NEG_EXP_NODE

#include <fmt/core.h>

#include "feature_creation/node/operator_nodes/OperatorNode.hpp"

// DocString: cls_neg_exp_node
/**
 * @brief Node for the negative exponential operator
 *
 * @details Defines the operation exp(-1.0 * A) (inherits from OperatorNode<1>)
 *
 */
class NegExpNode : public OperatorNode<1>
{
    friend class boost::serialization::access;

    /**
     * @brief Serialization function to send over MPI
     *
     * @param ar Archive representation of node
     */
    template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ar& boost::serialization::base_object<OperatorNode>(*this);
    }

public:
    /**
     * @brief Base Constructor
     * @details This is only used for serialization
     */
    NegExpNode();

    /**
     * @brief Constructor excluding bounds on the maximum absolute value of the Node
     *
     * @param feat (Node) shared_ptr of the feature to operate on (A)
     * @param feat_ind (int) Index of the new feature
     */
    NegExpNode(const node_ptr feat, const unsigned long int feat_ind);

    // DocString: neg_exp_node_init
    /**
     * @brief Constructor including bounds on the maximum absolute value of the Node
     *
     * @param feat (Node) shared_ptr of the feature to operate on (A)
     * @param feat_ind (int) Index of the new feature
     * @param l_bound (float) Minimum absolute value allowed for the feature.
     * @param u_bound (float) Maximum absolute value allowed for the feature.
     */
    NegExpNode(const node_ptr feat,
               const unsigned long int feat_ind,
               const double l_bound,
               const double u_bound);

    /**
     * @brief Constructor including bounds on the maximum absolute value of the Node
     *
     * @param feats arry to the shared_ptr of the features to operate on
     * @param feat_ind (int) Index of the new feature
     */
    NegExpNode(std::array<node_ptr, 1> feats, const unsigned long int feat_ind);

    virtual ~NegExpNode() = default;

    /**
     * @brief Makes a hard copy node (All members of the Node are independent of the original one)
     * @return A shared_ptr to the copied node
     */
    virtual node_ptr hard_copy() const override;

    // DocString: neg_exp_node_unit
    /**
     * @brief Get the unit of the feature (combine the units of _feats)
     */
    inline Unit unit() const override { return Unit(); }

    // DocString: neg_exp_node_expr
    /**
     * @brief A human readable equation representing the feature
     */
    inline std::string expr() const override { return fmt::format("(exp(-1.0 * {}))", _feats[0]->expr()); }

    // DocString: neg_exp_node_latex_expr
    /**
     * @brief Get the valid LaTeX expression that represents the feature
     */
    inline std::string get_latex_expr() const override
    {
        return fmt::format("\\left(\\exp{{ \\left(-{} \\right) }} \\right)",
                           _feats[0]->get_latex_expr());
    }

    // DocString: neg_exp_node_set_value
    /**
     * @brief Set the value of all training samples for the feature inside the central data storage array
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     */
    virtual void set_value(int offset = -1, const bool for_comp = false) const override;

    // DocString: neg_exp_node_set_test_value
    /**
     * @brief Set the value of all test samples for the feature inside the central data storage array
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     */
    virtual void set_test_value(int offset = -1, const bool for_comp = false) const override;

    /**
     * @brief Returns the type of node this is
     */
    virtual inline NODE_TYPE type() const override { return NODE_TYPE::NEG_EXP; }

    /**
     * @brief Get the term used in the postfix expression for this Node
     */
    inline std::string get_postfix_term() const override { return "nexp"; }

    // DocString: neg_exp_node_matlab_expr
    /**
     * @brief Get the string that corresponds to the code needed to evaluate the node in matlab
     *
     * @return The matlab code for the feature
     */
    inline std::string matlab_fxn_expr() const override
    {
        return fmt::format("exp(-1.0 * ({}))", _feats[0]->matlab_fxn_expr());
    }

    /**
     * @brief update the dictionary used to check if an Add/Sub/AbsDiff node is valid
     *
     * @param add_sub_leaves the dictionary used to check if an Add/Sub node is valid
     * @param pl_mn 1 for addition and -1 for subtraction
     * @param expected_abs_tot The expected absolute sum of all values in add_sub_leaves
     */
    void update_add_sub_leaves(std::map<std::string, int>& add_sub_leaves,
                               const int pl_mn,
                               int& expected_abs_tot) const override;

    /**
     * @brief update the dictionary used to check if an Mult/Div node is valid
     *
     * @param div_mult_leaves the dictionary used to check if an Mult/Div node is valid
     * @param fact amount to increment the element (a primary features) of the dictionary by
     * @param expected_abs_tot The expected absolute sum of all values in div_mult_leaves
     */
    void update_div_mult_leaves(std::map<std::string, double>& div_mult_leaves,
                                const double fact,
                                double& expected_abs_tot) const override;

    /**
     * @brief Get the domain of a feature
     *
     * @return The domain of the feature
     */
    inline Domain domain() const override { return _feats[0]->domain().neg().exp(); }

#ifdef PARAMETERIZE
    /**
     * @brief The parameters used for including individual scale and bias terms to each operator in the Node
     */
    virtual std::vector<double> parameters() const override { return {}; }

    /**
     * @brief Optimize the scale and bias terms for each operation in the Node.
     * @details Use optimizer to find the scale and bias terms that minimizes the associated loss function
     *
     * @param optimizer The optimizer used to evaluate the loss function for each optimization and find the optimal parameters
     */
    virtual void get_parameters(std::shared_ptr<NLOptimizer> optimizer) override { return; }

    /**
     * @brief Set the non-linear parameters
     * @param params The new parameters for the feature
     * @param check_sz if True check the size of the params vector with the expected size
     */
    virtual void set_parameters(const std::vector<double> params, const bool check_sz = true) override
    {
        return;
    }

    /**
     * @brief Set the non-linear parameters
     * @param params The new scale and bias terms of this node
     */
    virtual void set_parameters(const double* params) override { return; }

    /**
     * @brief Set the value of all training samples for the feature inside the central data storage array
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     * @param depth (int) How far down a given Node is from the root OperatorNode
     */
    void set_value(const double* params,
                   int offset = -1,
                   const bool for_comp = false,
                   const int depth = 1) const override;

    /**
     * @brief Set the value of all test samples for the feature inside the central data storage array
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     * @param depth (int) How far down a given Node is from the root OperatorNode
     */
    void set_test_value(const double* params,
                        int offset = -1,
                        const bool for_comp = false,
                        const int depth = 1) const override;

    /**
     * @brief A human readable equation representing the feature
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @return A human readable equation representing the feature
     */
    inline std::string expr(const double* params, const int depth = 1) const override
    {
        return fmt::format(
            "(exp(-1.0 * ({:.6e}*{})))",
            params[0],
            (depth < nlopt_wrapper::MAX_PARAM_DEPTH ? _feats[0]->expr(params + 2, depth + 1)
                                                    : _feats[0]->expr()));
    }

    /**
     * @brief Get the valid LaTeX expression that represents the feature
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param depth How far down a given Node is from the root OperatorNode
     * @return Get the valid LaTeX expression that represents the feature
     */
    inline std::string get_latex_expr(const double* params, const int depth = 1) const override
    {
        return fmt::format("\\left(\\exp{{ \\left(-1.0\\left({:.3e}{} \\right)\\right)}}\\right)",
                           params[0],
                           (depth < nlopt_wrapper::MAX_PARAM_DEPTH
                                ? _feats[0]->get_latex_expr(params + 2, depth + 1)
                                : _feats[0]->get_latex_expr()));
    }

    /**
     * @brief Get the string that corresponds to the code needed to evaluate the node in matlab
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param depth How far down a given Node is from the root OperatorNode
     * @return The matlab code for the feature
     */
    inline std::string matlab_fxn_expr(const double* params, const int depth = 1) const override
    {
        return fmt::format("exp(-1.0 .* ({:.6e}.*{}))",
                           params[0],
                           (depth < nlopt_wrapper::MAX_PARAM_DEPTH
                                ? _feats[0]->matlab_fxn_expr(params + 2, depth + 1)
                                : _feats[0]->matlab_fxn_expr()));
    }

    /**
     * @brief Set the upper and lower bounds for the scale and bias term of this Node and its children
     *
     * @param lb A pointer to the location where the lower bounds for the scale and bias term of this Node is set
     * @param ub A pointer to the location where the upper bounds for the scale and bias term of this Node is set
     * @param depth How far down a given Node is from the root OperatorNode
     */
    void set_bounds(double* lb, double* ub, const int depth = 1) const override;

    /**
     * @brief Initialize the scale and bias terms for this Node and its children
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param depth How far down a given Node is from the root OperatorNode
     */
    void initialize_params(double* params, const int depth = 1) const override;

    /**
     * @brief Calculates the derivative of an operation with respect to the parameters for a given sample
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param dfdp pointer to where the feature derivative pointers are located
     */
    inline void param_derivative(const double* params, double* dfdp, const int depth = 1) const override
    {
        double* val_ptr = (depth < nlopt_wrapper::MAX_PARAM_DEPTH)
                              ? _feats[0]->value_ptr(params + 2, depth + 1, -1, true)
                              : _feats[0]->value_ptr(-1, true);
        std::transform(val_ptr, val_ptr + _n_samp, dfdp, [params](double vp) {
            return -1.0 * std::exp(-1.0 * (params[0] * vp + params[1]));
        });
    }

    /**
     * @brief Get the domain of a feature
     *
     * @param params Pointer to the scale and shift parameters of the feature
     * @return The domain of the feature
     */
    inline Domain domain(const double* params, const int depth = 1) const override
    {
        Domain dom_0 = (depth < nlopt_wrapper::MAX_PARAM_DEPTH)
                           ? _feats[0]->domain(params + 2, depth + 1)
                           : _feats[0]->domain();
        return dom_0.parameterize(params[0], params[1]).neg().exp();
    }

#endif
};

/**
 * @brief Attempt to generate a new parameterized negative exponential (exp(-A)) node and add it to feat_list
 *
 * @param feat_list list of features already generated
 * @param feat feature to attempt to take the negative exponential of
 * @param feat_ind Index of the new feature
 * @param l_bound Minimum absolute value allowed for the feature.
 * @param u_bound Maximum absolute value allowed for the feature.
 */
void generateNegExpNode(std::vector<node_ptr>& feat_list,
                        const node_ptr feat,
                        unsigned long int& feat_ind,
                        const double l_bound,
                        const double u_bound);

// GCOV_EXCL_START     GCOVR_EXCL_START       LCOV_EXCL_START
#ifdef PY_BINDINGS
template <class NegExpNodeBase = NegExpNode>
class PyNegExpNode : public PyOperatorNode<1, NegExpNodeBase>
{
    using PyOperatorNode<1, NegExpNodeBase>::PyOperatorNode;

    node_ptr hard_copy() const override
    {
        PYBIND11_OVERRIDE(node_ptr, NegExpNodeBase, hard_copy, );
    }

    void set_value(int offset = -1, const bool for_comp = false) const override
    {
        PYBIND11_OVERRIDE(void, NegExpNodeBase, set_value, offset, for_comp);
    }

    void set_test_value(int offset = -1, const bool for_comp = false) const override
    {
        PYBIND11_OVERRIDE(void, NegExpNodeBase, set_test_value, offset, for_comp);
    }

    NODE_TYPE type() const override { PYBIND11_OVERRIDE(NODE_TYPE, NegExpNodeBase, type, ); }

    std::string get_latex_expr() const override
    {
        PYBIND11_OVERRIDE(std::string, NegExpNodeBase, get_latex_expr, );
    }

    double* value_ptr(int offset = -1, const bool for_comp = false) const override
    {
        PYBIND11_OVERRIDE(double*, NegExpNodeBase, value_ptr, offset, for_comp);
    }

    double* test_value_ptr(int offset = -1, const bool for_comp = false) const override
    {
        PYBIND11_OVERRIDE(double*, NegExpNodeBase, test_value_ptr, offset, for_comp);
    }

    void update_postfix(std::string& cur_expr, const bool add_params = true) const override
    {
        PYBIND11_OVERRIDE(void, NegExpNodeBase, update_postfix, cur_expr, add_params);
    }

    std::string get_postfix_term() const override
    {
        PYBIND11_OVERRIDE(std::string, NegExpNodeBase, get_postfix_term, );
    }

    std::string matlab_fxn_expr() const override
    {
        PYBIND11_OVERRIDE(std::string, NegExpNodeBase, matlab_fxn_expr, );
    }

    void update_add_sub_leaves(std::map<std::string, int>& add_sub_leaves,
                               const int pl_mn,
                               int& expected_abs_tot) const override
    {
        PYBIND11_OVERRIDE(
            void, NegExpNodeBase, update_add_sub_leaves, add_sub_leaves, pl_mn, expected_abs_tot);
    }

    void update_div_mult_leaves(std::map<std::string, double>& div_mult_leaves,
                                const double fact,
                                double& expected_abs_tot) const override
    {
        PYBIND11_OVERRIDE(
            void, NegExpNodeBase, update_div_mult_leaves, div_mult_leaves, fact, expected_abs_tot);
    }

    void reset_feats(std::vector<node_ptr>& phi) override
    {
        PYBIND11_OVERRIDE(void, NegExpNodeBase, reset_feats, phi);
    }

    std::vector<std::string> x_in_expr_list() const override
    {
        PYBIND11_OVERRIDE(std::vector<std::string>, NegExpNodeBase, x_in_expr_list, );
    }

    unsigned long long sort_score(unsigned int max_ind) const override
    {
        PYBIND11_OVERRIDE(unsigned long long, NegExpNodeBase, sort_score, max_ind);
    }

    std::string expr() const override { PYBIND11_OVERRIDE(std::string, NegExpNodeBase, expr, ); }

    Unit unit() const override { PYBIND11_OVERRIDE(Unit, NegExpNodeBase, unit, ); }

    Domain domain() const override { PYBIND11_OVERRIDE(Domain, NegExpNodeBase, domain, ); }

    std::vector<double> value() const override
    {
        PYBIND11_OVERRIDE(std::vector<double>, NegExpNodeBase, value, );
    }

    std::vector<double> test_value() const override
    {
        PYBIND11_OVERRIDE(std::vector<double>, NegExpNodeBase, test_value, );
    }

    bool is_nan() const override { PYBIND11_OVERRIDE(bool, NegExpNodeBase, is_nan, ); }

    bool is_const() const override { PYBIND11_OVERRIDE(bool, NegExpNodeBase, is_const, ); }

    void update_primary_feature_decomp(std::map<std::string, int>& pf_decomp) const override
    {
        PYBIND11_OVERRIDE(void, NegExpNodeBase, update_primary_feature_decomp, pf_decomp);
    }

    int n_feats() const override { PYBIND11_OVERRIDE(int, NegExpNodeBase, n_feats, ); }

    node_ptr feat(const int ind) const override
    {
        PYBIND11_OVERRIDE(node_ptr, NegExpNodeBase, feat, ind);
    }

#ifdef PARAMETERIZE

    const double* param_pointer() const override
    {
        PYBIND11_OVERRIDE(const double*, NegExpNodeBase, param_pointer, );
    }

    int n_params() const override { PYBIND11_OVERRIDE(int, NegExpNodeBase, n_params, ); }

    double* value_ptr(const double* params,
                      int offset = -1,
                      const bool for_comp = false,
                      const int depth = 1) const override
    {
        PYBIND11_OVERRIDE(double*, NegExpNodeBase, value_ptr, params, offset, for_comp, depth);
    }

    double* test_value_ptr(const double* params,
                           int offset = -1,
                           const bool for_comp = false,
                           const int depth = 1) const override
    {
        PYBIND11_OVERRIDE(double*, NegExpNodeBase, test_value_ptr, params, offset, for_comp, depth);
    }

    void param_derivative(const double* params, double* dfdp, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE(void, NegExpNodeBase, param_derivative, params, dfdp, depth);
    }

    void gradient(double* grad, double* dfdp) const override
    {
        PYBIND11_OVERRIDE(void, NegExpNodeBase, gradient, grad, dfdp);
    }

    void gradient(double* grad,
                  double* dfdp,
                  const double* params,
                  const int depth = 1) const override
    {
        PYBIND11_OVERRIDE(void, NegExpNodeBase, gradient, grad, dfdp, params, depth);
    }

    std::vector<double> parameters() const override
    {
        PYBIND11_OVERRIDE(std::vector<double>, NegExpNodeBase, parameters, );
    }

    int n_params_possible(const int n_cur = 0, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE(int, NegExpNodeBase, n_params_possible, n_cur, depth);
    }

    void set_value(const double* params,
                   int offset = -1,
                   const bool for_comp = false,
                   const int depth = 1) const override
    {
        PYBIND11_OVERRIDE(void, NegExpNodeBase, set_value, params, offset, for_comp, depth);
    }

    void set_test_value(const double* params,
                        int offset = -1,
                        const bool for_comp = false,
                        const int depth = 1) const override
    {
        PYBIND11_OVERRIDE(void, NegExpNodeBase, set_test_value, params, offset, for_comp, depth);
    }

    Domain domain(const double* params, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE(Domain, NegExpNodeBase, domain, params, depth);
    }

    std::string expr(const double* params, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE(std::string, NegExpNodeBase, expr, params, depth);
    }

    std::string get_latex_expr(const double* params, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE(std::string, NegExpNodeBase, get_latex_expr, params, depth);
    }

    std::string matlab_fxn_expr(const double* params, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE(std::string, NegExpNodeBase, matlab_fxn_expr, params, depth);
    }

    void set_bounds(double* lb, double* ub, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE(void, NegExpNodeBase, set_bounds, lb, ub, depth);
    }

    void initialize_params(double* params, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE(void, NegExpNodeBase, initialize_params, params, depth);
    }

    void get_parameters(std::shared_ptr<NLOptimizer> optimizer) override
    {
        PYBIND11_OVERRIDE(void, NegExpNodeBase, get_parameters, optimizer);
    }

    void set_parameters(const std::vector<double> params, const bool check_sz = true) override
    {
        PYBIND11_OVERRIDE(void, NegExpNodeBase, set_parameters, params, check_sz);
    }

    void set_parameters(const double* params) override
    {
        PYBIND11_OVERRIDE(void, NegExpNodeBase, set_parameters, params);
    }
#endif
};
#endif
// GCOV_EXCL_STOP     GCOVR_EXCL_STOP       LCOV_EXCL_STOP

#endif
