// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/operator_nodes/allowed_operator_nodes/inv/parameterized_inverse.cpp
 *  @brief Implements a class for the parameterized version of the inverse operator
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 *
 *  This class represents the parameterized unary operator -> 1.0 / (A + a)
 */

#include "feature_creation/node/operator_nodes/allowed_operator_nodes/inv/parameterized_inverse.hpp"

BOOST_SERIALIZATION_ASSUME_ABSTRACT(InvParamNode)

void generateInvParamNode(std::vector<node_ptr>& feat_list,
                          const node_ptr feat,
                          unsigned long int& feat_ind,
                          const double l_bound,
                          const double u_bound,
                          std::shared_ptr<NLOptimizer> optimizer)
{
    // If the input feature is an inverse operator or a division operator this feature will be a repeat
    if ((feat->type() == NODE_TYPE::DIV) || (feat->type() == NODE_TYPE::EXP) ||
        (feat->type() == NODE_TYPE::NEG_EXP) || (feat->type() == NODE_TYPE::INV) ||
        (feat->type() == NODE_TYPE::PARAM_DIV) || (feat->type() == NODE_TYPE::PARAM_EXP) ||
        (feat->type() == NODE_TYPE::PARAM_NEG_EXP) || (feat->type() == NODE_TYPE::PARAM_INV))
    {
        return;
    }

    ++feat_ind;
    node_ptr new_feat = std::make_shared<InvParamNode>(feat, feat_ind, optimizer);
    // Domain dom = new_feat->domain();

    // If a scale parameter is 0.0 feature is invalid
    if ((std::abs(new_feat->parameters()[0]) <= 1e-10)
        // ((!dom.is_empty()) && (std::isnan(dom.end_points()[0]) || std::isnan(dom.end_points()[1])))
    )
    {
        return;
    }

    Domain dom_0 = (1 < nlopt_wrapper::MAX_PARAM_DEPTH)
                       ? new_feat->feat(0)->domain(new_feat->param_pointer() + 2, 2)
                       : new_feat->feat(0)->domain();
    if (dom_0.parameterize(new_feat->param_pointer()[0], new_feat->param_pointer()[1])
            .contains(0.0))
    {
        return;
    }

    new_feat->set_value();

    // Check if the feature is NaN, greater than the allowed max of less than the allowed min
    if (new_feat->is_const() || new_feat->is_nan() ||
        (util_funcs::max_abs_val<double>(new_feat->value_ptr(), new_feat->n_samp()) > u_bound) ||
        (util_funcs::max_abs_val<double>(new_feat->value_ptr(), new_feat->n_samp()) < l_bound))
    {
        return;
    }

    feat_list.push_back(new_feat);
}

InvParamNode::InvParamNode() {}

InvParamNode::InvParamNode(const node_ptr feat,
                           const unsigned long int feat_ind,
                           const double l_bound,
                           const double u_bound,
                           std::shared_ptr<NLOptimizer> optimizer)
    : InvNode(feat, feat_ind)
{
    // If the input feature is an inverse operator or a division operator this feature will be a repeat
    if ((feat->type() == NODE_TYPE::DIV) || (feat->type() == NODE_TYPE::EXP) ||
        (feat->type() == NODE_TYPE::NEG_EXP) || (feat->type() == NODE_TYPE::INV) ||
        (feat->type() == NODE_TYPE::PARAM_DIV) || (feat->type() == NODE_TYPE::PARAM_EXP) ||
        (feat->type() == NODE_TYPE::PARAM_NEG_EXP) || (feat->type() == NODE_TYPE::PARAM_INV))
    {
        throw InvalidFeatureException();
    }

    _params.resize(n_params_possible(), 0.0);
    get_parameters(optimizer);

    Domain dom_0 = (1 < nlopt_wrapper::MAX_PARAM_DEPTH) ? feat->domain(&_params[2], 2)
                                                        : feat->domain();
    if (dom_0.parameterize(_params[0], _params[1]).contains(0.0))
    {
        return;
    }

    // Domain dom = domain();
    // Check if the scale parameter is 0.0 or if the feature is NaN, constant, greater than the allowed max of less than the allowed min
    if ((std::abs(_params[0]) <= 1e-10) ||
        // ((!dom.is_empty()) && (std::isnan(dom.end_points()[0]) || std::isnan(dom.end_points()[1]))) ||
        is_const() || is_nan() ||
        (util_funcs::max_abs_val<double>(value_ptr(), _n_samp) > u_bound) ||
        (util_funcs::max_abs_val<double>(value_ptr(), _n_samp) < l_bound))
    {
        throw InvalidFeatureException();
    }
}

InvParamNode::InvParamNode(const node_ptr feat,
                           const unsigned long int feat_ind,
                           std::shared_ptr<NLOptimizer> optimizer)
    : InvNode(feat, feat_ind)
{
    _params.resize(n_params_possible(), 0.0);
    get_parameters(optimizer);
}

InvParamNode::InvParamNode(const node_ptr feat,
                           const unsigned long int feat_ind,
                           const double l_bound,
                           const double u_bound)
    : InvNode(feat, feat_ind)
{
    _params.resize(n_params_possible(), 0.0);
}

InvParamNode::InvParamNode(std::array<node_ptr, 1> feats,
                           const unsigned long int feat_ind,
                           std::vector<double> params)
    : InvNode(feats, feat_ind), _params(params)
{
}

node_ptr InvParamNode::hard_copy() const
{
    node_ptr cp = std::make_shared<InvParamNode>(_feats[0]->hard_copy(), _feat_ind);
    cp->set_selected(_selected);
    cp->set_d_mat_ind(_d_mat_ind);
    cp->set_parameters(_params.data());
    return cp;
}

void InvParamNode::get_parameters(std::shared_ptr<NLOptimizer> optimizer)
{
    double min_res = optimizer->optimize_feature_params(this);
    if (min_res == std::numeric_limits<double>::infinity())
    {
        _params[0] = 0.0;
    }
}

void InvNode::set_value(const double* params,
                        int offset,
                        const bool for_comp,
                        const int depth) const
{
    bool is_root = (offset == -1);
    offset += is_root;

    double* vp_0;
    if (depth < nlopt_wrapper::MAX_PARAM_DEPTH)
    {
        vp_0 = _feats[0]->value_ptr(params + 2, 2 * offset, for_comp, depth + 1);
    }
    else
    {
        vp_0 = _feats[0]->value_ptr(2 * offset, for_comp);
    }

    double* val_ptr;
    if (_selected && is_root)
    {
        val_ptr = node_value_arrs::get_d_matrix_ptr(_d_mat_ind);
    }
    else
    {
        val_ptr = node_value_arrs::access_param_storage(rung(), offset, for_comp);
    }

    allowed_op_funcs::inv(_n_samp, vp_0, params[0], params[1], val_ptr);
}

void InvNode::set_test_value(const double* params,
                             int offset,
                             const bool for_comp,
                             const int depth) const
{
    offset += (offset == -1);

    double* vp_0;
    if (depth < nlopt_wrapper::MAX_PARAM_DEPTH)
    {
        vp_0 = _feats[0]->test_value_ptr(params + 2, 2 * offset, for_comp, depth + 1);
    }
    else
    {
        vp_0 = _feats[0]->test_value_ptr(2 * offset, for_comp);
    }

    allowed_op_funcs::inv(_n_samp_test,
                          vp_0,
                          params[0],
                          params[1],
                          node_value_arrs::access_param_storage_test(rung(), offset, for_comp));
}

void InvNode::set_bounds(double* lb, double* ub, const int depth) const
{
    lb[0] = 1.0;
    ub[0] = 1.0;

    if (depth >= nlopt_wrapper::MAX_PARAM_DEPTH)
    {
        return;
    }

    _feats[0]->set_bounds(lb + 2, ub + 2, depth + 1);
}

void InvNode::initialize_params(double* params, const int depth) const
{
    params[0] = 1.0;
    double* val_ptr;
    if (depth >= nlopt_wrapper::MAX_PARAM_DEPTH)
    {
        val_ptr = _feats[0]->value_ptr();
        params[1] = 1e-10 * std::any_of(val_ptr, val_ptr + _n_samp, [](double v) {
                        return std::abs(v) < 1e-17;
                    });
        return;
    }

    _feats[0]->initialize_params(params + 2, depth + 1);
    val_ptr = _feats[0]->value_ptr(params + 2);
    params[1] = 1e-10 * std::any_of(val_ptr, val_ptr + _n_samp, [](double v) {
                    return std::abs(v) < 1e-17;
                });
}

void InvParamNode::update_postfix(std::string& cur_expr, const bool add_params) const
{
    std::stringstream postfix;
    postfix << get_postfix_term();
    if (add_params)
    {
        postfix << ":" << std::setprecision(13) << std::scientific << _params[0];
        for (size_t pp = 1; pp < _params.size(); ++pp)
        {
            postfix << "," << std::setprecision(13) << std::scientific << _params[pp];
        }
    }
    cur_expr = postfix.str() + "|" + cur_expr;
    _feats[0]->update_postfix(cur_expr, false);
}
