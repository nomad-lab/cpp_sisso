// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/operator_nodes/allowed_operator_nodes/square.cpp
 *  @brief Implements a class for the square operator
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 *
 *  This class represents the unary operator -> (A)^2
 */

#include "feature_creation/node/operator_nodes/allowed_operator_nodes/sq/square.hpp"

void generateSqNode(std::vector<node_ptr>& feat_list,
                    const node_ptr feat,
                    unsigned long int& feat_ind,
                    const double l_bound,
                    const double u_bound)
{
    // Prevent possible repeats by combining other power operations together
    if ((feat->type() == NODE_TYPE::SQRT) || (feat->type() == NODE_TYPE::INV) ||
        (feat->type() == NODE_TYPE::EXP) || (feat->type() == NODE_TYPE::NEG_EXP) ||
        (feat->type() == NODE_TYPE::PARAM_SQRT) || (feat->type() == NODE_TYPE::PARAM_INV) ||
        (feat->type() == NODE_TYPE::PARAM_EXP) || (feat->type() == NODE_TYPE::PARAM_NEG_EXP))
    {
        return;
    }

    ++feat_ind;
    node_ptr new_feat = std::make_shared<SqNode>(feat, feat_ind);
    double* val_ptr = new_feat->value_ptr();
    // Domain dom = new_feat->domain();

    // Check if the feature is NaN, constant, greater than the allowed max of less than the allowed min
    if (new_feat->is_const() || new_feat->is_nan() ||
        std::any_of(
            val_ptr,
            val_ptr + new_feat->n_samp(),
            [&u_bound](double d) { return !std::isfinite(d) || (std::abs(d) > u_bound); }) ||
        (util_funcs::max_abs_val<double>(val_ptr, new_feat->n_samp()) < l_bound)
        // ((!dom.is_empty()) && (std::isnan(dom.end_points()[0]) || std::isnan(dom.end_points()[1])))
    )
    {
        return;
    }

    feat_list.push_back(new_feat);
}

SqNode::SqNode() {}

SqNode::SqNode(const node_ptr feat, const unsigned long int feat_ind)
    : OperatorNode({feat}, feat_ind)
{
}

SqNode::SqNode(std::array<node_ptr, 1> feats, const unsigned long int feat_ind)
    : OperatorNode(feats, feat_ind)
{
}

SqNode::SqNode(const node_ptr feat,
               const unsigned long int feat_ind,
               const double l_bound,
               const double u_bound)
    : OperatorNode({feat}, feat_ind)
{
    // Prevent possible repeats by combining other power operations together
    if ((feat->type() == NODE_TYPE::SQRT) || (feat->type() == NODE_TYPE::INV) ||
        (feat->type() == NODE_TYPE::EXP) || (feat->type() == NODE_TYPE::NEG_EXP) ||
        (feat->type() == NODE_TYPE::PARAM_SQRT) || (feat->type() == NODE_TYPE::PARAM_INV) ||
        (feat->type() == NODE_TYPE::PARAM_EXP) || (feat->type() == NODE_TYPE::PARAM_NEG_EXP))
    {
        throw InvalidFeatureException();
    }

    set_value();
    // Domain dom = domain();
    // Check if the feature is NaN, constant, greater than the allowed max of less than the allowed min
    if (
        // ((!dom.is_empty()) && (std::isnan(dom.end_points()[0]) || std::isnan(dom.end_points()[1]))) ||
        is_nan() || is_const() ||
        (util_funcs::max_abs_val<double>(value_ptr(), _n_samp) > u_bound) ||
        (util_funcs::max_abs_val<double>(value_ptr(), _n_samp) < l_bound))
    {
        throw InvalidFeatureException();
    }
}

node_ptr SqNode::hard_copy() const
{
    node_ptr cp = std::make_shared<SqNode>(_feats[0]->hard_copy(), _feat_ind);
    cp->set_selected(_selected);
    cp->set_d_mat_ind(_d_mat_ind);
    return cp;
}

void SqNode::update_add_sub_leaves(std::map<std::string, int>& add_sub_leaves,
                                   const int pl_mn,
                                   int& expected_abs_tot) const
{
    std::string key = expr();
    if (add_sub_leaves.count(key) > 0)
    {
        add_sub_leaves[key] += pl_mn;
    }
    else
    {
        add_sub_leaves[key] = pl_mn;
    }

    ++expected_abs_tot;
}

void SqNode::update_div_mult_leaves(std::map<std::string, double>& div_mult_leaves,
                                    const double fact,
                                    double& expected_abs_tot) const
{
    _feats[0]->update_div_mult_leaves(div_mult_leaves, fact * 2.0, expected_abs_tot);
}

void SqNode::set_value(int offset, const bool for_comp) const
{
    double* val_ptr;
    if (_selected && (offset == -1))
    {
        offset += (offset == -1);
        val_ptr = node_value_arrs::get_d_matrix_ptr(_d_mat_ind);
    }
    else
    {
        offset += (offset == -1);
        val_ptr = node_value_arrs::get_value_ptr(_arr_ind, _feat_ind, rung(), offset, for_comp);
    }

    allowed_op_funcs::sq(_n_samp, _feats[0]->value_ptr(2 * offset, for_comp), 1.0, 0.0, val_ptr);
}

void SqNode::set_test_value(int offset, const bool for_comp) const
{
    offset += (offset == -1);
    allowed_op_funcs::sq(
        _n_samp_test,
        _feats[0]->test_value_ptr(2 * offset, for_comp),
        1.0,
        0.0,
        node_value_arrs::get_test_value_ptr(_arr_ind, _feat_ind, rung(), offset, for_comp));
}
