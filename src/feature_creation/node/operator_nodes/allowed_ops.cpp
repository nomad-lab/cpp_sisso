// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/operator_nodes/allowed_ops.cpp
 *  @brief Implements the map that converts std::string descriptions of the operators into their generator functions for non-parameterized operations
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "feature_creation/node/operator_nodes/allowed_ops.hpp"

std::map<std::string, un_op_node_gen> allowed_op_maps::unary_operator_map;
std::map<std::string, bin_op_node_gen> allowed_op_maps::binary_operator_map;

void allowed_op_maps::set_node_maps()
{
    allowed_op_maps::binary_operator_map["add"] = generateAddNode;
    allowed_op_maps::binary_operator_map["sub"] = generateSubNode;
    allowed_op_maps::binary_operator_map["abs_diff"] = generateAbsDiffNode;
    allowed_op_maps::binary_operator_map["mult"] = generateMultNode;
    allowed_op_maps::binary_operator_map["div"] = generateDivNode;

    allowed_op_maps::unary_operator_map["exp"] = generateExpNode;
    allowed_op_maps::unary_operator_map["neg_exp"] = generateNegExpNode;
    allowed_op_maps::unary_operator_map["inv"] = generateInvNode;
    allowed_op_maps::unary_operator_map["sq"] = generateSqNode;
    allowed_op_maps::unary_operator_map["cb"] = generateCbNode;
    allowed_op_maps::unary_operator_map["six_pow"] = generateSixPowNode;
    allowed_op_maps::unary_operator_map["sqrt"] = generateSqrtNode;
    allowed_op_maps::unary_operator_map["cbrt"] = generateCbrtNode;
    allowed_op_maps::unary_operator_map["log"] = generateLogNode;
    allowed_op_maps::unary_operator_map["abs"] = generateAbsNode;
    allowed_op_maps::unary_operator_map["sin"] = generateSinNode;
    allowed_op_maps::unary_operator_map["cos"] = generateCosNode;
}
