// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/operator_nodes/allowed_ops.hpp
 *  @brief Defines the map that converts std::string descriptions of the operators into their generator functions
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef ALLOWED_OP_NODES
#define ALLOWED_OP_NODES

#ifdef PARAMETERIZE
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/abs/parameterized_absolute_value.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/abs_diff/parameterized_absolute_difference.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/add/parameterized_add.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/cb/parameterized_cube.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/cbrt/parameterized_cube_root.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/cos/parameterized_cos.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/div/parameterized_divide.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/exp/parameterized_exponential.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/inv/parameterized_inverse.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/log/parameterized_log.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/mult/parameterized_multiply.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/neg_exp/parameterized_negative_exponential.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/sin/parameterized_sin.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/six_pow/parameterized_sixth_power.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/sq/parameterized_square.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/sqrt/parameterized_square_root.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/sub/parameterized_subtract.hpp"
#else
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/abs/absolute_value.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/abs_diff/absolute_difference.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/add/add.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/cb/cube.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/cbrt/cube_root.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/cos/cos.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/div/divide.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/exp/exponential.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/inv/inverse.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/log/log.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/mult/multiply.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/neg_exp/negative_exponential.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/sin/sin.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/six_pow/sixth_power.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/sq/square.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/sqrt/square_root.hpp"
#include "feature_creation/node/operator_nodes/allowed_operator_nodes/sub/subtract.hpp"
#endif

#include <map>

typedef std::function<void(
    std::vector<node_ptr>&, const node_ptr, unsigned long int&, const double, const double)>
    un_op_node_gen;
typedef std::function<void(std::vector<node_ptr>&,
                           const node_ptr,
                           const node_ptr,
                           unsigned long int&,
                           const double,
                           const double)>
    bin_op_node_gen;

#ifdef PARAMETERIZE
typedef std::function<void(std::vector<node_ptr>&,
                           const node_ptr,
                           unsigned long int&,
                           const double,
                           const double,
                           const std::shared_ptr<NLOptimizer>)>
    un_param_op_node_gen;
typedef std::function<void(std::vector<node_ptr>&,
                           const node_ptr,
                           const node_ptr,
                           unsigned long int&,
                           const double,
                           const double,
                           const std::shared_ptr<NLOptimizer>)>
    bin_param_op_node_gen;
#endif

namespace allowed_op_maps
{
// clang-format off
    extern std::map<std::string, un_op_node_gen> unary_operator_map; //!< map that converts a string into an operator node generator function for all unary operators
    extern std::map<std::string, bin_op_node_gen> binary_operator_map; //!< map that converts a string into an operator node generator function for all binary operators
// clang-format on

/**
     * @brief initialize the maps from operator string to generator functions
     */
void set_node_maps();

#ifdef PARAMETERIZE
// clang-format off
        extern std::map<std::string, un_param_op_node_gen> unary_param_operator_map; //!< map that converts a string into an operator node generator function for all unary operators
        extern std::map<std::string, bin_param_op_node_gen> binary_param_operator_map; //!< map that converts a string into an operator node generator function for all binary operators
// clang-format on

/**
         * @brief initialize the maps for the parameter maps
         */
void set_param_node_maps();
#endif
}  // namespace allowed_op_maps

#endif
