// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/operator_nodes/OperatorNode.hpp
 *  @brief Defines and Implements the base class that represent all of the analytical operators in the Binary Expression Tree
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 *
 *  This class represents the non-leaf verticies in the binary expression tree. This also stores all edges of the graph through _feats
 */

#ifndef OPERATOR_NODE
#define OPERATOR_NODE


#include <iomanip>

#include "feature_creation/node/Node.hpp"
#include "feature_creation/node/operator_nodes/functions.hpp"

#ifdef PARAMETERIZE
#include "nl_opt/utils.hpp"
#endif

#ifdef PY_BINDINGS
namespace py = pybind11;
#endif

// DocString: cls_op_node
/**
 * @brief Base class to describe the operations in the binary expression tree (all non-leaf nodes) (inherits from Node)
 *
 * @tparam N Number of features the operator acts on (number of edges the vertex has)
 */
template <int N>
class OperatorNode : public Node
{
    friend class boost::serialization::access;

    /**
     * @brief Serialization function to send over MPI
     *
     * @param ar Archive representation of node
     */
    template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ar& boost::serialization::base_object<Node>(*this);
        ar& _feats;
    }

    using Node::expr;

protected:
    // clang-format off
    std::array<node_ptr, N> _feats; //!< The features for the operator nodes to act on (This Node's children)
    // clang-format on

public:
    /**
     * @brief Base Constructor
     * @details This is only used for serialization
     */
    OperatorNode() {}

    /**
     * @brief Constructor
     * @details Constructs an operator node with a set of features
     *
     * @param feats Array of features that the operator will act on
     * @param feat_ind The index of the feature
     */
    OperatorNode(const std::array<node_ptr, N> feats, const unsigned long int feat_ind)
        : Node(feat_ind, feats[0]->n_samp(), feats[0]->n_samp_test()), _feats(feats)
    {
    }

    /**
     * @brief Destructor
     */
    virtual ~OperatorNode() = default;

    /**
     * @brief Makes a hard copy node (All members of the Node are independent of the original one)
     * @return A shared_ptr to the copied node
     */
    virtual node_ptr hard_copy() const override = 0;

    /**
     * @brief Relinks all features of an OperatorNodes's _feat member to Nodes inside of phi
     *
     * @param phi A vector containing all Nodes that this Node can relink to
     */
    void reset_feats(std::vector<node_ptr>& phi) override
    {
        for (int ff = 0; ff < N; ++ff)
        {
            _feats[ff] = phi[_feats[ff]->arr_ind()];
        }
    }

    /**
     * @brief A vector storing the expressions for all primary features in the order they appear in the postfix expression
     */
    std::vector<std::string> x_in_expr_list() const override
    {
        std::vector<std::string> x_in_expr;
        for (auto& feat : _feats)
        {
            std::vector<std::string> feat_in_expr_list = feat->x_in_expr_list();
            x_in_expr.insert(x_in_expr.end(), feat_in_expr_list.begin(), feat_in_expr_list.end());
        }
        return x_in_expr;
    }

    /**
     * @brief The number of primary features (non-unique) this feature contains (The number of leaves of the Binary Expression Tree)
     *
     * @param cur_n_leaves The current count of the number of leaves
     */
    inline void update_n_leaves(int& cur_n_leaves) const override
    {
        std::for_each(_feats.begin(), _feats.end(), [&cur_n_leaves](node_ptr feat) {
            feat->update_n_leaves(cur_n_leaves);
        });
    }

    /**
     * @brief Get the score used to sort the features in the feature space (Based on the type of node and _feat_ind)
     *
     * @param max_ind The maximum index of the input nodes
     * @return The score used to sort the feature space
     */
    unsigned long long sort_score(unsigned int max_ind) const override
    {
        int ii = 2;
        return std::accumulate(_feats.begin(),
                               _feats.end(),
                               static_cast<unsigned long long>(type()),
                               [&ii, max_ind](unsigned long long tot, node_ptr feat) {
                                   --ii;
                                   return (static_cast<unsigned long long>(feat->feat_ind()) *
                                               static_cast<unsigned long long>(NODE_TYPE::MAX) *
                                               std::pow(max_ind, ii) +
                                           tot);
                               });
    }

    /**
     * @brief Get the valid LaTeX expression that represents the feature
     */
    virtual std::string get_latex_expr() const override = 0;

    /**
     * @brief A vector containing the values of the training set samples for the feature
     */
    std::vector<double> value() const override
    {
        std::vector<double> val(_n_samp, 0.0);
        std::copy_n(value_ptr(), _n_samp, val.data());
        return val;
    }

    /**
     * @brief A vector containing the values of the test set samples for the feature
     */
    std::vector<double> test_value() const override
    {
        std::vector<double> val(_n_samp_test, 0.0);
        std::copy_n(test_value_ptr(), _n_samp_test, val.data());
        return val;
    }

    // DocString: op_node_set_value
    /**
     * @brief Set the value of all training samples for the feature inside the central data storage array
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp(bool) If true then the evaluation is used for comparing features
     */
    virtual void set_value(int offset = -1, const bool for_comp = false) const override = 0;

    // DocString: op_node_set_test_value
    /**
     * @brief Set the value of all test samples for the feature inside the central data storage array
     *
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp(bool) If true then the evaluation is used for comparing features
     */
    virtual void set_test_value(int offset = -1, const bool for_comp = false) const override = 0;

    /**
     * @brief Get the pointer to the feature's training data
     * @details If the feature is not already stored in memory, then calculate the feature and return the pointer to the data
     *
     * @param offset (int) the integer value to offset the location in the temporary storage array
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     *
     * @return pointer to the feature's training value
     */
    virtual double* value_ptr(int offset = -1, const bool for_comp = false) const override
    {
        bool is_root = (offset == -1);
        if (_selected && is_root)
        {
            return node_value_arrs::get_d_matrix_ptr(_d_mat_ind);
        }
        if (((rung() > node_value_arrs::N_RUNGS_STORED) ||
             (_arr_ind > node_value_arrs::N_STORE_FEATURES)) &&
            (node_value_arrs::temp_storage_reg(_arr_ind, rung(), offset + is_root, for_comp) !=
             _feat_ind))
        {
            set_value(offset, for_comp);
        }
        offset += is_root;

        return node_value_arrs::get_value_ptr(_arr_ind, _feat_ind, rung(), offset, for_comp);
    }

    /**
     * @brief Get the pointer to the feature's test data
     * @details If the feature is not already stored in memory, then calculate the feature and return the pointer to the data
     *
     * @param offset (int) the integer value to offset the location in the temporary storage array
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     *
     * @return pointer to the feature's test values
     */

    virtual double* test_value_ptr(int offset = -1, const bool for_comp = false) const override
    {
        bool is_root = (offset == -1);
        if ((rung() > 0))
        {
            set_test_value(offset, for_comp);
        }
        offset += is_root;

        return node_value_arrs::get_test_value_ptr(_arr_ind, _feat_ind, rung(), offset, for_comp);
    }

    // DocString: op_node_is_nan
    /**
     * @brief Check if the feature has a NaN value in it
     *
     * @return True if one of the values of the feature is a NaN
     */
    inline bool is_nan() const override
    {
        double* val_ptr = value_ptr();
        return std::any_of(val_ptr, val_ptr + _n_samp, [](double d) { return !std::isfinite(d); });
    }

    // DocString: op_node_is_const
    /**
     * @brief Check if feature is constant for one of the tasks
     *
     * @return True if the feature is constant for all samples in any task
     */
    bool is_const() const override
    {
        double* val_ptr = value_ptr();

        bool is_c = false;
        int pos = 0;
        double norm = 0.0;
        for (auto& sz : node_value_arrs::TASK_SZ_TRAIN)
        {
            norm = util_funcs::norm(val_ptr + pos, sz);
            is_c = is_c || (norm < 1e-10) ||
                   (util_funcs::stand_dev(val_ptr + pos, sz) / util_funcs::norm(val_ptr + pos, sz) <
                    1.0e-10);
            pos += sz;
        }
        return is_c;
    }

    /**
     * @brief Update the rung of the feature (does nothing as the feature nodes are rung 0)
     *
     * @param cur_rung The current rung of the feature
     */
    void rung_update(int& cur_rung) const override
    {
        int update = 0;
        for (auto& feat : _feats)
        {
            int temp_rung = 0;
            feat->rung_update(temp_rung);
            update = std::max(temp_rung, update);
        }
        cur_rung += update + 1;
    }

    /**
     * @brief Returns the type of node this is
     */
    virtual NODE_TYPE type() const override = 0;

    /**
     * @brief Update the primary feature decomposition of a feature
     *
     * @param pf_decomp The primary feature decomposition of the feature calling this function.
     */
    void update_primary_feature_decomp(std::map<std::string, int>& pf_decomp) const override
    {
        for (auto& feat : _feats)
        {
            feat->update_primary_feature_decomp(pf_decomp);
        }
    }

    /**
     * @brief Converts a feature into a postfix expression (reverse polish notation)
     *
     * @details Recursively creates a postfix representation of the string
     *
     * @param cur_expr The current expression
     * @param add_params Add parameters to the expression
     * @return The current postfix expression of the feature
     */
    virtual void update_postfix(std::string& cur_expr, const bool add_params = true) const override
    {
        std::stringstream postfix;
        postfix << get_postfix_term();
        cur_expr = postfix.str() + "|" + cur_expr;
        for (int nn = N - 1; nn >= 0; --nn)
        {
            _feats[nn]->update_postfix(cur_expr);
        }
    }

    /**
     * @brief Get the term used in the postfix expression for this Node
     */
    virtual std::string get_postfix_term() const override = 0;

    // DocString: op_matlab_expr
    /**
     * @brief Get the string that corresponds to the code needed to evaluate the node in matlab
     *
     * @return The matlab code for the feature
     */
    virtual std::string matlab_fxn_expr() const override = 0;

    // DocString: op_node_n_feats
    /**
     * @brief The number of features used for an operator (Number of child node)
     */
    inline int n_feats() const override { return N; }

    // DocString: op_node_feat
    /**
     * @brief Return the ind^th feature stored by an operator node
     *
     * @param ind (int) the index of the feats list to be accessed
     * @return The feature stored in _feats[ind]
     */
    inline node_ptr feat(const int ind) const override
    {
        if (ind >= N)
        {
            throw std::logic_error("Index not found in _feats");
        }
        return _feats[ind];
    }

    /**
     * @brief update the dictionary used to check if an Add/Sub/AbsDiff node is valid
     *
     * @param add_sub_leaves the dictionary used to check if an Add/Sub node is valid
     * @param pl_mn 1 for addition and -1 for subtraction
     * @param expected_abs_tot The expected absolute sum of all values in add_sub_leaves
     */
    virtual void update_add_sub_leaves(std::map<std::string, int>& add_sub_leaves,
                                       const int pl_mn,
                                       int& expected_abs_tot) const override = 0;

    /**
     * @brief update the dictionary used to check if an Mult/Div node is valid
     *
     * @param div_mult_leaves the dictionary used to check if an Mult/Div node is valid
     * @param fact amount to increment the element (a primary features) of the dictionary by
     * @param expected_abs_tot The expected absolute sum of all values in div_mult_leaves
     */
    virtual void update_div_mult_leaves(std::map<std::string, double>& div_mult_leaves,
                                        const double fact,
                                        double& expected_abs_tot) const override = 0;

    /**
     * @brief Get the domain of a feature
     *
     * @return The domain of the feature
     */
    virtual Domain domain() const override = 0;

#ifdef PARAMETERIZE
    /**
     * @brief The parameters used for including individual scale and bias terms to each operator in the Node
     */
    virtual std::vector<double> parameters() const override = 0;

    // DocString: op_node_get_params
    /**
     * @brief Optimize the scale and bias terms for each operation in the Node.
     * @details Use optimizer to find the scale and bias terms that minimizes the associated loss function
     *
     * @param optimizer The optimizer used to evaluate the loss function for each optimization and find the optimal parameters
     */
    virtual void get_parameters(std::shared_ptr<NLOptimizer> optimizer) override = 0;

    /**
     * @brief Set the non-linear parameters
     * @param params The new parameters to use for the feature
     * @param check_sz If true make sure the number of parameters matches the expected value
     */
    virtual void set_parameters(const std::vector<double> params, const bool check_sz = true) override = 0;

    /**
     * @brief Set the non-linear parameters
     * @param params The new scale and bias terms of this node
     */
    virtual void set_parameters(const double* params) override = 0;

    // DocString: op_node_n_params_possible
    /**
     * @brief returns the number of theoretical parameters for this feature
     *
     * @param n_cur the current number of parameters
     * @param depth (int) How far down a given Node is from the root OperatorNode
     * @return the number of theoretical parameters
     */
    virtual inline int n_params_possible(const int n_cur = 0, const int depth = 1) const override
    {
        if (depth > nlopt_wrapper::MAX_PARAM_DEPTH)
        {
            return 0;
        }

        return std::accumulate(_feats.begin(), _feats.end(), 2, [&](double tot, node_ptr feat) {
            return tot + feat->n_params_possible(0, depth + 1);
        });
    }

    /**
     * @brief Set the value of all training samples for the feature inside the central data storage array
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     * @param depth (int) How far down a given Node is from the root OperatorNode
     */
    virtual void set_value(const double* params,
                           int offset = -1,
                           const bool for_comp = false,
                           const int depth = 1) const override = 0;

    /**
     * @brief The pointer to where the feature's training data is stored
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     * @param depth (int) How far down a given Node is from the root OperatorNode
     * @returns The pointer to the feature's data
     */
    double* value_ptr(const double* params,
                      int offset = -1,
                      const bool for_comp = false,
                      const int depth = 1) const override
    {
        if (_selected && (offset == -1))
        {
            return node_value_arrs::get_d_matrix_ptr(_d_mat_ind);
        }
        set_value(params, offset, for_comp, depth);

        offset += (offset == -1);
        return node_value_arrs::access_param_storage(rung(), offset, for_comp);
    }

    /**
     * @brief Set the value of all test samples for the feature inside the central data storage array
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     * @param depth (int) How far down a given Node is from the root OperatorNode
     */
    virtual void set_test_value(const double* params,
                                int offset = -1,
                                const bool for_comp = false,
                                const int depth = 1) const override = 0;

    /**
     * @brief The pointer to where the feature's test data is stored
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param offset (int) Where the current node is in the binary expression tree relative to other nodes at the same depth
     * @param for_comp (bool) If true then the evaluation is used for comparing features
     * @param depth (int) How far down a given Node is from the root OperatorNode
     * @returns The pointer to the feature's test data
     */
    double* test_value_ptr(const double* params,
                           int offset = -1,
                           const bool for_comp = false,
                           const int depth = 1) const override
    {
        set_test_value(params, offset, for_comp, depth);

        offset += (offset == -1);
        return node_value_arrs::access_param_storage_test(rung(), offset, for_comp);
    }

    /**
     * @brief A human readable equation representing the feature
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param depth How far down a given Node is from the root OperatorNode
     * @return A human readable equation representing the feature
     */
    virtual std::string expr(const double* params, const int depth = 1) const override = 0;

    /**
     * @brief Get the valid LaTeX expression that represents the feature
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param depth How far down a given Node is from the root OperatorNode
     * @return Get the valid LaTeX expression that represents the feature
     */
    virtual std::string get_latex_expr(const double* params, const int depth = 1) const override = 0;

    /**
     * @brief Get the string that corresponds to the code needed to evaluate the node in matlab
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param depth How far down a given Node is from the root OperatorNode
     * @return The matlab code for the feature
     */
    virtual std::string matlab_fxn_expr(const double* params, const int depth = 1) const override = 0;

    /**
     * @brief Set the upper and lower bounds for the scale and bias term of this Node and its children
     *
     * @param lb A pointer to the location where the lower bounds for the scale and bias term of this Node is set
     * @param ub A pointer to the location where the upper bounds for the scale and bias term of this Node is set
     * @param depth How far down a given Node is from the root OperatorNode
     */
    virtual void set_bounds(double* lb, double* ub, const int depth = 1) const override = 0;

    /**
     * @brief Initialize the scale and bias terms for this Node and its children
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param depth How far down a given Node is from the root OperatorNode
     */
    virtual void initialize_params(double* params, const int depth = 1) const override = 0;

    /**
     * @brief Calculates the derivative of an operation with respect to the parameters for a given sample
     *
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param dfdp pointer to where the feature derivative pointers are located
     */
    virtual void param_derivative(const double* params,
                                  double* dfdp,
                                  const int depth = 1) const override = 0;

    /**
     * @brief Get the parameter gradient for non-linear optimization
     *
     * @param grad pointer to the gradient storage
     * @param dfdp pointer to where the feature derivative pointers are located
     */
    void gradient(double* grad, double* dfdp) const override
    {
        if (n_params() == 0)
        {
            throw std::logic_error("Asking for the gradient of non-parameterized feature");
        }

        gradient(grad, dfdp, param_pointer());
    }

    /**
     * @brief Get the parameter gradient for non-linear optimization
     *
     * @param grad pointer to the gradient storage
     * @param dfdp pointer to where the feature derivative are located
     * @param params A pointer to the bias and scale terms for this Node and its children
     * @param depth The current depth in the binary expression tree
     */
    void gradient(double* grad, double* dfdp, const double* params, const int depth = 1) const override
    {
        int np = n_params_possible();
        // Calculate f' and x
        param_derivative(params, dfdp, depth);
        double* val_ptr;

        if (depth < nlopt_wrapper::MAX_PARAM_DEPTH)
        {
            val_ptr = _feats[N - 1]->value_ptr(params + 2, -1, true, depth + 1);
        }
        else
        {
            val_ptr = _feats[N - 1]->value_ptr(-1, true);
        }

        // df / d\alpha = x f'
        std::transform(dfdp, dfdp + _n_samp, grad, grad, std::multiplies<double>());
        std::transform(val_ptr, val_ptr + _n_samp, grad, grad, std::multiplies<double>());

        // df / da = f'
        std::transform(
            dfdp, dfdp + _n_samp, grad + _n_samp, grad + _n_samp, std::multiplies<double>());

        // Set up for the chain rule
        // df(g(x)) / dp_g = \alpha f' dg/dp_g
        for (int pp = 2; pp < np; ++pp)
        {
            std::transform(dfdp,
                           dfdp + _n_samp,
                           grad + pp * _n_samp,
                           grad + pp * _n_samp,
                           [params](double d, double g) { return params[0] * d * g; });
        }

        // Go down the chain rule
        int start = 2;
        if (depth < nlopt_wrapper::MAX_PARAM_DEPTH)
        {
            for (int ff = N - 1; ff >= 0; --ff)
            {
                _feats[ff]->gradient(grad + start * _n_samp, dfdp, params + start, depth + 1);
                start += _feats[ff]->n_params_possible();
            }
        }
    }

    /**
     * @brief Get the domain of a feature
     *
     * @param params Pointer to the scale and shift parameters of the feature
     * @return The domain of the feature
     */
    virtual Domain domain(const double* params, const int depth = 1) const override = 0;

#ifdef PY_BINDINGS

    // DocString: op_node_set_param_list
    /**
     * @brief Set the non-linear parameters
     * @param params The new parameters to use for the feature
     */
    inline void set_parameters_py(const std::vector<double> params)
    {
        set_parameters(params, false);
    }

    // DocString: op_node_set_param_arr
    /**
     * @brief Set the non-linear parameters
     *
     * @param params (np.ndarray) The new parameters of the node
     */
    inline void set_parameters(py::array_t<double> params)
    {
        set_parameters(python_conv_utils::from_array<double>(params), false);
    }
#endif
#endif
};

// GCOV_EXCL_START     GCOVR_EXCL_START       LCOV_EXCL_START
#ifdef PY_BINDINGS
template <int N, class OperatorNodeBase = OperatorNode<N>>
class PyOperatorNode : public PyNode<OperatorNodeBase>
{
    using PyNode<OperatorNodeBase>::PyNode;

    node_ptr hard_copy() const  override { PYBIND11_OVERRIDE_PURE(node_ptr, OperatorNodeBase, hard_copy, ); }

    std::string get_latex_expr() const override
    {
        PYBIND11_OVERRIDE_PURE(std::string, OperatorNodeBase, get_latex_expr, );
    }

    void set_value(int offset = -1, const bool for_comp = false) const override
    {
        PYBIND11_OVERRIDE_PURE(void, OperatorNodeBase, set_value, offset, for_comp);
    }

    void set_test_value(int offset = -1, const bool for_comp = false) const override
    {
        PYBIND11_OVERRIDE_PURE(void, OperatorNodeBase, set_test_value, offset, for_comp);
    }

    double* value_ptr(int offset = -1, const bool for_comp = false) const override
    {
        PYBIND11_OVERRIDE(double*, OperatorNodeBase, value_ptr, offset, for_comp);
    }
    double* test_value_ptr(int offset = -1, const bool for_comp = false) const override
    {
        PYBIND11_OVERRIDE(double*, OperatorNodeBase, test_value_ptr, offset, for_comp);
    }

    NODE_TYPE type() const override  { PYBIND11_OVERRIDE_PURE(NODE_TYPE, OperatorNodeBase, type, ); }

    void update_postfix(std::string& cur_expr, const bool add_params = true) const override
    {
        PYBIND11_OVERRIDE(void, OperatorNodeBase, update_postfix, cur_expr, add_params);
    }

    std::string get_postfix_term() const override
    {
        PYBIND11_OVERRIDE_PURE(std::string, OperatorNodeBase, get_postfix_term, );
    }

    std::string matlab_fxn_expr() const override
    {
        PYBIND11_OVERRIDE_PURE(std::string, OperatorNodeBase, matlab_fxn_expr, );
    }

    void update_add_sub_leaves(std::map<std::string, int>& add_sub_leaves,
                               const int pl_mn,
                               int& expected_abs_tot) const override
    {
        PYBIND11_OVERRIDE_PURE(
            void, OperatorNodeBase, update_add_sub_leaves, add_sub_leaves, pl_mn, expected_abs_tot);
    }

    void update_div_mult_leaves(std::map<std::string, double>& div_mult_leaves,
                                const double fact,
                                double& expected_abs_tot) const override
    {
        PYBIND11_OVERRIDE_PURE(void,
                               OperatorNodeBase,
                               update_div_mult_leaves,
                               div_mult_leaves,
                               fact,
                               expected_abs_tot);
    }

    void reset_feats(std::vector<node_ptr>& phi) override
    {
        PYBIND11_OVERRIDE(void, OperatorNodeBase, reset_feats, phi);
    }

    std::vector<std::string> x_in_expr_list() const override
    {
        PYBIND11_OVERRIDE(std::vector<std::string>, OperatorNodeBase, x_in_expr_list, );
    }

    void update_n_leaves(int& cur_n_leaves) const override
    {
        PYBIND11_OVERRIDE(void, OperatorNodeBase, update_n_leaves, cur_n_leaves);
    }

    unsigned long long sort_score(unsigned int max_ind) const override
    {
        PYBIND11_OVERRIDE(unsigned long long, OperatorNodeBase, sort_score, max_ind);
    }

    std::string expr() const override
    {
        PYBIND11_OVERRIDE_PURE(std::string, OperatorNodeBase, expr, );
    }

    Domain domain() const override { PYBIND11_OVERRIDE_PURE(Domain, OperatorNodeBase, domain, ); }

    Unit unit() const override { PYBIND11_OVERRIDE_PURE(Unit, OperatorNodeBase, unit, ); }

    std::vector<double> value() const override
    {
        PYBIND11_OVERRIDE(std::vector<double>, OperatorNodeBase, value, );
    }

    std::vector<double> test_value() const override
    {
        PYBIND11_OVERRIDE(std::vector<double>, OperatorNodeBase, test_value, );
    }

    bool is_nan() const override { PYBIND11_OVERRIDE(bool, OperatorNodeBase, is_nan, ); }

    bool is_const() const override { PYBIND11_OVERRIDE(bool, OperatorNodeBase, is_const, ); }

    void update_primary_feature_decomp(std::map<std::string, int>& pf_decomp) const override
    {
        PYBIND11_OVERRIDE(void, OperatorNodeBase, update_primary_feature_decomp, pf_decomp);
    }

    int n_feats() const override { PYBIND11_OVERRIDE(int, OperatorNodeBase, n_feats, ); }

    node_ptr feat(const int ind) const override
    {
        PYBIND11_OVERRIDE(node_ptr, OperatorNodeBase, feat, ind);
    }

#ifdef PARAMETERIZE

    const double* param_pointer() const override
    {
        PYBIND11_OVERRIDE(const double*, OperatorNodeBase, param_pointer, );
    }

    int n_params() const override { PYBIND11_OVERRIDE(int, OperatorNodeBase, n_params, ); }

    double* value_ptr(const double* params,
                      int offset = -1,
                      const bool for_comp = false,
                      const int depth = 1) const override
    {
        PYBIND11_OVERRIDE(double*, OperatorNodeBase, value_ptr, params, offset, for_comp, depth);
    }

    double* test_value_ptr(const double* params,
                           int offset = -1,
                           const bool for_comp = false,
                           const int depth = 1) const override
    {
        PYBIND11_OVERRIDE(
            double*, OperatorNodeBase, test_value_ptr, params, offset, for_comp, depth);
    }

    void param_derivative(const double* params, double* dfdp, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(void, OperatorNodeBase, param_derivative, params, dfdp, depth);
    }

    void gradient(double* grad, double* dfdp) const override
    {
        PYBIND11_OVERRIDE(void, OperatorNodeBase, gradient, grad, dfdp);
    }

    void gradient(double* grad,
                  double* dfdp,
                  const double* params,
                  const int depth = 1) const override
    {
        PYBIND11_OVERRIDE(void, OperatorNodeBase, gradient, grad, dfdp, params, depth);
    }

    std::vector<double> parameters() const override
    {
        PYBIND11_OVERRIDE_PURE(std::vector<double>, OperatorNodeBase, parameters, );
    }

    void get_parameters(std::shared_ptr<NLOptimizer> optimizer) override
    {
        PYBIND11_OVERRIDE_PURE(void, OperatorNodeBase, get_parameters, optimizer);
    }

    void set_parameters(const std::vector<double> params, const bool check_sz = true) override
    {
        PYBIND11_OVERRIDE_PURE(void, OperatorNodeBase, set_parameters, params, check_sz);
    }

    void set_parameters(const double* params) override
    {
        PYBIND11_OVERRIDE_PURE(void, OperatorNodeBase, set_parameters, params);
    }

    int n_params_possible(const int n_cur = 0, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE(int, OperatorNodeBase, n_params_possible, n_cur, depth);
    }

    void set_value(const double* params,
                   int offset = -1,
                   const bool for_comp = false,
                   const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(void, OperatorNodeBase, set_value, params, offset, for_comp, depth);
    }

    void set_test_value(const double* params,
                        int offset = -1,
                        const bool for_comp = false,
                        const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(
            void, OperatorNodeBase, set_test_value, params, offset, for_comp, depth);
    }

    Domain domain(const double* params, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(Domain, OperatorNodeBase, domain, params, depth);
    }

    std::string expr(const double* params, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(std::string, OperatorNodeBase, expr, params, depth);
    }

    std::string get_latex_expr(const double* params, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(std::string, OperatorNodeBase, get_latex_expr, params, depth);
    }

    std::string matlab_fxn_expr(const double* params, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(std::string, OperatorNodeBase, matlab_fxn_expr, params, depth);
    }

    void set_bounds(double* lb, double* ub, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(void, OperatorNodeBase, set_bounds, lb, ub, depth);
    }

    void initialize_params(double* params, const int depth = 1) const override
    {
        PYBIND11_OVERRIDE_PURE(void, OperatorNodeBase, initialize_params, params, depth);
    }
#endif
};
#endif
// GCOV_EXCL_STOP     GCOVR_EXCL_STOP       LCOV_EXCL_STOP

#endif
