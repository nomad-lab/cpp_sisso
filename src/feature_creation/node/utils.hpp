// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/node/utils.hpp
 *  @brief Defines utility functions for handling Features (Nodes)
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef NODE_UTILS
#define NODE_UTILS

#include <fstream>

#include "feature_creation/node/ModelNode.hpp"
#include "feature_creation/node/operator_nodes/allowed_ops.hpp"

namespace str2node
{
/**
 * @brief Convert a postfix expression of a feature into a node_ptr
 * @details Creates a stack to iteratively generate the feature represented by the expression
 *
 * @param postfix_expr The postfix expression of the feature node
 * @param phi_0 The primary feature space
 * @param feat_ind The index of the new feature
 * @param excluded_inds The list of primary feature indexes to not include in any features
 *
 * @return The feature node described by the postfix expression
 */
node_ptr postfix2node(const std::string postfix_expr,
                      const std::vector<node_ptr>& phi_0,
                      unsigned long int& feat_ind,
                      const std::vector<int>& excluded_inds);

// DocString: node_utils_phi_sel_from_file
/**
 * @brief Convert a feature_space/selected_features.txt into a phi_selected;
 * @details Read in the file to get the postfix expressions and regenerate the selected features using phi_0
 *
 * @param filename The name of the feature_space/selected_features.txt file
 * @param phi_0 The initial feature space
 * @param excluded_inds The list of primary feature indexes to not include in any features
 *
 * @return The selected feature set from the file
 */
std::vector<node_ptr> phi_selected_from_file(const std::string filename,
                                             const std::vector<node_ptr> phi_0,
                                             const std::vector<int>& excluded_inds);

/**
 * @brief Convert a text file containing postfix expressions of features into the feature space
 * @details Read in the file to get the postfix expressions and regenerate the features using phi_0
 *
 * @param filename The name of the file storing all the features
 * @param phi_0 The initial feature space
 * @param excluded_inds The list of primary feature indexes to not include in any features
 *
 * @return The feature set defined from the file
 */
std::vector<node_ptr> phi_from_file(const std::string filename,
                                    const std::vector<node_ptr> phi_0,
                                    const std::vector<int>& excluded_inds);

#ifdef PARAMETERIZE
/**
 * @brief set the parameters of a node from the postfix expression
 *
 * @param feat Features whose parameters need to be set
 * @param op_terms term used to define the object
 */
void set_parameters(node_ptr feat, const std::vector<std::string> op_terms);
#endif
}  // namespace str2node
namespace node_identifier
{
/**
 * @brief Convert a node type into the corresponding string identifier
 *
 * @param nt node type
 * @return string representation of the node type
 */
std::string feature_type_to_string(const NODE_TYPE nt);
}  // namespace node_identifier
#endif
