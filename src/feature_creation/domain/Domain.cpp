// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/domain/Domain.cpp
 *  @brief The implementation of the Domain class for a given feature
 *
 *  @author Thomas A. R. Purcell (tpurcell)
 *  @bug No known bugs.
 */

#include "feature_creation/domain/Domain.hpp"

Domain::Domain() : _is_empty(true) {}

Domain::Domain(std::array<double, 2> end_points) : _end_points(end_points), _is_empty(false)
{
    if (std::abs(end_points[0] - end_points[1]) < 1e-10)
    {
        _is_empty = true;
    }
}

Domain::Domain(std::array<double, 2> end_points, std::vector<double> excluded_pts)
    : _excluded_pts(vector_utils::unique<double>(excluded_pts)),
      _end_points(end_points),
      _is_empty(false)
{
    if ((std::abs(end_points[0] - end_points[1]) < 1e-10) &&
        std::all_of(excluded_pts.begin(), excluded_pts.end(), [end_points](double pt) {
            return std::abs(pt - end_points[0]) < 1e-10;
        }))
    {
        _is_empty = true;
    }
}

Domain::Domain(std::string domain_str)
{
    boost::ireplace_all(domain_str, " ", "");

    if (domain_str.size() == 0)
    {
        _is_empty = true;
    }
    else
    {
        _is_empty = false;

        std::vector<std::string> real_abs_end_points;
        std::vector<std::string> split_end_points;

        boost::algorithm::split(real_abs_end_points, domain_str, boost::algorithm::is_any_of("|"));
        boost::algorithm::split(
            split_end_points, real_abs_end_points[0], boost::algorithm::is_any_of("[,;]()"));

        if (split_end_points.size() < 4)
        {
            throw std::logic_error(
                "The domain string is not long enough, possibly from using a comma separator in a "
                "CSV file, use semicolon.");
        }

        if (split_end_points[1].substr(0, 4).compare("-inf") == 0)
        {
            _end_points[0] = -1.0 * std::numeric_limits<double>::infinity();
        }
        else
        {
            _end_points[0] = std::stod(split_end_points[1]);
        }

        if (real_abs_end_points[0].find('(') != std::string::npos)
        {
            _excluded_pts.push_back(_end_points[0]);
        }

        if (split_end_points[2].substr(0, 3).compare("inf") == 0)
        {
            _end_points[1] = std::numeric_limits<double>::infinity();
        }
        else
        {
            _end_points[1] = std::stod(split_end_points[2]);
        }

        if (real_abs_end_points[0].find(')') != std::string::npos)
        {
            _excluded_pts.push_back(_end_points[1]);
        }

        if (real_abs_end_points.size() > 1)
        {
            boost::algorithm::split(
                split_end_points, real_abs_end_points[1], boost::algorithm::is_any_of(",;[]"));
            for (size_t ii = 1; ii < split_end_points.size() - 1; ++ii)
            {
                _excluded_pts.push_back(std::stod(split_end_points[ii]));
            }
        }
        _excluded_pts = vector_utils::unique<double>(_excluded_pts);

        if ((std::abs(_end_points[0] - _end_points[1]) < 1e-10) &&
            std::all_of(_excluded_pts.begin(), _excluded_pts.end(), [this](double pt) {
                return std::abs(pt - _end_points[0]) < 1e-10;
            }))
        {
            _is_empty = true;
        }
    }
}

std::string Domain::toString() const
{
    if (_is_empty)
    {
        return "";
    }

    std::stringstream domain_str;
    if (_end_points[0] == -std::numeric_limits<double>::infinity() ||
        std::any_of(_excluded_pts.begin(), _excluded_pts.end(), [this](double pt) {
            return std::abs(pt - _end_points[0]) < 1e-10;
        }))
    {
        domain_str << "(";
    }
    else
    {
        domain_str << "[";
    }

    domain_str << std::setprecision(15) << std::scientific << _end_points[0] << ", "
               << _end_points[1];
    if (_end_points[1] == std::numeric_limits<double>::infinity() ||
        std::any_of(_excluded_pts.begin(), _excluded_pts.end(), [this](double pt) {
            return std::abs(pt - _end_points[1]) < 1e-10;
        }))
    {
        domain_str << ")";
    }
    else
    {
        domain_str << "]";
    }

    if (_excluded_pts.size() > 0)
    {
        domain_str << " | [";
        for (size_t pp = 0; pp < _excluded_pts.size() - 1; ++pp)
        {
            if ((_end_points[0] == _excluded_pts[pp]) || (_end_points[1] == _excluded_pts[pp]) ||
                (std::abs(_excluded_pts[pp]) == std::numeric_limits<double>::infinity()))
            {
                continue;
            }

            domain_str << std::setprecision(15) << std::scientific << _excluded_pts[pp] << ", ";
        }
        domain_str << std::setprecision(15) << std::scientific << _excluded_pts.back() << "]";
    }
    return domain_str.str();
}

Domain Domain::operator+(Domain domain_2) const
{
    if (_is_empty || domain_2.is_empty())
    {
        return Domain();
    }

    std::array<double, 2> end_points;
    std::transform(_end_points.begin(),
                   _end_points.end(),
                   domain_2.end_points().begin(),
                   end_points.begin(),
                   std::plus<double>());

    std::vector<double> excluded_pts;
    if (std::any_of(_excluded_pts.begin(),
                    _excluded_pts.end(),
                    [this](double pt) { return std::abs(pt - _end_points[1]) < 1e-10; }) ||
        std::any_of(
            domain_2.excluded_pts().begin(), domain_2.excluded_pts().end(), [domain_2](double pt) {
                return std::abs(pt - domain_2.end_points()[1]) < 1e-10;
            }))
    {
        excluded_pts.push_back(end_points[1]);
    }

    if (std::any_of(_excluded_pts.begin(),
                    _excluded_pts.end(),
                    [this](double pt) { return std::abs(pt - _end_points[0]) < 1e-10; }) ||
        std::any_of(
            domain_2.excluded_pts().begin(), domain_2.excluded_pts().end(), [domain_2](double pt) {
                return std::abs(pt - domain_2.end_points()[0]) < 1e-10;
            }))
    {
        excluded_pts.push_back(end_points[0]);
    }

    return Domain(end_points, excluded_pts);
}

Domain Domain::operator*(Domain domain_2) const
{
    if (_is_empty || domain_2.is_empty())
    {
        return Domain();
    }

    std::array<double, 4> possible_eps = {_end_points[0] * domain_2.end_points()[0],
                                          _end_points[1] * domain_2.end_points()[0],
                                          _end_points[0] * domain_2.end_points()[1],
                                          _end_points[1] * domain_2.end_points()[1]};

    // If there is a 0 * inf set it to 0 (only a problem if the Domain is [0, 0])
    std::transform(possible_eps.begin(), possible_eps.end(), possible_eps.begin(), [](double eps) {
        return eps * (!std::isnan(eps));
    });

    int min_ind = std::min_element(possible_eps.begin(), possible_eps.end()) - possible_eps.begin();
    int max_ind = std::max_element(possible_eps.begin(), possible_eps.end()) - possible_eps.begin();
    std::array<double, 2> end_points = {possible_eps[min_ind], possible_eps[max_ind]};

    std::vector<double> excluded_pts;
    if ((min_ind == 0) && ((std::find(_excluded_pts.begin(), _excluded_pts.end(), _end_points[0]) !=
                            _excluded_pts.end()) ||
                           (std::find(domain_2.excluded_pts().begin(),
                                      domain_2.excluded_pts().end(),
                                      domain_2.end_points()[0]) != domain_2.excluded_pts().end())))
    {
        excluded_pts.push_back(end_points[0]);
    }
    else if ((min_ind == 1) &&
             ((std::find(_excluded_pts.begin(), _excluded_pts.end(), _end_points[1]) !=
               _excluded_pts.end()) ||
              (std::find(domain_2.excluded_pts().begin(),
                         domain_2.excluded_pts().end(),
                         domain_2.end_points()[0]) != domain_2.excluded_pts().end())))
    {
        excluded_pts.push_back(end_points[0]);
    }
    else if ((min_ind == 2) &&
             ((std::find(_excluded_pts.begin(), _excluded_pts.end(), _end_points[0]) !=
               _excluded_pts.end()) ||
              (std::find(domain_2.excluded_pts().begin(),
                         domain_2.excluded_pts().end(),
                         domain_2.end_points()[1]) != domain_2.excluded_pts().end())))
    {
        excluded_pts.push_back(end_points[0]);
    }
    else if ((min_ind == 3) &&
             ((std::find(_excluded_pts.begin(), _excluded_pts.end(), _end_points[1]) !=
               _excluded_pts.end()) ||
              (std::find(domain_2.excluded_pts().begin(),
                         domain_2.excluded_pts().end(),
                         domain_2.end_points()[1]) != domain_2.excluded_pts().end())))
    {
        excluded_pts.push_back(end_points[0]);
    }

    if ((max_ind == 0) && ((std::find(_excluded_pts.begin(), _excluded_pts.end(), _end_points[0]) !=
                            _excluded_pts.end()) ||
                           (std::find(domain_2.excluded_pts().begin(),
                                      domain_2.excluded_pts().end(),
                                      domain_2.end_points()[0]) != domain_2.excluded_pts().end())))
    {
        excluded_pts.push_back(end_points[1]);
    }
    else if ((max_ind == 1) &&
             ((std::find(_excluded_pts.begin(), _excluded_pts.end(), _end_points[1]) !=
               _excluded_pts.end()) ||
              (std::find(domain_2.excluded_pts().begin(),
                         domain_2.excluded_pts().end(),
                         domain_2.end_points()[0]) != domain_2.excluded_pts().end())))
    {
        excluded_pts.push_back(end_points[1]);
    }
    else if ((max_ind == 2) &&
             ((std::find(_excluded_pts.begin(), _excluded_pts.end(), _end_points[0]) !=
               _excluded_pts.end()) ||
              (std::find(domain_2.excluded_pts().begin(),
                         domain_2.excluded_pts().end(),
                         domain_2.end_points()[1]) != domain_2.excluded_pts().end())))
    {
        excluded_pts.push_back(end_points[1]);
    }
    else if ((max_ind == 3) &&
             ((std::find(_excluded_pts.begin(), _excluded_pts.end(), _end_points[1]) !=
               _excluded_pts.end()) ||
              (std::find(domain_2.excluded_pts().begin(),
                         domain_2.excluded_pts().end(),
                         domain_2.end_points()[1]) != domain_2.excluded_pts().end())))
    {
        excluded_pts.push_back(end_points[1]);
    }

    if ((std::find(_excluded_pts.begin(), _excluded_pts.end(), 0.0) != _excluded_pts.end()) ||
        (std::find(domain_2.excluded_pts().begin(), domain_2.excluded_pts().end(), 0.0) !=
         domain_2.excluded_pts().end()))
    {
        excluded_pts.push_back(0.0);
    }

    return Domain(end_points, excluded_pts);
}

Domain Domain::operator+(double a) const
{
    if (_is_empty)
    {
        return Domain();
    }

    std::array<double, 2> end_points(_end_points);
    std::vector<double> excluded_pts(_excluded_pts);

    std::transform(end_points.begin(), end_points.end(), end_points.begin(), [a](double pt) {
        return pt + a;
    });
    std::transform(excluded_pts.begin(), excluded_pts.end(), excluded_pts.begin(), [a](double pt) {
        return pt + a;
    });

    return Domain(end_points, excluded_pts);
}

Domain Domain::operator-(double a) const
{
    if (_is_empty)
    {
        return Domain();
    }

    std::array<double, 2> end_points(_end_points);
    std::vector<double> excluded_pts(_excluded_pts);

    std::transform(end_points.begin(), end_points.end(), end_points.begin(), [a](double pt) {
        return pt - a;
    });
    std::transform(excluded_pts.begin(), excluded_pts.end(), excluded_pts.begin(), [a](double pt) {
        return pt - a;
    });

    return Domain(end_points, excluded_pts);
}

Domain Domain::operator*(double a) const
{
    if (_is_empty)
    {
        return Domain();
    }

    std::array<double, 2> end_points(_end_points);
    std::vector<double> excluded_pts(_excluded_pts);

    if (a > 0.0)
    {
        std::transform(end_points.begin(), end_points.end(), end_points.begin(), [a](double pt) {
            return pt * a;
        });
    }
    else
    {
        std::transform(
            _end_points.rbegin(), _end_points.rend(), end_points.begin(), [a](double pt) {
                return pt * a;
            });
    }
    std::transform(excluded_pts.begin(), excluded_pts.end(), excluded_pts.begin(), [a](double pt) {
        return pt * a;
    });

    return Domain(end_points, excluded_pts);
}

Domain Domain::operator/(double a) const
{
    if (_is_empty)
    {
        return Domain();
    }

    std::array<double, 2> end_points(_end_points);
    std::vector<double> excluded_pts(_excluded_pts);

    if (a > 0.0)
    {
        std::transform(end_points.begin(), end_points.end(), end_points.begin(), [a](double pt) {
            return pt / a;
        });
    }
    else
    {
        std::transform(
            _end_points.rbegin(), _end_points.rend(), end_points.begin(), [a](double pt) {
                return pt / a;
            });
    }
    std::transform(excluded_pts.begin(), excluded_pts.end(), excluded_pts.begin(), [a](double pt) {
        return pt / a;
    });

    return Domain(end_points, excluded_pts);
}

Domain Domain::parameterize(double scale, double shift) const
{
    if (_is_empty)
    {
        return Domain();
    }

    std::array<double, 2> end_points(_end_points);
    std::vector<double> excluded_pts(_excluded_pts);

    if (scale > 0.0)
    {
        std::transform(
            end_points.begin(), end_points.end(), end_points.begin(), [scale, shift](double pt) {
                return scale * pt + shift;
            });
    }
    else
    {
        std::transform(_end_points.rbegin(),
                       _end_points.rend(),
                       end_points.begin(),
                       [scale, shift](double pt) { return scale * pt + shift; });
    }
    std::transform(
        excluded_pts.begin(), excluded_pts.end(), excluded_pts.begin(), [scale, shift](double pt) {
            return scale * pt + shift;
        });

    return Domain(end_points, excluded_pts);
}

Domain Domain::neg() const
{
    if (_is_empty)
    {
        return Domain();
    }

    std::array<double, 2> end_points(_end_points);
    std::vector<double> excluded_pts(_excluded_pts);

    std::transform(_end_points.rbegin(), _end_points.rend(), end_points.begin(), [](double pt) {
        return -pt;
    });
    std::transform(excluded_pts.begin(), excluded_pts.end(), excluded_pts.begin(), [](double pt) {
        return -pt;
    });

    return Domain(end_points, excluded_pts);
}

Domain Domain::exp() const
{
    if (_is_empty)
    {
        return Domain();
    }

    std::array<double, 2> end_points(_end_points);
    std::vector<double> excluded_pts(_excluded_pts);

    std::transform(end_points.begin(), end_points.end(), end_points.begin(), [](double pt) {
        return std::exp(pt);
    });
    std::transform(excluded_pts.begin(), excluded_pts.end(), excluded_pts.begin(), [](double pt) {
        return std::exp(pt);
    });

    return Domain(end_points, excluded_pts);
}

Domain Domain::log() const
{
    if (_is_empty)
    {
        return Domain();
    }

    std::array<double, 2> end_points(_end_points);
    std::vector<double> excluded_pts(_excluded_pts);

    std::transform(end_points.begin(), end_points.end(), end_points.begin(), [](double pt) {
        return std::log(pt);
    });
    std::transform(excluded_pts.begin(), excluded_pts.end(), excluded_pts.begin(), [](double pt) {
        return std::log(pt);
    });

    return Domain(end_points, excluded_pts);
}

Domain Domain::pow(double power) const
{
    if (_is_empty)
    {
        return Domain();
    }

    std::array<double, 2> end_points(_end_points);
    std::vector<double> excluded_pts(_excluded_pts);

    if (std::abs(fmod(power, 2.0)) < 1e-10)
    {
        Domain abs_dom = abs();
        excluded_pts = abs_dom.excluded_pts();
        end_points = abs_dom.end_points();
    }

    if (power < 0)
    {
        power = std::abs(power);
        Domain inv_dom = Domain(end_points, excluded_pts).inv();
        excluded_pts = inv_dom.excluded_pts();
        end_points = inv_dom.end_points();
    }

    std::transform(end_points.begin(), end_points.end(), end_points.begin(), [power](double pt) {
        return std::pow(pt, power);
    });
    std::transform(
        excluded_pts.begin(), excluded_pts.end(), excluded_pts.begin(), [power](double pt) {
            return std::pow(pt, power);
        });

    return Domain(end_points, excluded_pts);
}

Domain Domain::cbrt() const
{
    if (_is_empty)
    {
        return Domain();
    }

    std::array<double, 2> end_points(_end_points);
    std::vector<double> excluded_pts(_excluded_pts);

    std::transform(end_points.begin(), end_points.end(), end_points.begin(), [](double pt) {
        return std::cbrt(pt);
    });
    std::transform(excluded_pts.begin(), excluded_pts.end(), excluded_pts.begin(), [](double pt) {
        return std::cbrt(pt);
    });

    return Domain(end_points, excluded_pts);
}

Domain Domain::sqrt() const
{
    if (_is_empty)
    {
        return Domain();
    }

    std::array<double, 2> end_points(_end_points);
    std::vector<double> excluded_pts(_excluded_pts);

    std::transform(end_points.begin(), end_points.end(), end_points.begin(), [](double pt) {
        return std::sqrt(pt);
    });
    std::transform(excluded_pts.begin(), excluded_pts.end(), excluded_pts.begin(), [](double pt) {
        return std::sqrt(pt);
    });

    return Domain(end_points, excluded_pts);
}

Domain Domain::abs() const
{
    if (_is_empty)
    {
        return Domain();
    }

    if ((_end_points[0] != 0.0) && (_end_points[1] != 0.0) &&
        (util_funcs::sign(_end_points[0]) != util_funcs::sign(_end_points[1])))
    {
        std::array<double, 2> end_points = {
            0.0, std::max(std::abs(_end_points[0]), std::abs(_end_points[1]))};
        std::vector<double> excluded_pts;
        excluded_pts.reserve(_excluded_pts.size());
        std::transform(
            excluded_pts.begin(), excluded_pts.end(), excluded_pts.begin(), [](double pt) {
                return std::abs(pt);
            });

        double min_end_pt = std::min(std::abs(_end_points[0]), std::abs(_end_points[1]));
        for (size_t ep = 0; ep < _excluded_pts.size(); ++ep)
        {
            if ((_excluded_pts[ep] == 0.0) || (_excluded_pts[ep] > min_end_pt) ||
                (std::find_if(_excluded_pts.begin(), _excluded_pts.end(), [this, ep](double pt) {
                     return std::abs(pt + _excluded_pts[ep]) < 1e-10;
                 }) != _excluded_pts.end()))
            {
                excluded_pts.push_back(_excluded_pts[ep]);
            }
        }
        return Domain(end_points, excluded_pts);
    }
    else if (util_funcs::sign(_end_points[0]) < 0)
    {
        return neg();
    }

    return *this;
}

Domain Domain::inv() const
{
    if (_is_empty)
    {
        return Domain();
    }

    std::array<double, 2> end_points;
    std::vector<double> excluded_pts(_excluded_pts);
    std::transform(excluded_pts.begin(), excluded_pts.end(), excluded_pts.begin(), [](double pt) {
        return 1.0 / pt;
    });

    if ((util_funcs::sign(_end_points[0]) != util_funcs::sign(_end_points[1])))
    {
        if ((_end_points[0] < -1e-10) && (_end_points[1] > 1e-10))
        {
            excluded_pts = {0.0,
                            -std::numeric_limits<double>::infinity(),
                            std::numeric_limits<double>::infinity()};
            end_points = {-std::numeric_limits<double>::infinity(),
                          std::numeric_limits<double>::infinity()};
        }
        else if (_end_points[0] < -1e-10)
        {
            std::transform(
                excluded_pts.begin(), excluded_pts.end(), excluded_pts.begin(), [](double pt) {
                    return -1.0 * std::abs(pt);
                });
            end_points = {-std::numeric_limits<double>::infinity(), 1.0 / _end_points[0]};
        }
        else
        {
            std::transform(
                excluded_pts.begin(), excluded_pts.end(), excluded_pts.begin(), [](double pt) {
                    return std::abs(pt);
                });
            end_points = {1.0 / _end_points[1], std::numeric_limits<double>::infinity()};
        }

        return Domain(end_points, excluded_pts);
    }

    std::transform(_end_points.rbegin(), _end_points.rend(), end_points.begin(), [](double pt) {
        return 1.0 / pt;
    });
    return Domain(end_points, excluded_pts);
}

bool Domain::operator==(Domain domain_2) const
{
    if ((_is_empty && domain_2.is_empty()))
    {
        return true;
    }

    if ((_is_empty || domain_2.is_empty()))
    {
        return false;
    }

    if ((_end_points[0] != domain_2.end_points()[0]) ||
        (_end_points[1] != domain_2.end_points()[1]) ||
        (_excluded_pts.size() != domain_2.excluded_pts().size()))
    {
        return false;
    }
    for (auto& pt : domain_2.excluded_pts())
    {
        if (std::find(_excluded_pts.begin(), _excluded_pts.end(), pt) == _excluded_pts.end())
        {
            return false;
        }
    }

    return true;
}

std::ostream& operator<<(std::ostream& outStream, const Domain& domain)
{
    outStream << domain.toString();
    return outStream;
}
