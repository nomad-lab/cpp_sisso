// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/domain/Domain.hpp
 *  @brief A definition of the domain for a given feature
 *
 *  Defines the domain of a given feature and provides utilities for manipulating them
 *
 *  @author Thomas A. R. Purcell (tpurcell)
 *  @bug No known bugs.
 */
#ifndef FEATURE_DOMAIN
#define FEATURE_DOMAIN
#include <algorithm>
#include <array>
#include <iomanip>
#include <iostream>
#include <limits>
#include <memory>
#include <sstream>
#include <string>
#include <utils/math_funcs.hpp>

#include "external/third_party_boost.hpp"
#include "external/third_party_py.hpp"
#include "utils/vector_utils.hpp"
#ifdef PY_BINDINGS
#include <python/py_binding_cpp_def/conversion_utils.hpp>
namespace py = pybind11;
#endif
// DocString: cls_end_points
/**
 * @brief Class to define the domain of a feature
 *
 */
class Domain
{
private:
    friend class boost::serialization::access;
    /**
     * @brief Function to serialize the data for MPI communication
     */
    template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ar& _excluded_pts;
        ar& _end_points;
        ar& _is_empty;
    }

protected:
    // clang-format off
    std::vector<double> _excluded_pts; //!< Points excluded from the domain
    std::array<double, 2> _end_points; //!< The boundaries of the domain

    bool _is_empty; //!< If true then the domain of the feature is neglected
    // clang-format on
public:
    /**
     * @brief Base Constructor
     * @details Creates a Domain object for serialization
     */
    Domain();

    /**
     * @brief Constructor for Domain
     *
     * @param end_points The min and max of the features
     */
    Domain(std::array<double, 2> end_points);

    /**
     * @brief Constructor for Domain
     *
     * @param end_points The min and max of the features
     */
    Domain(std::array<double, 2> end_points, std::vector<double> excluded_pts);

    /**
     * @brief Construct the Domain
     *
     * @param domain_str The string that will be converted to the Domain
     */
    Domain(std::string domain_str);

    /**
     * @brief Copy Constructor
     */
    Domain(const Domain& o) = default;

    /**
     * @brief Move Constructor
     */
    Domain(Domain&& o) = default;

    /**
     * @brief Copy assignment operator
     */
    Domain& operator=(const Domain& o) = default;

    /**
     * @brief Move assignment operator
     */
    Domain& operator=(Domain&& o) = default;

    /**
     * @brief The boundaries of the domain
     */
    inline const std::array<double, 2>& end_points() const { return _end_points; }

    /**
     * @brief Points excluded from the domain
     */
    inline const std::vector<double>& excluded_pts() const { return _excluded_pts; }

    /**
     * @brief If true the domain is empty and not used
     */
    inline bool is_empty() const { return _is_empty; }

    // DocString: domain_str
    /**
     * @brief Convert the domain into a string
     */
    std::string toString() const;

    // DocString: domain_add_op
    /**
     * @brief Addition operator for domain
     *
     * @param domain_2 The second domain to multiply by
     * @return The sum of this domain with domain_2
     */
    Domain operator+(Domain domain_2) const;

    // DocString: domain_sub_op
    /**
     * @brief Subtraction operator for domain
     *
     * @param domain_2 The second domain to subtract by
     * @return The difference of this domain with domain_2
     */
    inline Domain operator-(Domain domain_2) const { return *this + domain_2.neg(); }

    // DocString: domain_mult_op
    /**
     * @brief Multiplication operator for domain
     *
     * @param domain_2 The second domain to multiply by
     * @return The product of this domain with domain_2
     */
    Domain operator*(Domain domain_2) const;

    // DocString: domain_div_op
    /**
     * @brief Division operator for domain
     *
     * @param domain_2 The second domain to divide by
     * @return The quotient of this domain with domain_2
     */
    inline Domain operator/(Domain domain_2) const { return *this * domain_2.inv(); }

    // DocString: domain_scalar_add
    /**
     * @brief Scalar Addition operator for the domain
     *
     * @param a The scalar to multiply by
     * @return The resulting domain after scalar addition
     */
    Domain operator+(double a) const;

    // DocString: domain_scalar_sub
    /**
     * @brief Scalar Subtraction operator for the domain
     *
     * @param a The scalar to subtract by
     * @return The resulting domain after scalar subtraction
     */
    Domain operator-(double a) const;

    // DocString: domain_scalar_mult
    /**
     * @brief Scalar Multiplication operator for the domain
     *
     * @param a The scalar to multiply by
     * @return The resulting domain after scalar multiplication
     */
    Domain operator*(double a) const;

    // DocString: domain_scalar_div
    /**
     * @brief Scalar Division operator for the domain
     *
     * @param a The scalar to divide by
     * @return The resulting domain after scalar division
     */
    Domain operator/(double a) const;

    // DocString: domain_pow
    /**
     * @brief Exponentiation operator for domain
     *
     * @param power power to exponentiate the domain
     * @return The Domain raised to the power
     */
    inline Domain operator^(double power) const { return pow(power); }

    // DocString: domain_param
    /**
     * @brief Scale and shift the Domain
     *
     * @param scale The amount to scale the Domain by
     * @param shift The amount to shift the Domain by
     *
     * @return The scaled and shifted domain
     */
    Domain parameterize(double scale, double shift) const;

    // DocString: domain_neg
    /**
     * @brief Take the negative values of the domain
     */
    Domain neg() const;

    // DocString: domain_exp
    /**
     * @brief Take the exponential of the domain
     */
    Domain exp() const;

    // DocString: domain_log
    /**
     * @brief Take the logarithm of the domain
     */
    Domain log() const;

    // DocString: domain_pow
    /**
     * @brief Exponentiation of the domain
     *
     * @param power power of the exponent
     * @return [min^power, max^power]
     */
    Domain pow(double power) const;

    // DocString: domain_abs
    /**
     * @brief Take the absolute value of the domain
     */
    Domain abs() const;

    // DocString: domain_inv
    /**
     * @brief The inverse of the domain
     */
    Domain inv() const;

    // DocString: domain_cbrt
    /**
     * @brief The inverse of the domain
     */
    Domain cbrt() const;

    // DocString: domain_sqrt
    /**
     * @brief The inverse of the domain
     */
    Domain sqrt() const;

    /**
     * @brief return true if domain_2 == this
     *
     * @param domain_2 the domain to compare against
     * @return true if domain_2 is the same
     */
    inline bool equal(Domain domain_2) const { return domain_2 == *this; }

    /**
     * @brief return true if domain_2 == this
     *
     * @param domain_2 the domain to compare against
     * @return true if domain_2 is the same
     */
    bool operator==(Domain domain_2) const;

    /**
     * @brief return true if domain_2 == this
     *
     * @param domain_2 the domain to compare against
     * @return true if domain_2 is the same
     */
    inline bool operator!=(Domain domain_2) const { return !(domain_2 == *this); }

    // DocString: domain_contains
    /**
     * @brief Returns true if num is inside the domain
     *
     * @param num The number to check
     * @return True if number is inside the domain
     */
    inline bool contains(double num) const
    {
        return (!_is_empty && (num >= _end_points[0]) && (num <= _end_points[1]) &&
                std::none_of(_excluded_pts.begin(), _excluded_pts.end(), [num](double pt) {
                    return std::abs(pt - num) < 1e-10;
                }));
    }
};
/**
 * @brief Operator to print the domain to a string stream
 *
 * @param outStream Stream to print the unit to
 * @param domain Domain to print to the string stream
 */
std::ostream& operator<<(std::ostream& outStream, const Domain& domain);
/**
 * @brief Addition Operator
 *
 * @param a factor to affect domain by
 * @param b Domain to be modified
 *
 * @return The updated domain
 */
inline Domain operator+(double a, Domain b) { return b + a; }
/**
 * @brief Subtraction Operator
 *
 * @param a factor to affect domain by
 * @param b Domain to be modified
 *
 * @return The updated domain
 */
inline Domain operator-(double a, Domain b) { return b.neg() + a; }
/**
 * @brief Multiplication Operator
 *
 * @param a factor to affect domain by
 * @param b Domain to be modified
 *
 * @return The updated domain
 */
inline Domain operator*(double a, Domain b) { return b * a; }
/**
 * @brief Division Operator
 *
 * @param a factor to affect domain by
 * @param b Domain to be modified
 *
 * @return The updated domain
 */
inline Domain operator/(double a, Domain b) { return b.inv() * a; }

#endif
