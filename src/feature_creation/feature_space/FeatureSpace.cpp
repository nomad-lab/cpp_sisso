// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file feature_creation/feature_space/FeatureSpace.cpp
 *  @brief Implements the class for creating/operating on a feature space in SISSO
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "feature_creation/feature_space/FeatureSpace.hpp"

BOOST_CLASS_EXPORT_GUID(FeatureNode, "FeatureNode")
BOOST_CLASS_EXPORT_GUID(AddNode, "AddNode")
BOOST_CLASS_EXPORT_GUID(SubNode, "SubNode")
BOOST_CLASS_EXPORT_GUID(AbsDiffNode, "AbsDiffNode")
BOOST_CLASS_EXPORT_GUID(MultNode, "MultNode")
BOOST_CLASS_EXPORT_GUID(DivNode, "DivNode")
BOOST_CLASS_EXPORT_GUID(SqNode, "SqNode")
BOOST_CLASS_EXPORT_GUID(SqrtNode, "SqrtNode")
BOOST_CLASS_EXPORT_GUID(CbNode, "CbNode")
BOOST_CLASS_EXPORT_GUID(CbrtNode, "CbrtNode")
BOOST_CLASS_EXPORT_GUID(SixPowNode, "SixPowNode")
BOOST_CLASS_EXPORT_GUID(ExpNode, "ExpNode")
BOOST_CLASS_EXPORT_GUID(NegExpNode, "NegExpNode")
BOOST_CLASS_EXPORT_GUID(LogNode, "LogNode")
BOOST_CLASS_EXPORT_GUID(AbsNode, "AbsNode")
BOOST_CLASS_EXPORT_GUID(InvNode, "InvNode")
BOOST_CLASS_EXPORT_GUID(SinNode, "SinNode")
BOOST_CLASS_EXPORT_GUID(CosNode, "CosNode")

#ifdef PARAMETERIZE
BOOST_CLASS_EXPORT_GUID(AddParamNode, "AddParamNode")
BOOST_CLASS_EXPORT_GUID(SubParamNode, "SubParamNode")
BOOST_CLASS_EXPORT_GUID(AbsDiffParamNode, "AbsDiffParamNode")
BOOST_CLASS_EXPORT_GUID(MultParamNode, "MultParamNode")
BOOST_CLASS_EXPORT_GUID(DivParamNode, "DivParamNode")
BOOST_CLASS_EXPORT_GUID(SqParamNode, "SqParamNode")
BOOST_CLASS_EXPORT_GUID(SqrtParamNode, "SqrtParamNode")
BOOST_CLASS_EXPORT_GUID(CbParamNode, "CbParamNode")
BOOST_CLASS_EXPORT_GUID(CbrtParamNode, "CbrtParamNode")
BOOST_CLASS_EXPORT_GUID(SixPowParamNode, "SixPowParamNode")
BOOST_CLASS_EXPORT_GUID(ExpParamNode, "ExpParamNode")
BOOST_CLASS_EXPORT_GUID(NegExpParamNode, "NegExpParamNode")
BOOST_CLASS_EXPORT_GUID(LogParamNode, "LogParamNode")
BOOST_CLASS_EXPORT_GUID(AbsParamNode, "AbsParamNode")
BOOST_CLASS_EXPORT_GUID(InvParamNode, "InvParamNode")
BOOST_CLASS_EXPORT_GUID(SinParamNode, "SinParamNode")
BOOST_CLASS_EXPORT_GUID(CosParamNode, "CosParamNode")
#endif

void FeatureSpace::initaialize_storage()
{
    node_value_arrs::initialize_values_arr(
        _phi_0.back()->n_samp(), _phi_0.back()->n_samp_test(), _phi_0.size());
    node_value_arrs::set_task_sz_train(_task_sizes_train);
#ifdef PARAMETERIZE
    node_value_arrs::set_max_rung(_max_rung, _allowed_param_ops.size() > 0);
#else
    node_value_arrs::set_max_rung(_max_rung);
#endif

    for (auto& feat : _phi_0)
    {
        feat->set_value();
        feat->set_test_value();
    }
}

FeatureSpace::FeatureSpace(InputParser inputs)
    : _phi(inputs.phi_0_ptrs()),
      _phi_0(inputs.phi_0_ptrs()),
      _allowed_ops(inputs.allowed_ops_copy()),
      _prop_train(inputs.prop_train_copy()),
      _scores(inputs.phi_0().size()),
      _task_sizes_train(inputs.task_sizes_train_copy()),
      _start_rung(1, 0),
      _project_type(inputs.calc_type()),
      _feature_space_file("feature_space/selected_features.txt"),
      _feature_space_summary_file("feature_space/SIS_summary.txt"),
      _phi_out_file(inputs.phi_out_file()),
      _mpi_comm(inputs.mpi_comm()),
      _cross_cor_max(inputs.cross_cor_max()),
      _l_bound(inputs.l_bound()),
      _u_bound(inputs.u_bound()),
      _n_rung_store(inputs.n_rung_store()),
      _n_feat(inputs.phi_0().size()),
      _max_rung(inputs.max_rung()),
      _n_sis_select(inputs.n_sis_select()),
      _n_samp_train(inputs.n_samp_train()),
      _n_rung_generate(inputs.n_rung_generate()),
      _override_n_sis_select(inputs.override_n_sis_select())
{
#ifdef PARAMETERIZE
    _end_no_params.resize(1, inputs.phi_0().size());
    _start_rung_reparam.resize(1, 0);
    _allowed_param_ops = inputs.allowed_param_ops_copy();

    _max_param_depth = inputs.max_param_depth();
    _reparam_residual = inputs.reparam_residual() && (_allowed_param_ops.size() > 0);

    if (_max_param_depth == -1)
    {
        _max_param_depth = _max_rung;
    }
    if ((_max_param_depth < 0) || (_max_param_depth > _max_rung))
    {
        throw std::logic_error("Invalid parameter depth.");
    }
    nlopt_wrapper::MAX_PARAM_DEPTH = _max_param_depth;
#else
    if (inputs.allowed_param_ops_copy().size() > 0)
    {
        throw std::logic_error("To use parameterize features rebuild with -DBUILD_PARAMS=ON.");
    }
#endif
    if (node_value_arrs::N_PRIMARY_FEATURES == 0)
    {
        initaialize_storage();
    }

    for (long int ff = _phi.size() - 1; ff >= 0; --ff)
    {
        if (_phi[ff]->is_const())
        {
            std::cerr << "WARNING: Primary Feature " << _phi[ff]->expr()
                      << " is constant. Removing it from the feature space" << std::endl;
            std::fill_n(_phi[ff]->value_ptr(), _n_samp_train, -1.0);
            _phi.erase(_phi.begin() + ff);
        }
        else
        {
            _phi[ff]->set_value();
        }
    }
    _n_feat = _phi.size();
    node_value_arrs::resize_values_arr(0, _n_feat);
    for (size_t ff = 0; ff < _phi.size(); ++ff)
    {
        _phi[ff]->reindex(_phi[ff]->feat_ind(), ff);
        _phi[ff]->set_value();
    }

    if (_n_rung_store == -1)
    {
        _n_rung_store = _max_rung - 1;
    }
    if (_n_rung_generate > 1)
    {
        throw std::logic_error("A maximum of one rung can be generated on the fly.");
    }
    else if (_max_rung - _n_rung_generate < _n_rung_store)
    {
        throw std::logic_error("Requesting to store more rungs than what can be pre-generated.");
    }

    node_value_arrs::set_task_sz_train(_task_sizes_train);
    int n_max_ops = 0;
    for (int rr = 0; rr < _max_rung - _n_rung_store; ++rr)
    {
        n_max_ops += std::pow(2, rr);
    }

    initialize_fs_output_files();
    comp_feats::set_is_valid_fxn(
        _project_type, _cross_cor_max, _n_samp_train, _is_valid, _is_valid_feat_list);
    set_op_lists();

    double start = omp_get_wtime();
    generate_feature_space(_phi, _start_rung, _prop_train);
    _n_feat = _phi.size();

    if (_phi_out_file.size() > 0)
    {
        output_phi();
    }

    _mpi_comm->barrier();
    double duration = omp_get_wtime() - start;
    if (_mpi_comm->rank() == 0)
    {
        std::cout << "Time to generate feat space: " << duration << " s" << std::endl;
    }

    mpi_reduce_op::set_op(_project_type, _cross_cor_max, _n_sis_select);

    _scores.reserve(_phi.size());
    _scores.resize(_phi.size());
}

FeatureSpace::FeatureSpace(std::string feature_file,
                           std::vector<node_ptr> phi_0,
                           std::vector<double> prop,
                           std::vector<int> task_sizes,
                           std::string project_type,
                           int n_sis_select,
                           double cross_corr_max,
                           std::vector<int> excluded_inds,
                           bool override_n_sis_select)
    : _phi_0(phi_0),
      _prop_train(prop),
      _scores(phi_0.size(), 0.0),
      _task_sizes_train(task_sizes),
      _start_rung(1, 0),
      _project_type(project_type),
      _feature_space_file("feature_space/selected_features.txt"),
      _feature_space_summary_file("feature_space/SIS_summary.txt"),
      _mpi_comm(mpi_setup::comm),
      _cross_cor_max(cross_corr_max),
      _l_bound(1e-50),
      _u_bound(1e50),
      _n_rung_store(0),
      _n_feat(phi_0.size()),
      _n_sis_select(n_sis_select),
      _n_samp_train(_phi_0[0]->n_samp()),
      _n_rung_generate(0),
      _override_n_sis_select(override_n_sis_select)
{
    _max_rung = 10;
    if (_override_n_sis_select)
    {
        std::cerr << "WARNING: _override_n_sis_select is true. This can make hyperparameter "
                     "optimization inconsistent as this hyperparameter is mutable."
                  << std::endl;
    }
    assert(_n_samp_train == std::accumulate(_task_sizes_train.begin(), _task_sizes_train.end(), 0));
    if (node_value_arrs::N_PRIMARY_FEATURES == 0)
    {
        initaialize_storage();
    }

#ifdef PARAMETERIZE
    _max_param_depth = -1;
    _reparam_residual = false;
#endif

    comp_feats::set_is_valid_fxn(
        project_type, _cross_cor_max, _n_samp_train, _is_valid, _is_valid_feat_list);
    mpi_reduce_op::set_op(_project_type, _cross_cor_max, _n_sis_select);

    std::vector<node_ptr> phi_temp = str2node::phi_from_file(feature_file, _phi_0, excluded_inds);
    phi_temp.insert(phi_temp.begin(), _phi_0.begin(), _phi_0.end());

    _n_feat = phi_temp.size();
    _phi.resize(_n_feat);
    std::vector<int> rungs(_n_feat, 0);
    std::vector<unsigned int> sort_scores(_n_feat, 0);
    for (size_t ff = 0; ff < _n_feat; ++ff)
    {
        rungs[ff] = phi_temp[ff]->rung();
        sort_scores[ff] = phi_temp[ff]->rung() * _n_feat + phi_temp[ff]->arr_ind();
        if (phi_temp[ff]->type() == NODE_TYPE::FEAT)
        {
            continue;
        }

        std::string nt = node_identifier::feature_type_to_string(phi_temp[ff]->type());
        if (nt.substr(0, 2) == "p:")
        {
#ifdef PARAMETERIZE
            if (std::find(_allowed_param_ops.begin(), _allowed_param_ops.end(), nt) ==
                _allowed_param_ops.end())
            {
                _allowed_param_ops.push_back(nt.substr(2, nt.size() - 2));
            }
#else
            throw std::logic_error(
                "Parameterized features are not currently built, recompile with BUILD_PARAMS=ON.");
#endif
        }
        else
        {
            if (std::find(_allowed_ops.begin(), _allowed_ops.end(), nt) == _allowed_ops.end())
            {
                _allowed_ops.push_back(nt);
            }
        }
    }

    for (auto& op : _allowed_ops)
    {
        if ((op.compare("add") == 0) || (op.compare("sub") == 0) || (op.compare("mult") == 0) ||
            (op.compare("abs_diff") == 0))
        {
            _com_bin_operators.push_back(allowed_op_maps::binary_operator_map[op]);
        }
        else if ((op.compare("div") == 0))
        {
            _bin_operators.push_back(allowed_op_maps::binary_operator_map[op]);
        }
        else
        {
            _un_operators.push_back(allowed_op_maps::unary_operator_map[op]);
        }
    }

    _max_rung = *std::max_element(rungs.begin(), rungs.end());
#ifdef PARAMETERIZE
    node_value_arrs::set_max_rung(_max_rung, _allowed_param_ops.size() > 0);
#else
    node_value_arrs::set_max_rung(_max_rung);
#endif

    std::vector<int> rung_inds = util_funcs::argsort<unsigned int>(sort_scores);
    _phi[0] = phi_temp[rung_inds[0]];
    for (size_t ff = 1; ff < _n_feat; ++ff)
    {
        _phi[ff] = phi_temp[rung_inds[ff]];
        if (_phi[ff]->rung() != _phi[ff - 1]->rung())
        {
            _start_rung.push_back(ff);
        }
    }
#ifdef PARAMETERIZE

    for (auto& op : _allowed_param_ops)
    {
        if ((op.compare("add") == 0) || (op.compare("sub") == 0) || (op.compare("abs_diff") == 0))
        {
            _com_bin_param_operators.push_back(allowed_op_maps::binary_param_operator_map[op]);
        }
        else if ((op.compare("div") == 0) || (op.compare("mult") == 0))
        {
            _bin_param_operators.push_back(allowed_op_maps::binary_param_operator_map[op]);
        }
        else
        {
            _un_param_operators.push_back(allowed_op_maps::unary_param_operator_map[op]);
        }
    }

    _start_rung_reparam = {0};
    _end_no_params = {0};
    for (int rr = 1; rr < _max_rung; ++rr)
    {
        nlopt_wrapper::MAX_PARAM_DEPTH = rr;
        bool is_correct = true;
        for (auto& feat : _phi)
        {
            int param_size = feat->parameters().size();
            if ((param_size > 0) && (param_size != feat->n_params_possible()))
            {
                is_correct = false;
                break;
            }
        }
        if (is_correct)
        {
            _max_param_depth = rr;
            break;
        }
    }

    if (_max_param_depth == -1)
    {
        throw std::logic_error(
            "The maximum parameter depth could not be determined from the file.");
    }
    node_value_arrs::initialize_param_storage();

    for (int rr = 1; rr <= _max_rung; ++rr)
    {
        int n_feat_in_rung = (rr < _max_rung ? _start_rung[rr + 1] : _phi.size()) - _start_rung[rr];

        // Reorder features based on the number of parameters they have (none goes first)
        std::vector<int> feat_n_params(n_feat_in_rung);
        std::transform(_phi.begin() + _start_rung[rr],
                       _phi.begin() + _start_rung[rr] + n_feat_in_rung,
                       feat_n_params.begin(),
                       [](node_ptr feat) { return feat->n_params(); });
        std::vector<int> inds = util_funcs::argsort<int>(feat_n_params);
        std::vector<node_ptr> phi_copy(n_feat_in_rung);
        std::copy_n(_phi.begin() + _start_rung[rr], n_feat_in_rung, phi_copy.begin());
        std::transform(inds.begin(),
                       inds.end(),
                       _phi.begin() + _start_rung[rr],
                       [&phi_copy](int ind) { return phi_copy[ind]; });

        // Set how many features have no parameters
        _end_no_params.push_back(std::count_if(
            feat_n_params.begin(), feat_n_params.end(), [](int n_param) { return n_param == 0; }));
    }
#endif

    for (unsigned long int ff = 0; ff < _phi.size(); ++ff)
    {
        _phi[ff]->reindex(ff, ff);
    }
    _scores.resize(_n_feat);
    initialize_fs_output_files();
}

FeatureSpace::FeatureSpace(std::vector<node_ptr> phi_selected,
                           std::vector<node_ptr> phi,
                           std::vector<node_ptr> phi0,
                           std::vector<std::string> allowed_ops,
                           std::vector<std::string> allowed_param_ops,
                           std::vector<double> prop_train,
                           std::vector<double> scores,
                           std::vector<int> task_sizes_train,
                           std::vector<int> start_rung,
                           std::string project_type,
                           std::string feature_space_file,
                           std::string feature_space_summary_file,
                           std::string phi_out_file,
                           double cross_cor_max,
                           double l_bound,
                           double u_bound,
                           int n_rung_store,
                           int n_rung_generate,
                           int max_rung,
                           int n_sis_select,
                           std::vector<node_ptr> phi_reparam,
                           std::vector<int> end_no_params,
                           std::vector<int> start_rung_reparam,
                           int max_param_depth,
                           bool reparam_residual,
                           bool override_n_sis_select)
    : _phi(phi),
      _phi_0(phi0),
      _allowed_ops(allowed_ops),
      _prop_train(prop_train),
      _scores(scores),
      _task_sizes_train(task_sizes_train),
      _start_rung(start_rung),
      _project_type(project_type),
      _feature_space_file(feature_space_file),
      _feature_space_summary_file(feature_space_summary_file),
      _phi_out_file(phi_out_file),
      _mpi_comm(mpi_setup::comm),
      _cross_cor_max(cross_cor_max),
      _l_bound(l_bound),
      _u_bound(u_bound),
      _n_rung_store(n_rung_store),
      _n_feat(phi.size()),
      _max_rung(max_rung),
      _n_sis_select(n_sis_select),
      _n_samp_train(std::accumulate(task_sizes_train.begin(), task_sizes_train.end(), 0)),
      _n_rung_generate(n_rung_generate),
      _override_n_sis_select(override_n_sis_select)
{
    if (_override_n_sis_select)
    {
        std::cerr << "WARNING: _override_n_sis_select is true. This can make hyperparameter "
                     "optimization inconsistent as this hyperparameter is mutable."
                  << std::endl;
    }
#ifdef PARAMETERIZE
    _phi_reparam = phi_reparam;
    _end_no_params = end_no_params;
    _start_rung_reparam = start_rung_reparam;
    _max_param_depth = max_param_depth;
    _reparam_residual = reparam_residual;
    nlopt_wrapper::MAX_PARAM_DEPTH = _max_param_depth;
#endif

    if (node_value_arrs::N_PRIMARY_FEATURES == 0)
    {
        initaialize_storage();

        size_t max_stored = _phi.size();
        if (_n_rung_store < _max_rung)
        {
            max_stored = _start_rung[_n_rung_store + 1];
        }
        node_value_arrs::resize_values_arr(max_stored, _max_rung);
        for (size_t ff = 0; ff < max_stored; ++ff)
        {
            _phi[ff]->reindex(_phi[ff]->feat_ind(), ff);
            _phi[ff]->set_value();
            _phi[ff]->set_test_value();
        }
    }

    int n_max_ops = 0;
    for (int rr = 0; rr < _max_rung - _n_rung_store; ++rr)
    {
        n_max_ops += std::pow(2, rr);
    }

    initialize_fs_output_files();
    comp_feats::set_is_valid_fxn(
        _project_type, _cross_cor_max, _n_samp_train, _is_valid, _is_valid_feat_list);
    set_op_lists();
}

FeatureSpace::~FeatureSpace() {}

void FeatureSpace::set_op_lists()
{
    for (auto& op : _allowed_ops)
    {
        if ((op.compare("add") == 0) || (op.compare("mult") == 0) ||
            (op.compare("abs_diff") == 0) || (op.compare("sub") == 0))
        {
            _com_bin_operators.push_back(allowed_op_maps::binary_operator_map[op]);
        }
        else if ((op.compare("div") == 0))
        {
            _bin_operators.push_back(allowed_op_maps::binary_operator_map[op]);
        }
        else if (allowed_op_maps::unary_operator_map.count(op) > 0)
        {
            _un_operators.push_back(allowed_op_maps::unary_operator_map[op]);
        }
        else
        {
            throw std::logic_error("operator " + op + " not defined");
        }
    }

#ifdef PARAMETERIZE
    for (auto& op : _allowed_param_ops)
    {
        if ((op.compare("add") == 0) || (op.compare("sub") == 0) || (op.compare("abs_diff") == 0))
        {
            _com_bin_param_operators.push_back(allowed_op_maps::binary_param_operator_map[op]);
        }
        else if ((op.compare("div") == 0) || (op.compare("mult") == 0))
        {
            _bin_param_operators.push_back(allowed_op_maps::binary_param_operator_map[op]);
        }
        else if (allowed_op_maps::unary_param_operator_map.count(op) > 0)
        {
            _un_param_operators.push_back(allowed_op_maps::unary_param_operator_map[op]);
        }
        else
        {
            throw std::logic_error("operator " + op + " not defined");
        }
    }
#endif
}

void FeatureSpace::initialize_fs_output_files() const
{
    node_value_arrs::initialize_d_matrix_arr();
    if (_mpi_comm->rank() == 0)
    {
        // Create output directories if needed
        boost::filesystem::path p(_feature_space_file.c_str());
        boost::filesystem::create_directories(p.remove_filename());

        std::ofstream out_file_stream = std::ofstream();
        std::ofstream sum_file_stream = std::ofstream();
        out_file_stream.open(_feature_space_file);
        sum_file_stream.open(_feature_space_summary_file);
        out_file_stream << std::setw(14) << std::left << "# FEAT_ID"
                        << "Feature Postfix Expression (RPN)" << std::endl;
        sum_file_stream << std::setw(14) << std::left << "# FEAT_ID" << std::setw(24) << std::left
                        << "Score"
                        << "Feature Expression" << std::endl;
        out_file_stream.close();
        sum_file_stream.close();
    }
}

#ifdef PARAMETERIZE
void FeatureSpace::generate_param_feats(std::vector<node_ptr>::iterator& feat,
                                        std::vector<node_ptr>& feat_set,
                                        const std::vector<node_ptr>::iterator& start,
                                        unsigned long int& feat_ind,
                                        std::shared_ptr<NLOptimizer> optimizer,
                                        const double l_bound,
                                        const double u_bound)
{
    unsigned long int phi_ind = feat - start;
    feat_set.reserve(feat_set.size() + _un_param_operators.size() +
                     phi_ind * (_com_bin_param_operators.size() + 2 * _bin_param_operators.size()));
    for (auto& op : _un_param_operators)
    {
        op(feat_set, *feat, feat_ind, l_bound, u_bound, optimizer);
    }

    for (auto& op : _com_bin_param_operators)
    {
        for (auto feat_2 = start; feat_2 != feat; ++feat_2)
        {
            op(feat_set, *feat, *feat_2, feat_ind, l_bound, u_bound, optimizer);
        }
    }
    for (auto& op : _bin_param_operators)
    {
        for (auto feat_2 = start; feat_2 != feat; ++feat_2)
        {
            op(feat_set, *feat, *feat_2, feat_ind, l_bound, u_bound, optimizer);
            op(feat_set, *feat_2, *feat, feat_ind, l_bound, u_bound, optimizer);
        }
    }
}

void FeatureSpace::generate_reparam_feats(std::vector<node_ptr>::iterator& feat,
                                          std::vector<node_ptr>& feat_set,
                                          unsigned long int& feat_ind,
                                          std::shared_ptr<NLOptimizer> optimizer,
                                          const double l_bound,
                                          const double u_bound)
{
    int cur_rung = (*feat)->rung();
    unsigned long int max_n_feat = _end_no_params[cur_rung] + _phi_reparam.size();
    feat_set.reserve(feat_set.size() + _un_operators.size() +
                     max_n_feat * (_com_bin_operators.size() + 2 * _bin_operators.size()));

    for (auto& op : _un_param_operators)
    {
        op(feat_set, *feat, feat_ind, l_bound, u_bound, optimizer);
    }

    for (auto& op : _com_bin_param_operators)
    {
        for (int rr = 0; rr <= cur_rung; ++rr)
        {
            for (auto feat_2 = _phi.begin() + _start_rung[rr];
                 (feat_2 < feat) || (feat_2 < (_phi.begin() + _end_no_params[rr]));
                 ++feat_2)
            {
                op(feat_set, *feat, *feat_2, feat_ind, l_bound, u_bound, optimizer);
            }
        }
        if (_phi_reparam.size() > 0)
        {
            for (auto feat_2 = _phi_reparam.begin(); (feat_2 < _phi_reparam.end()); ++feat_2)
            {
                op(feat_set, *feat, *feat_2, feat_ind, l_bound, u_bound, optimizer);
            }
        }
    }

    for (auto& op : _bin_param_operators)
    {
        for (int rr = 0; rr <= cur_rung; ++rr)
        {
            for (auto feat_2 = _phi.begin() + _start_rung[rr];
                 (feat_2 < feat) || (feat_2 < (_phi.begin() + _end_no_params[rr]));
                 ++feat_2)
            {
                op(feat_set, *feat, *feat_2, feat_ind, l_bound, u_bound, optimizer);
                op(feat_set, *feat_2, *feat, feat_ind, l_bound, u_bound, optimizer);
            }
        }
        if (_phi_reparam.size() > 0)
        {
            for (auto feat_2 = _phi_reparam.begin(); (feat_2 < _phi_reparam.end()); ++feat_2)
            {
                op(feat_set, *feat, *feat_2, feat_ind, l_bound, u_bound, optimizer);
                op(feat_set, *feat_2, *feat, feat_ind, l_bound, u_bound, optimizer);
            }
        }
    }
}
#endif

void FeatureSpace::generate_non_param_feats(std::vector<node_ptr>::iterator& feat,
                                            std::vector<node_ptr>& feat_set,
                                            const std::vector<node_ptr>::iterator& start,
                                            unsigned long int& feat_ind,
                                            const double l_bound,
                                            const double u_bound)
{
    unsigned long int phi_ind = feat - start;
    feat_set.reserve(feat_set.size() + _un_operators.size() +
                     phi_ind * (_com_bin_operators.size() + 2 * _bin_operators.size()));
    for (auto& op : _un_operators)
    {
        op(feat_set, *feat, feat_ind, l_bound, u_bound);
    }

    for (auto& op : _com_bin_operators)
    {
        for (auto feat_2 = start; feat_2 < feat; ++feat_2)
        {
            op(feat_set, *feat, *feat_2, feat_ind, l_bound, u_bound);
        }
    }

    for (auto& op : _bin_operators)
    {
        for (auto feat_2 = start; feat_2 < feat; ++feat_2)
        {
            op(feat_set, *feat, *feat_2, feat_ind, l_bound, u_bound);
            op(feat_set, *feat_2, *feat, feat_ind, l_bound, u_bound);
        }
    }
}

#ifdef PARAMETERIZE
int FeatureSpace::reorder_by_n_params(std::vector<node_ptr>& feat_set, int start)
{
    std::vector<int> feat_n_params(feat_set.size() - start);
    std::transform(feat_set.begin() + start,
                   feat_set.end(),
                   feat_n_params.begin(),
                   [](node_ptr feat) { return feat->n_params(); });
    std::vector<int> inds = util_funcs::argsort<int>(feat_n_params);
    std::vector<node_ptr> feat_set_copy(feat_n_params.size());
    std::copy_n(feat_set.begin() + start, feat_n_params.size(), feat_set_copy.begin());

    std::transform(inds.begin(),
                   inds.end(),
                   feat_set.begin() + start,
                   [&feat_set_copy](int ind) { return feat_set_copy[ind]; });

    for (size_t ff = start; ff < feat_set.size(); ++ff)
    {
        feat_set[ff]->reindex(ff);
    }

    // Set how many features have no parameters
    return start + std::count_if(feat_n_params.begin(),
                                 feat_n_params.end(),
                                 [](int n_param) { return n_param == 0; });
}
#endif
void FeatureSpace::remove_duplicate_features(std::vector<node_ptr>& feat_set, int start)
{
    std::vector<double> scores(feat_set.size(), 0.0);
    project_funcs::project_r(_prop_train.data(), scores.data(), feat_set, _task_sizes_train, 1);

    scores.erase(scores.begin(), scores.begin() + start);
    if (scores.size() == 0)
    {
        throw std::logic_error("No features created during this rung (" +
                               std::to_string(feat_set.back()->rung() + 1) + ")");
    }
    std::vector<int> inds = util_funcs::argsort<double>(scores);

    std::vector<int> del_inds;
    node_value_arrs::clear_temp_reg();
    for (size_t sc = 0; sc < scores.size() - 1; ++sc)
    {
#ifdef PARAMETERIZE
        if (feat_set[inds[sc] + start]->n_params() > 0)
        {
            continue;
        }
#endif

        double* val_ptr = feat_set[start + inds[sc]]->stand_value_ptr();
        double base_val = std::abs(
            std::inner_product(val_ptr, val_ptr + _n_samp_train, val_ptr, 0.0));

        size_t sc2 = sc + 1;
        while ((sc2 < scores.size()) && (scores[inds[sc2]] - scores[inds[sc]] < 1e-7))
        {
            double comp = std::abs(
                base_val -
                std::abs(std::inner_product(val_ptr,
                                            val_ptr + _n_samp_train,
                                            feat_set[start + inds[sc2]]->stand_value_ptr(true),
                                            0.0)));

            if (comp / static_cast<double>(_n_samp_train) < 1e-10)
            {
                del_inds.push_back(-1 * (inds[sc] + start));
                break;
            }
            ++sc2;
        }
    }

    inds = util_funcs::argsort<int>(del_inds);
    for (size_t ii = 0; ii < inds.size(); ++ii)
    {
        feat_set.erase(feat_set.begin() - del_inds[inds[ii]]);
    }

    // Reindex
    for (size_t ff = start; ff < feat_set.size(); ++ff)
    {
        feat_set[ff]->reindex(ff);
    }
}

void FeatureSpace::generate_feature_space(std::vector<node_ptr>& feat_set,
                                          std::vector<int>& start_rung,
                                          const std::vector<double>& prop,
                                          bool reparam)
{
    double u_bound = 1e50;
    double l_bound = 1e-50;
    std::vector<int> inds;

    for (int nn = 1; nn <= _max_rung - _n_rung_generate; ++nn)
    {
        node_value_arrs::clear_temp_reg();
        if (nn == _max_rung)
        {
            u_bound = _u_bound;
            l_bound = _l_bound;
        }
        std::vector<node_ptr> next_phi;
        unsigned long int feat_ind;
        if (!reparam)
        {
            feat_ind = feat_set.back()->feat_ind() + 1;
            _n_feat = feat_set.size();
        }
        else
        {
#ifdef PARAMETERIZE
            feat_ind = _phi.size() + _phi_reparam.size();
#else
            throw std::logic_error(
                "Reparameterization only allowed if compiled with -DBUILD_PARAMS=ON");
#endif
        }

#pragma omp declare reduction(merge_nodes : std::vector<node_ptr> : omp_out=openmp_red::merge<node_ptr>(omp_out, omp_in)) initializer(omp_priv = omp_orig)
        node_value_arrs::clear_temp_reg();
#ifdef PARAMETERIZE
#pragma omp parallel firstprivate(feat_ind) shared(next_phi)
        {

            std::shared_ptr<NLOptimizer> optimizer;
            if(_allowed_param_ops.size() > 0)
            {
                optimizer = nlopt_wrapper::get_optimizer(_project_type, _task_sizes_train, _prop_train, _max_rung, _max_param_depth);
            }

            const size_t start = start_rung.back() + _mpi_comm->rank();
            const size_t end = feat_set.size();
            const size_t inc = _mpi_comm->size();
#ifdef OMP45
#pragma omp for schedule(monotonic : dynamic) reduction(merge_nodes : next_phi)
#else
#pragma omp for schedule(dynamic) reduction(merge_nodes : next_phi)
#endif
            for (size_t idx = start;
                 idx < end;
                 idx += inc)
            {
                auto feat_1 = feat_set.begin() + idx;
                generate_non_param_feats(
                    feat_1, next_phi, feat_set.begin(), feat_ind, l_bound, u_bound);
                generate_param_feats(
                    feat_1, next_phi, feat_set.begin(), feat_ind, optimizer, l_bound, u_bound);
            }

            if (reparam)
            {
                std::shared_ptr<NLOptimizer> optimizer_reparam;
                if(_allowed_param_ops.size() > 0)
                {
                    optimizer_reparam = nlopt_wrapper::get_optimizer(_project_type, _task_sizes_train, prop, _max_rung, _max_param_depth);
                }

                const size_t start2 = _start_rung[nn - 1] + _mpi_comm->rank();
                const size_t end2 = _end_no_params[nn - 1];
                const size_t inc2 = _mpi_comm->size();
#ifdef OMP45
#pragma omp for schedule(monotonic : dynamic) reduction(merge_nodes : next_phi)
#else
#pragma omp for schedule(dynamic) reduction(merge_nodes : next_phi)
#endif
                for (size_t idx = start2; idx < end2; idx += inc2)
                {
                    auto feat_1 = _phi.begin() + idx;
                    generate_reparam_feats(
                        feat_1, next_phi, feat_ind, optimizer_reparam, l_bound, u_bound);
                }
            }
        }
#else
#pragma omp parallel firstprivate(feat_ind)
        {
            const size_t start = start_rung.back() + _mpi_comm->rank();
            const size_t end = feat_set.size();
            const size_t inc = _mpi_comm->size();
#ifdef OMP45
#pragma omp for schedule(monotonic : dynamic) reduction(merge_nodes : next_phi)
#else
#pragma omp for schedule(dynamic) reduction(merge_nodes : next_phi)
#endif
            for (auto idx = start; idx < end; idx += inc)
            {
                auto feat_1 = feat_set.begin() + idx;
                generate_non_param_feats(
                    feat_1, next_phi, feat_set.begin(), feat_ind, l_bound, u_bound);
            }
        }
#endif
        start_rung.push_back(feat_set.size());
        node_value_arrs::clear_temp_reg();
        if ((nn < _max_rung) || (nn <= _n_rung_store) || (_mpi_comm->size() == 1))
        {
            int new_phi_size;
            if (_mpi_comm->rank() == 0)
            {
                std::vector<std::vector<node_ptr>> next_phi_gathered;
                mpi::gather(*_mpi_comm, next_phi, next_phi_gathered, 0);
                feat_ind = feat_set.size();
                for (auto& next_phi_vec : next_phi_gathered)
                {
                    feat_set.insert(feat_set.end(), next_phi_vec.begin(), next_phi_vec.end());
                }

                // Sort the features to ensure consistent feature spaces for all MPI/OpenMP configurations
                std::sort(feat_set.begin() + start_rung.back(),
                          feat_set.end(),
                          [feat_ind](node_ptr n1, node_ptr n2)
                          { return n1->sort_score(feat_ind) < n2->sort_score(feat_ind); });

                // Reindex sorted features
                std::for_each(feat_set.begin() + start_rung.back(),
                              feat_set.end(),
                              [&feat_ind](node_ptr n)
                              {
                                  n->reindex(feat_ind);
                                  ++feat_ind;
                              });
                if (nn < _max_rung)
                {
                    remove_duplicate_features(feat_set, start_rung.back());
                }
#ifdef PARAMETERIZE
                if (!reparam)
                {
                    int no_param_sz = reorder_by_n_params(feat_set, start_rung.back());
                    mpi::broadcast(*_mpi_comm, no_param_sz, 0);
                    _end_no_params.push_back(no_param_sz);
                }
#endif
                new_phi_size = feat_set.size();
                mpi::broadcast(*_mpi_comm, new_phi_size, 0);
                mpi::broadcast(
                    *_mpi_comm, &feat_set[start_rung.back()], new_phi_size - start_rung.back(), 0);
            }
            else
            {
                mpi::gather(*_mpi_comm, next_phi, 0);

#ifdef PARAMETERIZE
                if (!reparam)
                {
                    int no_param_sz;
                    mpi::broadcast(*_mpi_comm, no_param_sz, 0);
                    _end_no_params.push_back(no_param_sz);
                }
#endif

                mpi::broadcast(*_mpi_comm, new_phi_size, 0);
                feat_set.resize(new_phi_size);
                mpi::broadcast(
                    *_mpi_comm, &feat_set[start_rung.back()], new_phi_size - start_rung.back(), 0);
            }
            if (start_rung.back() == static_cast<int>(feat_set.size()))
            {
                throw std::logic_error("No features created during this rung (" +
                                       std::to_string(nn) + ")");
            }

            node_value_arrs::clear_temp_reg();
            if (!reparam)
            {
                for (size_t ff = start_rung.back(); ff < feat_set.size(); ++ff)
                {
                    feat_set[ff]->reset_feats(feat_set);
                }
                if (nn <= _n_rung_store)
                {
                    node_value_arrs::resize_values_arr(nn, feat_set.size());

                    for (size_t ff = start_rung.back(); ff < feat_set.size(); ++ff)
                    {
                        feat_set[ff]->set_value();
                        feat_set[ff]->set_test_value();
                    }
                }
            }
        }
        else
        {
            std::vector<size_t> next_phi_sizes(_mpi_comm->size());
            if (_mpi_comm->rank() == 0)
            {
                mpi::gather(*_mpi_comm, next_phi.size(), next_phi_sizes.data(), 0);
                mpi::broadcast(*_mpi_comm, next_phi_sizes.data(), next_phi_sizes.size(), 0);
            }
            else
            {
                mpi::gather(*_mpi_comm, next_phi.size(), 0);
                mpi::broadcast(*_mpi_comm, next_phi_sizes.data(), next_phi_sizes.size(), 0);
            }

            size_t n_feat = std::accumulate(next_phi_sizes.begin(), next_phi_sizes.end(), 0);
            if (n_feat == 0)
            {
                throw std::logic_error("No features created during this rung (" +
                                       std::to_string(nn) + ")");
            }

            size_t n_feat_rank = n_feat / _mpi_comm->size();
            size_t n_feat_below_rank = _mpi_comm->rank() * n_feat_rank;
            size_t n_feat_added = 0;
            if (_mpi_comm->rank() < static_cast<int>(n_feat % _mpi_comm->size()))
            {
                ++n_feat_rank;
                n_feat_below_rank += _mpi_comm->rank();
            }
            else
            {
                n_feat_below_rank += n_feat % _mpi_comm->size();
            }

            while ((n_feat_added < n_feat_rank) && (next_phi.size() > 0))
            {
                feat_set.push_back(next_phi.back());
                next_phi.pop_back();
                ++n_feat_added;
            }

            // This can be calculated without an all_gather, using it to not introduce too many things at one time
            std::vector<size_t> next_phi_needed(_mpi_comm->size());
            std::vector<size_t> next_phi_excess(_mpi_comm->size());
            if (_mpi_comm->rank() == 0)
            {
                mpi::gather(*_mpi_comm, next_phi.size(), next_phi_excess.data(), 0);
                mpi::gather(*_mpi_comm, n_feat_rank - n_feat_added, next_phi_needed.data(), 0);

                mpi::broadcast(*_mpi_comm, next_phi_excess.data(), next_phi_excess.size(), 0);
                mpi::broadcast(*_mpi_comm, next_phi_needed.data(), next_phi_needed.size(), 0);
            }
            else
            {
                mpi::gather(*_mpi_comm, next_phi.size(), 0);
                mpi::gather(*_mpi_comm, n_feat_rank - n_feat_added, 0);

                mpi::broadcast(*_mpi_comm, next_phi_excess.data(), next_phi_excess.size(), 0);
                mpi::broadcast(*_mpi_comm, next_phi_needed.data(), next_phi_needed.size(), 0);
            }

            std::vector<size_t> send_sizes(next_phi_sizes.size(), 0);
            std::vector<size_t> recv_sizes(next_phi_sizes.size(), 0);
            // Is this rank sending or receiving?
            if (next_phi_excess[_mpi_comm->rank()] > 0)
            {
                size_t total_sent = std::accumulate(
                    next_phi_excess.begin(), next_phi_excess.begin() + _mpi_comm->rank(), 0);
                size_t prev_sent_recv = 0;
                size_t send_size = 0;
                int ind = 0;
                while ((prev_sent_recv <= total_sent) && (ind < _mpi_comm->size()))
                {
                    prev_sent_recv += next_phi_needed[ind];
                    ++ind;
                }
                send_size = std::min(next_phi.size(), prev_sent_recv - total_sent);
                send_sizes[ind - 1] = send_size;
                total_sent = send_size;
                while ((total_sent < next_phi.size()) && (ind < _mpi_comm->size()))
                {
                    send_size = std::min(next_phi.size() - total_sent, next_phi_needed[ind]);
                    send_sizes[ind] = send_size;
                    total_sent += send_size;
                    ++ind;
                }

                total_sent = 0;
                for (size_t pp = 0; pp < send_sizes.size(); ++pp)
                {
                    if (send_sizes[pp] == 0) continue;

                    std::vector<node_ptr> to_send(send_sizes[pp]);
                    std::copy_n(next_phi.begin() + total_sent, send_sizes[pp], to_send.begin());
                    _mpi_comm->send(
                        pp, _mpi_comm->cantor_tag_gen(_mpi_comm->rank(), pp, 1, 0), to_send);
                    total_sent += send_sizes[pp];
                }
            }
            else
            {
                size_t total_recv = std::accumulate(
                    next_phi_needed.begin(), next_phi_needed.begin() + _mpi_comm->rank(), 0);
                size_t prev_recv_sent = 0;
                size_t recv_size = 0;
                int ind = 0;
                while ((prev_recv_sent <= total_recv) && (ind < _mpi_comm->size()))
                {
                    prev_recv_sent += next_phi_excess[ind];
                    ++ind;
                }
                recv_size = std::min(n_feat_rank - n_feat_added, prev_recv_sent - total_recv);
                recv_sizes[ind - 1] = recv_size;
                total_recv = recv_size;
                while ((total_recv < n_feat_rank - n_feat_added) && (ind < _mpi_comm->size()))
                {
                    recv_size = std::min(n_feat_rank - n_feat_added - total_recv,
                                         next_phi_excess[ind]);
                    recv_sizes[ind] = recv_size;
                    total_recv += recv_size;
                    ++ind;
                }

                total_recv = 0;
                for (size_t pp = 0; pp < recv_sizes.size(); ++pp)
                {
                    if (recv_sizes[pp] == 0)
                    {
                        continue;
                    }

                    std::vector<node_ptr> to_recv;
                    _mpi_comm->recv(
                        pp, _mpi_comm->cantor_tag_gen(pp, _mpi_comm->rank(), 1, 0), to_recv);
                    for (auto& feat : to_recv)
                    {
                        feat_set.push_back(feat);
                    }
                }
            }
            for (size_t ff = start_rung.back(); ff < feat_set.size(); ++ff)
            {
                feat_set[ff]->reindex(ff + n_feat_below_rank, ff);
            }

#ifdef PARAMETERIZE
            if (!reparam)
            {
                int no_param_sz = reorder_by_n_params(feat_set, start_rung.back());
                _end_no_params.push_back(no_param_sz);
            }
#endif
        }
    }
}

void FeatureSpace::generate_and_project(std::shared_ptr<LossFunction> loss,
                                        std::vector<node_sc_pair>& phi_sc_sel,
                                        const std::vector<int>& scores_selected_sort_inds)
{
    std::vector<double> scores_sel_all(node_value_arrs::N_SELECTED);
    if (node_value_arrs::N_SELECTED > _n_sis_select)
    {
        scores_sel_all.resize(_phi_selected.size());
        project_funcs::project_loss(loss, _phi_selected, scores_sel_all.data());
    }
    std::transform(phi_sc_sel.begin(),
                   phi_sc_sel.end(),
                   &scores_sel_all[node_value_arrs::N_SELECTED - _n_sis_select],
                   [](node_sc_pair sc_pair) { return sc_pair.score(); });
    int worst_score_ind = std::max_element(phi_sc_sel.begin(), phi_sc_sel.end()) -
                          phi_sc_sel.begin();
    double worst_score = phi_sc_sel[worst_score_ind].score();

#pragma omp declare reduction(merge_nodes : std::vector<node_sc_pair> : omp_out=openmp_red::merge<node_sc_pair>(omp_out, omp_in)) initializer(omp_priv = omp_orig)
    unsigned int num_valid_feats = phi_sc_sel.size();

#pragma omp parallel firstprivate(worst_score, worst_score_ind, scores_sel_all) shared(phi_sc_sel)
    {
        std::shared_ptr<LossFunction> loss_copy = loss_function_util::copy(loss);
        int index_base = _phi.size() +
                         2 * _n_sis_select * (omp_get_thread_num() + _mpi_comm->size());
        unsigned int num_valid_feats_private = num_valid_feats;

#ifdef PARAMETERIZE
        std::shared_ptr<NLOptimizer> optimizer = nullptr;
        std::shared_ptr<NLOptimizer> reparam_optimizer = nullptr;
        if (_allowed_param_ops.size() > 0)
        {
            optimizer = nlopt_wrapper::get_optimizer(
                _project_type, _task_sizes_train, _prop_train, _max_rung, _max_param_depth);
        }

        if (_reparam_residual)
        {
            std::vector<double> prop_vec(loss_copy->prop_project());
            reparam_optimizer = nlopt_wrapper::get_optimizer(
                _project_type, _task_sizes_train, prop_vec, _max_rung, _max_param_depth);
        }
#endif

        const size_t start = _start_rung.back() + _mpi_comm->rank();
        const size_t end = _phi.size();
        const size_t inc = _mpi_comm->size();
#ifdef OMP45
#pragma omp for schedule(monotonic : dynamic) reduction(merge_nodes : phi_sc_sel)
#else
#pragma omp for schedule(dynamic) reduction(merge_nodes : phi_sc_sel)
#endif
        for (auto idx = start; idx < end; idx += inc)
        {
            auto feat = _phi.begin() + idx;
            unsigned long int feat_ind = _phi.size() +
                                         2 * _n_sis_select *
                                             (omp_get_num_threads() + _mpi_comm->size());

            node_value_arrs::clear_temp_reg_thread();
            std::vector<node_ptr> generated_phi;

            bool is_sel = (*feat)->selected();
            (*feat)->set_selected(false);
#ifdef PARAMETERIZE
            generate_non_param_feats(
                feat, generated_phi, _phi.begin(), feat_ind, _l_bound, _u_bound);
            generate_param_feats(
                feat, generated_phi, _phi.begin(), feat_ind, optimizer, _l_bound, _u_bound);
            if (reparam_optimizer && (feat < _phi.begin() + _end_no_params.back()))
            {
                generate_reparam_feats(
                    feat, generated_phi, feat_ind, reparam_optimizer, _l_bound, _u_bound);
            }
#else
            generate_non_param_feats(
                feat, generated_phi, _phi.begin(), feat_ind, _l_bound, _u_bound);
#endif
            (*feat)->set_selected(is_sel);

            if (generated_phi.size() == 0)
            {
                continue;
            }

            node_value_arrs::clear_temp_reg_thread();
            std::vector<double> scores(generated_phi.size());
            project_funcs::project_loss_no_omp(loss_copy, generated_phi, scores.data());

            std::vector<int> inds = util_funcs::argsort<double>(scores);

            size_t ii = 0;
            while ((ii < inds.size()) && (scores[inds[ii]] < (-1.0 - 1e-10)))
            {
                ++ii;
            }

            while ((ii < inds.size()) &&
                   ((scores[inds[ii]] < worst_score) || (phi_sc_sel.size() < _n_sis_select)))
            {
                node_value_arrs::clear_temp_reg_thread();
                double cur_score = scores[inds[ii]];
                int valid_feat = _is_valid(generated_phi[inds[ii]]->stand_value_ptr(),
                                           _n_samp_train,
                                           _cross_cor_max,
                                           scores_sel_all,
                                           scores_selected_sort_inds,
                                           cur_score,
                                           node_value_arrs::N_SELECTED - _n_sis_select,
                                           0);
                if (valid_feat > 0)
                {
                    valid_feat = _is_valid_feat_list(generated_phi[inds[ii]]->stand_value_ptr(),
                                                     _n_samp_train,
                                                     _cross_cor_max,
                                                     phi_sc_sel,
                                                     cur_score);

                    if ((valid_feat > 0) && (num_valid_feats_private >= _n_sis_select))
                    {
                        generated_phi[inds[ii]]->reindex(index_base + worst_score_ind);
                        phi_sc_sel[worst_score_ind] = node_sc_pair(generated_phi[inds[ii]],
                                                                   cur_score);
                    }
                    else if (valid_feat != 0)
                    {
                        num_valid_feats_private += valid_feat > 0;
                        generated_phi[inds[ii]]->reindex(index_base + phi_sc_sel.size());
                        phi_sc_sel.push_back(node_sc_pair(generated_phi[inds[ii]], cur_score));
                    }
                    worst_score_ind = std::max_element(phi_sc_sel.begin(), phi_sc_sel.end()) -
                                      phi_sc_sel.begin();
                    worst_score = phi_sc_sel[worst_score_ind].score();
                }
                ++ii;
            }
        }
    }
    // Go through all features and remove any that were valid, but no longer are due to new better features appearing. If _cross_cor_max == 1.0 then this is not necessary
    int index_base = _phi.size() + 2 * _n_sis_select + _mpi_comm->rank();
    std::vector<int> inds = util_funcs::argsort(phi_sc_sel);

    std::vector<node_sc_pair> phi_sel_copy(phi_sc_sel);
    phi_sc_sel.clear();
    phi_sc_sel.push_back(phi_sel_copy[inds[0]]);

    size_t ii = 1;
    while ((ii < inds.size()) && (phi_sc_sel.size() < _n_sis_select))
    {
        node_value_arrs::clear_temp_reg_thread();

        if (_is_valid_feat_list(phi_sel_copy[inds[ii]].obj()->stand_value_ptr(),
                                _n_samp_train,
                                _cross_cor_max,
                                phi_sc_sel,
                                phi_sel_copy[inds[ii]].score()) > 0)
        {
            phi_sc_sel.push_back(phi_sel_copy[inds[ii]]);
            if (phi_sc_sel.back().obj()->rung() == _max_rung)
            {
                phi_sc_sel.back().obj()->reindex(index_base + phi_sc_sel.size());
            }
        }
        ++ii;
    }
}

void FeatureSpace::sis(const std::vector<double>& prop)
{
    sis(loss_function_util::get_loss_function(_project_type,
                                              prop,
                                              {},
                                              _task_sizes_train,
                                              std::vector<int>(_task_sizes_train.size(), 0),
                                              false));
}

void FeatureSpace::sis(std::shared_ptr<LossFunction> loss)
{
    if (_n_sis_select == 0)
    {
        std::cerr << "WARNING: n_sis_select was previously set to 0. All features already "
                     "selected, SIS will do nothing."
                  << std::endl;
        return;
    }
#ifdef PARAMETERIZE
    // Reparameterize for the residuals
    if (_reparam_residual && (_phi_selected.size() > 0))
    {
        _phi.resize(_n_feat);
        _phi_reparam.resize(0);
        _start_rung_reparam.resize(0);
        generate_feature_space(_phi_reparam, _start_rung_reparam, loss->prop_project(), true);
        _phi.insert(_phi.end(), _phi_reparam.begin(), _phi_reparam.end());
        _scores.resize(_phi.size());
    }
#endif

    // Create output directories if needed
    boost::filesystem::path p(_feature_space_file.c_str());
    boost::filesystem::create_directories(p.remove_filename());

    // Set up output file generation
    std::ofstream out_file_stream = std::ofstream();
    out_file_stream.open(_feature_space_file, std::ios::app);

    std::ofstream sum_file_stream = std::ofstream();
    sum_file_stream.open(_feature_space_summary_file, std::ios::app);

    // Set up selection data structures
    std::vector<node_ptr> phi_sel;
    std::vector<double> scores_sel(_n_sis_select, std::numeric_limits<double>::infinity());
    phi_sel.reserve(_n_sis_select);

    unsigned int cur_feat = node_value_arrs::N_SELECTED;
    node_value_arrs::resize_d_matrix_arr(_n_sis_select);
    _phi_selected.reserve(_phi_selected.size() + _n_sis_select);

    // Get projection scores
    double start_time = omp_get_wtime();
    project_funcs::project_loss(loss, _phi, _scores.data());
    _mpi_comm->barrier();
    if (_mpi_comm->rank() == 0)
    {
        std::cout << "Projection time: " << omp_get_wtime() - start_time << " s" << std::endl;
    }

    // Sort the scores to get inds
    std::vector<int> inds = util_funcs::argsort<double>(_scores);
    size_t ii = 0;
    unsigned int cur_feat_local = 0;

    // Advance the the first valid score
    while (_scores[inds[ii]] < -1.0000000001)
    {
        ++ii;
    }

    // Get the scores of the previously selected features to facilitate the comparison
    std::vector<double> scores_sel_all(node_value_arrs::N_SELECTED, 0.0);
    std::vector<int> scores_sel_inds_sorted(node_value_arrs::N_SELECTED, 0);
    std::iota(scores_sel_inds_sorted.begin(), scores_sel_inds_sorted.end(), 0);
    if (node_value_arrs::N_SELECTED > _n_sis_select)
    {
        project_funcs::project_loss(loss, _phi_selected, scores_sel_all.data());
        util_funcs::argsort<double>(
            scores_sel_inds_sorted.data(),
            &scores_sel_inds_sorted.data()[node_value_arrs::N_SELECTED - _n_sis_select],
            scores_sel_all.data());
    }

    // Get the best features currently generated on this rank
    start_time = omp_get_wtime();
    while ((cur_feat_local != _n_sis_select) && (ii < _scores.size()))
    {
        if (_is_valid(_phi[inds[ii]]->stand_value_ptr(),
                      _n_samp_train,
                      _cross_cor_max,
                      scores_sel_all,
                      scores_sel_inds_sorted,
                      _scores[inds[ii]],
                      cur_feat + cur_feat_local,
                      0))
        {
            scores_sel[cur_feat_local] = _scores[inds[ii]];
            scores_sel_all[cur_feat + cur_feat_local] = _scores[inds[ii]];
            phi_sel.push_back(_phi[inds[ii]]);
            phi_sel.back()->set_selected(true);
            phi_sel.back()->set_d_mat_ind(cur_feat + cur_feat_local);
            phi_sel.back()->set_value();
            phi_sel.back()->set_standardized_value();
            ++cur_feat_local;
        }
        ++ii;
    }

    _mpi_comm->barrier();
    if (_mpi_comm->rank() == 0)
    {
        std::cout << "Time to get best features on rank : " << omp_get_wtime() - start_time << " s"
                  << std::endl;
    }

    // Remove selection criteria
    for (auto& feat : phi_sel)
    {
        feat->set_selected(false);
        feat->set_d_mat_ind(-1);
    }
    std::vector<node_sc_pair> local_sel(_n_sis_select);
    std::transform(phi_sel.begin(),
                   phi_sel.end(),
                   scores_sel.begin(),
                   local_sel.begin(),
                   [](node_ptr feat, double sc) { return node_sc_pair(feat, sc); });

    // Generate features on the fly and get the best features for this rank
    if (_n_rung_generate > 0)
    {
#ifdef PARAMETERIZE
        _phi.resize(_n_feat);
        _phi.insert(
            _phi.end(), _phi_reparam.begin() + _start_rung_reparam.back(), _phi_reparam.end());
        _scores.resize(_phi.size());
#endif
        start_time = omp_get_wtime();

        local_sel.resize(cur_feat_local);
        generate_and_project(loss, local_sel, scores_sel_inds_sorted);

        _mpi_comm->barrier();
        if (_mpi_comm->rank() == 0)
        {
            std::cout << "Projection time for features generated on the fly: "
                      << omp_get_wtime() - start_time << " s" << std::endl;
        }
    }
    // Reset scores
    std::fill_n(&scores_sel_all[cur_feat], _n_sis_select, 0.0);

    // If we are only on one process then phi_sel are the selected features
    start_time = omp_get_wtime();
    if (_mpi_comm->size() > 1)
    {
        local_sel.resize(_n_sis_select, node_sc_pair());
        // Collect the best features from all ranks
        std::vector<node_sc_pair> selected(_n_sis_select);
        mpi::all_reduce(*_mpi_comm, local_sel, selected, mpi_reduce_op::select_top_feats);

        // Move the selected features to _phi_selected and set the value properly
        cur_feat_local = 0;
        for (auto& sel : selected)
        {
            _phi_selected.push_back(sel.obj());
            _phi_selected.back()->set_selected(true);
            _phi_selected.back()->set_d_mat_ind(cur_feat);
            _phi_selected.back()->set_value();
            _phi_selected.back()->set_standardized_value();
            ++cur_feat_local;
            ++cur_feat;
        }

        // Output the information about the selected features
        if (_mpi_comm->rank() == 0)
        {
            double prefact = std::pow(-1, (_project_type != "classification"));
            cur_feat -= cur_feat_local;
            for (auto& sel : selected)
            {
                out_file_stream << std::setw(14) << std::left << cur_feat
                                << _phi_selected[cur_feat]->postfix_expr() << std::endl;
                sum_file_stream << std::setw(14) << std::left << cur_feat << std::setw(24)
                                << std::setprecision(18) << std::left << prefact * sel.score();
                sum_file_stream << _phi_selected[cur_feat]->expr() << std::endl;

                ++cur_feat;
            }
        }
        // Ensure that enough features were selected
        if (selected.size() < _n_sis_select)
        {
            if (!_override_n_sis_select)
            {
                throw std::logic_error("SIS went through all features and did not select enough (" +
                                       std::to_string(selected.size()) + " not " +
                                       std::to_string(_n_sis_select) + ").");
            }
            else
            {
                node_value_arrs::resize_d_matrix_arr(selected.size() - _n_sis_select);
                _n_sis_select = selected.size();
                std::cerr << "WARNING: The size of the SIS selection subspace is changing to "
                          << _n_sis_select
                          << ". This can create problems with hyperparameter optimization."
                          << std::endl;
            }
        }
    }
    else
    {
        // Ensure that enough features were selected
        if (local_sel.size() < _n_sis_select)
        {
            if (!_override_n_sis_select)
            {
                throw std::logic_error("SIS went through all features and did not select enough (" +
                                       std::to_string(local_sel.size()) + " not " +
                                       std::to_string(_n_sis_select) + ").");
            }
            else
            {
                node_value_arrs::resize_d_matrix_arr(local_sel.size() - _n_sis_select);
                _n_sis_select = local_sel.size();
                std::cerr << "WARNING: The size of the SIS selection subspace is changing to "
                          << _n_sis_select
                          << ". This can create problems with hyperparameter optimization."
                          << std::endl;
            }
        }

        cur_feat_local = 0;
        // Move selected features into _phi_selected and add the features to the output files
        inds = util_funcs::argsort<node_sc_pair>(local_sel);
        double prefact = std::pow(-1, (_project_type != "classification"));
        for (auto& ind : inds)
        {
            if (!local_sel[ind].obj())
            {
                if (!_override_n_sis_select)
                {
                    throw std::logic_error(
                        "SIS went through all features and did not select enough (" +
                        std::to_string(cur_feat_local) + " not " + std::to_string(_n_sis_select) +
                        ").");
                }
                else
                {
                    node_value_arrs::resize_d_matrix_arr(cur_feat_local - _n_sis_select);
                    _n_sis_select = cur_feat_local;
                    std::cerr << "WARNING: The size of the SIS selection subspace is changing to "
                              << _n_sis_select
                              << ". This can create problems with hyperparameter optimization."
                              << std::endl;
                    break;
                }
            }
            node_value_arrs::clear_temp_reg();
            out_file_stream << std::setw(14) << std::left << cur_feat
                            << local_sel[ind].obj()->postfix_expr() << std::endl;
            sum_file_stream << std::setw(14) << std::left << cur_feat << std::setw(24)
                            << std::setprecision(18) << std::left
                            << prefact * local_sel[ind].score();
            sum_file_stream << local_sel[ind].obj()->expr() << std::endl;

            _phi_selected.push_back(local_sel[ind].obj());
            _phi_selected.back()->set_selected(true);
            _phi_selected.back()->set_d_mat_ind(cur_feat);
            _phi_selected.back()->set_value();
            _phi_selected.back()->set_standardized_value();

            ++cur_feat;
            ++cur_feat_local;
        }
    }

    if (cur_feat != node_value_arrs::N_SELECTED)
    {
        if (!_override_n_sis_select)
        {
            throw std::logic_error("SIS went through all features and did not select enough (" +
                                   std::to_string(cur_feat) + " not " +
                                   std::to_string(_n_sis_select) + ").");
        }
        else
        {
            node_value_arrs::resize_d_matrix_arr(cur_feat - _n_sis_select);
            _n_sis_select = cur_feat;
            std::cerr << "WARNING: The size of the SIS selection subspace is changing to "
                      << _n_sis_select
                      << ". This can create problems with hyperparameter optimization."
                      << std::endl;
        }
    }

    if (_mpi_comm->rank() == 0)
    {
        std::cout << "Complete final combination/selection from all ranks: "
                  << omp_get_wtime() - start_time << " s" << std::endl;
        out_file_stream << "#";
        for (int dd = 0; dd < 71; ++dd)
        {
            out_file_stream << "-";
        }
        out_file_stream << std::endl;
        out_file_stream.close();

        sum_file_stream << "#";
        for (int dd = 0; dd < 71; ++dd)
        {
            sum_file_stream << "-";
        }
        sum_file_stream << std::endl;
        sum_file_stream.close();
    }
}

void FeatureSpace::remove_feature(const int ind)
{
    if (_phi[ind]->selected())
    {
        throw std::logic_error("Can't remove a selected feature");
    }

    _phi.erase(_phi.begin() + ind);
    --_n_feat;
}

void FeatureSpace::output_phi()
{
    if (str_utils::split_string_trim(_phi_out_file, "/").size() > 1)
    {
        boost::filesystem::path p(_phi_out_file.c_str());
        boost::filesystem::create_directories(p.remove_filename());
    }

    std::ofstream out_file_stream = std::ofstream();
    out_file_stream.open(_phi_out_file);

    out_file_stream << "# Number of Features: " << _n_feat << std::endl;
    out_file_stream << "# Maximum Rung of the Calculation: " << _max_rung << std::endl;

    for (auto& feat : _phi)
    {
        out_file_stream << feat->postfix_expr() << std::endl;
    }
    out_file_stream.close();
}
