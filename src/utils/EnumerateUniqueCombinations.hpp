// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#pragma once

#include <fmt/format.h>

#include <algorithm>
#include <cassert>
#include <vector>

#include "math_funcs.hpp"

/**
 * Class to enumerate a multidimensional index space.
 *
 * The index range is defined by 0..max_index. This class will iterate over all possible
 * num_dimensional tuples. The tuples are sorted and the indices are unique and always valid.
 */
class EnumerateUniqueCombinations
{
public:
    EnumerateUniqueCombinations(const int max_index, const int num_dimensions)
        : _max_index(max_index),
          _num_dimensions(num_dimensions),
          _num_total_combinations(1),
          _indices(num_dimensions)
    {
        if (num_dimensions <= 0)
        {
            throw std::invalid_argument(fmt::format(
                "EnumerateUniqueCombinations: number of dimensions ({}) has to be larger than 0!",
                num_dimensions));
        }
        if (max_index < num_dimensions - 1)
        {
            throw std::invalid_argument(
                fmt::format("EnumerateUniqueCombinations: index range ({}) is too small for the "
                            "requested number of dimensions ({})!",
                            max_index,
                            num_dimensions));
        }
        auto idx = max_index;
        int dim_factor = 1;
        for (auto i = 0; i < num_dimensions; ++i, --idx)
        {
            assert(idx >= 0);
            _indices[i] = idx;
            _num_total_combinations *= (idx + 1);
            dim_factor *= (i + 1);
        }
        _num_total_combinations /= dim_factor;
    }

    EnumerateUniqueCombinations& operator+=(const int inc)
    {
        _finished = !increment(_num_dimensions, inc);
        if (_finished)
        {
            for (auto i = 0; i < _num_dimensions; ++i)
            {
                _indices[i] = _num_dimensions - 1 - i;
                assert(_indices[i] >= 0);
            }
        }
        return *this;
    }

    /// prefix increment
    EnumerateUniqueCombinations& operator++()
    {
        *this += 1;
        return *this;
    }

    /// postfix increment
    EnumerateUniqueCombinations operator++(int)
    {
        auto old = *this;
        operator++();
        return old;
    }

    auto get_num_total_combinations() const { return _num_total_combinations; }
    auto is_finished() const { return _finished; }
    const auto& get_current_combination() const { return _indices; }

private:
    int _max_index;
    int _num_dimensions;
    int _num_total_combinations;
    std::vector<int> _indices;
    bool _finished = false;

    bool increment(int64_t size, int64_t inc)
    {
        if ((size == 1) && (_indices[0] < inc))
        {
            return false;
        }

        _indices[size - 1] -= inc;
        bool cont = true;
        if (_indices[size - 1] < 0)
        {
            while (cont && (_indices[size - 1] < 0))
            {
                cont = increment(size - 1, 1);
                _indices[size - 1] += _indices[size - 2];
            }
        }
        return cont;
    }
};
