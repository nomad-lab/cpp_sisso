// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file utils/compare_features.hpp
 *  @brief Implements a set of functions to compare features to see if they are too correlated to other selected features to be selected here
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "utils/compare_features.hpp"
std::vector<double> comp_feats::DGEMV_OUT;
std::vector<double> comp_feats::RANK;
std::vector<int> comp_feats::INDEX;

void comp_feats::set_is_valid_fxn(
    const std::string project_type,
    const double max_corr,
    const int n_samp,
    std::function<bool(const double*,
                       const int,
                       const double,
                       const std::vector<double>&,
                       const std::vector<int>&,
                       const double,
                       const int,
                       const int)>& is_valid,
    std::function<int(
        const double*, const int, const double, const std::vector<node_sc_pair>&, const double)>&
        is_valid_feat_list)
{
    if (project_type.compare("classification") != 0)
    {
        if (max_corr < 0.99999)
        {
            DGEMV_OUT.resize(n_samp);
            is_valid = valid_feature_against_selected_pearson;
            is_valid_feat_list = valid_feature_against_selected_pearson_feat_list;
        }
        else
        {
            is_valid = valid_feature_against_selected_pearson_max_corr_1;
            is_valid_feat_list = valid_feature_against_selected_pearson_max_corr_1_feat_list;
        }
    }
    else
    {
        // Resize the rank and index to fit all of the data for each thread with padding
        RANK.resize(4 * omp_get_max_threads() * n_samp);
        INDEX.resize(2 * omp_get_max_threads() * n_samp);
        if (max_corr < 0.99999)
        {
            is_valid = valid_feature_against_selected_spearman;
            is_valid_feat_list = valid_feature_against_selected_spearman_feat_list;
        }
        else
        {
            is_valid = valid_feature_against_selected_spearman_max_corr_1;
            is_valid_feat_list = valid_feature_against_selected_spearman_max_corr_1_feat_list;
        }
    }
}

void comp_feats::reset_vectors()
{
    RANK.resize(0);
    INDEX.resize(0);
}

bool comp_feats::valid_feature_against_selected_pearson_max_corr_1(
    const double* val_ptr,
    const int n_samp,
    const double cross_cor_max,
    const std::vector<double>& scores_sel,
    const std::vector<int>& sorted_score_inds,
    const double cur_score,
    const int end_sel,
    const int start_sel)
{
    double base_val = std::inner_product(val_ptr, val_ptr + n_samp, val_ptr, 0.0);

    int ii = std::find_if(sorted_score_inds.begin() + start_sel,
                          sorted_score_inds.begin() + end_sel,
                          [&](int ind) { return std::abs(scores_sel[ind] - cur_score) < 1e-5; }) -
             sorted_score_inds.begin();

    while ((ii < end_sel) && (std::abs(cur_score - scores_sel[sorted_score_inds[ii]]) < 1e-5))
    {
        double comp_value = 1.0 / static_cast<double>(n_samp) *
                            (base_val -
                             std::abs(std::inner_product(
                                 val_ptr,
                                 val_ptr + n_samp,
                                 node_value_arrs::get_stand_d_matrix_ptr(sorted_score_inds[ii]),
                                 0.0)));

        if (std::abs(comp_value) < 5.0e-9)
        {
            return false;
        }
        ++ii;
    }

    return true;
}

int comp_feats::valid_feature_against_selected_pearson_max_corr_1_feat_list(
    const double* val_ptr,
    const int n_samp,
    const double cross_cor_max,
    const std::vector<node_sc_pair>& scores_phi_sel,
    const double cur_score)
{
    double base_val = std::inner_product(val_ptr, val_ptr + n_samp, val_ptr, 0.0);

    for (size_t ff = 0; ff < scores_phi_sel.size(); ++ff)
    {
        if (std::abs(cur_score - scores_phi_sel[ff].score()) > 1e-5)
        {
            continue;
        }

        double comp_value = 1.0 / static_cast<double>(n_samp) *
                            (base_val - std::abs(std::inner_product(
                                            val_ptr,
                                            val_ptr + n_samp,
                                            scores_phi_sel[ff].obj()->stand_value_ptr(true),
                                            0.0)));

        if (std::abs(comp_value) < 5.0e-9)
        {
            return 0;
        }
    }
    return 1;
}

bool comp_feats::valid_feature_against_selected_pearson_max_corr_1_mpi_op(
    const double* val_ptr,
    const int n_samp,
    const double cross_cor_max,
    const std::vector<node_sc_pair>& out_vec,
    const double cur_score)
{
    double base_val = std::inner_product(val_ptr, val_ptr + n_samp, val_ptr, 0.0);

    for (auto& feat_sc : out_vec)
    {
        if (abs(cur_score - feat_sc.score()) > 1e-5)
        {
            continue;
        }

        double comp_value = 1.0 / static_cast<double>(n_samp) *
                            (base_val -
                             std::abs(std::inner_product(val_ptr,
                                                         val_ptr + n_samp,
                                                         feat_sc.obj()->stand_value_ptr(true),
                                                         0.0)));

        if (std::abs(comp_value) < 5.0e-9)
        {
            return false;
        }
    }
    return true;
}

bool comp_feats::valid_feature_against_selected_pearson(const double* val_ptr,
                                                        const int n_samp,
                                                        const double cross_cor_max,
                                                        const std::vector<double>& scores_sel,
                                                        const std::vector<int>& sorted_score_inds,
                                                        const double cur_score,
                                                        const int end_sel,
                                                        const int start_sel)
{
    if (end_sel <= start_sel)
    {
        return true;
    }

    DGEMV_OUT.resize(end_sel - start_sel);
    dgemv_('T',
           n_samp,
           DGEMV_OUT.size(),
           1.0 / static_cast<double>(n_samp),
           node_value_arrs::get_stand_d_matrix_ptr(start_sel),
           n_samp,
           val_ptr,
           1,
           0.0,
           DGEMV_OUT.data(),
           1);

    return std::abs(DGEMV_OUT[idamax_(DGEMV_OUT.size(), DGEMV_OUT.data(), 1) - 1]) <= cross_cor_max;
}

int comp_feats::valid_feature_against_selected_pearson_feat_list(
    const double* val_ptr,
    const int n_samp,
    const double cross_cor_max,
    const std::vector<node_sc_pair>& scores_phi_sel,
    const double cur_score)
{
    int is_valid = 1;
    double comp_value = 1.0;
    for (size_t ff = 0; ff < scores_phi_sel.size(); ++ff)
    {
        comp_value = 1.0 / static_cast<double>(n_samp) *
                     std::abs(std::inner_product(val_ptr,
                                                 val_ptr + n_samp,
                                                 scores_phi_sel[ff].obj()->stand_value_ptr(true),
                                                 0.0));

        if ((comp_value > cross_cor_max) && (cur_score > scores_phi_sel[ff].score()))
        {
            return 0;
        }
        is_valid -= 2 * (comp_value > cross_cor_max);
    }

    return is_valid;
}

bool comp_feats::valid_feature_against_selected_pearson_mpi_op(
    const double* val_ptr,
    const int n_samp,
    const double cross_cor_max,
    const std::vector<node_sc_pair>& out_vec,
    const double cur_score)
{
    for (auto& feat_sc : out_vec)
    {
        double comp_value = 1.0 / static_cast<double>(n_samp) *
                            std::abs(std::inner_product(val_ptr,
                                                        val_ptr + n_samp,
                                                        feat_sc.obj()->stand_value_ptr(true),
                                                        0.0));

        if (comp_value > cross_cor_max)
        {
            return false;
        }
    }
    return true;
}

bool comp_feats::valid_feature_against_selected_spearman_max_corr_1(
    const double* val_ptr,
    const int n_samp,
    const double cross_cor_max,
    const std::vector<double>& scores_sel,
    const std::vector<int>& sorted_score_inds,
    const double cur_score,
    const int end_sel,
    const int start_sel)
{
    double base_val = std::abs(
        util_funcs::spearman_r(val_ptr,
                               val_ptr,
                               &RANK[omp_get_thread_num() * 4 * n_samp],
                               &RANK[(omp_get_thread_num() * 4 + 2) * n_samp],
                               &INDEX[omp_get_thread_num() * 2 * n_samp],
                               n_samp));
    int ii = std::find_if(sorted_score_inds.begin() + start_sel,
                          sorted_score_inds.begin() + end_sel,
                          [&](int ind) {
                              return std::abs(std::floor(scores_sel[ind]) - std::floor(cur_score)) <
                                     1e-5;
                          }) -
             sorted_score_inds.begin();

    while ((ii < end_sel) &&
           (std::abs(std::floor(scores_sel[sorted_score_inds[ii]]) - std::floor(cur_score)) < 1e-5))
    {
        // Rank the new variable and take the Pearson correlation of the rank variables (val_ptr rank still in &RANK[(omp_get_thread_num() * 4 + 2) * n_samp])
        util_funcs::rank(node_value_arrs::get_d_matrix_ptr(sorted_score_inds[ii]),
                         &RANK[omp_get_thread_num() * 4 * n_samp],
                         &INDEX[omp_get_thread_num() * 2 * n_samp],
                         n_samp);
        double comp_value = (base_val -
                             std::abs(util_funcs::r(&RANK[omp_get_thread_num() * 4 * n_samp],
                                                    &RANK[(omp_get_thread_num() * 4 + 2) * n_samp],
                                                    n_samp)));
        if (std::abs(comp_value) < 5.0e-9)
        {
            return false;
        }
        ++ii;
    }
    return true;
}

int comp_feats::valid_feature_against_selected_spearman_max_corr_1_feat_list(
    const double* val_ptr,
    const int n_samp,
    const double cross_cor_max,
    const std::vector<node_sc_pair>& scores_phi_sel,
    const double cur_score)
{
    double base_val = std::abs(
        util_funcs::spearman_r(val_ptr,
                               val_ptr,
                               &RANK[omp_get_thread_num() * 4 * n_samp],
                               &RANK[(omp_get_thread_num() * 4 + 2) * n_samp],
                               &INDEX[omp_get_thread_num() * 2 * n_samp],
                               n_samp));

    for (size_t ff = 0; ff < scores_phi_sel.size(); ++ff)
    {
        if (std::abs(std::floor(cur_score) - std::floor(scores_phi_sel[ff].score())) > 1e-5)
        {
            continue;
        }

        // Rank the new variable and take the Pearson correlation of the rank variables (val_ptr rank still in &RANK[(omp_get_thread_num() * 4 + 2) * n_samp])
        util_funcs::rank(scores_phi_sel[ff].obj()->value_ptr(-1, true),
                         &RANK[omp_get_thread_num() * 4 * n_samp],
                         &INDEX[omp_get_thread_num() * 2 * n_samp],
                         n_samp);
        double comp_value = (base_val -
                             std::abs(util_funcs::r(&RANK[omp_get_thread_num() * 4 * n_samp],
                                                    &RANK[(omp_get_thread_num() * 4 + 2) * n_samp],
                                                    n_samp)));
        if (std::abs(comp_value) < 5.0e-9)
        {
            return 0;
        }
    }
    return 1;
}

bool comp_feats::valid_feature_against_selected_spearman_max_corr_1_mpi_op(
    const double* val_ptr,
    const int n_samp,
    const double cross_cor_max,
    const std::vector<node_sc_pair>& out_vec,
    const double cur_score)
{
    double base_val = std::abs(
        util_funcs::spearman_r(val_ptr,
                               val_ptr,
                               &RANK[omp_get_thread_num() * 4 * n_samp],
                               &RANK[(omp_get_thread_num() * 4 + 2) * n_samp],
                               &INDEX[omp_get_thread_num() * 2 * n_samp],
                               n_samp));

    for (auto& feat_sc : out_vec)
    {
        if (abs(std::floor(cur_score) - std::floor(feat_sc.score())) > 1e-5)
        {
            continue;
        }

        util_funcs::rank(feat_sc.obj()->value_ptr(-1, true),
                         &RANK[omp_get_thread_num() * 4 * n_samp],
                         &INDEX[omp_get_thread_num() * 2 * n_samp],
                         n_samp);
        double comp_value = (base_val -
                             std::abs(util_funcs::r(&RANK[omp_get_thread_num() * 4 * n_samp],
                                                    &RANK[(omp_get_thread_num() * 4 + 2) * n_samp],
                                                    n_samp)));
        if (std::abs(comp_value) < 5.0e-9)
        {
            return false;
        }
    }
    return true;
}

bool comp_feats::valid_feature_against_selected_spearman(const double* val_ptr,
                                                         const int n_samp,
                                                         const double cross_cor_max,
                                                         const std::vector<double>& scores_sel,
                                                         const std::vector<int>& sorted_score_inds,
                                                         const double cur_score,
                                                         const int end_sel,
                                                         const int start_sel)
{
    util_funcs::rank(val_ptr,
                     &RANK[(omp_get_thread_num() * 4 + 2) * n_samp],
                     &INDEX[omp_get_thread_num() * 2 * n_samp],
                     n_samp);
    for (int dd = start_sel; dd < end_sel; ++dd)
    {
        // Rank the new variable and take the Pearson correlation of the rank variables (val_ptr rank still in &RANK[(omp_get_thread_num() * 4 + 2) * n_samp])
        util_funcs::rank(node_value_arrs::get_d_matrix_ptr(dd),
                         &RANK[omp_get_thread_num() * 4 * n_samp],
                         &INDEX[omp_get_thread_num() * 2 * n_samp],
                         n_samp);
        double comp_value = std::abs(util_funcs::r(&RANK[omp_get_thread_num() * 4 * n_samp],
                                                   &RANK[(omp_get_thread_num() * 4 + 2) * n_samp],
                                                   n_samp));
        if (comp_value > cross_cor_max)
        {
            return false;
        }
    }
    return true;
}

int comp_feats::valid_feature_against_selected_spearman_feat_list(
    const double* val_ptr,
    const int n_samp,
    const double cross_cor_max,
    const std::vector<node_sc_pair>& scores_phi_sel,
    const double cur_score)
{
    util_funcs::rank(val_ptr,
                     &RANK[(omp_get_thread_num() * 4 + 2) * n_samp],
                     &INDEX[omp_get_thread_num() * 2 * n_samp],
                     n_samp);
    int is_valid = 1;
    for (size_t ff = 0; ff < scores_phi_sel.size(); ++ff)
    {
        // Rank the new variable and take the Pearson correlation of the rank variables (val_ptr rank still in &RANK[(omp_get_thread_num() * 4 + 2) * n_samp])
        util_funcs::rank(scores_phi_sel[ff].obj()->value_ptr(-1, true),
                         &RANK[omp_get_thread_num() * 4 * n_samp],
                         &INDEX[omp_get_thread_num() * 2 * n_samp],
                         n_samp);
        double comp_value = std::abs(util_funcs::r(&RANK[omp_get_thread_num() * 4 * n_samp],
                                                   &RANK[(omp_get_thread_num() * 4 + 2) * n_samp],
                                                   n_samp));
        if ((comp_value > cross_cor_max) && (cur_score > scores_phi_sel[ff].score()))
        {
            return 0;
        }
        is_valid -= 2 * (std::abs(comp_value) > cross_cor_max);
    }
    return is_valid;
}

bool comp_feats::valid_feature_against_selected_spearman_mpi_op(
    const double* val_ptr,
    const int n_samp,
    const double cross_cor_max,
    const std::vector<node_sc_pair>& out_vec,
    const double cur_score)
{
    util_funcs::rank(val_ptr,
                     &RANK[(omp_get_thread_num() * 4 + 2) * n_samp],
                     &INDEX[omp_get_thread_num() * 2 * n_samp],
                     n_samp);
    for (auto& feat_sc : out_vec)
    {
        util_funcs::rank(feat_sc.obj()->value_ptr(-1, true),
                         &RANK[omp_get_thread_num() * 4 * n_samp],
                         &INDEX[omp_get_thread_num() * 2 * n_samp],
                         n_samp);
        double comp_value = std::abs(util_funcs::r(&RANK[omp_get_thread_num() * 4 * n_samp],
                                                   &RANK[(omp_get_thread_num() * 4 + 2) * n_samp],
                                                   n_samp));
        if (comp_value > cross_cor_max)
        {
            return false;
        }
    }
    return true;
}
