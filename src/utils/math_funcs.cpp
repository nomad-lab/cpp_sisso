// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file utils/math_funcs.cpp
 *  @brief Implements a set of functions to get standardized mathematical operations on
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "utils/math_funcs.hpp"

bool util_funcs::iterate(std::vector<int>& inds, int size, int incriment)
{
    if ((size == 1) && (inds[0] < incriment))
    {
        return false;
    }

    inds[size - 1] -= incriment;
    bool cont = true;
    if (inds[size - 1] < 0)
    {
        while (cont && (inds[size - 1] < 0))
        {
            cont = iterate(inds, size - 1, 1);
            inds[size - 1] += inds[size - 2];
        }
    }
    return cont;
}

void util_funcs::standardize(const double* val, const int sz, double* stand_val)
{
    double avg = mean(val, sz);
    double std = stand_dev(val, sz, avg);
    std::transform(val, val + sz, stand_val, [&](double vv) { return (vv - avg) / std; });
}
