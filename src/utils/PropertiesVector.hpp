// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#pragma once

#include "external/third_party_kokkos.hpp"
#include "feature_creation/node/value_storage/nodes_value_containers.hpp"

class PropertiesVector
{
public:
    using VECTOR_TYPE = Kokkos::View<double*, Kokkos::LayoutLeft>::const_type;
    PropertiesVector(const std::vector<double>& properties)
        : d_properties_vector(Kokkos::ViewAllocateWithoutInitializing("properties_vector"),
                              properties.size())
    {
        h_properties_vector = Kokkos::create_mirror_view(d_properties_vector);

        for (int material_idx = 0; material_idx < properties.size(); ++material_idx)
        {
            h_properties_vector(material_idx) = properties[material_idx];
        }

        Kokkos::deep_copy(d_properties_vector, h_properties_vector);
    }

    VECTOR_TYPE getDevicePropertiesVector() const { return d_properties_vector; }
    VECTOR_TYPE::HostMirror getHostPropertiesVector() const { return h_properties_vector; }

private:
    VECTOR_TYPE::non_const_type d_properties_vector;
    VECTOR_TYPE::HostMirror::non_const_type h_properties_vector;
};
