// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#pragma once

#include "external/third_party_kokkos.hpp"
#include "feature_creation/node/value_storage/nodes_value_containers.hpp"

class DescriptorMatrix
{
public:
    using MATRIX_TYPE = Kokkos::View<double**, Kokkos::LayoutLeft>::const_type;
    DescriptorMatrix()
        : d_descriptor_matrix(Kokkos::ViewAllocateWithoutInitializing("descriptor_matrix"),
                              node_value_arrs::N_SAMPLES,
                              node_value_arrs::N_SELECTED)
    {
        h_descriptor_matrix = Kokkos::create_mirror_view(d_descriptor_matrix);
        for (int feature_idx = 0; feature_idx < node_value_arrs::N_SELECTED; ++feature_idx)
        {
            for (int sample_idx = 0; sample_idx < node_value_arrs::N_SAMPLES; ++sample_idx)
            {
                h_descriptor_matrix(sample_idx, feature_idx) = node_value_arrs::D_MATRIX
                    [feature_idx * node_value_arrs::N_SAMPLES + sample_idx];
            }
        }
        Kokkos::deep_copy(d_descriptor_matrix, h_descriptor_matrix);
    }

    MATRIX_TYPE getDeviceDescriptorMatrix() const { return d_descriptor_matrix; }
    MATRIX_TYPE::HostMirror getHostDescriptorMatrix() const { return h_descriptor_matrix; }

private:
    MATRIX_TYPE::non_const_type d_descriptor_matrix;
    MATRIX_TYPE::HostMirror::non_const_type h_descriptor_matrix;
};
