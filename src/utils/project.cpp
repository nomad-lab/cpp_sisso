// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file utils/project.cpp
 *  @brief Implements a set of functions to project features onto a second vector
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "utils/project.hpp"

void project_funcs::project_r(const double* prop,
                              double* scores,
                              const std::vector<node_ptr>& phi,
                              const std::vector<int>& sizes,
                              const int n_prop)
{
    int n_samp = std::accumulate(sizes.begin(), sizes.end(), 0);

#pragma omp parallel firstprivate(prop)
    {
        int start = (omp_get_thread_num() * (phi.size() / omp_get_num_threads()) +
                     std::min(omp_get_thread_num(),
                              static_cast<int>(phi.size()) % omp_get_num_threads()));
        int end = ((omp_get_thread_num() + 1) * (phi.size() / omp_get_num_threads()) +
                   std::min(omp_get_thread_num() + 1,
                            static_cast<int>(phi.size()) % omp_get_num_threads()));

        std::vector<double> mean_prop(sizes.size(), 0.0);
        std::vector<double> std_prop(sizes.size(), 0.0);
        std::fill_n(scores + start, end - start, 0.0);
        for (int pp = 0; pp < n_prop; ++pp)
        {
            int pos = 0;
            for (size_t tt = 0; tt < sizes.size(); ++tt)
            {
                mean_prop[tt] = util_funcs::mean<double>(prop + pos, sizes[tt]);
                std_prop[tt] = util_funcs::stand_dev(prop + pos, sizes[tt], mean_prop[tt]);
                pos += sizes[tt];
            }
            std::transform(phi.begin() + start,
                           phi.begin() + end,
                           scores + start,
                           scores + start,
                           [&prop, &sizes, &mean_prop, &std_prop](node_ptr feat, double sc) {
                               return std::min(
                                   sc,
                                   -1 * std::abs(util_funcs::r(prop,
                                                               feat->value_ptr(-1, true),
                                                               sizes.data(),
                                                               mean_prop.data(),
                                                               std_prop.data(),
                                                               sizes.size())));
                           });
            prop += n_samp;
        }
        std::transform(scores + start, scores + end, scores + start, [](double score) {
            return std::isnan(score) ? 0.0 : score;
        });

#pragma omp barrier
    }
}

void project_funcs::project_r_no_omp(const double* prop,
                                     double* scores,
                                     const std::vector<node_ptr>& phi,
                                     const std::vector<int>& sizes,
                                     const int n_prop)
{
    int n_samp = std::accumulate(sizes.begin(), sizes.end(), 0);

    std::vector<double> mean_prop(sizes.size(), 0.0);
    std::vector<double> std_prop(sizes.size(), 0.0);

    std::fill_n(scores, phi.size(), 0.0);

    for (int pp = 0; pp < n_prop; ++pp)
    {
        int pos = 0;
        for (size_t tt = 0; tt < sizes.size(); ++tt)
        {
            mean_prop[tt] = util_funcs::mean<double>(prop + pos, sizes[tt]);
            std_prop[tt] = util_funcs::stand_dev(prop + pos, sizes[tt], mean_prop[tt]);
            pos += sizes[tt];
        }
        std::transform(phi.begin(),
                       phi.end(),
                       scores,
                       scores,
                       [&prop, &sizes, &mean_prop, &std_prop](node_ptr feat, double sc) {
                           return std::min(sc,
                                           -1 * util_funcs::r(prop,
                                                              feat->value_ptr(-1, true),
                                                              sizes.data(),
                                                              mean_prop.data(),
                                                              std_prop.data(),
                                                              sizes.size()));
                       });
        prop += n_samp;
    }
    std::transform(scores, scores + phi.size(), scores, [](double score) {
        return std::isnan(score) ? 0.0 : score;
    });
}

void project_funcs::project_loss(std::shared_ptr<LossFunction> loss,
                                 const std::vector<node_ptr>& feats,
                                 double* scores)
{
    std::fill_n(scores, feats.size(), std::numeric_limits<double>::max());
#pragma omp parallel
    {
        std::shared_ptr<LossFunction> loss_copy;
#pragma omp critical
        {
            loss_copy = loss_function_util::copy(loss);
        }
        int start = (omp_get_thread_num() * (feats.size() / omp_get_num_threads()) +
                     std::min(omp_get_thread_num(),
                              static_cast<int>(feats.size()) % omp_get_num_threads()));

        int end = ((omp_get_thread_num() + 1) * (feats.size() / omp_get_num_threads()) +
                   std::min(omp_get_thread_num() + 1,
                            static_cast<int>(feats.size()) % omp_get_num_threads()));

        std::transform(feats.begin() + start,
                       feats.begin() + end,
                       scores + start,
                       [&loss_copy](node_ptr feat) { return loss_copy->project(feat); });

#pragma omp barrier
    }
}

void project_funcs::project_loss_no_omp(std::shared_ptr<LossFunction> loss,
                                        const std::vector<node_ptr>& feats,
                                        double* scores)
{
    std::fill_n(scores, feats.size(), std::numeric_limits<double>::max());
    std::transform(
        feats.begin(), feats.end(), scores, [&loss](node_ptr feat) { return loss->project(feat); });
}
