// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 *  @brief Wrapper for all CUDA related functionality.
 *
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#pragma once

#include <fmt/format.h>

#include <iostream>

#ifdef SISSO_ENABLE_CUDA
#include <cublas_v2.h>
#include <cuda_runtime.h>

#define CHECK_CUDA_ERROR(ans)                        \
    {                                                \
        check_cuda_error((ans), __FILE__, __LINE__); \
    }
inline void check_cuda_error(cudaError_t code, const char* file, int line, bool abort = true)
{
    if (code != cudaSuccess)
    {
        std::cerr << fmt::format("CUDA ERROR: {} {} {}", cudaGetErrorString(code), file, line)
                  << std::endl;
        if (abort) exit(code);
    }
}
#else
struct cublasContext
{
};
typedef struct cublasContext* cublasHandle_t;

struct cudaError_t
{
};

#define CHECK_CUDA_ERROR(ans) \
    {                         \
    }

inline void check_cuda_error(cudaError_t /*code*/,
                             const char* /*file*/,
                             int /*line*/,
                             bool /*abort*/)
{
}

typedef enum
{
    CUBLAS_STATUS_SUCCESS = 0,
    CUBLAS_STATUS_NOT_INITIALIZED = 1,
    CUBLAS_STATUS_ALLOC_FAILED = 3,
    CUBLAS_STATUS_INVALID_VALUE = 7,
    CUBLAS_STATUS_ARCH_MISMATCH = 8,
    CUBLAS_STATUS_MAPPING_ERROR = 11,
    CUBLAS_STATUS_EXECUTION_FAILED = 13,
    CUBLAS_STATUS_INTERNAL_ERROR = 14,
    CUBLAS_STATUS_NOT_SUPPORTED = 15,
    CUBLAS_STATUS_LICENSE_ERROR = 16
} cublasStatus_t;

inline cublasStatus_t cublasCreate(cublasHandle_t* handle)
{
    throw std::runtime_error("CUDA support is not enabled!");
};

inline cublasStatus_t cublasDestroy(cublasHandle_t handle)
{
    throw std::runtime_error("CUDA support is not enabled!");
}

template <class T>
inline cudaError_t cudaMallocManaged(T** devPtr, size_t size, unsigned int flags = 0)
{
    throw std::runtime_error("CUDA support is not enabled!");
}

inline cudaError_t cudaFree(void* /*devPtr*/)
{
    throw std::runtime_error("CUDA support is not enabled!");
}

typedef enum
{
    CUBLAS_OP_N = 0,
    CUBLAS_OP_T = 1,
    CUBLAS_OP_C = 2,
    CUBLAS_OP_HERMITAN = 2, /* synonym if CUBLAS_OP_C */
    CUBLAS_OP_CONJG = 3     /* conjugate, placeholder - not supported in the current release */
} cublasOperation_t;

inline cublasStatus_t cublasDgelsBatched(cublasHandle_t handle,
                                         cublasOperation_t trans,
                                         int m,
                                         int n,
                                         int nrhs,
                                         double* const Aarray[], /*Device pointer*/
                                         int lda,
                                         double* const Carray[], /*Device pointer*/
                                         int ldc,
                                         int* info,
                                         int* devInfoArray, /*Device pointer*/
                                         int batchSize)
{
    throw std::runtime_error("CUDA support is not enabled!");
}

inline cudaError_t cudaDeviceSynchronize(void)
{
    throw std::runtime_error("CUDA support is not enabled!");
}
#endif