// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file descriptor_identifier/solver/SISSOSolver.cpp
 *  @brief Implements the base class for creating solvers using SISSO
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "descriptor_identifier/solver/SISSOSolver.hpp"

#include "KSmallest.hpp"
#include "loss_function/RMSEGPU.hpp"
#include "utils/DescriptorMatrix.hpp"
#include "utils/EnumerateUniqueCombinations.hpp"
#include "utils/PropertiesVector.hpp"

SISSOSolver::SISSOSolver(const InputParser inputs, const std::shared_ptr<FeatureSpace> feat_space)
    : _sample_ids_train(inputs.sample_ids_train()),
      _sample_ids_test(inputs.sample_ids_test()),
      _task_names(inputs.task_names()),
      _task_sizes_train(inputs.task_sizes_train()),
      _task_sizes_test(inputs.task_sizes_test()),
      _leave_out_inds(inputs.leave_out_inds()),
      _prop_unit(inputs.prop_unit()),
      _prop_label(inputs.prop_label()),
      _feat_space(feat_space),
      _mpi_comm(feat_space->mpi_comm()),
      _n_task(inputs.task_sizes_train().size()),
      _n_samp(inputs.prop_train().size()),
      _n_dim(inputs.n_dim()),
      _n_residual(inputs.n_residual()),
      _n_models_store(inputs.n_models_store()),
      _fix_intercept(inputs.fix_intercept())
{
    if (node_value_arrs::TASK_SZ_TEST.size() == 0)
    {
        node_value_arrs::set_task_sz_test(_task_sizes_test);
    }

    _loss = loss_function_util::get_loss_function(inputs.calc_type(),
                                                  inputs.prop_train(),
                                                  inputs.prop_test(),
                                                  _task_sizes_train,
                                                  _task_sizes_test,
                                                  _fix_intercept);
}

SISSOSolver::SISSOSolver(std::vector<std::string> sample_ids_train,
                         std::vector<std::string> sample_ids_test,
                         std::vector<std::string> task_names,
                         std::vector<int> task_sizes_train,
                         std::vector<int> task_sizes_test,
                         std::vector<int> leave_out_inds,
                         Unit prop_unit,
                         std::string prop_label,
                         std::vector<double> prop_train,
                         std::vector<double> prop_test,
                         std::string calc_type,
                         std::shared_ptr<FeatureSpace> feat_space,
                         int n_dim,
                         int n_residual,
                         int n_models_store,
                         bool fix_intercept)
    : _sample_ids_train(sample_ids_train),
      _sample_ids_test(sample_ids_test),
      _task_names(task_names),
      _task_sizes_train(task_sizes_train),
      _task_sizes_test(task_sizes_test),
      _leave_out_inds(leave_out_inds),
      _prop_unit(prop_unit),
      _prop_label(prop_label),
      _feat_space(feat_space),
      _mpi_comm(mpi_setup::comm),
      _n_task(task_sizes_train.size()),
      _n_samp(sample_ids_train.size()),
      _n_dim(n_dim),
      _n_residual(n_residual),
      _n_models_store(n_models_store),
      _fix_intercept(fix_intercept)
{
    assert(static_cast<int>(sample_ids_train.size()) ==
           std::accumulate(task_sizes_train.begin(), task_sizes_train.end(), 0));
    assert(static_cast<int>(sample_ids_test.size()) ==
           std::accumulate(task_sizes_test.begin(), task_sizes_test.end(), 0));
    assert(sample_ids_train.size() == prop_train.size());
    assert(sample_ids_test.size() == prop_test.size());
    assert(sample_ids_test.size() == leave_out_inds.size());
    assert(task_sizes_train.size() == task_sizes_test.size());
    assert(task_sizes_train.size() == task_names.size());

    if (node_value_arrs::TASK_SZ_TEST.size() == 0)
    {
        node_value_arrs::set_task_sz_test(_task_sizes_test);
    }

    _loss = loss_function_util::get_loss_function(
        calc_type, prop_train, prop_test, _task_sizes_train, _task_sizes_test, _fix_intercept);
}

void SISSOSolver::l0_regularization(const int n_dim)
{
    const int n_get_models = std::max(_n_residual, _n_models_store);

    std::vector<int> inds(n_dim, 0);
    std::vector<inds_sc_pair> min_sc_inds(n_get_models);

    unsigned long long int n_interactions = 1;
    int n_dim_fact = 1;
    for (int rr = 0; rr < n_dim; ++rr)
    {
        inds[rr] = _feat_space->phi_selected().size() - 1 - rr;
        n_interactions *= inds[rr] + 1;
        n_dim_fact *= (rr + 1);
    }
    n_interactions /= n_dim_fact;
    setup_regulairzation();

#pragma omp declare reduction(n_max_inds : std::vector<inds_sc_pair> : omp_out=openmp_red::n_max<inds_sc_pair>(omp_out, omp_in)) initializer(omp_priv = omp_orig)
    if (inds.back() >= 0)
    {
#pragma omp parallel firstprivate(inds) shared(min_sc_inds)
        {
            std::shared_ptr<LossFunction> loss_copy;
#pragma omp critical
            {
                loss_copy = loss_function_util::copy(_loss);
            }

            int max_error_ind = 0;
            unsigned long long int ii_prev = 0;

#ifdef OMP45
#pragma omp for schedule(monotonic : dynamic) reduction(n_max_inds : min_sc_inds)
#else
#pragma omp for schedule(dynamic) reduction(n_max_inds : min_sc_inds)
#endif
            for (unsigned long long int ii = _mpi_comm->rank(); ii < n_interactions;
                 ii += static_cast<unsigned long long int>(_mpi_comm->size()))
            {
                util_funcs::iterate(inds, inds.size(), ii - ii_prev);
                ii_prev = ii;
                double score = (*loss_copy)(inds);

                if (score <= min_sc_inds[max_error_ind].score())
                {
                    update_min_inds_scores(inds, score, max_error_ind, min_sc_inds);
                    max_error_ind = std::max_element(min_sc_inds.begin(), min_sc_inds.end()) -
                                    min_sc_inds.begin();
                }
            }
        }
    }

    std::vector<inds_sc_pair> all_min_sc_inds(_mpi_comm->size() * n_get_models);

    mpi::all_gather(*_mpi_comm, min_sc_inds.data(), n_get_models, all_min_sc_inds);

    inds = util_funcs::argsort<inds_sc_pair>(all_min_sc_inds);
    std::vector<std::vector<int>> indexes(n_get_models, std::vector<int>(n_dim));
    for (int rr = 0; rr < n_get_models; ++rr)
    {
        node_value_arrs::clear_temp_test_reg();
        indexes[rr] = all_min_sc_inds[inds[rr]].obj();
        if (indexes[rr].size() == 0)
        {
            throw std::logic_error("A selected model contains no features.");
        }
    }
    add_models(indexes);
}

void SISSOSolver::l0_regularization_gpu(const int n_dim)
{
    const int n_get_models = std::max(_n_residual, _n_models_store);
    KSmallest<inds_sc_pair> best_feature_combinations(n_get_models, inds_sc_pair());

    setup_regulairzation();

    DescriptorMatrix descriptorMatrix;
    PropertiesVector properties(_loss->prop_train());
    RMSEGPU loss(descriptorMatrix.getDeviceDescriptorMatrix(),
                 properties.getDevicePropertiesVector(),
                 _loss->task_sizes_train(),
                 _loss->fix_intercept(),
                 _loss->n_feat());

    EnumerateUniqueCombinations feature_combinations(_feat_space->phi_selected().size() - 1, n_dim);
    feature_combinations += _mpi_comm->rank();

    std::vector<std::vector<int>> feature_indices;
    while (!feature_combinations.is_finished())
    {
        const size_t MAX_BATCH_SIZE = 262144;
        feature_indices.clear();
        for (auto counter = 0; counter < MAX_BATCH_SIZE; ++counter)
        {
            if (feature_combinations.is_finished()) break;
            feature_indices.push_back(feature_combinations.get_current_combination());
            feature_combinations += _mpi_comm->size();
        }

        auto scores = loss(feature_indices);

        for (size_t model_idx = 0; model_idx < feature_indices.size(); ++model_idx)
        {
            best_feature_combinations.insert({feature_indices[model_idx], scores[model_idx]});
        }
    }

    std::vector<inds_sc_pair> all_min_sc_inds(_mpi_comm->size() * n_get_models);

    mpi::all_gather(*_mpi_comm, best_feature_combinations.data().data(), n_get_models, all_min_sc_inds);

    auto inds = util_funcs::argsort<inds_sc_pair>(all_min_sc_inds);
    std::vector<std::vector<int>> indexes(n_get_models, std::vector<int>(n_dim));
    for (int rr = 0; rr < n_get_models; ++rr)
    {
        node_value_arrs::clear_temp_test_reg();
        indexes[rr] = all_min_sc_inds[inds[rr]].obj();
        if (indexes[rr].size() == 0)
        {
            throw std::logic_error("A selected model contains no features.");
        }
    }
    add_models(indexes);
}

void SISSOSolver::fit()
{
    int dd = 1;
    while (continue_calc(dd))
    {
        double start = omp_get_wtime();
        _feat_space->sis(_loss);

        _mpi_comm->barrier();
        double duration = omp_get_wtime() - start;
        if (_mpi_comm->rank() == 0)
        {
            std::cout << "Time for SIS: " << duration << " s" << std::endl;
        }

        start = omp_get_wtime();
        if (_loss->type() == LOSS_TYPE::PEARSON_RMSE_GPU)
            l0_regularization_gpu(dd);
        else
            l0_regularization(dd);

        _mpi_comm->barrier();
        duration = omp_get_wtime() - start;

        if (_mpi_comm->rank() == 0)
        {
            std::cout << "Time for l0-norm: " << duration << " s" << std::endl;
        }
        output_models();
        ++dd;
    }
}
