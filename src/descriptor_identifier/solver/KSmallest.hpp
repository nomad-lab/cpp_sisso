// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#pragma once

#include <cstdint>
#include <vector>

template <typename T>
class KSmallest
{
public:
    KSmallest(const int64_t k, const T& initial_value) : _max_idx(0), _items(k, initial_value) {}
    bool insert(const T& item)
    {
        if (_items[_max_idx] > item)
        {
            _items[_max_idx] = item;
            _max_idx = std::max_element(_items.begin(), _items.end()) -
                                _items.begin();
            return true;
        }
        return false;
    }

    auto& data() {return _items;}

private:
    int64_t _max_idx;
    std::vector<T> _items;
};