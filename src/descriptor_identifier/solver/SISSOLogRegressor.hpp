// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file descriptor_identifier/SISSOLogRegressor.hpp
 *  @brief Defines a class for solving logarithmic regression problems with SISSO
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef SISSO_LOG_REGRESSOR
#define SISSO_LOG_REGRESSOR

#include "descriptor_identifier/model/ModelLogRegressor.hpp"
#include "descriptor_identifier/solver/SISSORegressor.hpp"

#ifdef PY_BINDINGS
namespace py = pybind11;
#endif

// DocString: cls_sisso_log_reg
/**
 * @brief The solver for regression problems
 *
 * @details A slover that uses the least-squares regression to find the best low-dimensional linear model for the DI step after perfoming a log-transformation on all data (inherits from SISSORegressor)
 */
class SISSOLogRegressor : public SISSORegressor
{
private:
    // clang-format off
    std::vector<std::vector<ModelLogRegressor>> _models; //!< List of models
    // clang-format on

public:
    // DocString: sisso_log_reg_init
    /**
     * @brief Constructor for the log regressor
     *
     * @param inputs (Inputs) The InputParser object for the calculation
     * @param feat_space (FeatureSpace) Feature Space for the problem
     */
    SISSOLogRegressor(const InputParser inputs, const std::shared_ptr<FeatureSpace> feat_space);

    /**
     * @brief Constructor for the SISSOLogRegressor from unpickling
     *
     * @param sample_ids_train Vector storing all sample ids for the training samples
     * @param sample_ids_test Vector storing all sample ids for the test samples
     * @param task_names Vector storing the ID of the task names
     * @param task_sizes_train Number of training samples per task
     * @param task_sizes_test Number of testing samples per task
     * @param leave_out_inds List of indexes from the initial data file in the test set
     * @param prop_unit The Unit of the property
     * @param prop_label The label of the property
     * @param prop_train The property for all training samples
     * @param prop_test The property for all the test samples
     * @param feat_space Feature Space for the problem
     * @param n_dim The maximum number of features allowed in the linear model
     * @param n_residual Number of residuals to pass to the next sis model
     * @param n_models_store The number of models to output to files
     * @param fix_intercept If true the bias term is fixed at 0
     * @param models The final models of the analysis
     */
    SISSOLogRegressor(std::vector<std::string> sample_ids_train,
                      std::vector<std::string> sample_ids_test,
                      std::vector<std::string> task_names,
                      std::vector<int> task_sizes_train,
                      std::vector<int> task_sizes_test,
                      std::vector<int> leave_out_inds,
                      Unit prop_unit,
                      std::string prop_label,
                      std::vector<double> prop_train,
                      std::vector<double> prop_test,
                      std::shared_ptr<FeatureSpace> feat_space,
                      int n_dim,
                      int n_residual,
                      int n_models_store,
                      bool fix_intercept,
                      std::vector<std::vector<ModelLogRegressor>> models);

    /**
     * @brief Create a Model for a given set of features and store them in _models
     *
     * @param indexes Vector storing all of the indexes of features in _feat_space->phi_selected() to use for the model
     */
    void add_models(const std::vector<std::vector<int>> indexes) override;

    /**
     * @brief Output the models to files and copy the residuals
     */
    void output_models() override;

    // DocString: sisso_log_reg_models_py
    /**
     * @brief The selected models (n_dim, n_models_store)
     */
    inline std::vector<std::vector<ModelLogRegressor>> models() const { return _models; }
};

#endif
