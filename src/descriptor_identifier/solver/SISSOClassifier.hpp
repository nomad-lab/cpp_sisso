// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file descriptor_identifier/SISSOClassifier.hpp
 *  @brief Define a class for solving classification problems with SISSO
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef SISSO_CLASSIFIER
#define SISSO_CLASSIFIER

#include "classification/prop_sorted_d_mat.hpp"
#include "descriptor_identifier/model/ModelClassifier.hpp"
#include "descriptor_identifier/solver/SISSOSolver.hpp"
#include "utils/vector_utils.hpp"

#ifdef PY_BINDINGS
namespace py = pybind11;
#endif

// DocString: cls_sisso_class
/**
 * @brief The solver for classification problems
 *
 * @details A slover that uses the number of points in the convex-hull overlap region and SVM to find the best low-dimensional linear model (inherits from SISSOSolver)
 */
class SISSOClassifier : public SISSOSolver
{
protected:
    // clang-format off
    std::vector<std::vector<ModelClassifier>> _models; //!< List of models
    std::vector<SVMWrapper> _svm_vec; //!< Vector storing the SVMWrappers for the problem

    std::map<int, int> _sample_inds_to_sorted_dmat_inds; //!< map from input sample inds to the SORTED_D_MATRIX_INDS

    const double _c; //!< the C value for the SVM results
    const double _width; //!< The tolerance used for calculating overlap (data point to check, 1.0 +/- width for the row constraints)

    int _n_class; //!< The number of classes in the calculation
    // clang-format on

public:
    // DocString: sisso_class_init
    /**
     * @brief Constructor for the classifier
     *
     * @param inputs (Inputs) The InputParser object for the calculation
     * @param feat_space (FeatureSpace) Feature Space for the problem
     */
    SISSOClassifier(const InputParser inputs, const std::shared_ptr<FeatureSpace> feat_space);

    /**
     * @brief Constructor for the SISSOClassifier from unpickling
     *
     * @param sample_ids_train Vector storing all sample ids for the training samples
     * @param sample_ids_test Vector storing all sample ids for the test samples
     * @param task_names Vector storing the ID of the task names
     * @param task_sizes_train Number of training samples per task
     * @param task_sizes_test Number of testing samples per task
     * @param leave_out_inds List of indexes from the initial data file in the test set
     * @param prop_unit The Unit of the property
     * @param prop_label The label of the property
     * @param prop_train The property for all training samples
     * @param prop_test The property for all the test samples
     * @param feat_space Feature Space for the problem
     * @param n_dim The maximum number of features allowed in the linear model
     * @param n_residual Number of residuals to pass to the next sis model
     * @param n_models_store The number of models to output to files
     * @param fix_intercept If true the bias term is fixed at 0
     * @param models The final models of the analysis
     */
    SISSOClassifier(std::vector<std::string> sample_ids_train,
                    std::vector<std::string> sample_ids_test,
                    std::vector<std::string> task_names,
                    std::vector<int> task_sizes_train,
                    std::vector<int> task_sizes_test,
                    std::vector<int> leave_out_inds,
                    Unit prop_unit,
                    std::string prop_label,
                    std::vector<double> prop_train,
                    std::vector<double> prop_test,
                    std::shared_ptr<FeatureSpace> feat_space,
                    int n_dim,
                    int n_residual,
                    int n_models_store,
                    bool fix_intercept,
                    std::vector<std::vector<ModelClassifier>> models);

    /**
     * @brief Setup the svm and descriptor matrix transfer vectors
     */
    void setup_svm_d_mat();

    /**
     * @brief Transfer data from the node value array D-matrix into a property sorted array
     */
    void transfer_d_mat_to_sorted() const;

    /**
     * @brief Find the SVM error and margin for a given set of indexes
     *
     * @param svm The SVMWrapper object to perform SVM over
     * @param feat_inds The indexes to develop the SVM model for
     * @return {The number of misclassified points, the SVM margin}
     */
    std::array<double, 2> svm_error(std::vector<SVMWrapper>& svm,
                                    const std::vector<int>& feat_inds) const;

    /**
     * @brief Sort the property vector by class and store the mapped indexes to _sample_inds_to_sorted_dmat_inds
     */
    void setup_d_mat_transfer();

    /**
     * @brief If true calculate the model for the dimension dd
     *
     * @param dd Dimension of the model to train
     * @return true if the requested dimension should be calculated.
     */
    bool continue_calc(int dd) override;

    /**
     * @brief Output the models to files and copy the residuals
     */
    void output_models() override;

    /**
     * @brief Perform any steps that need to be done to initialize the regularization
     */
    inline void setup_regulairzation() override { transfer_d_mat_to_sorted(); }

    /**
     * @brief Set the min_scores and min_inds vectors given a score and max_error_ind
     *
     * @param inds The current set of indexes
     * @param score The score for the current set of indexes
     * @param max_error_ind The current index of the maximum score among the best models
     * @param min_sc_inds Current list of the indexes that make the best models and their respective scores
     */
    void update_min_inds_scores(const std::vector<int>& inds,
                                double score,
                                int max_error_ind,
                                std::vector<inds_sc_pair>& min_sc_inds) override;

    /**
     * @brief Create a Model for a given set of features and store them in _models
     *
     * @param indexes Vector storing all of the indexes of features in _feat_space->phi_selected() to use for the model
     */
    virtual void add_models(const std::vector<std::vector<int>> indexes) override;

    // DocString: sisso_class_models_py
    /**
     * @brief The selected models (n_dim, n_models_store)
     */
    inline std::vector<std::vector<ModelClassifier>> models() const { return _models; }
};

// GCOV_EXCL_START      GCOVR_EXCL_START        LCOV_EXCL_START
#ifdef PY_BINDINGS
namespace py = pybind11;
template <class SISSOClassifierBase = SISSOClassifier>
class PySISSOClassifier : public PySISSOSolver<SISSOClassifierBase>
{
public:
    using PySISSOSolver<SISSOClassifierBase>::PySISSOSolver;

    void output_models() override { PYBIND11_OVERRIDE(void, SISSOClassifierBase, output_models, ); }

    void update_min_inds_scores(const std::vector<int>& inds,
                                double score,
                                int max_error_ind,
                                std::vector<inds_sc_pair>& min_sc_inds) override
    {
        PYBIND11_OVERRIDE(void,
                          SISSOClassifierBase,
                          update_min_inds_scores,
                          inds,
                          score,
                          max_error_ind,
                          min_sc_inds);
    }

    void add_models(const std::vector<std::vector<int>> indexes) override
    {
        PYBIND11_OVERRIDE(void, SISSOClassifierBase, add_models, indexes);
    }
};
#endif
// GCOV_EXCL_STOP      GCOVR_EXCL_STOP        LCOV_EXCL_STOP
#endif
