// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file descriptor_identifier/model/ModelRegressor.hpp
 *  @brief Defines a class for the output models from solving a regression problem
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef MODEL_REGRESSOR
#define MODEL_REGRESSOR

#include "descriptor_identifier/model/Model.hpp"

// DocString: cls_model_reg
/**
 * @brief Class to store the output models for regression problems with SISSO (inherits from Model)
 */
class ModelRegressor : public Model
{
public:
    using Model::error_summary_string;
    using Model::eval;

    /**
     * @brief Default Constructor
     */
    ModelRegressor();

    /**
     * @brief Construct a ModelRegressor using a loss function and a set of features
     *
     * @param prop_label The label for the property
     * @param prop_unit The unit of the property
     * @param loss The LossFunction used to calculate the model
     * @param feats The features of the model
     * @param leave_out_inds The indexes of the samples for the test set
     * @param sample_ids_train A vector storing all sample ids for the training samples
     * @param sample_ids_test A vector storing all sample ids for the test samples
     * @param task_names A vector storing the ID of the task names
     */
    ModelRegressor(const std::string prop_label,
                   const Unit prop_unit,
                   const std::shared_ptr<LossFunction> loss,
                   const std::vector<model_node_ptr> feats,
                   const std::vector<int> leave_out_inds,
                   const std::vector<std::string> sample_ids_train,
                   const std::vector<std::string> sample_ids_test,
                   const std::vector<std::string> task_names);

    // DocString: model_reg_init_pickle
    /**
     * @brief Construct a Model using a loss function and a set of features
     *
     * @param prop_label The label for the property
     * @param prop_unit The unit of the property
     * @param prop_train The property vector of the training set
     * @param prop_test The property vector of the test set
     * @param task_sizes_train The number of samples in the training set for each task
     * @param task_sizes_test The number of samples in the test set for each task
     * @param feats The features of the model
     * @param leave_out_inds The indexes of the samples for the test set
     * @param sample_ids_train A vector storing all sample ids for the training samples
     * @param sample_ids_test A vector storing all sample ids for the test samples
     * @param task_names A vector storing the ID of the task names
     * @param fix_intercept If true set the bias term to 0
     */
    ModelRegressor(std::string prop_label,
                   Unit prop_unit,
                   std::vector<double> prop_train,
                   std::vector<double> prop_test,
                   std::vector<int> task_sizes_train,
                   std::vector<int> task_sizes_test,
                   std::vector<model_node_ptr> feats,
                   std::vector<int> leave_out_inds,
                   std::vector<std::string> sample_ids_train,
                   std::vector<std::string> sample_ids_test,
                   std::vector<std::string> task_names,
                   bool fix_intercept);

    // DocString: model_reg_init_train
    /**
     * @brief Construct a ModelRegressor from a training output file
     *
     * @param train_file (str) Previously generated model output file for the training data
     */
    ModelRegressor(const std::string train_file);

    // DocString: model_reg_init_test_train
    /**
     * @brief Construct a ModelRegressor from a training and testing output file
     *
     * @param train_file (str) Previously generated model output file for the training data
     * @param test_file (str) Previously generated model output file for the test data
     */
    ModelRegressor(const std::string train_file, const std::string test_file);

    /**
     * @brief Copy Constructor
     *
     * @param o ModelRegressor to be copied
     */
    ModelRegressor(const ModelRegressor& o) = default;

    /**
     * @brief Move Constructor
     *
     * @param o ModelRegressor to be moved
     */
    ModelRegressor(ModelRegressor&& o) = default;

    virtual ~ModelRegressor() = default;

    /**
     * @brief Create the LossFunctionPearsonRMSE for the Model when constructed from a file
     *
     * @param prop_train The property vector for all of the training samples
     * @param prop_test The property vector for all of the test samples
     * @param task_sizes_train The number of training samples per task
     * @param task_sizes_test The number of test samples per task
     */
    virtual void make_loss(std::vector<double>& prop_train,
                           std::vector<double>& prop_test,
                           std::vector<int>& task_sizes_train,
                           std::vector<int>& task_sizes_test) override;

    /**
     * @brief Evaluate the model for a new point
     *
     * @param feature_values The value of each selected feature at the given point
     * @param task The task associated with the point
     *
     * @return The prediction of the model for a given data point
     */
    virtual double eval_from_feature_vals(std::vector<double>& feature_vals,
                                          std::string task) const override;

    /**
     * @brief Evaluate the model for a new set of new points
     *
     * @param feature_values The value of each selected feature at each new point
     * @param task The task associated with each new point
     *
     * @return The prediction of the model for the set of data poitns
     */
    virtual std::vector<double> eval_from_feature_vals(
        std::vector<std::vector<double>>& feature_vals,
        const std::vector<std::string>& tasks) const override;

    /**
     * @brief Copy Assignment operator
     *
     * @param o ModelRegressor to be copied
     */
    ModelRegressor& operator=(const ModelRegressor& o) = default;

    /**
     * @brief Move Assignment operator
     *
     * @param o ModelRegressor to be moved
     */
    ModelRegressor& operator=(ModelRegressor&& o) = default;

    /**
     * @brief Based off of the model line in a model file determine if the bias term is 0.0
     *
     * @param model_line the model line from the file
     * @return True if the intercept should be fixed
     */
    virtual inline bool has_fixed_intercept(std::string model_line) override
    {
        return model_line.substr(0, 4).compare("# c0") != 0;
    }

    /**
     * @brief Check if a given line summarizes the error of a model
     *
     * @param line to see if it contains information about the error
     * @return True if the line summarizes the error
     */
    inline bool is_error_line(std::string line) override
    {
        return line.substr(0, 8).compare("# RMSE: ") == 0;
    }

    /**
     * @brief Set error summary variables from a file
     *
     * @param error_line the line to set the error from
     * @param train True if file is for training data
     */
    inline void set_error_from_file(std::string error_line, bool train) override{};

    // DocString: model_reg_str
    /**
     * @brief Convert the model to a string

     * @return The string representation of the model
     */
    virtual std::string toString() const override;

    // DocString: model_reg_latex_str
    /**
     * @brief Convert the model to a latexified string

     * @return The string representation of the model
     */
    std::string toLatexString() const override;

    /**
     * @brief Get the matlab expression for this model

     * @return The matlab expression for the model
     */
    virtual std::string matlab_expr() const override;

    /**
     * @brief Get the block used to summarize the error in the output files
     *
     * @param train True if summary is for the training file
     */
    std::string error_summary_string(bool train) const override;

    /**
     * @brief Get the block used to summarize the error in the output files
     *
     * @param prop The value of the true property
     * @param prop_est The value of the estimated property
     *
     * @returns error_summary The summary of the model error
     */
    std::string error_summary_string(std::vector<double> prop, std::vector<double> prop_est) const override;

    /**
     * @brief Get the coefficients list header for output file
     */
    std::string write_coefs() const override;

    /**
     * @brief Copy the training error into a different vector
     *
     * @param res Pointer to the head of the new vector to store the residual
     */
    virtual inline void copy_error(double* res) const override
    {
        std::copy_n(_loss->error_train().data(), _n_samp_train, res);
    }

    // DocString: model_reg_rmse
    /**
     * @brief The training root mean square error of the model
     */
    inline double rmse() const
    {
        std::vector<double> et = train_error();
        return util_funcs::norm(et.data(), _n_samp_train) /
               std::sqrt(static_cast<double>(_n_samp_train));
    }

    // DocString: model_reg_test_rmse
    /**
     * @brief The test root mean square error of the model
     */
    inline double test_rmse() const
    {
        std::vector<double> et = test_error();
        return util_funcs::norm(et.data(), _n_samp_test) /
               std::sqrt(static_cast<double>(_n_samp_test));
    }

    // DocString: model_reg_r2
    /**
     * @brief The training R^2 of the model
     */
    inline double r2() const
    {
        std::vector<double> p = prop_train();
        std::vector<double> p_est = prop_train_est();
        return util_funcs::r2(p.data(), p_est.data(), _n_samp_train);
    }

    // DocString: model_reg_test_r2
    /**
     * @brief The test R^2 of the model
     */
    inline double test_r2() const
    {
        std::vector<double> p = prop_test();
        std::vector<double> p_est = prop_test_est();
        return util_funcs::r2(p.data(), p_est.data(), _n_samp_test);
    }

    // DocString: model_reg_max_ae
    /**
     * @brief The max absolute error of the training data
     */
    inline double max_ae() const
    {
        std::vector<double> et = train_error();
        return std::abs(*std::max_element(et.begin(), et.end(), [](double d1, double d2) {
            return std::abs(d1) < std::abs(d2);
        }));
    }

    // DocString: model_reg_test_max_ae
    /**
     * @brief The max absolute error of the testing data
     */
    inline double test_max_ae() const
    {
        std::vector<double> et = test_error();
        return std::abs(*std::max_element(et.begin(), et.end(), [](double d1, double d2) {
            return std::abs(d1) < std::abs(d2);
        }));
    }

    // DocString: model_reg_mae
    /**
     * @brief The mean absolute error of the model
     */
    inline double mae() const
    {
        std::vector<double> et = train_error();
        return 1.0 / static_cast<double>(_n_samp_train) *
               std::accumulate(et.begin(), et.end(), 0.0, [](double total, double e) {
                   return total + std::abs(e);
               });
    }

    // DocString: model_reg_test_mae
    /**
     * @brief The mean absolute test error of the model
     */
    inline double test_mae() const
    {
        std::vector<double> et = test_error();
        return 1.0 / static_cast<double>(_n_samp_test) *
               std::accumulate(et.begin(), et.end(), 0.0, [](double total, double e) {
                   return total + std::abs(e);
               });
    }

    // DocString: model_reg_mape
    /**
     * @brief The mean absolute percent error of the model
     */
    double mape() const;

    // DocString: model_reg_test_mape
    /**
     * @brief The mean absolute percent test error of the model
     */
    double test_mape() const;

    // DocString: model_reg_percentile_25_ae
    /**
     * @brief The the 25 percentile error of the model
     */
    inline double percentile_25_ae() const
    {
        return sorted_error()[static_cast<int>(floor(_n_samp_train * 0.25))];
    }

    // DocString: model_reg_test_percentile_25_ae
    /**
     * @brief The 25 percentile test error of the model
     */
    inline double percentile_25_test_ae() const
    {
        return sorted_test_error()[static_cast<int>(floor(_n_samp_test * 0.25))];
    }

    // DocString: model_reg_percentile_50_ae
    /**
     * @brief The 50 percentile error of the model
     */
    inline double percentile_50_ae() const
    {
        return sorted_error()[static_cast<int>(floor(_n_samp_train * 0.50))];
    }

    // DocString: model_reg_test_percentile_50_ae
    /**
     * @brief The 50 percentile test error of the model
     */
    inline double percentile_50_test_ae() const
    {
        return sorted_test_error()[static_cast<int>(floor(_n_samp_test * 0.50))];
    }

    // DocString: model_reg_percentile_75_ae
    /**
     * @brief The 75 percentile error of the model
     */
    inline double percentile_75_ae() const
    {
        return sorted_error()[static_cast<int>(floor(_n_samp_train * 0.75))];
    }

    // DocString: model_reg_test_percentile_75_ae
    /**
     * @brief The 75 percentile test error of the model
     */
    inline double percentile_75_test_ae() const
    {
        return sorted_test_error()[static_cast<int>(floor(_n_samp_test * 0.75))];
    }

    // DocString: model_reg_percentile_95_ae
    /**
     * @brief The 95 percentile error of the model
     */
    inline double percentile_95_ae() const
    {
        return sorted_error()[static_cast<int>(floor(_n_samp_train * 0.95))];
    }

    // DocString: model_reg_test_percentile_95_ae
    /**
     * @brief The 95 percentile test error of the model
     */
    inline double percentile_95_test_ae() const
    {
        return sorted_test_error()[static_cast<int>(floor(_n_samp_test * 0.95))];
    }

    /**
     * @brief Sort the training error based on magnitude
     * @return The error vector sorted
     */
    std::vector<double> sorted_error() const;

    /**
     * @brief Sort the training test_error based on magnitude
     * @return The test_error vector sorted
     */
    std::vector<double> sorted_test_error() const;
};

/**
 * @brief Print a model to an string stream
 *
 * @param outStream The output stream the model is to be printed
 * @param model The model to be printed
 */
std::ostream& operator<<(std::ostream& outStream, const ModelRegressor& model);

// GCOV_EXCL_START      GCOVR_EXCL_START        LCOV_EXCL_START
#ifdef PY_BINDINGS
template <class ModelRegressorBase = ModelRegressor>
class PyModelRegressor : public PyModel<ModelRegressorBase>
{
    using PyModel<ModelRegressorBase>::PyModel;  // Inherit constructors

    std::string error_summary_string(std::vector<double> prop,
                                     std::vector<double> prop_est) const override
    {
        PYBIND11_OVERRIDE(std::string, ModelRegressorBase, error_summary_string, prop, prop_est);
    }

    std::string error_summary_string(bool train) const override
    {
        PYBIND11_OVERRIDE_PURE(std::string, ModelRegressorBase, error_summary_string, train);
    }

    std::string toString() const override
    {
        PYBIND11_OVERRIDE(std::string, ModelRegressorBase, toString,);
    }

    std::string toLatexString() const override
    {
        PYBIND11_OVERRIDE(std::string, ModelRegressorBase, toLatexString,);
    }

    std::string matlab_expr() const override
    {
        PYBIND11_OVERRIDE(std::string, ModelRegressorBase, matlab_expr,);
    }
};
#endif
// GCOV_EXCL_STOP      GCOVR_EXCL_STOP        LCOV_EXCL_STOP
#endif
