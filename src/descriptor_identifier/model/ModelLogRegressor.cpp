// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file descriptor_identifier/model/ModelLogRegressor.cpp
 *  @brief Implements a class for the output models from solving a log regression problem
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "descriptor_identifier/model/ModelLogRegressor.hpp"

ModelLogRegressor::ModelLogRegressor() {}

ModelLogRegressor::ModelLogRegressor(const std::string prop_label,
                                     const Unit prop_unit,
                                     const std::shared_ptr<LossFunction> loss,
                                     const std::vector<model_node_ptr> feats,
                                     const std::vector<int> leave_out_inds,
                                     const std::vector<std::string> sample_ids_train,
                                     const std::vector<std::string> sample_ids_test,
                                     const std::vector<std::string> task_names)
    : ModelRegressor(prop_label,
                     prop_unit,
                     loss,
                     feats,
                     leave_out_inds,
                     sample_ids_train,
                     sample_ids_test,
                     task_names)
{
}

ModelLogRegressor::ModelLogRegressor(std::string prop_label,
                                     Unit prop_unit,
                                     std::vector<double> prop_train,
                                     std::vector<double> prop_test,
                                     std::vector<int> task_sizes_train,
                                     std::vector<int> task_sizes_test,
                                     std::vector<model_node_ptr> feats,
                                     std::vector<int> leave_out_inds,
                                     std::vector<std::string> sample_ids_train,
                                     std::vector<std::string> sample_ids_test,
                                     std::vector<std::string> task_names,
                                     bool fix_intercept)
    : ModelRegressor(prop_label,
                     prop_unit,
                     prop_train,
                     prop_test,
                     task_sizes_train,
                     task_sizes_test,
                     feats,
                     leave_out_inds,
                     sample_ids_train,
                     sample_ids_test,
                     task_names,
                     fix_intercept)
{
    make_loss(prop_train, prop_test, task_sizes_train, task_sizes_test);

    _loss->set_nfeat(_feats.size());
    (*_loss)(_feats);
    _loss->test_loss(_feats);

    _coefs.clear();
    for (size_t cc = 0; cc < _loss->coefs().size() / (_loss->n_dim()); ++cc)
    {
        _coefs.push_back(std::vector<double>(_loss->n_dim()));
        std::copy_n(&_loss->coefs()[cc * _loss->n_dim()], _loss->n_dim(), _coefs.back().data());
    }
}

ModelLogRegressor::ModelLogRegressor(const std::string train_file) { populate_model(train_file); }

ModelLogRegressor::ModelLogRegressor(const std::string train_file, std::string test_file)
{
    populate_model(train_file, test_file);
}

std::string ModelLogRegressor::error_summary_string(std::vector<double> prop,
                                                    std::vector<double> prop_est) const
{
    std::vector<double> sq_err(prop.size(), 0.0);
    std::transform(
        prop.begin(), prop.end(), prop_est.begin(), sq_err.begin(), [](double p, double pe) {
            return std::pow(std::log(p) - std::log(pe), 2.0);
        });
    double rmse = std::sqrt(util_funcs::mean(sq_err));
    double max_ae = std::sqrt(*std::max_element(sq_err.begin(), sq_err.end()));

    std::stringstream error_stream;
    error_stream << "# RMSE: " << std::setprecision(15) << rmse << "; Max AE: " << max_ae
                 << std::endl;
    return error_stream.str();
}

void ModelLogRegressor::make_loss(std::vector<double>& prop_train,
                                  std::vector<double>& prop_test,
                                  std::vector<int>& task_sizes_train,
                                  std::vector<int>& task_sizes_test)
{
    std::transform(prop_train.begin(), prop_train.end(), prop_train.begin(), [](double pt) {
        return std::log(pt);
    });
    std::transform(prop_test.begin(), prop_test.end(), prop_test.begin(), [](double pt) {
        return std::log(pt);
    });
    _loss = std::make_shared<LossFunctionLogPearsonRMSE>(
        prop_train, prop_test, task_sizes_train, task_sizes_test, _fix_intercept, _n_dim);
}

double ModelLogRegressor::eval_from_feature_vals(std::vector<double>& feature_vals,
                                                 std::string task) const
{
    int task_ind = _task_names.find(task)->second;
    double result = std::exp(_coefs[task_ind].back());

    for (size_t ff = 0; ff < _feats.size(); ++ff)
    {
        result *= std::pow(feature_vals[ff], _coefs[task_ind][ff]);
    }
    return result;
}

std::vector<double> ModelLogRegressor::eval_from_feature_vals(
    std::vector<std::vector<double>>& feature_vals, const std::vector<std::string>& tasks) const
{
    std::vector<double> result(feature_vals[0].size(), 0.0);
    std::transform(tasks.begin(), tasks.end(), result.begin(), [this](std::string task) {
        int task_ind = _task_names.find(task)->second;
        return std::exp(_coefs[task_ind].back());
    });

    for (size_t ff = 0; ff < _feats.size(); ++ff)
    {
        std::vector<double> node_eval(feature_vals[ff].size());
        std::transform(tasks.begin(),
                       tasks.end(),
                       feature_vals[ff].begin(),
                       node_eval.begin(),
                       [this, ff](std::string task, double ne) {
                           int task_ind = _task_names.find(task)->second;
                           return std::pow(ne, _coefs[task_ind][ff]);
                       });
        std::transform(node_eval.begin(),
                       node_eval.end(),
                       result.begin(),
                       result.begin(),
                       std::multiplies<double>());
    }

    return result;
}

std::string ModelLogRegressor::toString() const
{
    std::stringstream model_rep;
    if (_fix_intercept)
    {
        model_rep << "(" << _feats[0]->expr() << ")^a0";
        for (size_t ff = 1; ff < _feats.size(); ++ff)
        {
            model_rep << " * (" << _feats[ff]->expr() << ")^a" << ff;
        }
    }
    else
    {
        model_rep << "exp(c0)";
        for (size_t ff = 0; ff < _feats.size(); ++ff)
        {
            model_rep << " * (" << _feats[ff]->expr() << ")^a" << ff;
        }
    }
    return model_rep.str();
}

std::string ModelLogRegressor::toLatexString() const
{
    std::stringstream model_rep;
    if (_fix_intercept)
    {
        model_rep << "$\\left(" << _feats[0]->get_latex_expr() << "\\right)^{a_0}";
        for (size_t ff = 1; ff < _feats.size(); ++ff)
        {
            model_rep << "\\left(" << _feats[ff]->get_latex_expr() << "\\right)^{a_" << ff << "}";
        }
        model_rep << "$";
    }
    else
    {
        model_rep << "$\\exp\\left(c_0\\right)";
        for (size_t ff = 0; ff < _feats.size(); ++ff)
        {
            model_rep << "\\left(" << _feats[ff]->get_latex_expr() << "\\right)^{a_" << ff << "}";
        }
        model_rep << "$";
    }
    return model_rep.str();
}

std::string ModelLogRegressor::matlab_expr() const
{
    std::string to_ret = "";
    to_ret += "exp(c0)";
    for (size_t ff = 0; ff < _feats.size(); ++ff)
    {
        to_ret += " .* f" + std::to_string(ff) + ".^a" + std::to_string(ff);
    }
    return to_ret;
}

std::ostream& operator<<(std::ostream& outStream, const ModelLogRegressor& model)
{
    outStream << model.toString();
    return outStream;
}

std::vector<double> ModelLogRegressor::prop_train() const
{
    std::vector<double> exp_prop_train(_loss->prop_train());
    std::transform(
        exp_prop_train.begin(), exp_prop_train.end(), exp_prop_train.begin(), [](double val) {
            return std::exp(val);
        });
    return exp_prop_train;
}

std::vector<double> ModelLogRegressor::prop_test() const
{
    std::vector<double> exp_prop_test(_loss->prop_test());
    std::transform(
        exp_prop_test.begin(), exp_prop_test.end(), exp_prop_test.begin(), [](double val) {
            return std::exp(val);
        });
    return exp_prop_test;
}

std::vector<double> ModelLogRegressor::prop_train_est() const
{
    std::vector<double> exp_prop_train_est(_loss->prop_train_est());
    std::transform(exp_prop_train_est.begin(),
                   exp_prop_train_est.end(),
                   exp_prop_train_est.begin(),
                   [](double val) { return std::exp(val); });
    return exp_prop_train_est;
}

std::vector<double> ModelLogRegressor::prop_test_est() const
{
    std::vector<double> exp_prop_test_est(_loss->prop_test_est());
    std::transform(exp_prop_test_est.begin(),
                   exp_prop_test_est.end(),
                   exp_prop_test_est.begin(),
                   [](double val) { return std::exp(val); });
    return exp_prop_test_est;
}

std::vector<double> ModelLogRegressor::train_error() const
{
    std::vector<double> exp_prop_train(_loss->prop_train());
    std::vector<double> exp_prop_train_est(_loss->prop_train_est());
    std::vector<double> exp_train_error(exp_prop_train.size());

    std::transform(exp_prop_train.begin(),
                   exp_prop_train.end(),
                   exp_prop_train_est.begin(),
                   exp_train_error.begin(),
                   [](double val1, double val2) { return std::exp(val1) - std::exp(val2); });
    return exp_train_error;
}

std::vector<double> ModelLogRegressor::test_error() const
{
    std::vector<double> exp_prop_test(_loss->prop_test());
    std::vector<double> exp_prop_test_est(_loss->prop_test_est());
    std::vector<double> exp_test_error(exp_prop_test.size());

    std::transform(exp_prop_test.begin(),
                   exp_prop_test.end(),
                   exp_prop_test_est.begin(),
                   exp_test_error.begin(),
                   [](double val1, double val2) { return std::exp(val1) - std::exp(val2); });
    return exp_test_error;
}
