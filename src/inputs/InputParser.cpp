// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file inputs/InputParser.cpp
 *  @brief Implements the class that parses the input file
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 *
 *  This class parses and stores the parameters in the input file
 */

#include "inputs/InputParser.hpp"
InputParser::InputParser()
    : _filename(""),
      _data_file("data.csv"),
      _prop_key("prop"),
      _prop_label("prop"),
      _task_key("Task"),
      _calc_type("regression"),
      _phi_out_file(""),
      _mpi_comm(mpi_setup::comm),
      _cross_cor_max(1.0),
      _l_bound(1e-50),
      _u_bound(1e50),
      _n_dim(1),
      _max_rung(0),
      _n_rung_store(-1),
      _n_rung_generate(0),
      _n_sis_select(1),
      _n_samp(0),
      _n_samp_train(0),
      _n_samp_test(0),
      _n_residual(1),
      _n_models_store(1),
      _max_param_depth(-1),
      _nlopt_seed(42),
      _fix_intercept(false),
      _data_file_relative_to_json(false),
      _global_param_opt(false),
      _reparam_residual(false),
      _override_n_sis_select(false)
{
    if (_mpi_comm == nullptr)
    {
        mpi_setup::init_mpi_env();
        _mpi_comm = mpi_setup::comm;
    }
}

InputParser::InputParser(pt::ptree ip, std::string fn, std::shared_ptr<MPI_Interface> comm)
    : _allowed_param_ops(as_vector<std::string>(ip, "param_opset")),
      _allowed_ops(as_vector<std::string>(ip, "opset")),
      _leave_out_inds(as_vector<int>(ip, "leave_out_inds")),
      _filename(fn),
      _data_file(ip.get<std::string>("data_file", "data.csv")),
      _prop_key(ip.get<std::string>("property_key", "prop")),
      _task_key(ip.get<std::string>("task_key", "task")),
      _calc_type(ip.get<std::string>("calc_type", "regression")),
      _phi_out_file(ip.get<std::string>("phi_out_file", "")),
      _mpi_comm(comm),
      _cross_cor_max(ip.get<double>("max_feat_cross_correlation", 1.0)),
      _l_bound(ip.get<double>("min_abs_feat_val", 1e-50)),
      _u_bound(ip.get<double>("max_abs_feat_val", 1e50)),
      _n_dim(ip.get<int>("desc_dim")),
      _max_rung(ip.get<int>("max_rung")),
      _n_rung_store(ip.get<int>("n_rung_store", -1)),
      _n_rung_generate(ip.get<int>("n_rung_generate", 0)),
      _n_sis_select(ip.get<int>("n_sis_select")),
      _n_samp(0),
      _n_samp_train(0),
      _n_samp_test(0),
      _n_residual(ip.get<int>("n_residual", 1)),
      _n_models_store(ip.get<int>("n_models_store", _n_residual)),
      _max_param_depth(ip.get<int>("max_feat_param_depth", _max_rung)),
      _nlopt_seed(ip.get<int>("nlopt_seed", 42)),
      _fix_intercept(ip.get<bool>("fix_intercept", false)),
      _data_file_relative_to_json(ip.get<bool>("data_file_relatice_to_json", false)),
      _global_param_opt(ip.get<bool>("global_param_opt", false)),
      _reparam_residual(ip.get<bool>("reparam_residual", false)),
      _override_n_sis_select(ip.get<bool>("override_n_sis_select", false))
{
    if (_data_file_relative_to_json)
    {
        if ((_data_file[0] == '/') || (_data_file[0] == '\\'))
        {
            throw std::logic_error(
                "The data file is an absolute path, but _data_file_relative_to_json is true.");
        }
        std::vector<std::string> filepath = str_utils::split_string_trim(fn, "/");
        if (filepath.size() > 1)
        {
            _data_file = str_utils::join("/", filepath.data(), filepath.size() - 1) + "/" +
                         _data_file;
        }
    }

// Check if param ops are passed without being build with parameterized features
#ifndef PARAMETERIZE
    if (_allowed_param_ops.size() > 0)
    {
        throw std::logic_error(
            "To use parameterized operators please rebuild with -DBUILD_PARAMS=ON");
    }
#else
    nlopt::srand(_nlopt_seed);
    nlopt_wrapper::NLOPT_SEED = _nlopt_seed;
    nlopt_wrapper::MAX_PARAM_DEPTH = _max_param_depth;
    nlopt_wrapper::USE_GLOBAL = _global_param_opt;
#endif

    // Read the data file
    std::ifstream data_stream;
    std::string line;
    data_stream.open(_data_file, std::ios::in);

    // Get the header line for the names of all primary features
    std::getline(data_stream, line);
    std::vector<std::string> split_line;
    boost::algorithm::split(split_line, line, boost::algorithm::is_any_of(","));
    if (split_line.size() == 1)
    {
        throw std::logic_error("data_file is not a comma separated file");
    }

    // Separate names/units from the headers
    std::vector<std::string> headers;
    std::vector<Unit> units;
    std::vector<Domain> domains;
    for (size_t dd = 1; dd < split_line.size(); ++dd)
    {
        std::vector<std::string> name_unit_split;
        std::vector<std::string> domain_split;
        boost::algorithm::split(domain_split, split_line[dd], boost::algorithm::is_any_of("|"));
        boost::algorithm::split(
            name_unit_split, domain_split[0], boost::algorithm::is_any_of("()"));
        if (name_unit_split.size() == 1)
        {
            boost::algorithm::trim(name_unit_split[0]);
            headers.push_back(name_unit_split[0]);
            units.push_back(Unit());
        }
        else if (name_unit_split.size() == 3)
        {
            boost::algorithm::trim(name_unit_split[0]);
            boost::algorithm::trim(name_unit_split[1]);

            headers.push_back(name_unit_split[0]);
            units.push_back(name_unit_split[1]);
        }
        else
        {
            throw std::logic_error("Invalid feature name \"" + split_line[dd] +
                                   "\" in header of file");
        }

        if (domain_split.size() > 1)
        {
            domains.push_back(
                Domain(str_utils::join("|", &domain_split[1], domain_split.size() - 1)));
        }
        else
        {
            domains.push_back(Domain());
        }
    }

    // Get the column corresponding the the task ID
    size_t taskind = 0;
    while ((taskind < headers.size()) && (headers[taskind] != _task_key))
    {
        ++taskind;
    }

    // Get the tasks IDs for each row
    std::map<std::string, std::vector<int>> tasks;
    if (taskind >= headers.size())
    {
        // If no task id then everything is in a task called none
        while (std::getline(data_stream, line))
        {
            ++_n_samp;
        }
        tasks["all"] = std::vector<int>(_n_samp);
        std::iota(tasks["all"].begin(), tasks["all"].end(), 0);
    }
    else
    {
        // Otherwise correctly assign each row to a task
        while (std::getline(data_stream, line))
        {
            boost::algorithm::split(split_line, line, [](char c) { return c == ','; });
            if (tasks.count(split_line[taskind + 1]) > 0)
            {
                tasks[split_line[taskind + 1]].push_back(_n_samp);
            }
            else
            {
                tasks[split_line[taskind + 1]] = {_n_samp};
            }
            ++_n_samp;
        }
    }

    // Make the train-test split
    double leave_out_frac = ip.get<double>(
        "leave_out_frac",
        static_cast<double>(ip.get<int>("n_leave_out", 0)) / static_cast<double>(_n_samp));
    if ((_leave_out_inds.size() == 0) && leave_out_frac > 0.0)
    {
        // If only fraction given create a test set  that pull equally from all tasks
        if (comm->rank() == 0)
        {
            unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
            int start = 0;
            for (auto& el : tasks)
            {
                _task_sizes_test.push_back(
                    static_cast<int>(std::round(leave_out_frac * el.second.size())));
                _task_sizes_train.push_back(el.second.size() - _task_sizes_test.back());

                _leave_out_inds.resize(_leave_out_inds.size() + _task_sizes_test.back());

                std::vector<int> inds(el.second);
                std::shuffle(inds.begin(), inds.end(), std::default_random_engine(seed));

                std::copy_n(inds.begin(), _task_sizes_test.back(), _leave_out_inds.begin() + start);
                std::sort(_leave_out_inds.begin() + start, _leave_out_inds.end());

                start += _task_sizes_test.back();
            }
        }

        // Broadcast the task sizes/leave out indexes
        mpi::broadcast(*_mpi_comm, _leave_out_inds, 0);
        mpi::broadcast(*_mpi_comm, _task_sizes_test, 0);
        mpi::broadcast(*_mpi_comm, _task_sizes_train, 0);
    }
    else if (_leave_out_inds.size() > 0)
    {
        // If row numbers of the test set samples are passed use those and set up the correct task sizes
        for (auto& el : tasks)
        {
            int n_test = 0;
            for (auto& ind : el.second)
            {
                if (std::any_of(_leave_out_inds.begin(), _leave_out_inds.end(), [ind](int i1) {
                        return i1 == ind;
                    }))
                {
                    ++n_test;
                }
            }
            _task_sizes_test.push_back(n_test);
            _task_sizes_train.push_back(el.second.size() - n_test);
        }
    }
    else
    {
        // Everything is training
        for (auto& el : tasks)
        {
            _task_sizes_test.push_back(0);
            _task_sizes_train.push_back(el.second.size());
        }
    }

    _n_samp_train = std::accumulate(_task_sizes_train.begin(), _task_sizes_train.end(), 0);
    _n_samp_test = std::accumulate(_task_sizes_test.begin(), _task_sizes_test.end(), 0);
    assert(_n_samp_train + _n_samp_test == _n_samp);

    if ((_allowed_ops.size() == 0) && (_allowed_param_ops.size() == 0))
    {
        _allowed_ops = {"exp",
                        "neg_exp",
                        "inv",
                        "sq",
                        "cb",
                        "six_pow",
                        "sqrt",
                        "cbrt",
                        "log",
                        "abs",
                        "sin",
                        "cos",
                        "add",
                        "sub",
                        "abs_diff",
                        "mult",
                        "div"};
    }

    // Generate a feature space from the data file
    generate_phi_0(headers, units, domains, tasks, taskind);
}

InputParser::InputParser(std::string fn)
    : InputParser(get_prop_tree(fn, mpi_setup::comm), fn, mpi_setup::comm)
{
    if (_mpi_comm->rank() == 0)
    {
        std::vector<std::string> filepath = str_utils::split_string_trim(fn, "/");
        if (filepath.size() > 1)
        {
            fn = str_utils::join("/", filepath.data(), filepath.size() - 1) + "/stripped_" +
                 filepath.back();
        }
        else
        {
            fn = "stripped_" + fn;
        }
        boost::filesystem::remove(fn);
    }
}

void InputParser::generate_phi_0(std::vector<std::string> headers,
                                 std::vector<Unit> units,
                                 std::vector<Domain> domains,
                                 std::map<std::string, std::vector<int>> tasks,
                                 size_t taskind)
{
    // Open the data file
    std::string line;
    std::ifstream data_stream;
    data_stream.open(_data_file, std::ios::in);
    std::getline(data_stream, line);

    // Get the sample id's
    std::vector<std::string> samples;
    std::vector<int> task_size;

    // Initialize the initial data objects
    std::vector<std::vector<double>> data(headers.size(), std::vector<double>(_n_samp_train));

    std::vector<std::vector<double>> test_data(headers.size(), std::vector<double>(_n_samp_test));

    _sample_ids_train.resize(_n_samp_train);
    _sample_ids_test.resize(_n_samp_test);

    std::vector<std::string> bad_samples;

    int cur_line = 0;
    int n_train_samp = 0;
    int n_samp_test = 0;

    for (auto& task : tasks)
    {
        _task_names.push_back(task.first);
    }

    while (std::getline(data_stream, line))
    {
        std::vector<std::string> split_line;
        boost::algorithm::split(split_line, line, [](char c) { return c == ','; });
        samples.push_back(split_line[0]);

        // Check that the rows are all have the same number of columns as the header line
        if (split_line.size() != headers.size() + 1)
        {
            bad_samples.push_back(split_line[0]);
        }

        if (std::any_of(_leave_out_inds.begin(), _leave_out_inds.end(), [&cur_line](int ind) {
                return ind == cur_line;
            }))
        {
            // This data point is in the validation dataset

            // Find where the row should be in the data structure
            n_samp_test = 0;
            for (auto& task : tasks)
            {
                size_t task_ind = std::find(task.second.begin(), task.second.end(), cur_line) -
                                  task.second.begin();
                for (size_t ii = 0; ii < task_ind; ++ii)
                {
                    if (std::any_of(_leave_out_inds.begin(),
                                    _leave_out_inds.end(),
                                    [&ii, &task](int ind) { return ind == task.second[ii]; }))
                    {
                        ++n_samp_test;
                    }
                }

                if (task_ind < task.second.size())
                {
                    break;
                }
            }

            // Add the sample id to _sample_ids_test
            _sample_ids_test[n_samp_test] = split_line[0];

            // Add the point to the appropriate location in the test data structure
            for (size_t ii = 1; ii < split_line.size(); ++ii)
            {
                if (ii - 1 != taskind)
                {
                    test_data[ii - 1][n_samp_test] = std::stod(split_line[ii]);
                }
            }
        }
        else
        {
            // This data point is in the training dataset
            n_train_samp = 0;
            // Find where the row should be in the data structure
            for (auto& task : tasks)
            {
                size_t task_ind = std::find(task.second.begin(), task.second.end(), cur_line) -
                                  task.second.begin();
                for (size_t ii = 0; ii < task_ind; ++ii)
                {
                    if (std::none_of(_leave_out_inds.begin(),
                                     _leave_out_inds.end(),
                                     [&ii, &task](int ind) { return ind == task.second[ii]; }))
                    {
                        ++n_train_samp;
                    }
                }

                if (task_ind < task.second.size())
                {
                    break;
                }
            }

            // Add the sample id to _sample_ids_train
            _sample_ids_train[n_train_samp] = split_line[0];

            // Add the point to the appropriate location in the test data structure
            for (size_t ii = 1; ii < split_line.size(); ++ii)
            {
                if (ii - 1 != taskind)
                {
                    data[ii - 1][n_train_samp] = std::stod(split_line[ii]);
                }
            }
        }
        ++cur_line;
    }

    // If there are any rows with a different number of columns than the header line throw an error
    if (bad_samples.size() > 0)
    {
        std::string msg = "The data for the following samples are incomplete: ";
        for (auto& sample : bad_samples)
        {
            msg += sample + ", ";
        }
        throw std::logic_error(msg.substr(0, msg.size() - 2));
    }

    // Find which column corresponds to the target property
    size_t propind = 0;
    while ((propind < headers.size()) && (headers[propind] != _prop_key))
    {
        ++propind;
    }

    if (propind >= headers.size())
    {
        throw std::logic_error("_propkey not found in data_file");
    }
    else
    {
        // Get the relevant property information associated with that column
        _prop_unit = units[propind];
        _prop_label = headers[propind];
        _prop_train = std::vector<double>(data[propind].size(), 0.0);
        _prop_test = std::vector<double>(test_data[propind].size(), 0.0);

        std::copy_n(data[propind].begin(), data[propind].size(), _prop_train.begin());
        std::copy_n(test_data[propind].begin(), test_data[propind].size(), _prop_test.begin());

        // Remove that column from the data sets to not make a feature with the property's data
        data.erase(data.begin() + propind);
        test_data.erase(test_data.begin() + propind);
        headers.erase(headers.begin() + propind);
        units.erase(units.begin() + propind);
        domains.erase(domains.begin() + propind);
    }

    // If the task is in a column to the right of the property then subtract 1 from the index as one of the columns was removed
    if (taskind > propind)
    {
        --taskind;
    }

    // Remove the task column to not make a feature with that data
    if (taskind < headers.size())
    {
        data.erase(data.begin() + taskind);
        test_data.erase(test_data.begin() + taskind);
        headers.erase(headers.begin() + taskind);
        units.erase(units.begin() + taskind);
        domains.erase(domains.begin() + taskind);
    }

// Initialize the central data storage area
#ifdef PARAMETERIZE
    node_value_arrs::initialize_values_arr(_task_sizes_train,
                                           _task_sizes_test,
                                           headers.size(),
                                           _max_rung,
                                           _allowed_param_ops.size() > 0);
#else
    node_value_arrs::initialize_values_arr(
        _task_sizes_train, _task_sizes_test, headers.size(), _max_rung, false);
#endif

    // Create \Phi_0 of primary features
    for (size_t ff = 0; ff < headers.size(); ++ff)
    {
        _phi_0.push_back(
            FeatureNode(ff, headers[ff], data[ff], test_data[ff], units[ff], domains[ff]));
    }
}

void InputParser::clear_data()
{
    _phi_0.clear();
    _sample_ids_train.clear();
    _sample_ids_test.clear();
    _prop_train.clear();
    _prop_test.clear();
    _task_names.clear();
    _task_sizes_train.clear();
    _task_sizes_test.clear();
    _leave_out_inds.clear();

    _n_samp_train = 0;
    _n_samp_test = 0;
    _n_samp = 0;
}

std::vector<node_ptr> InputParser::phi_0_ptrs() const
{
    if (_phi_0.size() == 0)
    {
        throw std::logic_error("Accessing an unset member (_phi_0).");
    }
    std::vector<node_ptr> phi_0_node_ptr(_phi_0.size());
    std::transform(_phi_0.begin(), _phi_0.end(), phi_0_node_ptr.begin(), [](FeatureNode feat) {
        return std::make_shared<FeatureNode>(feat);
    });
    return phi_0_node_ptr;
}

void InputParser::set_task_sizes_train(std::vector<int> task_sizes_train)
{
    if ((_task_sizes_test.size() > 0) && (_task_sizes_test.size() != task_sizes_train.size()))
    {
        throw std::logic_error(
            "The updated task_sizes_train is not of the same size as the existing "
            "_task_sizes_test.");
    }
    else if (_task_sizes_test.size() == 0)
    {
        _task_sizes_test.resize(task_sizes_train.size(), 0);
        _n_samp_test = 0;
    }

    if ((_task_names.size() > 0) && (_task_names.size() != task_sizes_train.size()))
    {
        throw std::logic_error(
            "The updated task_sizes_train is not of the same size as the existing _task_names.");
    }

    _task_sizes_train = task_sizes_train;
    _n_samp_train = std::accumulate(task_sizes_train.begin(), task_sizes_train.end(), 0);
    _n_samp = _n_samp_train + _n_samp_test;

    if ((_prop_train.size() > 0) && (static_cast<int>(_prop_train.size()) != _n_samp_train))
    {
        throw std::logic_error(
            "The total number of samples in the updated task size vector is not the same as the "
            "number of samples of the property vector for the training set.");
    }

    for (auto& feat : _phi_0)
    {
        if (feat.n_samp() != _n_samp_train)
        {
            throw std::logic_error(
                "The total number of samples in the updated task size vector is not the same as "
                "the number of samples of one of the features in the training set.");
        }
    }

    if ((_sample_ids_train.size() > 0) &&
        (static_cast<int>(_sample_ids_train.size()) != _n_samp_train))
    {
        throw std::logic_error(
            "The total number of samples in the updated task size vector is not the same as the "
            "number of samples ids for the training set.");
    }
    node_value_arrs::set_task_sz_train(_task_sizes_train);
}

void InputParser::set_task_sizes_test(std::vector<int> task_sizes_test)
{
    if ((_task_sizes_train.size() > 0) && (_task_sizes_train.size() != task_sizes_test.size()))
    {
        throw std::logic_error(
            "The updated task_sizes_test is not of the same size as the existing "
            "_task_sizes_train.");
    }
    else if (_task_sizes_train.size() == 0)
    {
        _task_sizes_train.resize(task_sizes_test.size(), 0);
        _n_samp_train = 0;
    }

    if ((_task_names.size() > 0) && (_task_names.size() != task_sizes_test.size()))
    {
        throw std::logic_error(
            "The updated task_sizes_test is not of the same size as the existing _task_names.");
    }

    _task_sizes_test = task_sizes_test;
    _n_samp_test = std::accumulate(task_sizes_test.begin(), task_sizes_test.end(), 0);
    _n_samp = _n_samp_train + _n_samp_test;

    if ((_prop_test.size() > 0) && (static_cast<int>(_prop_test.size()) != _n_samp_test))
    {
        throw std::logic_error(
            "The total number of samples in the updated task size vector is not the same as the "
            "number of samples of the property vector for the test set.");
    }

    if ((_leave_out_inds.size() > 0) && (static_cast<int>(_leave_out_inds.size()) != _n_samp_test))
    {
        throw std::logic_error(
            "The total number of samples in the updated task size vector is not the same as the "
            "number of samples left out as the test set.");
    }

    for (auto& feat : _phi_0)
    {
        if (feat.n_samp_test() != _n_samp_test)
        {
            throw std::logic_error(
                "The total number of samples in the updated task size vector is not the same as "
                "the number of samples of one of the features in the test set.");
        }
    }

    if ((_sample_ids_test.size() > 0) &&
        (static_cast<int>(_sample_ids_test.size()) != _n_samp_test))
    {
        throw std::logic_error(
            "The total number of samples in the updated task size vector is not the same as the "
            "number of samples ids for the test set.");
    }
    node_value_arrs::set_task_sz_test(_task_sizes_test);
}

void InputParser::set_task_names(std::vector<std::string> task_names)
{
    if ((_task_sizes_test.size() > 0) && (_task_sizes_test.size() != task_names.size()))
    {
        throw std::logic_error(
            "The updated task_sizes_name is not of the same size as the existing "
            "_task_sizes_test.");
    }
    else if (_task_sizes_test.size() == 0)
    {
        _task_sizes_test.resize(task_names.size(), 0);
        _n_samp_test = 0;
    }

    if ((_task_sizes_train.size() > 0) && (_task_sizes_train.size() != task_names.size()))
    {
        throw std::logic_error(
            "The updated task_sizes_name is not of the same size as the existing "
            "_task_sizes_train.");
    }
    else if (_task_sizes_train.size() == 0)
    {
        _task_sizes_train.resize(task_names.size(), 0);
        _n_samp_test = 0;
    }

    _task_names = task_names;
}
void InputParser::set_prop_train(std::vector<double> prop_train)
{
    if ((_n_samp_train != 0) && (static_cast<int>(prop_train.size()) != _n_samp_train))
    {
        throw std::logic_error(
            "The number of samples in the property vector is not the same as the expected number "
            "of samples in the training set.");
    }

    for (auto& feat : _phi_0)
    {
        if (feat.n_samp() != static_cast<int>(prop_train.size()))
        {
            throw std::logic_error(
                "The number of samples in the property vector is not the same as the number of "
                "samples of one of the features in the training set.");
        }
    }

    if ((_sample_ids_train.size() > 0) && (_sample_ids_train.size() != prop_train.size()))
    {
        throw std::logic_error(
            "The number of samples in the property vector is not the same as the number of samples "
            "ids for the training set.");
    }

    _prop_train = prop_train;
}

void InputParser::set_prop_test(std::vector<double> prop_test)
{
    if ((_n_samp_test != 0) && (static_cast<int>(prop_test.size()) != _n_samp_test))
    {
        throw std::logic_error(
            "The number of samples in the property vector is not the same as the expected number "
            "of samples in the test set.");
    }

    if ((_leave_out_inds.size() > 0) && (_leave_out_inds.size() != prop_test.size()))
    {
        throw std::logic_error(
            "The number of samples in the property vector is not the same as the number of samples "
            "left out as the test set.");
    }

    for (auto& feat : _phi_0)
    {
        if (feat.n_samp_test() != static_cast<int>(prop_test.size()))
        {
            throw std::logic_error(
                "The number of samples in the property vector is not the same as the number of "
                "samples of one of the features in the test set.");
        }
    }

    if ((_sample_ids_test.size() > 0) && (_sample_ids_test.size() != prop_test.size()))
    {
        throw std::logic_error(
            "The number of samples in the property vector is not the same as the number of samples "
            "ids for the test set.");
    }

    _prop_test = prop_test;
}

void InputParser::set_leave_out_inds(std::vector<int> leave_out_inds)
{
    if ((_n_samp_test != 0) && (static_cast<int>(leave_out_inds.size()) != _n_samp_test))
    {
        throw std::logic_error(
            "The number of samples left out as the test set is not the same as the expected number "
            "of samples in the test set.");
    }

    if ((_prop_test.size() > 0) && (_prop_test.size() != leave_out_inds.size()))
    {
        throw std::logic_error(
            "The number of samples left out as the test set is not the same as the number of "
            "samples of the property vector for the test set.");
    }

    for (auto& feat : _phi_0)
    {
        if (feat.n_samp_test() != static_cast<int>(leave_out_inds.size()))
        {
            throw std::logic_error(
                "The number of samples left out as the test set is not the same as the number of "
                "samples of one of the features in the test set.");
        }
    }

    if ((_sample_ids_test.size() > 0) && (_sample_ids_test.size() != leave_out_inds.size()))
    {
        throw std::logic_error(
            "The number of samples left out as the test set is not the same as the number of "
            "samples ids for the test set.");
    }

    _leave_out_inds = leave_out_inds;
}

void InputParser::set_sample_ids_train(std::vector<std::string> sample_ids_train)
{
    if ((_n_samp_train != 0) && (static_cast<int>(sample_ids_train.size()) != _n_samp_train))
    {
        throw std::logic_error(
            "The number of samples is not the same as the expected number of samples in the "
            "training set.");
    }

    for (auto& feat : _phi_0)
    {
        if (feat.n_samp() != static_cast<int>(sample_ids_train.size()))
        {
            throw std::logic_error(
                "The number of samples is not the same as the number of samples of one of the "
                "features in the training set.");
        }
    }

    if ((_prop_train.size() > 0) && (_prop_train.size() != sample_ids_train.size()))
    {
        throw std::logic_error(
            "The number of samples is not the same as the number of samples of the property vector "
            "for the training set.");
    }

    _sample_ids_train = sample_ids_train;
}

void InputParser::set_sample_ids_test(std::vector<std::string> sample_ids_test)
{
    if ((_n_samp_test != 0) && (static_cast<int>(sample_ids_test.size()) != _n_samp_test))
    {
        throw std::logic_error(
            "The number of sample ids is not the same as the expected number of samples in the "
            "test set.");
    }

    if ((_leave_out_inds.size() > 0) && (_leave_out_inds.size() != sample_ids_test.size()))
    {
        throw std::logic_error(
            "The number of sample ids is not the same as the number of samples left out as the "
            "test set.");
    }

    for (auto& feat : _phi_0)
    {
        if (feat.n_samp_test() != static_cast<int>(sample_ids_test.size()))
        {
            throw std::logic_error(
                "The number of sample ids is not the same as the number of samples of one of the "
                "features in the test set.");
        }
    }

    if ((_prop_test.size() > 0) && (_prop_test.size() != sample_ids_test.size()))
    {
        throw std::logic_error(
            "The number of sample ids is not the same as the number of samples of the property "
            "vector for the test set.");
    }

    _sample_ids_test = sample_ids_test;
}

void InputParser::set_phi_0(std::vector<node_ptr> phi_0_ptrs)
{
    assert(phi_0_ptrs.size() > 0);

    int n_samp_train_feats = phi_0_ptrs[0]->n_samp();
    int n_samp_test_feats = phi_0_ptrs[0]->n_samp_test();

    for (size_t ff = 1; ff < phi_0_ptrs.size(); ++ff)
    {
        if (phi_0_ptrs[ff]->n_samp() != n_samp_train_feats)
        {
            throw std::logic_error(
                "Not all the features in phi_0 have the same number of training samples.");
        }

        if (phi_0_ptrs[ff]->n_samp_test() != n_samp_test_feats)
        {
            throw std::logic_error(
                "Not all the features in phi_0 have the same number of test samples.");
        }
    }

    if ((_n_samp_train != 0) && (n_samp_train_feats != _n_samp_train))
    {
        throw std::logic_error(
            "The total number of samples in the updated primary feature set is not the same as the "
            "expected number of samples in the training set.");
    }

    if ((_prop_train.size() > 0) && (static_cast<int>(_prop_train.size()) != n_samp_train_feats))
    {
        throw std::logic_error(
            "The total number of samples in the updated primary feature set is not the same as the "
            "number of samples of the property vector for the training set.");
    }

    if ((_sample_ids_train.size() > 0) &&
        (static_cast<int>(_sample_ids_train.size()) != n_samp_train_feats))
    {
        throw std::logic_error(
            "The total number of samples in the updated primary feature set is not the same as the "
            "number of samples ids for the training set.");
    }

    if ((_n_samp_test != 0) && (n_samp_test_feats != _n_samp_test))
    {
        throw std::logic_error(
            "The total number of samples in the updated primary feature set is not the same as the "
            "expected number of samples in the test set.");
    }

    if ((_leave_out_inds.size() > 0) &&
        (static_cast<int>(_leave_out_inds.size()) != n_samp_test_feats))
    {
        throw std::logic_error(
            "The total number of samples in the updated primary feature set is not the same as the "
            "number of samples left out as the test set.");
    }

    if ((_prop_test.size() > 0) && (static_cast<int>(_prop_test.size()) != n_samp_test_feats))
    {
        throw std::logic_error(
            "The total number of samples in the updated primary feature set is not the same as the "
            "number of samples of the property vector for the test set.");
    }

    if ((_sample_ids_test.size() > 0) &&
        (static_cast<int>(_sample_ids_test.size()) != n_samp_test_feats))
    {
        throw std::logic_error(
            "The total number of samples in the updated primary feature set is not the same as the "
            "number of samples ids for the test set.");
    }
    std::vector<std::string> exprs(phi_0_ptrs.size());
    std::vector<std::vector<double>> values(phi_0_ptrs.size());
    std::vector<std::vector<double>> test_values(phi_0_ptrs.size());
    std::vector<Unit> units(phi_0_ptrs.size());
    std::vector<Domain> domains(phi_0_ptrs.size());
    for (size_t ff = 0; ff < phi_0_ptrs.size(); ++ff)
    {
        exprs[ff] = phi_0_ptrs[ff]->expr();
        values[ff] = phi_0_ptrs[ff]->value();
        test_values[ff] = phi_0_ptrs[ff]->test_value();
        units[ff] = phi_0_ptrs[ff]->unit();
        domains[ff] = phi_0_ptrs[ff]->domain();
    }

    node_value_arrs::initialize_values_arr(
        phi_0_ptrs[0]->n_samp(), phi_0_ptrs[0]->n_samp_test(), phi_0_ptrs.size());

    _phi_0.clear();
    for (size_t feat_ind = 0; feat_ind < phi_0_ptrs.size(); ++feat_ind)
    {
        _phi_0.push_back(FeatureNode(feat_ind,
                                     exprs[feat_ind],
                                     values[feat_ind],
                                     test_values[feat_ind],
                                     units[feat_ind],
                                     domains[feat_ind],
                                     true));
    }
}

void InputParser::set_phi_0(std::vector<FeatureNode> phi_0)
{
    std::vector<node_ptr> phi_0_ptrs(phi_0.size());
    std::transform(phi_0.begin(), phi_0.end(), phi_0_ptrs.begin(), [](FeatureNode feat) {
        return std::make_shared<FeatureNode>(feat);
    });
    set_phi_0(phi_0_ptrs);
}

void strip_comments(std::string& filename)
{
    //Open input and output file
    std::vector<std::string> filepath = str_utils::split_string_trim(filename, "/");
    std::string new_fn;
    if (filepath.size() > 1)
    {
        new_fn = str_utils::join("/", filepath.data(), filepath.size() - 1) + "/stripped_" +
                 filepath.back();
    }
    else
    {
        new_fn = "stripped_" + filename;
    }
    std::fstream inputfile;
    inputfile.open(filename);
    std::ofstream inputcopy;
    inputcopy.open(new_fn);

    //search for '//' or '#', delete everything following, print remainder to new file
    std::string line;
    size_t found, found2, find_py_comment;
    while (std::getline(inputfile, line))
    {
        found = line.find('/');
        found2 = line.find('/', found + 1);
        find_py_comment = line.find('#');
        if (found != line.npos && found2 == found + 1)
        {
            inputcopy << line.erase(found, line.length()) << std::endl;
        }
        else if (find_py_comment != std::string::npos)
        {
            inputcopy << line.erase(find_py_comment, line.length()) << std::endl;
        }
        else
        {
            inputcopy << line << std::endl;
        }
    }
    inputcopy.close();
    //update filename;
    filename = new_fn;
}

pt::ptree get_prop_tree(std::string fn, std::shared_ptr<MPI_Interface>& mpi_comm)
{
    if (mpi_comm->rank() == 0)
    {
        strip_comments(fn);
    }
    else
    {
        std::vector<std::string> filepath = str_utils::split_string_trim(fn, "/");
        if (filepath.size() > 1)
        {
            fn = str_utils::join("/", filepath.data(), filepath.size() - 1) + "/stripped_" +
                 filepath.back();
        }
        else
        {
            fn = "stripped_" + fn;
        }
    }

    mpi_comm->barrier();

    pt::ptree prop_tree;
    pt::json_parser::read_json(fn, prop_tree);

    mpi_comm->barrier();

    return prop_tree;
}
