// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file main.cpp
 *  @brief The main executable used to run SISSO++
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include <Kokkos_Core.hpp>

#include "descriptor_identifier/solver/SISSOClassifier.hpp"
#include "descriptor_identifier/solver/SISSOLogRegressor.hpp"
#include "inputs/InputParser.hpp"

int main(int argc, char *argv[])
{
    Kokkos::ScopeGuard kokkos(argc, argv);

    allowed_op_maps::set_node_maps();
#ifdef PARAMETERIZE
    allowed_op_maps::set_param_node_maps();
#endif

    mpi_setup::init_mpi_env();

    std::string filename;
    double duration = 0.0;
    if (argc < 2)
    {
        filename = "sisso.json";
    }
    else
    {
        filename = argv[1];
    }

    double start = omp_get_wtime();
    InputParser inputs(filename);
    mpi_setup::comm->barrier();
    duration = omp_get_wtime() - start;
    if (mpi_setup::comm->rank() == 0)
    {
        std::cout << "time input_parsing: " << duration << " s" << std::endl;
    }

    std::shared_ptr<FeatureSpace> feat_space = std::make_shared<FeatureSpace>(inputs);

    node_value_arrs::initialize_d_matrix_arr();
    if ((inputs.calc_type().compare("regression") == 0) || (inputs.calc_type().compare("regression_gpu") == 0))
    {
        SISSORegressor sisso(inputs, feat_space);
        sisso.fit();

        if (mpi_setup::comm->rank() == 0)
        {
            for (size_t ii = 0; ii < sisso.models().size(); ++ii)
            {
                std::cout << "Train RMSE: " << sisso.models()[ii][0].rmse() << " "
                          << inputs.prop_unit();
                if (inputs.prop_test().size() > 0)
                {
                    std::cout << "; Test RMSE: " << sisso.models()[ii][0].test_rmse() << " "
                              << inputs.prop_unit() << std::endl;
                }
                else
                {
                    std::cout << std::endl;
                }
                std::cout << sisso.models()[ii][0] << "\n" << std::endl;
            }
        }
    }
    else if (inputs.calc_type().compare("log_regression") == 0)
    {
        SISSOLogRegressor sisso(inputs, feat_space);
        sisso.fit();

        if (mpi_setup::comm->rank() == 0)
        {
            for (size_t ii = 0; ii < sisso.models().size(); ++ii)
            {
                std::cout << "Train RMSE: " << sisso.models()[ii][0].rmse();
                if (inputs.prop_test().size() > 0)
                {
                    std::cout << "; Test RMSE: " << sisso.models()[ii][0].test_rmse() << std::endl;
                }
                else
                {
                    std::cout << std::endl;
                }
                std::cout << sisso.models()[ii][0] << "\n" << std::endl;
            }
        }
    }
    else if (inputs.calc_type().compare("classification") == 0)
    {
        SISSOClassifier sisso(inputs, feat_space);
        sisso.fit();

        if (mpi_setup::comm->rank() == 0)
        {
            for (size_t ii = 0; ii < sisso.models().size(); ++ii)
            {
                std::cout << "Percent of training data in the convex overlap region: "
                          << sisso.models()[ii][0].percent_train_error() << "%";
                if (inputs.prop_test().size() > 0)
                {
                    std::cout << "; Percent of test data in the convex overlap region: "
                              << sisso.models()[ii][0].percent_test_error() << "%" << std::endl;
                }
                else
                {
                    std::cout << std::endl;
                }
                std::cout << sisso.models()[ii][0] << "\n" << std::endl;
            }
        }
        prop_sorted_d_mat::finalize_sorted_d_matrix_arr();
        comp_feats::reset_vectors();
    }
    mpi_setup::finalize_mpi_env();
    return 0;
}
