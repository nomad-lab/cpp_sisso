// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file loss_function/LossFunctionConvexHull.hpp
 *  @brief Defines the class that uses a convex hull projection operator and a convex hull based objective function
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef LOSS_FUNCTION_CONVEX_HULL
#define LOSS_FUNCTION_CONVEX_HULL

#include <fmt/core.h>

#include "classification/ConvexHull1D.hpp"
#include "classification/LPWrapper.hpp"
#include "loss_function/LossFunction.hpp"
#include "utils/vector_utils.hpp"

// DocString: cls_loss_function_convex_hull
/**
 * @brief The loss function used for classification problems
 *
 */
class LossFunctionConvexHull : public LossFunction
{
protected:
    // clang-format off
    std::vector<ConvexHull1D> _convex_hull; //!< Vector to the convex hull objects for projection
    std::vector<LPWrapper> _lp; //!< Vector for calculating the LP-optimization
    std::vector<std::shared_ptr<SVMWrapper>> _svm; //!< Vector containing objects used to create the SVM models

    std::map<double, double> _class_map; //!< Map of the actual class
    std::vector<double> _scores; //!< The scores for each of the projection properties
    std::map<int, int> _sample_inds_to_sorted_dmat_inds; //!< map from input sample inds to the SORTED_D_MATRIX_INDS
    std::map<int, int> _test_sample_inds_to_sorted_dmat_inds; //!< map from input sample inds to the SORTED_D_MATRIX_INDS
    std::vector<std::string> _coef_labels; //!< The labels for each set of coefficients
    std::vector<int> _n_class_per_task; //!< Number of classes in the property

    int _n_class; //!< Number of classes in the property
    const double _width; //!< The width used as the tolerance for the LP optimization
    // clang-format on

public:
    /**
     * @brief Constructor
     *
     * @param prop_train The value of the property to evaluate the loss function against for the training set
     * @param prop_test The value of the property to evaluate the loss function against for the test set
     * @param task_sizes_train Number of training samples per task
     * @param task_sizes_test Number of testing samples per task
     */
    LossFunctionConvexHull(std::vector<double> prop_train,
                           std::vector<double> prop_test,
                           std::vector<int> task_sizes_train,
                           std::vector<int> task_sizes_test,
                           int n_feat = 1);

    LossFunctionConvexHull(std::shared_ptr<LossFunction> o);

    /**
     * @brief Copy Constructor
     *
     * @param o LossFunctionConvexHull to be copied
     */
    LossFunctionConvexHull(const LossFunctionConvexHull& o) = default;

    /**
     * @brief Move Constructor
     *
     * @param o LossFunctionConvexHull to be moved
     */
    LossFunctionConvexHull(LossFunctionConvexHull&& o) = default;

    /**
     * @brief Initialize the LPWrappers used for calculating the loss function
     *
     * @param initialize_sorted_d_mat True if the sorted_d_mat needs to be initialized
     */
    void setup_lp(bool initialize_sorted_d_mat = false);

    /**
     * @brief Perform set up needed to calculate the projection scores
     */
    void prepare_project() override;

    /**
     * @brief Calculate the projection score of a feature
     *
     * @param feat Feature to calculate the projection score of
     * @return The projection score for the feature
     */
    virtual double project(const node_ptr& feat) override;

    /**
     * @brief Evaluate the loss function for a set of features whose data is stored in a central location
     *
     * @param inds Indexes to access the central storage area
     * @return The value of the loss function
     */
    double operator() (const std::vector<int>& inds) override;

    /**
     * @brief Evaluate the loss function for a set of features
     *
     * @param feats The features used to evaluate the loss function
     * @return The value of the loss function
     */
    double operator() (const std::vector<model_node_ptr>& feats) override;

    /**
     * @brief Evaluate the loss function for the test set
     *
     * @param feats The features used to evaluate the loss function
     * @return The value of the loss function for the test data
     */
    virtual double test_loss(const std::vector<model_node_ptr>& feats) override;

    /**
     * @brief Reset the the property used for projection
     *
     * @param model_list The models used to update the projection property
     */
    void reset_projection_prop(const std::vector<std::vector<model_node_ptr>>& models) override;

    /**
     * @brief The type of LossFunction
     */
    inline LOSS_TYPE type() const override { return LOSS_TYPE::CONVEX_HULL; }

    /**
     * @brief Set the number of features and linear model constants to those for the new n_feat
     *
     * @param n_feat The updated number of features for the model
     * @param initialize_sorted_d_mat If true then initialize the sorted descriptor matrix
     */
    void set_nfeat(int n_feat, bool initialize_sorted_d_mat);

    /**
     * @brief Set the number of features and linear model constants to those for the new n_feat
     *
     * @param n_feat The updated number of features for the model
     */
    inline void set_nfeat(int n_feat) override { set_nfeat(n_feat, false); }

    /**
     * @brief The number of classes in the calculation for a given task
     *
     * @param task_num The task to get the number of classes for
     */
    inline int n_class(int task_num) const override { return _n_class_per_task[task_num]; }

    /**
     * @brief The maximum number of classes in the calculation
     */
    inline int n_class() const override { return _n_class; }

    /**
     * @brief The labels for each set of coefficients
     */
    inline std::vector<std::string> coef_labels() const override { return _coef_labels; }

    /**
     * @brief Number of classes in the property
     */
    inline std::vector<int> n_class_per_task() const override { return _n_class_per_task; }

    inline std::shared_ptr<SVMWrapper> svm(int tt) override { return _svm[tt]; }
};

#endif
