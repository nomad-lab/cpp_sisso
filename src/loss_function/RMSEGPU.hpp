// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 *  @brief Defines the class that uses a Pearson correlation projection operator and a least-squares regression objective function
 *
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#pragma once

#include "external/third_party_kokkos.hpp"

#include "loss_function/LossFunction.hpp"
#include "utils/cuda.hpp"
#include "utils/DescriptorMatrix.hpp"
#include "utils/PropertiesVector.hpp"

// DocString: cls_loss_function_pearson_rmse
/**
 * @brief The loss function used for regression problems on GPU
 *
 */
class RMSEGPU
{
private:
    static constexpr int MAX_BATCHES = 262144;

    DescriptorMatrix::MATRIX_TYPE _descriptor_matrix;
    PropertiesVector::VECTOR_TYPE _properties;
    std::vector<int> _task_sizes;

    bool _fix_intercept; //!< If true then the bias term is fixed at 0
    int _n_feat; //!< Number features in the linear model
    int _n_dim; //!< Total number of constants to fit (scale and bias terms)
    int _n_samp; //!< Number of samples in the training set
    int _n_task; //!< Number of tasks

    /// dim 0: material samples
    /// dim 1: features
    /// dim 2: batch
    Kokkos::View<double** [MAX_BATCHES], Kokkos::LayoutLeft> _a;
    /// dim 0: material properties
    /// dim 1: batch
    Kokkos::View<double* [MAX_BATCHES], Kokkos::LayoutLeft> _b;
    Kokkos::View<double[MAX_BATCHES]> _batched_scores;
    Kokkos::View<int* [MAX_BATCHES], Kokkos::LayoutLeft> _models;

    /// The estimated value of the property to evaluate the loss function against for the training set
    Kokkos::View<double* [MAX_BATCHES], Kokkos::LayoutLeft> _estimated_training_properties;

    cublasHandle_t _cublas_handle;
    double** _batched_As = nullptr;
    double** _batched_bs = nullptr;

public:
    /**
     * @brief Constructor
     *
     * @param descriptor_matrix descriptor matrix
     * @param properties properties vector
     * @param task_sizes number of items per task
     * @param fix_intercept use a fixed offset?
     * @param n_feat Number features in the linear model
     */
    RMSEGPU(const DescriptorMatrix::MATRIX_TYPE& descriptor_matrix,
            const PropertiesVector::VECTOR_TYPE& properties,
            const std::vector<int>& task_sizes,
            bool fix_intercept = false,
            int n_feat = 1);

    ~RMSEGPU();

    /**
     * @brief Evaluate the loss function for a set of features
     *
     * @param feature_indices index tuples pointing into the descriptor matrix
     * @return Final score for every index tuple
     */
    Kokkos::View<double*> operator()(const std::vector<std::vector<int>>& feature_indices);

    /**
     * @brief Set the A matrix used for solving the least squares regression
     *
     * @param models index tuples
     * @param taskind The task used for the least squares regression
     * @param start The offset needed from the head of the feature's training data to where the task starts
     * @param batch_size number of systems to solve simultaneously
     */
    void set_a(const Kokkos::View<const int**, Kokkos::LayoutLeft>& models,
               int taskind,
               int start,
               int batch_size = MAX_BATCHES);

    /**
     * @brief Set the right hand side of the least square systems
     *
     * @param taskind The task used for the least squares regression
     * @param start The offset needed from the head of the feature's training data to where the task starts
     * @param batch_size number of systems to solve simultaneously
     */
    void set_b(int taskind, int start, int batch_size = MAX_BATCHES);

    /**
     * @brief Calculate estimated properties
     *
     * @param estimated_training_properties estimated properties
     * @param taskind The task for used for the least squares regression
     * @param start The offset needed from the head of the feature's training data to where the task starts
     * @param batch_size number of systems to solve simultaneously
     */
    void set_prop_train_est(
        Kokkos::View<double* [MAX_BATCHES], Kokkos::LayoutLeft> estimated_training_properties,
        int taskind,
        int start,
        int batch_size = MAX_BATCHES);

    /**
     * @brief Perform least squares regression
     *
     * @param taskind The task for used for the least squares regression
     * @param start The offset needed from the head of the feature's test data to where the task starts
     * @return info The final info value from dgels
     */
    int least_squares(int taskind, int start, int batch_size = MAX_BATCHES);
};
