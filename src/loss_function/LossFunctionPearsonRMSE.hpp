// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file loss_function/LossFunctionPearsonRMSE.hpp
 *  @brief Defines the class that uses a Pearson correlation projection operator and a least-squares regression objective function
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef LOSS_FUNCTION_PEARSON_RMSE
#define LOSS_FUNCTION_PEARSON_RMSE

#include "loss_function/LossFunction.hpp"

// DocString: cls_loss_function_pearson_rmse
/**
 * @brief The loss function used for regression problems
 *
 */
class LossFunctionPearsonRMSE : public LossFunction
{
protected:
    // clang-format off
    std::vector<double> _a; //!< The A matrix used for calculating the least squares solution of the problem
    std::vector<double> _b; //!< The property vector used to for solving the least squares solution
    std::vector<double> _c; //!< Vector used to temporarily store _projection_prop_sum
    std::vector<double> _scores; //!< Vector used to temporarily store _projection_prop_sum
    std::vector<double> _work; //!< Work vector for dgels

    int _lwork; //!< Ideal size of the work size
    // clang-format on

public:
    /**
     * @brief Constructor
     *
     * @param prop_train The value of the property to evaluate the loss function against for the training set
     * @param prop_test The value of the property to evaluate the loss function against for the test set
     * @param task_sizes_train Number of training samples per task
     * @param task_sizes_test Number of testing samples per task
     */
    LossFunctionPearsonRMSE(std::vector<double> prop_train,
                            std::vector<double> prop_test,
                            std::vector<int> task_sizes_train,
                            std::vector<int> task_sizes_test,
                            bool fix_intercept = false,
                            int n_feat = 1);

    LossFunctionPearsonRMSE(std::shared_ptr<LossFunction> o);

    /**
     * @brief Copy Constructor
     *
     * @param o LossFunctionPearsonRMSE to be copied
     */
    LossFunctionPearsonRMSE(const LossFunctionPearsonRMSE& o) = default;

    /**
     * @brief Move Constructor
     *
     * @param o LossFunctionPearsonRMSE to be moved
     */
    LossFunctionPearsonRMSE(LossFunctionPearsonRMSE&& o) = default;

    /**
     * @brief Copy Assignment operator
     *
     * @param o LossFunctionPearsonRMSE to be copied
     */
    LossFunctionPearsonRMSE& operator=(const LossFunctionPearsonRMSE& o) = default;

    /**
     * @brief Move Assignment operator
     *
     * @param o LossFunctionPearsonRMSE to be moved
     */
    LossFunctionPearsonRMSE& operator=(LossFunctionPearsonRMSE&& o) = default;

    virtual ~LossFunctionPearsonRMSE() = default;

    /**
     * @brief Perform set up needed to calculate the projection scores
     */
    void prepare_project() override;

    /**
     * @brief Calculate the projection score of a feature
     *
     * @param feat Feature to calculate the projection score of
     * @return The projection score for the feature
     */
    virtual double project(const node_ptr& feat) override;

    /**
     * @brief Calculate the max Pearson correlation for all features
     *
     * @param feat_val_ptr pointer to the data of the feature
     * @return The max Pearson correlation between the feature and properties
     */
    double calc_max_pearson(double* feat_val_ptr);

    /**
     * @brief Evaluate the loss function for a set of features whose data is stored in a central location
     *
     * @param inds Indexes to access the central storage area
     * @return The value of the loss function
     */
    double operator()(const std::vector<int>& inds) override;

    /**
     * @brief Evaluate the loss function for a set of features
     *
     * @param feats The features used to evaluate the loss function
     * @return The value of the loss function
     */
    double operator()(const std::vector<model_node_ptr>& feats) override;

    /**
     * @brief Evaluate the loss function for the test set
     *
     * @param feats The features used to evaluate the loss function
     * @return The value of the loss function for the test data
     */
    double test_loss(const std::vector<model_node_ptr>& feats) override;

    /**
     * @brief Get the optimal size of the work vector
     */
    void set_opt_lwork();

    /**
     * @brief Set the A matrix used for solving the least squares regression
     *
     * @param inds A vector storing the _d_mat_ind of the features to pull data from the Descriptor Matrix storage array
     * @param taskind The task for used for the least squares regression
     * @param start The offset needed from the head of the feature's training data to where the task starts
     */
    virtual void set_a(const std::vector<int>& inds, int taskind, int start);

    /**
     * @brief Set the A matrix used for solving the least squares regression
     *
     * @param feats The features used to evaluate the loss function
     * @param taskind The task for used for the least squares regression
     * @param start The offset needed from the head of the feature's training data to where the task starts
     */
    virtual void set_a(const std::vector<model_node_ptr>& feats, int taskind, int start);

    /**
     * @brief Set the error vector
     *
     * @param inds Indexes to access the central storage area
     * @param taskind The task for used for the least squares regression
     * @param start The offset needed from the head of the feature's training data to where the task starts
     */
    virtual void set_prop_train_est(const std::vector<int>& inds, int taskind, int start);

    /**
     * @brief Set the error
     *
     * @param feats The features used to evaluate the loss function
     * @param taskind The task for used for the least squares regression
     * @param start The offset needed from the head of the feature's training data to where the task starts
     */
    virtual void set_prop_train_est(const std::vector<model_node_ptr>& feats,
                                    int taskind,
                                    int start);

    /**
     * @brief Set the test error and return the RMSE
     *
     * @param feats The features used to evaluate the loss function for the test data
     * @param taskind The task for used for the least squares regression
     * @param start The offset needed from the head of the feature's test data to where the task starts
     */
    virtual void set_prop_test_est(const std::vector<model_node_ptr>& feats,
                                   int taskind,
                                   int start);

    /**
     * @brief Perform least squares regression
     *
     * @param taskind The task for used for the least squares regression
     * @param start The offset needed from the head of the feature's test data to where the task starts
     * @return info The final info value from dgels
     */
    int least_squares(int taskind, int start);

    /**
     * @brief Reset the the property used for projection
     *
     * @param model_list The models used to update the projection property
     */
    void reset_projection_prop(const std::vector<std::vector<model_node_ptr>>& models) override;

    /**
     * @brief The type of LossFunction
     */
    virtual inline LOSS_TYPE type() const override { return LOSS_TYPE::PEARSON_RMSE; }

    /**
     * @brief Set the number of features and linear model constants to those for the new n_feat
     *
     * @param n_feat The updated number of features for the model
     */
    void set_nfeat(int n_feat) override;
};

#endif
