// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file loss_function/LossFunctionLogPearsonRMSE.cpp
 *  @brief Implements the class that uses a logarithmic Pearson correlation projection operator and a logarithmic least-squares regression objective function
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "loss_function/LossFunctionLogPearsonRMSE.hpp"

LossFunctionLogPearsonRMSE::LossFunctionLogPearsonRMSE(std::vector<double> prop_train,
                                                       std::vector<double> prop_test,
                                                       std::vector<int> task_sizes_train,
                                                       std::vector<int> task_sizes_test,
                                                       bool fix_intercept,
                                                       int n_feat)
    : LossFunctionPearsonRMSE(
          prop_train, prop_test, task_sizes_train, task_sizes_test, fix_intercept, n_feat),
      _log_feat(_n_samp, 0.0)
{
    set_nfeat(_n_feat);
}

LossFunctionLogPearsonRMSE::LossFunctionLogPearsonRMSE(std::shared_ptr<LossFunction> o)
    : LossFunctionPearsonRMSE(o), _log_feat(_n_samp, 0.0)
{
    set_nfeat(_n_feat);
}

double LossFunctionLogPearsonRMSE::project(const node_ptr& feat)
{
    std::fill_n(_scores.begin(), _scores.size(), 0.0);

    double* val_ptr = feat->value_ptr(-1, true);
    std::transform(
        val_ptr, val_ptr + _n_samp, _log_feat.data(), [](double val) { return std::log(val); });

    double r2 = calc_max_pearson(_log_feat.data());
    return std::isfinite(r2) ? r2 : 0.0;
}

void LossFunctionLogPearsonRMSE::set_a(const std::vector<int>& inds, int taskind, int start)
{
    for (size_t ff = 0; ff < inds.size(); ++ff)
    {
        double* val_ptr = node_value_arrs::get_d_matrix_ptr(inds[ff]) + start;
        std::transform(val_ptr,
                       val_ptr + _task_sizes_train[taskind],
                       &_a[ff * _task_sizes_train[taskind] + start * _n_dim],
                       [](double val) { return std::log(val); });
    }
}

void LossFunctionLogPearsonRMSE::set_a(const std::vector<model_node_ptr>& feats,
                                       int taskind,
                                       int start)
{
    for (size_t ff = 0; ff < feats.size(); ++ff)
    {
        double* val_ptr = feats[ff]->value_ptr() + start;
        std::transform(val_ptr,
                       val_ptr + _task_sizes_train[taskind],
                       &_a[ff * _task_sizes_train[taskind] + start * _n_dim],
                       [](double val) { return std::log(val); });
    }
}

void LossFunctionLogPearsonRMSE::set_prop_train_est(const std::vector<int>& inds,
                                                    int taskind,
                                                    int start)
{
    std::fill_n(_prop_train_est.begin() + start,
                _task_sizes_train[taskind],
                (!_fix_intercept) * _coefs[(taskind + 1) * _n_dim - 1]);

    for (size_t ff = 0; ff < inds.size(); ++ff)
    {
        std::transform(&_prop_train_est[start],
                       &_prop_train_est[start + _task_sizes_train[taskind]],
                       node_value_arrs::get_d_matrix_ptr(inds[ff]) + start,
                       &_prop_train_est[start],
                       [this, &taskind, &ff](double p_est, double val) {
                           return p_est + _coefs[taskind * _n_dim + ff] * std::log(val);
                       });
    }
}

void LossFunctionLogPearsonRMSE::set_prop_train_est(const std::vector<model_node_ptr>& feats,
                                                    int taskind,
                                                    int start)
{
    std::fill_n(_prop_train_est.begin() + start,
                _task_sizes_train[taskind],
                (!_fix_intercept) * _coefs[(taskind + 1) * _n_dim - 1]);

    for (size_t ff = 0; ff < feats.size(); ++ff)
    {
        std::transform(&_prop_train_est[start],
                       &_prop_train_est[start + _task_sizes_train[taskind]],
                       feats[ff]->value_ptr() + start,
                       &_prop_train_est[start],
                       [this, &taskind, &ff](double p_est, double val) {
                           return p_est + _coefs[taskind * _n_dim + ff] * std::log(val);
                       });
    }
}

void LossFunctionLogPearsonRMSE::set_prop_test_est(const std::vector<model_node_ptr>& feats,
                                                   int taskind,
                                                   int start)
{
    std::fill_n(_prop_test_est.begin() + start,
                _task_sizes_test[taskind],
                (!_fix_intercept) * _coefs[(taskind + 1) * _n_dim - 1]);

    for (size_t ff = 0; ff < feats.size(); ++ff)
    {
        std::transform(&_prop_test_est[start],
                       &_prop_test_est[start + _task_sizes_test[taskind]],
                       feats[ff]->test_value_ptr() + start,
                       &_prop_test_est[start],
                       [this, &taskind, &ff](double p_est, double val) {
                           return p_est + _coefs[taskind * _n_dim + ff] * std::log(val);
                       });
    }
}
