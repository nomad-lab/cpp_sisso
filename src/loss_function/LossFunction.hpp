// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file loss_function/LossFunction.hpp
 *  @brief Defines a base class used to calculate the projection score and l0-regularization objective function
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef LOSS_FUNCTION
#define LOSS_FUNCTION

#include "classification/SVMWrapper.hpp"
#include "feature_creation/node/ModelNode.hpp"

// DocString: cls_loss_function
/**
 * @brief The loss function used to project features onto the property and calculate the loss function of the models
 *
 */
class LossFunction
{
protected:
    // clang-format off
    std::vector<double> _projection_prop; //!< The property vector used for projection
    std::vector<double> _projection_prop_std; //!< The standardized property vector used for projection

    std::vector<double> _prop_train; //!< The value of the property to evaluate the loss function against for the training set
    std::vector<double> _prop_test; //!< The value of the property to evaluate the loss function against for the test set

    std::vector<double> _prop_train_est; //!< The estimated value of the property to evaluate the loss function against for the training set
    std::vector<double> _prop_test_est; //!< The estimated value of the property to evaluate the loss function against for the test set

    std::vector<double> _error_train; //!< The error vector for the training set
    std::vector<double> _error_test; //!< The error vector for the test set
    std::vector<double> _coefs; //!< Coefficients for the linear model

    const std::vector<int> _task_sizes_train; //!< Number of training samples per task
    const std::vector<int> _task_sizes_test; //!< Number of testing samples per task

    const int _n_samp; //!< Number of samples in the training set
    const int _n_samp_test; //!< Number of samples in the test set
    const int _n_task; //!< Number of tasks
    int _n_project_prop; //!< Number of properties to project over
    int _n_feat; //!< Number features in the linear model
    int _n_dim; //!< Total number of constants to fit (scale and bias terms)
    const bool _fix_intercept; //!< If true then the bias term is fixed at 0
    // clang-format on
public:
    /**
     * @brief Constructor
     *
     * @param prop_train The value of the property to evaluate the loss function against for the training set
     * @param prop_test The value of the property to evaluate the loss function against for the test set
     * @param task_sizes_train Number of training samples per task
     * @param task_sizes_test Number of testing samples per task
     */
    LossFunction(std::vector<double> prop_train,
                 std::vector<double> prop_test,
                 std::vector<int> task_sizes_train,
                 std::vector<int> task_sizes_test,
                 bool fix_intercept = false,
                 int n_feat = 1);

    LossFunction(std::shared_ptr<LossFunction> o);

    /**
     * @brief Copy Constructor
     *
     * @param o LossFunction to be copied
     */
    LossFunction(const LossFunction& o) = default;

    /**
     * @brief Move Constructor
     *
     * @param o LossFunction to be moved
     */
    LossFunction(LossFunction&& o) = default;

    /**
     * @brief Copy Assignment operator
     *
     * @param o LossFunction to be copied
     */
    LossFunction& operator=(const LossFunction& o) = default;

    /**
     * @brief Move Assignment operator
     *
     * @param o LossFunction to be moved
     */
    LossFunction& operator=(LossFunction&& o) = default;

    virtual ~LossFunction() = default;

    /**
     * @brief Perform set up needed to calculate the projection scores
     */
    virtual void prepare_project() = 0;

    /**
     * @brief Calculate the projection score of a feature
     *
     * @param feat Feature to calculate the projection score of
     * @return The projection score for the feature
     */
    virtual double project(const node_ptr& feat) = 0;

    /**
     * @brief Evaluate the loss function for a set of features whose data is stored in a central location
     *
     * @param inds Indexes to access the central storage area
     * @return The value of the loss function
     */
    virtual double operator()(const std::vector<int>& inds) = 0;

    /**
     * @brief Evaluate the loss function for a set of features
     *
     * @param feats The features used to evaluate the loss function
     * @return The value of the loss function
     */
    virtual double operator()(const std::vector<model_node_ptr>& feats) = 0;

    /**
     * @brief Evaluate the loss function for the test set
     *
     * @param feats The features used to evaluate the loss function
     * @return The value of the loss function for the test data
     */
    virtual double test_loss(const std::vector<model_node_ptr>& feats) = 0;

    /**
     * @brief Set the model list from the previous generation
     * @details [long description]
     *
     * @param model_list [description]
     */
    virtual void reset_projection_prop(const std::vector<std::vector<model_node_ptr>>& models) = 0;

    /**
     * @brief The type of LossFunction
     */
    virtual LOSS_TYPE type() const = 0;

    /**
     * @brief Coefficients for the linear model
     */
    std::vector<double> coefs() const { return _coefs; };

    /**
     * @brief Number of training samples per task
     */
    inline std::vector<int> task_sizes_train() const { return _task_sizes_train; }

    /**
     * @brief Number of testing samples per task
     */
    inline std::vector<int> task_sizes_test() const { return _task_sizes_test; }

    /**
     * @brief The value of the property to evaluate the loss function against for the training set
     */
    inline const std::vector<double>& prop_train() const { return _prop_train; }

    /**
     * @brief The value of the property to evaluate the loss function against for the test set
     */
    inline const std::vector<double>& prop_test() const { return _prop_test; }

    /**
     * @brief The estimated value of the property to evaluate the loss function against for the training set
     */
    inline const std::vector<double>& prop_train_est() const { return _prop_train_est; }

    /**
     * @brief The estimated value of the property to evaluate the loss function against for the test set
     */
    inline const std::vector<double>& prop_test_est() const { return _prop_test_est; }

    /**
     * @brief The property vector used for projection
     */
    inline const std::vector<double>& prop_project() const { return _projection_prop; }

    /**
     * @brief The standardized property vector used for projection
     */
    inline const std::vector<double>& prop_project_std() const { return _projection_prop_std; }

    /**
     * @brief Copy of the value of the property to evaluate the loss function against for the training set
     */
    inline const std::vector<double> prop_train_copy() const { return _prop_train; }

    /**
     * @brief Copy of the value of the property to evaluate the loss function against for the test set
     */
    inline const std::vector<double> prop_test_copy() const { return _prop_test; }

    /**
     * @brief Copy of the estimated value of the property to evaluate the loss function against for the training set
     */
    inline const std::vector<double> prop_train_est_copy() const { return _prop_train_est; }

    /**
     * @brief Copy of the estimated value of the property to evaluate the loss function against for the test set
     */
    inline const std::vector<double> prop_test_est_copy() const { return _prop_test_est; }

    /**
     * @brief Copy of the property vector used for projection
     */
    inline const std::vector<double> prop_project_copy() const { return _projection_prop; }

    /**
     * @brief Copy of the standardized property vector used for projection
     */
    inline const std::vector<double> prop_project_std_copy() const { return _projection_prop_std; }

    /**
     * @brief Pointer to the head of the value of the property to evaluate the loss function against for the training set
     */
    inline const double* prop_pointer() const { return _prop_train.data(); }

    /**
     * @brief Pointer to the head of the value of the property to evaluate the loss function against for the test set
     */
    inline const double* prop_test_pointer() const { return _prop_test.data(); }

    /**
     * @brief Pointer to the head of the estimated value of the property to evaluate the loss function against for the training set
     */
    inline const double* prop_est_pointer() const { return _prop_train_est.data(); }

    /**
     * @brief Pointer to the head of the estimated value of the property to evaluate the loss function against for the test set
     */
    inline const double* prop_test_est_pointer() const { return _prop_test_est.data(); }

    /**
     * @brief Pointer to the head of the property vector used for projection
     */
    inline const double* prop_project_pointer() const { return _projection_prop.data(); }

    /**
     * @brief Pointer to the head of the standardized property vector used for projection
     */
    inline const double* prop_project_std_pointer() const { return _projection_prop_std.data(); }

    /**
     * @brief The error vector for the training set
     */
    inline const std::vector<double>& error_train() const { return _error_train; }

    /**
     * @brief The error vector for the test set
     */
    inline const std::vector<double>& error_test() const { return _error_test; }

    /**
     * @brief Copy of the error vector for the training set
     */
    inline const std::vector<double> error_train_copy() const { return _error_train; }

    /**
     * @brief Copy of the error vector for the test set
     */
    inline const std::vector<double> error_test_copy() const { return _error_test; }

    /**
     * @brief If true, then the bias term is fixed at 0
     */
    inline bool fix_intercept() const { return _fix_intercept; }

    /**
     * @brief Number features in the linear model
     */
    inline int n_feat() const { return _n_feat; }

    /**
     * @brief Total number of constants to fit (scale and bias terms)
     */
    inline int n_dim() const { return _n_dim; }

    /**
     * @brief Number of samples in the training setCoefficients for the linear model
     */
    inline int n_samp() const { return _n_samp; }

    /**
     * @brief  Number of samples in the test setCoefficients for the linear model
     */
    inline int n_samp_test() const { return _n_samp_test; }

    /**
     * @brief Number of tasks
     */
    inline int n_task() const { return _n_task; }

    /**
     * @brief Number of properties to project over
     */
    inline int n_prop_project() { return _n_project_prop; }

    /**
     * @brief Set the number of features and linear model constants to those for the new n_feat
     *
     * @param n_feat The updated number of features for the model
     */
    virtual inline void set_nfeat(int n_feat)
    {
        _n_feat = n_feat;
        _n_dim = n_feat + (!_fix_intercept);
    }

    /**
     * @brief Number of classes in the property
     */
    virtual inline std::vector<int> n_class_per_task() const { return {}; }

    /**
     * @brief The number of classes in the calculation for a given task
     *
     * @param task_num The task to get the number of classes for
     */
    virtual inline int n_class(int task_num) const { return 0; }

    /**
     * @brief The maximum number of classes in the calculation
     */
    virtual inline int n_class() const { return 0; }

    /**
     * @brief The labels for each set of coefficients
     */
    virtual inline std::vector<std::string> coef_labels() const { return {}; }

    virtual inline std::shared_ptr<SVMWrapper> svm(int tt)
    {
        throw std::logic_error("SVMWrappers only available for classification problems");
        return nullptr;
    }
};

#ifdef PY_BINDINGS
class PyLossFunction : public LossFunction
{
    using LossFunction::LossFunction;

    void prepare_project() override { PYBIND11_OVERRIDE_PURE(void, LossFunction, prepare_project,); }

    double project(const node_ptr& feat) override
    {
        PYBIND11_OVERRIDE_PURE(double, LossFunction, project, feat);
    }

    double operator()(const std::vector<int>& inds) override
    {
        PYBIND11_OVERRIDE_PURE(double, LossFunction, operator(), inds);
    }

    double operator()(const std::vector<model_node_ptr>& feats) override
    {
        PYBIND11_OVERRIDE_PURE(double, LossFunction, operator(), feats);
    }

    double test_loss(const std::vector<model_node_ptr>& feats) override
    {
        PYBIND11_OVERRIDE_PURE(double, LossFunction, test_loss, feats);
    }

    void reset_projection_prop(const std::vector<std::vector<model_node_ptr>>& models) override
    {
        PYBIND11_OVERRIDE_PURE(void, LossFunction, reset_projection_prop, models);
    }

    LOSS_TYPE type() const override { PYBIND11_OVERRIDE_PURE(LOSS_TYPE, LossFunction, type,); }

    void set_nfeat(int n_feat) override
    {
        PYBIND11_OVERRIDE(void, LossFunction, set_nfeat, n_feat);
    }

    std::vector<int> n_class_per_task() const override
    {
        PYBIND11_OVERRIDE(std::vector<int>, LossFunction, n_class_per_task,);
    }

    int n_class(int task_num) const override
    {
        PYBIND11_OVERRIDE(int, LossFunction, n_class, task_num);
    }

    int n_class() const override { PYBIND11_OVERRIDE(int, LossFunction, n_class, ); }

    std::vector<std::string> coef_labels() const override
    {
        PYBIND11_OVERRIDE(std::vector<std::string>, LossFunction, coef_labels,);
    }

    std::shared_ptr<SVMWrapper> svm(int tt) override
    {
        PYBIND11_OVERRIDE(std::shared_ptr<SVMWrapper>, LossFunction, svm, tt);
    }
};
#endif

#endif
