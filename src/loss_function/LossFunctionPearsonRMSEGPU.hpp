// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 *  @brief Defines the class that uses a Pearson correlation projection operator and a least-squares regression objective function
 *
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 *  @bug No known bugs.
 */

#pragma once

#include "loss_function/LossFunction.hpp"

// DocString: cls_loss_function_pearson_rmse_gpu
/**
 * @brief A proxy type for the loss function used for regression problems on gpu
 *
 */
class LossFunctionPearsonRMSEGPU : public LossFunctionPearsonRMSE
{
public:
    /**
 * @brief Constructor
 *
 * @param prop_train The value of the property to evaluate the loss function against for the training set
 * @param prop_test The value of the property to evaluate the loss function against for the test set
 * @param task_sizes_train Number of training samples per task
 * @param task_sizes_test Number of testing samples per task
 */
    LossFunctionPearsonRMSEGPU(std::vector<double> prop_train,
                               std::vector<double> prop_test,
                               std::vector<int> task_sizes_train,
                               std::vector<int> task_sizes_test,
                               bool fix_intercept = false,
                               int n_feat = 1)
        : LossFunctionPearsonRMSE(
              prop_train, prop_test, task_sizes_train, task_sizes_test, fix_intercept, n_feat){};

    LossFunctionPearsonRMSEGPU(std::shared_ptr<LossFunction> o) : LossFunctionPearsonRMSE(o){};

    /**
     * @brief Copy Constructor
     *
     * @param o LossFunctionLogPearsonRMSE to be copied
     */
    LossFunctionPearsonRMSEGPU(const LossFunctionPearsonRMSEGPU& o) = default;

    /**
     * @brief Move Constructor
     *
     * @param o LossFunctionLogPearsonRMSE to be moved
     */
    LossFunctionPearsonRMSEGPU(LossFunctionPearsonRMSEGPU&& o) = default;

    /**
     * @brief Copy Assignment operator
     *
     * @param o LossFunctionLogPearsonRMSE to be copied
     */
    LossFunctionPearsonRMSEGPU& operator=(const LossFunctionPearsonRMSEGPU& o) = default;

    /**
     * @brief Move Assignment operator
     *
     * @param o LossFunctionLogPearsonRMSE to be moved
     */
    LossFunctionPearsonRMSEGPU& operator=(LossFunctionPearsonRMSEGPU&& o) = default;

    /**
     * @brief The type of LossFunction
     */
    inline LOSS_TYPE type() const override { return LOSS_TYPE::PEARSON_RMSE_GPU; }
};
