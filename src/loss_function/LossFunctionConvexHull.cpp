// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file loss_function/LossFunctionConvexHull.cpp
 *  @brief Implements the class that uses a convex hull projection operator and a convex hull based objective function
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#include "loss_function/LossFunctionConvexHull.hpp"

LossFunctionConvexHull::LossFunctionConvexHull(std::vector<double> prop_train,
                                               std::vector<double> prop_test,
                                               std::vector<int> task_sizes_train,
                                               std::vector<int> task_sizes_test,
                                               int n_feat)
    : LossFunction(prop_train, prop_test, task_sizes_train, task_sizes_test, false, n_feat),
      _n_class_per_task(task_sizes_train.size(), 0),
      _n_class(0),
      _width(1e-5)
{
    for (auto& pt : prop_test)
    {
        if (std::none_of(prop_train.begin(), prop_train.end(), [&pt](double pp) {
                return std::abs(pp - pt) < 1e-10;
            }))
        {
            throw std::logic_error(
                "A class in the property vector (test set) is not in the training set.");
        }
    }

    int start = 0.0;
    std::vector<double> unique_classes;
    for (size_t tt = 0; tt < _task_sizes_train.size(); ++tt)
    {
        unique_classes = vector_utils::unique<double>(prop_train.data() + start,
                                                      _task_sizes_train[tt]);
        _n_class_per_task[tt] = unique_classes.size();
        for (size_t c1 = 0; c1 < unique_classes.size(); ++c1)
        {
            for (size_t c2 = c1 + 1; c2 < unique_classes.size(); ++c2)
            {
                _coef_labels.push_back(
                    fmt::format("_{:.1f}_{:.1f}", unique_classes[c1], unique_classes[c2]));
            }
        }
        start += _task_sizes_train[tt];
    }

    unique_classes = vector_utils::unique<double>(prop_train);
    _n_class = unique_classes.size();
    for (int cc = 0; cc < _n_class; ++cc)
    {
        _class_map[unique_classes[cc]] = static_cast<double>(cc);
    }

    std::transform(prop_train.begin(), prop_train.end(), prop_train.begin(), [this](double pp) {
        return _class_map[pp];
    });
    std::transform(prop_test.begin(), prop_test.end(), prop_test.begin(), [this](double pp) {
        return _class_map[pp];
    });

    std::vector<bool> not_int(prop_train.size());
    std::transform(prop_train.begin(), prop_train.end(), not_int.begin(), [](double pp) {
        return std::abs(pp - std::round(pp)) >= 1e-10;
    });
    if (std::any_of(not_int.begin(), not_int.end(), [](bool b) { return b; }))
    {
        throw std::logic_error("For classification the property (training set) must be an integer");
    }

    not_int.resize(prop_test.size());
    std::transform(prop_test.begin(), prop_test.end(), not_int.begin(), [](double pp) {
        return std::abs(pp - std::round(pp)) >= 1e-10;
    });
    if (std::any_of(not_int.begin(), not_int.end(), [](bool b) { return b; }))
    {
        throw std::logic_error("For classification the property (test set) must be an integer");
    }

    set_nfeat(_n_feat, true);
    prepare_project();
}

LossFunctionConvexHull::LossFunctionConvexHull(std::shared_ptr<LossFunction> o)
    : LossFunction(o),
      _coef_labels(o->coef_labels()),
      _n_class_per_task(o->n_class_per_task()),
      _n_class(o->n_class()),
      _width(1e-5)
{
    set_nfeat(_n_feat);
    prepare_project();
}

void LossFunctionConvexHull::set_nfeat(int n_feat, bool initialize_sorted_d_mat)
{
    _n_feat = n_feat;
    _n_dim = n_feat + 1;
    setup_lp(initialize_sorted_d_mat);
    int n_class_combos = std::accumulate(
        _n_class_per_task.begin(), _n_class_per_task.end(), 0, [](int tot, int nc) {
            return tot + nc * (nc - 1) / 2;
        });
    _coefs.resize(n_class_combos * _n_dim, 0.0);

    int start = 0;
    _svm.clear();
    for (int tt = 0; tt < _n_task; ++tt)
    {
        _svm.push_back(std::make_shared<SVMWrapper>(
            _n_class_per_task[tt], _n_feat, _task_sizes_train[tt], &_prop_train[start]));
        start += _task_sizes_train[tt];
    }
}

void LossFunctionConvexHull::prepare_project()
{
    _scores.resize(_n_project_prop);
    _convex_hull.resize(_n_project_prop);
    for (int pp = 0; pp < _n_project_prop; ++pp)
    {
        _convex_hull[pp].initialize_prop(_task_sizes_train, &_projection_prop[pp * _n_samp]);

        int start = 0;
        for (int tt = 0; tt < _n_task; ++tt)
        {
            double prop_mean = util_funcs::mean(_projection_prop.data() + pp * _n_samp + start,
                                                _task_sizes_train[tt]);

            double prop_std = util_funcs::stand_dev(_projection_prop.data() + pp * _n_samp + start,
                                                    _task_sizes_train[tt]);
            std::transform(
                _projection_prop.begin() + pp * _n_samp + start,
                _projection_prop.begin() + pp * _n_samp + start + _task_sizes_train[tt],
                _projection_prop_std.begin() + pp * _task_sizes_train[tt] + start * _n_project_prop,
                [prop_mean, prop_std](double p) { return (p - prop_mean) / prop_std; });
            start += _task_sizes_train[tt];
        }
    }
}

double LossFunctionConvexHull::project(const node_ptr& feat)
{
    double* val_ptr = feat->value_ptr(-1, true);
    for (int pp = 0; pp < _n_project_prop; ++pp)
    {
        _scores[pp] = _convex_hull[pp].overlap_1d(val_ptr);
    }
    return *std::min_element(_scores.begin(), _scores.end());
}

void LossFunctionConvexHull::setup_lp(bool initialize_sorted_d_mat)
{
    int task_start = 0;
    int task_start_test = 0;
    _lp = {};

    std::map<double, int> rep_map;
    std::vector<double> unique_classes = vector_utils::unique(_prop_train);
    std::sort(unique_classes.begin(), unique_classes.end());

    for (size_t uc = 0; uc < unique_classes.size(); ++uc)
    {
        rep_map[unique_classes[uc]] = uc;
    }

    std::vector<int> n_samp_per_class(_n_class * _n_task);
    for (int tt = 0; tt < _n_task; ++tt)
    {
        std::vector<int> inds(_task_sizes_train[tt]);
        std::iota(inds.begin(), inds.end(), task_start);
        util_funcs::argsort<double>(inds.data(), inds.data() + inds.size(), _prop_train.data());

        for (size_t ii = 0; ii < inds.size(); ++ii)
        {
            _sample_inds_to_sorted_dmat_inds[inds[ii]] = ii + task_start;
            ++n_samp_per_class[tt * _n_class + rep_map[_prop_train[inds[ii]]]];
        }

        std::vector<int> samp_per_class(_n_class);
        std::copy_n(n_samp_per_class.begin() + tt * _n_class, _n_class, samp_per_class.begin());
        task_start += _task_sizes_train[tt];

        std::vector<int> samp_test_per_class(samp_per_class.size(), 0);
        if (_task_sizes_test[tt] > 0)
        {
            inds.resize(_task_sizes_test[tt]);
            std::iota(inds.begin(), inds.end(), task_start_test);
            util_funcs::argsort<double>(inds.data(), inds.data() + inds.size(), _prop_test.data());

            for (size_t ii = 0; ii < inds.size(); ++ii)
            {
                _test_sample_inds_to_sorted_dmat_inds[inds[ii]] = ii + task_start_test;
                ++samp_test_per_class[rep_map[_prop_test[inds[ii]]]];
            }
            task_start_test += _task_sizes_test[tt];
        }
        LPWrapper lp(
            samp_per_class,
            tt,
            _n_class,
            _n_feat,
            std::accumulate(n_samp_per_class.begin() + tt * _n_class, n_samp_per_class.end(), 0),
            _width,
            samp_test_per_class,
            std::accumulate(samp_test_per_class.begin(), samp_test_per_class.end(), 0));
        _lp.push_back(lp);
    }
    if ((omp_get_thread_num() == 0) && initialize_sorted_d_mat)
    {
        prop_sorted_d_mat::initialize_sorted_d_matrix_arr(
            std::max(prop_sorted_d_mat::N_FEATURES, _n_feat), _n_task, _n_class, n_samp_per_class);
    }
}

void LossFunctionConvexHull::reset_projection_prop(
    const std::vector<std::vector<model_node_ptr>>& models)
{
    _n_project_prop = models.size();
    _projection_prop.resize(_n_samp * _n_project_prop, 0);
    _projection_prop_std.resize(_n_samp * _n_project_prop, 0);
    for (int mm = 0; mm < _n_project_prop; ++mm)
    {
        (*this)(models[mm]);
        std::copy_n(_error_train.data(), _error_train.size(), &_projection_prop[mm * _n_samp]);
    }

    set_nfeat(models.back().size() + 1);
    prepare_project();
}

double LossFunctionConvexHull::operator()(const std::vector<int>& inds)
{
    double n_convex_overlap = 0.0;
    for (auto& lp : _lp)
    {
        n_convex_overlap += static_cast<double>(lp.get_n_overlap(inds));
    }
    return n_convex_overlap * _n_samp * _n_class;
}

double LossFunctionConvexHull::operator()(const std::vector<model_node_ptr>& feats)
{
    std::vector<std::vector<double>> sorted_values(feats.size(), std::vector<double>(_n_samp, 0.0));
    std::vector<std::vector<double>> sorted_test_values(feats.size(),
                                                        std::vector<double>(_n_samp_test, 0.0));

    for (int ff = 0; ff < _n_feat; ++ff)
    {
        double* val_ptr = feats[ff]->value_ptr();
        std::for_each(_sample_inds_to_sorted_dmat_inds.begin(),
                      _sample_inds_to_sorted_dmat_inds.end(),
                      [&sorted_values, ff, val_ptr](auto& iter) {
                          sorted_values[ff][iter.second] = val_ptr[iter.first];
                      });

        val_ptr = feats[ff]->test_value_ptr();

        std::for_each(_test_sample_inds_to_sorted_dmat_inds.begin(),
                      _test_sample_inds_to_sorted_dmat_inds.end(),
                      [&sorted_test_values, ff, val_ptr](auto& iter) {
                          sorted_test_values[ff][iter.second] = val_ptr[iter.first];
                      });
    }

    int start = 0;
    int start_test = 0;
    double n_convex_overlap = 0.0;
    std::vector<double> err_train(_n_samp);
    std::vector<double> err_test(_n_samp_test);
    for (int tt = 0; tt < _n_task; ++tt)
    {
        std::vector<double*> val_ptrs(feats.size());
        std::vector<double*> test_val_ptrs(feats.size());
        std::transform(sorted_values.begin(),
                       sorted_values.end(),
                       val_ptrs.begin(),
                       [start](std::vector<double>& sv) { return &sv[start]; });

        std::transform(sorted_test_values.begin(),
                       sorted_test_values.end(),
                       test_val_ptrs.begin(),
                       [start_test](std::vector<double>& sv) { return &sv[start_test]; });
        _lp[tt].set_n_overlap(val_ptrs, test_val_ptrs, &err_train[start], &err_test[start_test]);
        n_convex_overlap += static_cast<double>(_lp[tt].n_overlap());

        start += _task_sizes_train[tt];
        start_test += _task_sizes_test[tt];
    }

    std::for_each(
        _sample_inds_to_sorted_dmat_inds.begin(),
        _sample_inds_to_sorted_dmat_inds.end(),
        [&err_train, this](auto& iter) { _error_train[iter.first] = err_train[iter.second]; });
    std::transform(_error_train.begin(),
                   _error_train.end(),
                   _prop_train.begin(),
                   _error_train.begin(),
                   [](double err, double real) { return (err < 1e-10) ? -1 : real; });

    std::for_each(
        _test_sample_inds_to_sorted_dmat_inds.begin(),
        _test_sample_inds_to_sorted_dmat_inds.end(),
        [&err_test, this](auto& iter) { _error_test[iter.first] = err_test[iter.second]; });
    std::transform(_error_test.begin(),
                   _error_test.end(),
                   _prop_test.begin(),
                   _error_test.begin(),
                   [](double err, double real) { return (err < 1e-10) ? -1 : real; });

    // Perform SVM to set estimated prop
    start = 0;
    start_test = 0;
    int start_coefs = 0;
    for (size_t tt = 0; tt < _task_sizes_train.size(); ++tt)
    {
        std::vector<double*> node_val_ptrs(_n_feat);
        std::vector<double*> node_test_val_ptrs(_n_feat);

        for (int dd = 0; dd < _n_feat; ++dd)
        {
            node_val_ptrs[dd] = feats[dd]->value_ptr() + start;
            node_test_val_ptrs[dd] = feats[dd]->test_value_ptr() + start_test;
        }

        _svm[tt]->train(node_val_ptrs);
        std::vector<std::vector<double>> coefs = _svm[tt]->coefs();
        for (size_t cc = 0; cc < coefs.size(); ++cc)
        {
            std::copy_n(coefs[cc].data(), coefs[cc].size(), &_coefs[start_coefs * _n_dim]);
            _coefs[start_coefs * _n_dim + _n_feat] = _svm[tt]->intercept()[cc];
            ++start_coefs;
        }
        std::copy_n(
            _svm[tt]->y_estimate().begin(), _task_sizes_train[tt], _prop_train_est.begin() + start);
        std::copy_n(_svm[tt]->predict(_task_sizes_test[tt], node_test_val_ptrs).begin(),
                    _task_sizes_test[tt],
                    _prop_test_est.begin() + start_test);

        start += _task_sizes_train[tt];
        start_test += _task_sizes_test[tt];
    }
    return n_convex_overlap;
}

double LossFunctionConvexHull::test_loss(const std::vector<model_node_ptr>& feats)
{
    (*this)(feats);
    double n_convex_overlap_test = 0.0;
    for (auto& lp : _lp)
    {
        n_convex_overlap_test += lp.n_overlap_test();
    }
    return n_convex_overlap_test;
}
