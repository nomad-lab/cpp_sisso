// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file loss_function/LossFunctionLogPearsonRMSE.hpp
 *  @brief Defines the class that uses a logarithmic Pearson correlation projection operator and a logarithmic least-squares regression objective function
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef LOSS_FUNCTION_LOG_PEARSON_RMSE
#define LOSS_FUNCTION_LOG_PEARSON_RMSE

#include "loss_function/LossFunctionPearsonRMSE.hpp"

// DocString: cls_loss_function_log_pearson_rmse
/**
 * @brief The loss function used for log regression problems
 *
 */
class LossFunctionLogPearsonRMSE : public LossFunctionPearsonRMSE
{
protected:
    std::vector<double> _log_feat;  //!< A feature to store the log of the Features' data

public:
    /**
     * @brief Constructor
     *
     * @param prop_train The value of the property to evaluate the loss function against for the training set
     * @param prop_test The value of the property to evaluate the loss function against for the test set
     * @param task_sizes_train Number of training samples per task
     * @param task_sizes_test Number of testing samples per task
     */
    LossFunctionLogPearsonRMSE(std::vector<double> prop_train,
                               std::vector<double> prop_test,
                               std::vector<int> task_sizes_train,
                               std::vector<int> task_sizes_test,
                               bool fix_intercept = false,
                               int n_feat = 1);

    LossFunctionLogPearsonRMSE(std::shared_ptr<LossFunction> o);

    /**
     * @brief Copy Constructor
     *
     * @param o LossFunctionLogPearsonRMSE to be copied
     */
    LossFunctionLogPearsonRMSE(const LossFunctionLogPearsonRMSE& o) = default;

    /**
     * @brief Move Constructor
     *
     * @param o LossFunctionLogPearsonRMSE to be moved
     */
    LossFunctionLogPearsonRMSE(LossFunctionLogPearsonRMSE&& o) = default;

    /**
     * @brief Copy Assignment operator
     *
     * @param o LossFunctionLogPearsonRMSE to be copied
     */
    LossFunctionLogPearsonRMSE& operator=(const LossFunctionLogPearsonRMSE& o) = default;

    /**
     * @brief Move Assignment operator
     *
     * @param o LossFunctionLogPearsonRMSE to be moved
     */
    LossFunctionLogPearsonRMSE& operator=(LossFunctionLogPearsonRMSE&& o) = default;

    /**
     * @brief Calculate the projection score of a feature
     *
     * @param feat Feature to calculate the projection score of
     * @return The projection score for the feature
     */
    double project(const node_ptr& feat) override;

    /**
     * @brief Set the A matrix used for solving the least squares regression
     *
     * @param inds A vector storing the _d_mat_ind of the features to pull data from the Descriptor Matrix storage array
     * @param taskind The task for used for the least squares regression
     * @param start The offset needed from the head of the feature's training data to where the task starts
     */
    void set_a(const std::vector<int>& inds, int taskind, int start) override;

    /**
     * @brief Set the A matrix used for solving the least squares regression
     *
     * @param feats The features used to evaluate the loss function
     * @param taskind The task for used for the least squares regression
     * @param start The offset needed from the head of the feature's training data to where the task starts
     */
    void set_a(const std::vector<model_node_ptr>& feats, int taskind, int start) override;

    /**
     * @brief Set the error vector
     *
     * @param inds Indexes to access the central storage area
     * @param taskind The task for used for the least squares regression
     * @param start The offset needed from the head of the feature's training data to where the task starts
     */
    void set_prop_train_est(const std::vector<int>& inds, int taskind, int start) override;

    /**
     * @brief Set the error
     *
     * @param feats The features used to evaluate the loss function
     * @param taskind The task for used for the least squares regression
     * @param start The offset needed from the head of the feature's training data to where the task starts
     */
    void set_prop_train_est(const std::vector<model_node_ptr>& feats, int taskind, int start) override;

    /**
     * @brief Set the test error and return the RMSE
     *
     * @param feats The features used to evaluate the loss function for the test data
     * @param taskind The task for used for the least squares regression
     * @param start The offset needed from the head of the feature's test data to where the task starts
     */
    void set_prop_test_est(const std::vector<model_node_ptr>& feats, int taskind, int start) override;

    /**
     * @brief The type of LossFunction
     */
    inline LOSS_TYPE type() const override { return LOSS_TYPE::LOG_PEARSON_RMSE; }
};

#endif
