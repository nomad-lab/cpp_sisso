// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 *  @brief Implements the class that uses a Pearson correlation projection operator and a least-squares regression objective function
 *
 *  @author Sebastian Eibl <sebastian.eibl@mpcdf.mpg.de>
 */

#include "RMSEGPU.hpp"

void get_mean_squared_difference(const Kokkos::View<double*>& output,
                                 const Kokkos::View<const double*>& input1,
                                 const Kokkos::View<const double**, Kokkos::LayoutLeft>& input2)
{
    assert(output.extent(0) == input2.extent(1));
    assert(input1.extent(0) == input2.extent(0));

    auto policy = Kokkos::TeamPolicy<>(input2.extent(1), Kokkos::AUTO);
    auto kernel = KOKKOS_LAMBDA(const Kokkos::TeamPolicy<>::member_type& teamMember)
    {
        const int batch_idx = teamMember.league_rank();
        double squared_difference = 0;

        Kokkos::parallel_reduce(
            Kokkos::TeamThreadRange(teamMember, input1.extent(0)),
            [&](const int idx, double& innerUpdate) {
                innerUpdate += (input1(idx) - input2(idx, batch_idx)) *
                               (input1(idx) - input2(idx, batch_idx));
            },
            squared_difference);

        if (teamMember.team_rank() == 0)
            output(batch_idx) = std::sqrt(squared_difference /
                                          static_cast<double>(input1.extent(0)));
    };
    Kokkos::parallel_for("get_mean_squared_difference", policy, kernel);
    Kokkos::fence();
}

RMSEGPU::RMSEGPU(const DescriptorMatrix::MATRIX_TYPE& descriptor_matrix,
                 const PropertiesVector::VECTOR_TYPE& properties,
                 const std::vector<int>& task_sizes,
                 bool fix_intercept,
                 int n_feat)
    : _descriptor_matrix(descriptor_matrix),
      _properties(properties),
      _task_sizes(task_sizes),
      _fix_intercept(fix_intercept),
      _n_feat(n_feat),
      _n_dim(n_feat + (!fix_intercept)),
      _n_samp(std::accumulate(task_sizes.begin(), task_sizes.end(), 0)),
      _n_task(task_sizes.size()),
      _a("A", _n_samp, _n_dim),
      _b("b", _n_samp),
      _batched_scores("batched_scores"),
      _models("models", n_feat),
      _estimated_training_properties("estimated_training_properties", properties.extent(0))
{
    auto stat = cublasCreate(&_cublas_handle);
    if (stat != CUBLAS_STATUS_SUCCESS)
    {
        throw std::runtime_error("CUBLAS initialization failed\n");
    }

    CHECK_CUDA_ERROR(cudaMallocManaged<double*>(&_batched_As, sizeof(_batched_As) * MAX_BATCHES));
    CHECK_CUDA_ERROR(cudaMallocManaged<double*>(&_batched_bs, sizeof(_batched_bs) * MAX_BATCHES));

    for (size_t batch_idx = 0; batch_idx < MAX_BATCHES; ++batch_idx)
    {
        _batched_As[batch_idx] = &_a(0, 0, batch_idx);
        _batched_bs[batch_idx] = &_b(0, batch_idx);
    }
}

RMSEGPU::~RMSEGPU()
{
    cublasDestroy(_cublas_handle);
    CHECK_CUDA_ERROR(cudaFree(_batched_As));
    CHECK_CUDA_ERROR(cudaFree(_batched_bs));
}

Kokkos::View<double*> RMSEGPU::operator()(const std::vector<std::vector<int>>& feature_indices)
{
    const int batch_size = static_cast<int>(feature_indices.size());
    assert(batch_size <= MAX_BATCHES);

    assert(feature_indices.size() <= _models.extent(1));
    assert(feature_indices[0].size() == _models.extent(0));
    for (int model_idx = 0; model_idx < feature_indices.size(); ++model_idx)
    {
        for (int ff = 0; ff < feature_indices[model_idx].size(); ++ff)
        {
            _models(ff, model_idx) = feature_indices[model_idx][ff];
        }
    }

    int start = 0;
    for (int task_idx = 0; task_idx < _n_task; ++task_idx)
    {
        set_a(_models, task_idx, start, batch_size);
        set_b(task_idx, start, batch_size);
        Kokkos::fence();
        least_squares(task_idx, start, batch_size);

        set_a(_models, task_idx, start, batch_size);
        Kokkos::fence();
        set_prop_train_est(_estimated_training_properties, task_idx, start, batch_size);

        start += _task_sizes[task_idx];
    }

    ::get_mean_squared_difference(_batched_scores, _properties, _estimated_training_properties);

    return _batched_scores;
}

void RMSEGPU::set_a(const Kokkos::View<const int**, Kokkos::LayoutLeft>& models,
                    int taskind,
                    int start,
                    int batch_size)
{
    assert(_descriptor_matrix.extent(0) >= _task_sizes[taskind] + start);

    auto a = _a;
    auto descriptor_matrix = _descriptor_matrix;
    int num_features = static_cast<int>(models.extent(0));
    auto n_dim = _n_dim;

    auto policy = Kokkos::MDRangePolicy<Kokkos::Rank<2>>({0, 0},
                                                         {_task_sizes[taskind], batch_size});
    auto kernel = KOKKOS_LAMBDA(const int sample_idx, const int model_idx)
    {
        for (auto feature_idx = 0; feature_idx < num_features; ++feature_idx)
        {
            a(sample_idx, feature_idx, model_idx) = descriptor_matrix(
                sample_idx + start, models(feature_idx, model_idx));
        }
        if (num_features != n_dim)
        {
            a(sample_idx, num_features, model_idx) = 1;
        }
    };
    Kokkos::parallel_for("LossFunctionPearsonRMSE::set_a", policy, kernel);
}

void RMSEGPU::set_b(int taskind, int start, int batch_size)
{
    auto b = _b;
    auto training_properties = _properties;
    auto policy = Kokkos::MDRangePolicy<Kokkos::Rank<2>>({0, 0},
                                                         {_task_sizes[taskind], batch_size});
    auto kernel = KOKKOS_LAMBDA(const int material_idx, const int batch_idx)
    {
        b(material_idx, batch_idx) = training_properties(start + material_idx);
    };
    Kokkos::parallel_for("LossFunctionPearsonRMSE::set_b", policy, kernel);
}

int RMSEGPU::least_squares(int taskind, int start, int batch_size)
{
    int info;

    cublasDgelsBatched(_cublas_handle,
                       CUBLAS_OP_N,
                       _task_sizes[taskind],
                       _n_dim,
                       1,
                       _batched_As,
                       _a.stride_1(),
                       _batched_bs,
                       _task_sizes[taskind],
                       &info,
                       nullptr,
                       batch_size);
    cudaDeviceSynchronize();

    return info;
}

void RMSEGPU::set_prop_train_est(
    Kokkos::View<double* [MAX_BATCHES], Kokkos::LayoutLeft> estimated_training_properties,
    int taskind,
    int start,
    int batch_size)
{
    assert(estimated_training_properties.extent(0) >= start + _task_sizes[taskind]);
    assert(estimated_training_properties.extent(1) <= MAX_BATCHES);

    auto a = _a;
    auto coefficients = _b;
    auto n_dim = _n_dim;

    auto policy = Kokkos::MDRangePolicy<Kokkos::Rank<2>>({0, 0},
                                                         {_task_sizes[taskind], batch_size});
    auto kernel = KOKKOS_LAMBDA(const int material_idx, const int model_idx)
    {
        double sum = 0;
        for (size_t feature_idx = 0; feature_idx < n_dim; ++feature_idx)
        {
            sum += a(material_idx, feature_idx, model_idx) * coefficients(feature_idx, model_idx);
        }
        estimated_training_properties(start + material_idx, model_idx) = sum;
    };
    Kokkos::parallel_for("LossFunctionPearsonRMSE::set_prop_train_est", policy, kernel);
    Kokkos::fence();
}
