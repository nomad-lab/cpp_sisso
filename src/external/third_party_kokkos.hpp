#ifndef THIRD_PARTY_KOKKOS_HPP_INCLUDED
#define THIRD_PARTY_KOKKOS_HPP_INCLUDED
#ifdef __GNUC__
// Avoid tons of warnings with root code
#pragma GCC system_header
#endif

#include <Kokkos_Core.hpp>
#include <Kokkos_StdAlgorithms.hpp>


#endif
