#ifndef THIRD_PARTY_HPP_CLASS_INCLUDED
#define THIRD_PARTY_HPP_CLASS_INCLUDED

#ifdef __GNUC__
// Avoid tons of warnings with root code
#pragma GCC system_header
#endif

#include <coin/ClpSimplex.hpp>
#include "external/libsvm/svm.h"

#endif
