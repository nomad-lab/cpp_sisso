#ifndef THIRD_PARTY_GTEST_HPP_INCLUDED
#define THIRD_PARTY_GTEST_HPP_INCLUDED
#ifdef __GNUC__
// Avoid tons of warnings with root code
#pragma GCC system_header
#endif

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#endif
