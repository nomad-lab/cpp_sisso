#ifndef THIRD_PARTY_PY_HPP_INCLUDED
#define THIRD_PARTY_PY_HPP_INCLUDED

#ifdef __GNUC__
// Avoid tons of warnings with root code
#pragma GCC system_header
#endif

#ifdef PY_BINDINGS
#include "external/pybind11/include/pybind11/numpy.h"
#include "external/pybind11/include/pybind11/operators.h"
#include "external/pybind11/include/pybind11/pybind11.h"
#include "external/pybind11/include/pybind11/stl.h"
#endif

#endif
