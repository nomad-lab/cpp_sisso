#ifndef THIRD_PARTY_HPP_PARAM_INCLUDED
#define THIRD_PARTY_HPP_PARAM_INCLUDED

#ifdef __GNUC__
// Avoid tons of warnings with root code
#pragma GCC system_header
#endif

#ifdef PARAMETERIZE
#include <nlopt.hpp>
#endif

#endif
