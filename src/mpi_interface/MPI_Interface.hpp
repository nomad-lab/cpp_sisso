// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file mpi_interface/MPI_Interface.hpp
 *  @brief Defines the MPI interface for the calculations
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 *
 *  This class modifies the boost mpi::communicator for easier communication
 */

#ifndef SISSO_MPI_INTERFACE
#define SISSO_MPI_INTERFACE

#include <array>
#include "external/third_party_boost.hpp"

namespace mpi = boost::mpi;

// DocString: cls_mpi_interface
/**
 * @brief Augment the boost MPI communicator class with some other useful data and other functions
 *
 */
class MPI_Interface : public mpi::communicator
{
public:
    /**
     * @brief      Constructor for the MPI_Interface
     */
    MPI_Interface();

    /**
     * @brief      Unique int tag generator
     *
     * @param[in]  proc_send   sending process
     * @param[in]  proc_recv   receiving process
     * @param[in]  max_offset  number of different communication processes possible between two processes within the same operation
     * @param[in]  offset     the assigned offset corresponding to a single communication within the same operation
     *
     * @return     A unique tag to send information between two processes
     */
    int cantor_tag_gen(const unsigned int proc_send,
                       const unsigned int proc_recv,
                       const unsigned int max_offset,
                       const unsigned int offset)
    {
        return (int((proc_send + proc_recv) * (proc_send + proc_send + 1) / 2) + proc_recv) *
                   max_offset +
               offset;
    }

    /**
     * @brief get the start and end index for splitting up a list across MPI ranks
     *
     * @param sz size of the list to split up
     * @param start the starting point of where to begin the split in the list
     *
     * @return The start and end indexes of what this rank is responsible for
     */
    std::array<int, 2> get_start_end_from_list(const int sz, const int start = 0);
};

namespace mpi_setup
{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"
static mpi::environment* env = 0;  //!< The MPI environment
#pragma GCC diagnostic pop

extern std::shared_ptr<MPI_Interface> comm;  //!< The MPI communicator

/**
     * @brief Initialize MPI enviroment
     *
     */
void init_mpi_env();

/**
     * @brief Finalize the mpi::enviroment at the end of the calculation.
     */
void finalize_mpi_env();
}  // namespace mpi_setup

#endif
