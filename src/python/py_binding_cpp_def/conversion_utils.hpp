// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file python/py_binding_cpp_def/conversion_utils.hpp
 *  @brief Define a set of functions to convert std::vectors to and from python objects
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */

#ifndef UTILS_PY_CONVERSION
#define UTILS_PY_CONVERSION

#define BOOST_PYTHON_MAX_ARITY 20

#include <algorithm>
#include <array>
#include <iostream>
#include <map>
#include <vector>

#include "external/third_party_py.hpp"

namespace python_conv_utils
{
namespace py = pybind11;

/**
     * @brief Convert a numpy array object to a std::vector
     *
     * @param lst numpy array to convert to a std::vector
     * @tparam T type of objects inside the numpy array
     * @return A vector of the numpy array
     */
template <typename T>
std::vector<T> from_array(py::array_t<T> arr)
{
    if (arr.ndim() != 1)
    {
        throw std::logic_error("Array must be one dimensional");
    }
    std::vector<T> vec(arr.shape(0));
    // Not copy_n because of strides
    for (int ii = 0; ii < arr.shape(0); ++ii)
    {
        vec[ii] = *arr.data(ii);
    }
    return vec;
}

/**
     * @brief Convert a numpy array object to a std::vector
     *
     * @param lst numpy array to convert to a std::vector
     * @tparam T type of objects inside the numpy array
     * @return A vector of the numpy array
     */
template <typename T>
std::vector<std::vector<T>> from_2D_array(py::array_t<T> arr)
{
    if (arr.ndim() != 2)
    {
        throw std::logic_error("Array must be two dimensional");
    }
    int stride_ii = arr.strides(0) / sizeof(T);
    int stride_jj = arr.strides(1) / sizeof(T);

    std::vector<std::vector<T>> vec(arr.shape(0), std::vector<T>(arr.shape(1)));
    for (int ii = 0; ii < arr.shape(0); ++ii)
    {
        for (int jj = 0; jj < arr.shape(1); ++jj)
        {
            vec[ii][jj] = arr.data()[ii * stride_ii + jj * stride_jj];
        }
    }
    return vec;
}

/**
     * @brief Convert a numpy array object to a std::vector
     *
     * @param lst numpy array to convert to a std::vector
     * @tparam T type of objects inside the numpy array
     * @return A vector of the numpy array
     */
template <typename T>
std::vector<std::vector<T>> from_2D_array_transpose(py::array_t<T> arr)
{
    if (arr.ndim() != 2)
    {
        throw std::logic_error("Array must be two dimensional");
    }
    int stride_ii = arr.strides(0) / sizeof(T);
    int stride_jj = arr.strides(1) / sizeof(T);

    std::vector<std::vector<T>> vec(arr.shape(1), std::vector<T>(arr.shape(0)));
    for (int ii = 0; ii < arr.shape(0); ++ii)
    {
        for (int jj = 0; jj < arr.shape(1); ++jj)
        {
            vec[jj][ii] = arr.data()[ii * stride_ii + jj * stride_jj];
        }
    }
    return vec;
}

template <typename key, typename T>
std::map<key, std::vector<T>> convert_map(std::map<key, py::array_t<T>> in_map)
{
    std::map<key, std::vector<T>> out_map;
    for (auto& el : in_map)
    {
        out_map[el.first] = from_array<T>(el.second);
    }
    return out_map;
}

/**
     * @brief Convert a numpy string array object to a std::vector
     *
     * @param arr numpy array to convert to a std::vector
     * @return A vector of the numpy array
     */
std::vector<std::string> from_string_array(py::array arr);
}  // namespace python_conv_utils

#endif
