#include "python/py_binding_cpp_def/conversion_utils.hpp"
namespace py = pybind11;

std::vector<std::string> python_conv_utils::from_string_array(py::array arr)
{
    py::buffer_info info = arr.request();
    if ((info.format.find_first_not_of("0123456789") != info.format.size() - 2) &&
        (info.format.back() != 'w'))
    {
        throw std::runtime_error("Array does not contain wstrings");
    }

    if (arr.ndim() != 1)
    {
        throw std::logic_error("arr must be one dimensional.");
    }

    std::vector<std::string> vec(arr.shape(0));
    std::vector<char> char_arr((arr.strides(0) / 4) + 1, '\0');
    for (int ii = 0; ii < arr.shape(0); ++ii)
    {
        std::transform(static_cast<const wchar_t*>(arr.data(ii)),
                       static_cast<const wchar_t*>(arr.data(ii)) + char_arr.size() - 1,
                       char_arr.begin(),
                       [](wchar_t in_char) { return static_cast<char>(in_char); });

        vec[ii] = std::string(char_arr.data());
    }

    return vec;
}
