#include "python/py_binding_cpp_def/pickle.hpp"
namespace py = pybind11;

py::tuple python_pickling::pickle_feat_space(const FeatureSpace& feat_space)
{
#ifdef PARAMETERIZE
    std::vector<std::string> allowed_param_ops = feat_space.allowed_param_ops();
    std::vector<node_ptr> phi_reparam = feat_space.phi_reparam();
    std::vector<int> end_no_params = feat_space.end_no_params();
    std::vector<int> start_rung_reparam = feat_space.start_rung_reparam();
    int max_param_depth = feat_space.max_param_depth();
    bool reparam_residual = feat_space.reparam_residual();
#else
    std::vector<std::string> allowed_param_ops;
    std::vector<node_ptr> phi_reparam;
    std::vector<int> end_no_params;
    std::vector<int> start_rung_reparam;
    int max_param_depth = 0;
    bool reparam_residual = false;
#endif

    return py::make_tuple(feat_space.phi_selected(),
                          feat_space.phi(),
                          feat_space.phi0(),
                          feat_space.allowed_ops(),
                          allowed_param_ops,
                          feat_space.prop_train(),
                          feat_space.scores(),
                          feat_space.task_sizes_train(),
                          feat_space.start_rung(),
                          feat_space.project_type(),
                          feat_space.feature_space_file(),
                          feat_space.feature_space_summary_file(),
                          feat_space.phi_out_file(),
                          feat_space.cross_cor_max(),
                          feat_space.l_bound(),
                          feat_space.u_bound(),
                          feat_space.n_rung_store(),
                          feat_space.n_rung_generate(),
                          feat_space.max_rung(),
                          feat_space.n_sis_select(),
                          phi_reparam,
                          end_no_params,
                          start_rung_reparam,
                          max_param_depth,
                          reparam_residual);
}
