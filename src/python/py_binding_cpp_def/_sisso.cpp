// Copyright 2021 Thomas A. R. Purcell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/** @file python/py_bindings_cpp_def/_sisso.cpp
 *  @brief Implements the SISSO Library that binds the C++ structure to python
 *
 *  @author Thomas A. R. Purcell (tpurcell90)
 *  @bug No known bugs.
 */
#include "mpi_interface/MPI_Interface.hpp"
#include "python/py_binding_cpp_def/bindings.hpp"

#define STRINGIFY(x) #x
#define MACRO_STRINGIFY(x) STRINGIFY(x)

class Module
{
public:
    Module()
    {
        allowed_op_maps::set_node_maps();
#ifdef PARAMETERIZE
        allowed_op_maps::set_param_node_maps();
#endif
        mpi_setup::init_mpi_env();
    }
    ~Module() { mpi_setup::finalize_mpi_env(); }
};

namespace py = pybind11;

auto module_doc = R"pbdoc(
    SISSO++ Python Interface
    ------------------------

    .. currentmodule:: sissopp

    .. autosummary::
       :toctree: _generate

)pbdoc";
PYBIND11_MODULE(_sissopp, m)
{
    static Module module;  //!< module variable to ensure boost mpi intialization/cleanup
    sisso::register_all(m);

#ifdef VERSION_INFO
    m.attr("__version__") = MACRO_STRINGIFY(VERSION_INFO);
#else
    m.attr("__version__") = "dev";
#endif
}
