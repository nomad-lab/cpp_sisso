# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Configure plots into a standardized format"""
import matplotlib as mpl
import matplotlib.pyplot as plt
import toml
import shutil

from pathlib import Path

parent = Path(__file__).parent

plt.style.use(str(parent / "default.mplstyle"))
fontsize = plt.rcParams["font.size"]

config = toml.load(str(parent / "config.toml"))

# size
if "height" not in config["size"]:
    config["size"]["height"] = config["size"]["width"] / config["size"]["ratio"]

plt.rcParams["pdf.fonttype"] = 42
plt.rcParams["ps.fonttype"] = 42
if shutil.which("pdflatex") is not None:
    plt.rcParams[
        "text.latex.preamble"
    ] = "\\DeclareFontFamily{U}{mathc}{}\n\\DeclareFontShape{U}{mathc}{m}{it}%\n{<->s*[1.03] mathc10}{}\n\\DeclareMathAlphabet{\\mathscr}{U}{mathc}{m}{it}"
else:
    plt.rcParams["text.usetex"] = False
    plt.rcParams["font.family"] = "sans-serif"
