# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Create Model objects from the output files

Functions

sort_model_file_key: Function to determine the order of model files to import
load_model: Loads the correct model from a training and test file
get_models: From a regular expression generate a list of models
create_model_csv: Create a csv file for a model containing only the relevant features as columns, in the correct order
"""
from sissopp import ModelRegressor, ModelLogRegressor, ModelClassifier
from sissopp.py_interface.import_dataframe import strip_units

from glob import glob
import numpy as np


def sort_model_file_key(s):
    """Function to determine the order of model files to import

    Args:
        s (str): filename

    Return:
        int: key to sort model files
    """
    return s.split("/")[-1].split("_")[2]


def load_model(train_file, test_file=None):
    """Loads the correct model from a training and test file

    Args:
        tarin_file (str): The filename of the training file
        test_file (str): The filename of the testing file

    Returns:
        Model: The model from the files
    """
    model_line = open(train_file, "r").readline()[2:]
    if model_line[0] == "[":
        if test_file:
            return ModelClassifier(train_file, test_file)
        else:
            return ModelClassifier(train_file)
    elif (model_line[:4] == "c0 +") or (model_line[:4] == "a0 *"):
        if test_file:
            return ModelRegressor(train_file, test_file)
        else:
            return ModelRegressor(train_file)
    else:
        if test_file:
            return ModelLogRegressor(train_file, test_file)
        else:
            return ModelLogRegressor(train_file)


def get_models(dir_expr):
    """From a regular expression generate a list of models

    Args:
        dir_expr (str): Base expression to find the models

    Return:
        list of Model: Models represented by the expression
    """
    dir_list = sorted(glob(dir_expr))
    train_model_list = []
    test_model_list = []
    for direc in dir_list:
        train_model_list += list(glob(direc + "/models/train_*_model_0.dat"))
        test_model_list += list(glob(direc + "/models/test_*_model_0.dat"))

    train_model_list = sorted(train_model_list, key=sort_model_file_key)
    test_model_list = sorted(test_model_list, key=sort_model_file_key)

    if (len(train_model_list) != len(test_model_list)) and (len(test_model_list) > 0):
        raise ValueError("Train and test model list are not of the same length")

    n_dim = int(train_model_list[-1].split("/")[-1].split("_")[2])
    n_models_per_dim_train = np.array(
        [
            sum([1 for mf in train_model_list if f"dim_{nn}" in mf])
            for nn in range(1, n_dim + 1)
        ]
    )
    n_models_per_dim_test = np.array(
        [
            sum([1 for mf in test_model_list if f"dim_{nn}" in mf])
            for nn in range(1, n_dim + 1)
        ]
    )

    if not np.all(n_models_per_dim_train == n_models_per_dim_train[0]):
        raise ValueError("The number of training models per dimension is not the same")

    if (len(test_model_list) > 0) and not np.all(
        n_models_per_dim_test == n_models_per_dim_train[0]
    ):
        raise ValueError(
            "The number of test models per dimension is not the same as the training data"
        )

    if len(test_model_list) > 0:
        models = [
            load_model(train_file, test_file)
            for train_file, test_file in zip(train_model_list, test_model_list)
        ]
    else:
        models = [load_model(train_file) for train_file in train_model_list]

    return np.array(models).reshape((n_dim, n_models_per_dim_train[0]))


def create_model_csv(df, model, filename=None):
    """Create a csv file for a model containing only the relevant features as columns, in the correct order

    Args:
        df (pd.DataFrame): The data file to pull the data from
        model (Model): The model to pull the primary features from
        filename (str): The filename to print the new csv file to

    Returns:
        pd.DataFrame: The reduced data file
    """
    df = strip_units(df)

    sel_feats = np.array([x_in for feat in model.feats for x_in in feat.x_in_expr_list])
    unique_feats, inds = np.unique(sel_feats, return_index=True)
    inds.sort()

    df_red = df.loc[:, sel_feats[inds]]
    if filename:
        df_red.to_csv(filename)

    return df_red
