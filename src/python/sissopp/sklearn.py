# Copyright 2021 Thomas A. R. Purcell
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Interface to sklearn

This wrapper was inspired by the classes provided by pysisso: https://github.com/Matgenix/pysisso

Classes:
SISSOSolver: BaseClass for the Scikit leran wrapper for all SISSO calculations
SISSORegressor: Scikit learn wrapper for SISSORegressor
SISSOLogRegressor: Scikit learn wrapper for SISSOLogRegressor
SISSOClassifier: Scikit learn wrapper for SISSOClassifier

Functions:
timestamp: Get the string representation of the current timestamp
cwd: Temporarily change the current working directory of the calculation.
cross_validate_from_splitter: Perform a cross-validation using an scikit-learn splitter and metric
"""
import contextlib
import glob
import os
import shutil

from datetime import datetime
from pathlib import Path

import numpy as np
import pandas as pd

from sklearn.base import BaseEstimator, RegressorMixin
from sklearn.metrics import mean_squared_error, accuracy_score, make_scorer, get_scorer

from sissopp.py_interface import get_fs_solver, read_csv
from sissopp import Unit, Inputs


def timestamp(ts=None):
    """Get the string of the timestamp

    Args:
        ts (datetime.datetime): Object representing the currnet time

    Returns:
        str: The string representation of the timestamp
    """
    if ts is None:
        ts = datetime.now()

    return ts.strftime("%Y-%m-%d_%H:%M:%S")


@contextlib.contextmanager
def cwd(path, mkdir=False, clean=False):
    """Change cwd temporarily

    Args:
        path (str or Path):Path to change working directory to
        mkdir (bool): If True make path if it does not exist
        clean (bool): If True then delete the path on the system
    """
    CWD = Path.cwd()
    path = Path(path)

    if mkdir:
        path.mkdir(exist_ok=True, parents=True)

    os.chdir(path)
    yield
    os.chdir(CWD)
    if clean and CWD.absolute() != path.absolute():
        shutil.rmtree(path)


class SISSOSolver(RegressorMixin, BaseEstimator):
    def __init__(
        self,
        task_key=None,
        prop_label="Property",
        prop_unit="",
        allowed_ops="all",
        allowed_param_ops=None,
        cross_cor_max=1.0,
        l_bound=1e-50,
        u_bound=1e50,
        n_dim=1,
        max_rung=0,
        n_rung_store=0,
        n_rung_generate=0,
        n_sis_select=1,
        n_residual=1,
        n_models_store=1,
        max_param_depth=None,
        nlopt_seed=42,
        fix_intercept=False,
        global_param_opt=True,
        reparam_residual=False,
        workdir=None,
        clean_workdir=False,
    ):
        """Initialize the SISSORegressor for SciKit learn

        Args:
            task_key (str): Key used to find the task column in the data file
            prop_label (str): The label of the property
            prop_unit (Unit or str): The Unit of the property
            allowed_ops (list): A list containing all allowed operators strings for operators with free parameters
            allowed_param_ops (list): A list containing all allowed operators strings
            cross_cor_max (float): Maximum cross-correlation used for selecting features
            l_bound (float): The lower bound for the maximum absolute value of the features
            u_bound (float): The upper bound for the maximum absolute value of the features
            n_dim (int): The maximum number of features allowed in the linear model
            max_rung (int): Maximum rung for the feature creation
            n_rung_store (int): The number of rungs to calculate and store the value of the features for all samples
            n_rung_generate (int): Either 0 or 1, and is the number of rungs to generate on the fly during SIS
            n_sis_select (int): Number of features to select during each SIS iteration
            n_residual (int): Number of residuals to pass to the next sis model
            n_models_store (int): The number of models to output to files
            max_param_depth (int): The maximum depth in the binary expression tree to set non-linear optimization
            nlopt_seed (int): The seed used for the nlOpt library
            fix_intercept (bool): If true the bias term is fixed at 0
            global_param_opt (bool): True if global optimization is requested for non-linear optimization of parameters (Can break reproducibility)
            reparam_residual (bool): If True then reparameterize features using the residuals of each model
            workdir (str or Path): The working directory to run the calculation in
            clean_workdir (bool): If True then clean the running directory once the calculation is finished.
        """
        if allowed_ops == "all":
            allowed_ops = [
                "add",
                "sub",
                "mult",
                "div",
                "abs_diff",
                "inv",
                "abs",
                "cos",
                "sin",
                "exp",
                "neg_exp",
                "log",
                "sq",
                "sqrt",
                "cb",
                "cbrt",
                "six_pow",
            ]

        if allowed_param_ops is None:
            allowed_param_ops = []

        if max_param_depth is None:
            max_param_depth = max_rung

        self.task_key = task_key
        self.prop_label = prop_label

        if isinstance(prop_unit, Unit):
            self.prop_unit = str(prop_unit)
        else:
            self.prop_unit = prop_unit

        self.allowed_ops = allowed_ops
        self.allowed_param_ops = allowed_param_ops
        self.cross_cor_max = cross_cor_max
        self.l_bound = l_bound
        self.u_bound = u_bound
        self.n_dim = n_dim
        self.max_rung = max_rung
        self.n_rung_store = n_rung_store
        self.n_rung_generate = n_rung_generate
        self.n_sis_select = n_sis_select
        self.n_residual = n_residual
        self.n_models_store = n_models_store
        self.max_param_depth = max_param_depth
        self.nlopt_seed = nlopt_seed
        self.fix_intercept = fix_intercept
        self.global_param_opt = global_param_opt
        self.reparam_residual = reparam_residual
        self.workdir = workdir
        self.clean_workdir = clean_workdir

        self.columns = None
        self.inputs = Inputs()
        self.solver = None
        self.feature_space = None

    def fit(self, X, y, index=None, columns=None):
        """Fit a model using SISSORegression

        Args:
            X (pd.DataFrame or array-like object with shape (n_samples, n_features)): Features vectors
            y (array-like with shape (n_samples))): Property vector
            index (list of str): List of identifiers for the samples
            columns (list of str): List of column identifiers
        """
        if not isinstance(X, pd.DataFrame):
            data = np.array(X)
            if columns is None:
                columns = [f"feature_{ff}" for ff in range(data.shape[1])]

            if index is None:
                index = [f"sample_{ss}" for ss in range(data.shape[0])]
            train = pd.DataFrame(data=data, index=index, columns=columns)
        else:
            train = X.copy()

        if isinstance(self.task_key, int):
            self.task_key = train.columns[self.task_key]

        if self.task_key not in train.columns:
            self.task_key = None

        self.columns = train.columns

        prop_key = f"{self.prop_label} ({self.prop_unit})"
        train[prop_key] = y
        self.inputs = read_csv(
            train,
            prop_key,
            task_key=self.task_key,
            max_rung=self.max_rung,
            inputs=self.inputs,
        )

        self.inputs.allowed_ops = self.allowed_ops
        self.inputs.allowed_param_ops = self.allowed_param_ops
        self.inputs.cross_cor_max = self.cross_cor_max
        self.inputs.l_bound = self.l_bound
        self.inputs.u_bound = self.u_bound
        self.inputs.n_dim = self.n_dim
        self.inputs.n_rung_store = self.n_rung_store
        self.inputs.n_rung_generate = self.n_rung_generate
        self.inputs.n_sis_select = self.n_sis_select
        self.inputs.n_residual = self.n_residual
        self.inputs.n_models_store = self.n_models_store
        self.inputs.max_param_depth = self.max_param_depth
        self.inputs.nlopt_seed = self.nlopt_seed
        self.inputs.fix_intercept = self.fix_intercept
        self.inputs.global_param_opt = self.global_param_opt
        self.inputs.reparam_residual = self.reparam_residual
        self.inputs.calc_type = self.calc_type

        reset_workdir = False
        if self.workdir is None:
            self.workdir = f"SISSO_{timestamp()}"
            reset_workdir = True

        with cwd(self.workdir, mkdir=True, clean=self.clean_workdir):
            self.feature_space, self.solver = get_fs_solver(self.inputs, True)
            self.solver.fit()

        if reset_workdir:
            self.workdir = None

    def predict(self, X, index=None):
        """Predict the output using the SISSO model

        Args:
            X (pd.DataFrame or array-like object with shape (n_samples, n_features)): Features vectors
            index (list of str): List of identifiers for the samples
        """
        if not isinstance(X, pd.DataFrame):
            data = np.array(X)
            assert data.shape[1] == len(self.columns)

            if index is None:
                index = [f"test_sample_{ss}" for ss in range(data.shape[0])]

            pred = pd.DataFrame(data=data, index=index, columns=self.columns)
        else:
            pred = X.copy()

        data_dct = {}
        task_id = None
        for col in pred.columns:
            if col not in self.columns:
                warnings.warn("Column inside of X was not in the initial training set")
            if col == self.task_key:
                task_id = pred[col].values.astype(str)
            else:
                data_dct[col.split("(")[0].strip()] = pred[col].values.astype(float)

        if task_id is not None:
            return self.solver.models[-1][0].eval_many(data_dct, task_id)

        return self.solver.models[-1][0].eval_many(data_dct)


class SISSORegressor(SISSOSolver):
    def __init__(
        self,
        task_key=None,
        prop_label="Property",
        prop_unit="",
        allowed_ops="all",
        allowed_param_ops=None,
        cross_cor_max=1.0,
        l_bound=1e-50,
        u_bound=1e50,
        n_dim=1,
        max_rung=0,
        n_rung_store=0,
        n_rung_generate=0,
        n_sis_select=1,
        n_residual=1,
        n_models_store=1,
        max_param_depth=None,
        nlopt_seed=42,
        fix_intercept=False,
        global_param_opt=True,
        reparam_residual=False,
        workdir=None,
        clean_workdir=False,
    ):
        """Initialize the SISSOSolver for SciKit learn

        Args:
            task_key (str): Key used to find the task column in the data file
            prop_label (str): The label of the property
            prop_unit (Unit or str): The Unit of the property
            allowed_ops (list): A list containing all allowed operators strings for operators with free parameters
            allowed_param_ops (list): A list containing all allowed operators strings
            cross_cor_max (float): Maximum cross-correlation used for selecting features
            l_bound (float): The lower bound for the maximum absolute value of the features
            u_bound (float): The upper bound for the maximum absolute value of the features
            n_dim (int): The maximum number of features allowed in the linear model
            max_rung (int): Maximum rung for the feature creation
            n_rung_store (int): The number of rungs to calculate and store the value of the features for all samples
            n_rung_generate (int): Either 0 or 1, and is the number of rungs to generate on the fly during SIS
            n_sis_select (int): Number of features to select during each SIS iteration
            n_residual (int): Number of residuals to pass to the next sis model
            n_models_store (int): The number of models to output to files
            max_param_depth (int): The maximum depth in the binary expression tree to set non-linear optimization
            nlopt_seed (int): The seed used for the nlOpt library
            fix_intercept (bool): If true the bias term is fixed at 0
            global_param_opt (bool): True if global optimization is requested for non-linear optimization of parameters (Can break reproducibility)
            reparam_residual (bool): If True then reparameterize features using the residuals of each model
            workdir (str or Path): The working directory to run the calculation in
            clean_workdir (bool): If True then clean the running directory once the calculation is finished.
        """
        args = locals()
        del args["self"]
        del args["__class__"]
        super().__init__(**args)
        self.calc_type = "regression"

    @classmethod
    def OMP(
        cls,
        task_key=None,
        prop_label="Property",
        prop_unit="",
        n_dim=1,
        fix_intercept=False,
        workdir=None,
        clean_workdir=False,
    ):
        """Perform an Orthogonal Matching Pursuit calculation using only the primary features

        Args:
            task_key (str): Key used to find the task column in the data file
            prop_label (str): The label of the property
            prop_unit (Unit or str): The Unit of the property
            fix_intercept (bool): If true the bias term is fixed at 0
            n_dim (int): The maximum number of features allowed in the linear model
            workdir (str or Path): The working directory to run the calculation in
            clean_workdir (bool): If True then clean the running directory once the calculation is finished.
        """
        return cls(
            task_key=None,
            prop_label=prop_label,
            prop_unit=prop_unit,
            allowed_ops=[],
            allowed_param_ops=None,
            n_dim=n_dim,
            fix_intercept=fix_intercept,
            global_param_opt=False,
            workdir=workdir,
            clean_workdir=clean_workdir,
        )

    @classmethod
    def from_inputs(cls, inputs, workdir=None, clean_workdir=False):
        """Create a SISSORegressor from an Inputs object

        Args:
            inputs (Inputs): The inputs for the calculation
            workdir (str or Path): The working directory to run the calculation in
            clean_workdir (bool): If True then clean the running directory once the calculation is finished.
        """
        return cls(
            task_key=inputs.task_key,
            prop_label=inputs.prop_label,
            prop_unit=inputs.prop_unit,
            allowed_ops=inputs.allowed_ops,
            allowed_param_ops=inputs.allowed_param_ops,
            cross_cor_max=inputs.cross_cor_max,
            l_bound=inputs.l_bound,
            u_bound=inputs.u_bound,
            n_dim=inputs.n_dim,
            max_rung=inputs.max_rung,
            n_rung_store=inputs.n_rung_store,
            n_rung_generate=inputs.n_rung_generate,
            n_sis_select=inputs.n_sis_select,
            n_residual=inputs.n_residual,
            n_models_store=inputs.n_models_store,
            max_param_depth=inputs.max_param_depth,
            nlopt_seed=inputs.nlopt_seed,
            fix_intercept=inputs.fix_intercept,
            global_param_opt=inputs.global_param_opt,
            reparam_residual=inputs.reparam_residual,
            workdir=workdir,
            clean_workdir=clean_workdir,
        )


class SISSOLogRegressor(SISSOSolver):
    def __init__(
        self,
        task_key=None,
        prop_label="Property",
        prop_unit="",
        allowed_ops="all",
        allowed_param_ops=None,
        cross_cor_max=1.0,
        l_bound=1e-50,
        u_bound=1e50,
        n_dim=1,
        max_rung=0,
        n_rung_store=0,
        n_rung_generate=0,
        n_sis_select=1,
        n_residual=1,
        n_models_store=1,
        max_param_depth=None,
        nlopt_seed=42,
        fix_intercept=False,
        global_param_opt=True,
        reparam_residual=False,
        workdir=None,
        clean_workdir=False,
    ):
        """Initialize the SISSOSolver for SciKit learn

        Args:
            task_key (str): Key used to find the task column in the data file
            prop_label (str): The label of the property
            prop_unit (Unit or str): The Unit of the property
            allowed_ops (list): A list containing all allowed operators strings for operators with free parameters
            allowed_param_ops (list): A list containing all allowed operators strings
            cross_cor_max (float): Maximum cross-correlation used for selecting features
            l_bound (float): The lower bound for the maximum absolute value of the features
            u_bound (float): The upper bound for the maximum absolute value of the features
            n_dim (int): The maximum number of features allowed in the linear model
            max_rung (int): Maximum rung for the feature creation
            n_rung_store (int): The number of rungs to calculate and store the value of the features for all samples
            n_rung_generate (int): Either 0 or 1, and is the number of rungs to generate on the fly during SIS
            n_sis_select (int): Number of features to select during each SIS iteration
            n_residual (int): Number of residuals to pass to the next sis model
            n_models_store (int): The number of models to output to files
            max_param_depth (int): The maximum depth in the binary expression tree to set non-linear optimization
            nlopt_seed (int): The seed used for the nlOpt library
            fix_intercept (bool): If true the bias term is fixed at 0
            global_param_opt (bool): True if global optimization is requested for non-linear optimization of parameters (Can break reproducibility)
            reparam_residual (bool): If True then reparameterize features using the residuals of each model
            workdir (str or Path): The working directory to run the calculation in
            clean_workdir (bool): If True then clean the running directory once the calculation is finished.
        """
        args = locals()
        del args["self"]
        del args["__class__"]
        super().__init__(**args)
        self.calc_type = "log_regression"

    @classmethod
    def OMP(
        cls,
        task_key=None,
        prop_label="Property",
        prop_unit="",
        n_dim=1,
        fix_intercept=False,
        workdir=None,
        clean_workdir=False,
    ):
        """Perform an Orthogonal Matching Pursuit calculation using only the primary features

        Args:
            task_key (str): Key used to find the task column in the data file
            prop_label (str): The label of the property
            prop_unit (Unit or str): The Unit of the property
            fix_intercept (bool): If true the bias term is fixed at 0
            n_dim (int): The maximum number of features allowed in the linear model
            workdir (str or Path): The working directory to run the calculation in
            clean_workdir (bool): If True then clean the running directory once the calculation is finished.
        """
        return cls(
            task_key=None,
            prop_label=prop_label,
            prop_unit=prop_unit,
            allowed_ops=[],
            allowed_param_ops=None,
            n_dim=n_dim,
            fix_intercept=fix_intercept,
            global_param_opt=False,
            workdir=workdir,
            clean_workdir=clean_workdir,
        )

    @classmethod
    def from_inputs(cls, inputs, workdir=None, clean_workdir=False):
        """Create a SISSORegressor from an Inputs object

        Args:
            inputs (Inputs): The inputs for the calculation
            workdir (str or Path): The working directory to run the calculation in
            clean_workdir (bool): If True then clean the running directory once the calculation is finished.
        """
        return cls(
            task_key=inputs.task_key,
            prop_label=inputs.prop_label,
            prop_unit=inputs.prop_unit,
            allowed_ops=inputs.allowed_ops,
            allowed_param_ops=inputs.allowed_param_ops,
            cross_cor_max=inputs.cross_cor_max,
            l_bound=inputs.l_bound,
            u_bound=inputs.u_bound,
            n_dim=inputs.n_dim,
            max_rung=inputs.max_rung,
            n_rung_store=inputs.n_rung_store,
            n_rung_generate=inputs.n_rung_generate,
            n_sis_select=inputs.n_sis_select,
            n_residual=inputs.n_residual,
            n_models_store=inputs.n_models_store,
            max_param_depth=inputs.max_param_depth,
            nlopt_seed=inputs.nlopt_seed,
            fix_intercept=inputs.fix_intercept,
            global_param_opt=inputs.global_param_opt,
            reparam_residual=inputs.reparam_residual,
            workdir=workdir,
            clean_workdir=clean_workdir,
        )


class SISSOClassifier(SISSOSolver):
    def __init__(
        self,
        task_key=None,
        prop_label="Property",
        prop_unit="",
        allowed_ops="all",
        allowed_param_ops=None,
        cross_cor_max=1.0,
        l_bound=1e-50,
        u_bound=1e50,
        n_dim=1,
        max_rung=0,
        n_rung_store=0,
        n_rung_generate=0,
        n_sis_select=1,
        n_residual=1,
        n_models_store=1,
        max_param_depth=None,
        nlopt_seed=42,
        fix_intercept=False,
        global_param_opt=True,
        reparam_residual=False,
        workdir=None,
        clean_workdir=False,
    ):
        """Initialize the SISSOSolver for SciKit learn

        Args:
            task_key (str): Key used to find the task column in the data file
            prop_label (str): The label of the property
            prop_unit (Unit or str): The Unit of the property
            allowed_ops (list): A list containing all allowed operators strings for operators with free parameters
            allowed_param_ops (list): A list containing all allowed operators strings
            cross_cor_max (float): Maximum cross-correlation used for selecting features
            l_bound (float): The lower bound for the maximum absolute value of the features
            u_bound (float): The upper bound for the maximum absolute value of the features
            n_dim (int): The maximum number of features allowed in the linear model
            max_rung (int): Maximum rung for the feature creation
            n_rung_store (int): The number of rungs to calculate and store the value of the features for all samples
            n_rung_generate (int): Either 0 or 1, and is the number of rungs to generate on the fly during SIS
            n_sis_select (int): Number of features to select during each SIS iteration
            n_residual (int): Number of residuals to pass to the next sis model
            n_models_store (int): The number of models to output to files
            max_param_depth (int): The maximum depth in the binary expression tree to set non-linear optimization
            nlopt_seed (int): The seed used for the nlOpt library
            fix_intercept (bool): If true the bias term is fixed at 0
            global_param_opt (bool): True if global optimization is requested for non-linear optimization of parameters (Can break reproducibility)
            reparam_residual (bool): If True then reparameterize features using the residuals of each model
            workdir (str or Path): The working directory to run the calculation in
            clean_workdir (bool): If True then clean the running directory once the calculation is finished.
        """
        args = locals()
        del args["self"]
        del args["__class__"]
        super().__init__(**args)
        self.calc_type = "classification"

    @classmethod
    def OMP(
        cls,
        task_key=None,
        prop_label="Property",
        prop_unit="",
        n_dim=1,
        fix_intercept=False,
        workdir=None,
        clean_workdir=False,
    ):
        """Perform an Orthogonal Matching Pursuit calculation using only the primary features

        Args:
            task_key (str): Key used to find the task column in the data file
            prop_label (str): The label of the property
            prop_unit (Unit or str): The Unit of the property
            fix_intercept (bool): If true the bias term is fixed at 0
            n_dim (int): The maximum number of features allowed in the linear model
            workdir (str or Path): The working directory to run the calculation in
            clean_workdir (bool): If True then clean the running directory once the calculation is finished.
        """
        return cls(
            task_key=None,
            prop_label=prop_label,
            prop_unit=prop_unit,
            allowed_ops=[],
            allowed_param_ops=None,
            n_dim=n_dim,
            fix_intercept=fix_intercept,
            global_param_opt=False,
            workdir=workdir,
            clean_workdir=clean_workdir,
        )

    @classmethod
    def from_inputs(cls, inputs, workdir=None, clean_workdir=False):
        """Create a SISSORegressor from an Inputs object

        Args:
            inputs (Inputs): The inputs for the calculation
            workdir (str or Path): The working directory to run the calculation in
            clean_workdir (bool): If True then clean the running directory once the calculation is finished.
        """
        return cls(
            task_key=inputs.task_key,
            prop_label=inputs.prop_label,
            prop_unit=inputs.prop_unit,
            allowed_ops=inputs.allowed_ops,
            allowed_param_ops=inputs.allowed_param_ops,
            cross_cor_max=inputs.cross_cor_max,
            l_bound=inputs.l_bound,
            u_bound=inputs.u_bound,
            n_dim=inputs.n_dim,
            max_rung=inputs.max_rung,
            n_rung_store=inputs.n_rung_store,
            n_rung_generate=inputs.n_rung_generate,
            n_sis_select=inputs.n_sis_select,
            n_residual=inputs.n_residual,
            n_models_store=inputs.n_models_store,
            max_param_depth=inputs.max_param_depth,
            nlopt_seed=inputs.nlopt_seed,
            fix_intercept=inputs.fix_intercept,
            global_param_opt=inputs.global_param_opt,
            reparam_residual=inputs.reparam_residual,
            workdir=workdir,
            clean_workdir=clean_workdir,
        )


def log_regression_metric(y_true, y_pred):
    """Find the score of a log-regression model for a set of predictions

    Args:
        prop (np.ndarray): The actual property values
        prop_est (np.ndarray): The estimated property values

    Returns:
        The RMSE of the log of the predicted values
    """
    return np.sqrt(np.power(np.log(y_true) - np.log(y_pred), 2.0).mean())


regression_metric = lambda y_true, y_pred: mean_squared_error(
    y_true, y_pred, squared=False
)
classification_metric = lambda y_true, y_pred: accuracy_score(
    y_true, y_pred, normalize=True
)


def get_default_model_metric(model):
    """Returns the default metric for a given model.

    Args:
        model (sklearn.SISSOSolver): The model to get the metric for

    Returns
        sklearn.metric: The metric used for that model
    """
    if isinstance(model, SISSORegressor):
        return regression_metric
    elif isinstance(model, SISSOLogRegressor):
        return log_regression_metric
    elif isinstance(model, SISSOClassifier):
        return classification_metric


def cross_validate_from_splitter(
    X, y, sisso_model, splitter, scoring=None, groups=None
):
    """Perform a cross-validation using an scikit-learn splitter and metric

    Args:
        X (pd.DataFrame): The input data
        y (np.ndarray): The Property values
        sisso_model (sissopp.sklearn.SISSOSolver): The SISSOSolver object to fit the model with
        splitter (sklearn.model_selection.Splitter): Object to define how to split the training/test sets
        scoring (str, callable): The object used to score each model
        groups (array-like):Group labels for the samples used while splitting the dataset into train/test set.

    Returns:
        np.ndarray: The test scores for each split
    """
    if groups is not None:
        split = lambda x: splitter.split(x, groups=groups)
    else:
        split = lambda x: splitter.split(x)

    if scoring is None:
        metric = get_default_model_metric(sisso_model)
        scorer = make_scorer(
            metric, greater_is_better=isinstance(sisso_model, SISSOClassifier)
        )
    else:
        scorer = get_scorer(scoring)

    original_workdir = sisso_model.workdir
    if sisso_model.workdir is None:
        workdir = str(Path.cwd())
    else:
        workdir = str(sisso_model.workdir)

    scores = []
    for tt, (train_index, test_index) in enumerate(split(X)):
        sisso_model.workdir = f"{workdir}/cv_{tt}/"

        X_train, X_test = X.iloc[train_index, :], X.iloc[test_index, :]
        y_train, y_test = y[train_index], y[test_index]

        sisso_model.fit(X_train, y_train)
        score = scorer(sisso_model, X_test, y_test)
        scores.append(score)

        task_ids = []
        if sisso_model.task_key is not None:
            task_ids = [str(id) for id in X_test.loc[:, sisso_model.task_key]]
            X_model_out = X_test.drop(columns=sisso_model.task_key)
        else:
            task_ids = []
            X_model_out = X_test.copy()

        with cwd(f"{sisso_model.workdir}/models/", clean=False):
            for dd, model_list in enumerate(sisso_model.solver.models):
                for mm, model in enumerate(model_list[: sisso_model.n_models_store]):
                    model.prediction_to_file(
                        f"test_dim_{dd + 1}_model_{mm}.dat",
                        y_test,
                        X_model_out.values,
                        X_model_out.index.astype(str).tolist(),
                        task_ids,
                        test_index.astype(np.int32),
                    )

    sisso_model.workdir = original_workdir
    return scores
